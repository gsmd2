/*
 *  generic.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 */

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <glib.h>
#include <glib/gstdio.h>

#include "../src/modem.h"
#include "../src/utils.h"


static gint generic_open_serial (SerialInterface *serial_if, const gchar *device)
{
        g_debug("%s",__func__);
        return gsmd_utils_default_serial_init(device,
                                              CS8 | CLOCAL | CREAD | B115200,
                                              B115200);

}
static void generic_close_serial (SerialInterface *serial_if, gint fd)
{
        g_debug("%s",__func__);
        return;
}

gboolean serial_plugin_init ( SerialInterface *serial_if, ModemInterface *modem )
{
        g_debug("%s",__func__);
        serial_if->priv = modem;
        serial_if->open_serial = &generic_open_serial;
        serial_if->close_serial = &generic_close_serial;
        return TRUE;
}

