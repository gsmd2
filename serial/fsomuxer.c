/*
 *  fsomuxer.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 */

#include <glib.h>

#include "../src/modem.h"
#include "../src/utils.h"

#include "fsomuxer_client.h"

static GList* fsomuxer_get_ports ( SerialInterface *serial_if )
{
        GError *error = NULL;
        DBusGConnection *conn = dbus_g_bus_get (DBUS_BUS_SYSTEM, &error);
        if( !conn ) {
                g_warning("%s : Cannot get connection: %s", __func__, error->message);
                g_error_free( error );
                return NULL;
        }
        DBusGProxy *proxy = dbus_g_proxy_new_for_name (conn,
                                                       "org.pyneo.muxer",
                                                       "/org/pyneo/Muxer",
                                                       "org.freesmartphone.GSM.MUX");
        GList *channels = NULL;
        gchar *channel = NULL;
        if( !org_freesmartphone_GSM_MUX_alloc_channel (proxy, "gsmd2", &channel, &error) ) {
                g_warning("%s : AllocChannel failed: %s", __func__, error->message);
                g_error_free( error );
        } else {
                channels = g_list_append(channels, channel);
                channel = NULL;
        }
        g_object_unref (proxy);
        proxy = NULL;
        return channels;
}
static gint fsomuxer_open_serial (SerialInterface *serial_if, const gchar *device)
{
        g_debug("%s",__func__);
        return gsmd_utils_default_serial_init(device,
                                              CS8 | CLOCAL | CREAD | B115200,
                                              B115200);

}
static void fsomuxer_close_serial (SerialInterface *serial_if, gint fd)
{
        g_debug("%s",__func__);
        return;
}

gboolean serial_plugin_init ( SerialInterface *serial_if, ModemInterface *modem )
{
        g_debug("%s",__func__);
        serial_if->priv = modem;
        serial_if->open_serial = &fsomuxer_open_serial;
        serial_if->close_serial = &fsomuxer_close_serial;
        serial_if->get_ports = &fsomuxer_get_ports;
        return TRUE;
}

