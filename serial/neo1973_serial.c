/*
 *  neo1973_serial.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 */

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <glib.h>
#include <glib/gstdio.h>

#include "../src/modem.h"
#include "../src/utils.h"

static void neo1973_close_serial (SerialInterface *serial_if, gint fd)
{
        return;
}

static const gchar *files[] = {
        "/sys/bus/platform/devices/neo1973-pm-gsm.0/download",
        "/sys/bus/platform/devices/neo1973-pm-gsm.0/power_on",
        "/sys/bus/platform/devices/neo1973-pm-gsm.0/power_on",
        "/sys/bus/platform/devices/neo1973-pm-gsm.0/reset",
        "/sys/bus/platform/devices/neo1973-pm-gsm.0/reset",
        NULL
};
static const gchar *file_contents[] = {
        "1",
        "0",
        "1",
        "0",
        "1",
        NULL
};
static gint neo1973_open_serial (SerialInterface *serial_if, const gchar *device)
{
        gint i = 0;
        while( files[i] && file_contents[i] ) {
                if( g_file_test(files[i], G_FILE_TEST_IS_REGULAR) ) {
                        FILE *f = g_fopen(files[i],"wb");
                        if( f ) {
                                gint len = strlen(file_contents[i]);
                                gint written = fwrite (file_contents[i], 1,len , f);
                                if( written < len ) {
                                        g_debug("Failed to write file: %s", files[i]);
                                }
                                fclose(f);
                                g_usleep(G_USEC_PER_SEC);
                        } else {
                                g_debug("Failed to open file: %s", files[i]);
                        }
                }
                i++;
        }
        g_usleep(G_USEC_PER_SEC);
        return gsmd_utils_default_serial_init(device,
                                              CS8 | CRTSCTS | CREAD | B115200,
                                              B115200);
}

gboolean serial_plugin_init ( SerialInterface *serial_if, ModemInterface *modem )
{
        serial_if->priv = modem;
        serial_if->open_serial = &neo1973_open_serial;
        serial_if->close_serial = &neo1973_close_serial;
        return TRUE;
}
