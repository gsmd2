/*
 *  vendor_telit.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>

#include "../src/modem.h"
#include "../src/utils.h"
#include "../src/call_interface.h"

#define GSMD_VENDOR_KEY_ECAM "ecam"

/**
* @brief Call Type
*/
typedef enum {
        CALLTYPE_VOICE = 1, /**< Voice Call*/
        CALLTYPE_DATA     /**< Data Call*/
} CallType;


typedef enum {
    ECAM_CALL_IDLE=0,
    ECAM_CALL_CALLING,
    ECAM_CONNECTING,
    ECAM_ACTIVE,
    ECAM_CALL_HOLD,
    ECAM_CALL_WAITING,
    ECAM_ALERTING,
    ECAM_BUSY
} EcamState;

enum {
    ENABLE_ECAM = VENDOR_COMMAND
    };
enum {
    SYMBOL_ECAM = VENDOR_SYMBOL

    };

static void vendor_unsolicite_handler_init (ModemInterface* modem,
                                            VendorInterface *vendor);
static AtCommandHandlerStatus vendor_unsolicite_handler (ModemInterface* modem,
                                                         AtCommandContext *at,
                                                         GString *response);

static gboolean vendor_at_init_sim_ready (VendorInterface* vendor);
static void gsm_ecam_parse (GScanner* scanner,
                            gint* call_id,
                            EcamState* state,
                            CallType* call_type);

static AtCommandHandlerStatus telit_call_initiate_handler(ModemInterface *modem,
                                                          AtCommandContext *at,
                                                          GString *response);
static const AtCommand
commands[] = {
        //Vendor command
        { ENABLE_ECAM,        "AT#ECAM=1\r\n",   100,    TRUE, 1,
                NULL,NULL,SIM_UNKNOWN,NULL }
}, *command_p = commands;

static const SymbolTable
symbols[] = {
        { "ECAM", SYMBOL_ECAM, },//vendor command
        { NULL, 0, },
}, *symbol_p = symbols;

typedef struct _Vendor {
        ModemInterface *modem;

        AtCommandHandlerStatus (*orig_call_initiate_handler) (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response);
} Vendor;

static Vendor *vendor_priv = NULL;

/** vendor_handle_init should be called from modem.c when the lib loaded*/
gboolean vendor_handle_init (VendorInterface *vendor, ModemInterface* modem)
{
        if ( vendor_priv ) {
                g_warning("%s : Already initialized!", __func__);
                return FALSE;
        }
        vendor_priv = g_new0(Vendor, 1);
        g_debug("%s", __func__);
        vendor->init_at = NULL;
        vendor->init_at_sim_ready = vendor_at_init_sim_ready;
        vendor->priv = (gpointer)vendor_priv;
        vendor_unsolicite_handler_init (modem, vendor);
        vendor_priv->modem = modem;

        AtCommand *call_initiate = gsmd_modem_get_command(modem, INITIATE_CALL);
        vendor_priv->orig_call_initiate_handler = call_initiate->handler;
        call_initiate->handler = &telit_call_initiate_handler;
        return TRUE;
}
/*************************************/
/* if need replace default at_command, vendor at_init bellow*/



static gboolean vendor_at_init_sim_ready (VendorInterface* vendor)
{
        ModemInterface *modem = vendor_priv->modem;
        g_assert(modem);
        g_debug("%s", __func__);

        gsmd_modem_general_at_init_sim_ready (modem);

        gsmd_modem_post_at_command_id(modem,
                                      ENABLE_ECAM,
                                      NULL,NULL,NULL,NULL,
                                      INTERFACE_GENERAL,
                                      NULL);

        return TRUE;
}

/********************************/

static void vendor_unsolicite_handler_init(ModemInterface *modem,
                                           VendorInterface *vendor)
{
        g_debug("%s", __func__);
        //add the symbol to the modem scanner
        while (symbol_p->symbol_name) {
                g_scanner_add_symbol (modem->scanner, symbol_p->symbol_name,
                                      GINT_TO_POINTER(symbol_p->symbol_token));
                symbol_p ++;
        }

        //add the command to the modem command table
        while (command_p->command) {
                gsmd_modem_register_command(modem, command_p);
                command_p++;
        }
        vendor->unsolicite_handler = vendor_unsolicite_handler;

}

static void gsm_ecam_parse (GScanner* scanner,
                            gint* call_id,
                            EcamState* state,
                            CallType* call_type)
{
//: call_id, call_state, call_type
        g_scanner_get_next_token (scanner);
        if (scanner->token == ':') {
                g_scanner_get_next_token (scanner);
                *call_id = scanner->value.v_int;

                g_scanner_get_next_token (scanner);//get ;
                g_scanner_get_next_token (scanner);
                *state = scanner->value.v_int;

                g_scanner_get_next_token (scanner);//get ;
                g_scanner_get_next_token (scanner);
                *call_type = scanner->value.v_int;
        } else {
                g_warning ("unexpexted symbol in ecam_parse\n");
        }
}

void vendor_handle_voice_ecam(ModemInterface *modem,
                              EcamState state,
                              gint vendor_id)
{
        Call *call = NULL;
        g_debug("%s : call vendor id: %d, state: %d", __func__, vendor_id,state);

        switch (state) {
                //Phone call established
        case ECAM_ACTIVE:
                g_debug("%s: ECAM_ACTIVE",__func__);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if (call) {
                        gsmd_utils_call_send_status(modem,
                                                    call,
                                                    "active");

                        call->status = CALL_CONNECTED;
                }
                break;

                //Got disconnected
        case ECAM_CALL_IDLE:
                g_debug("%s: ECAM_CALL_IDLE",__func__);
                gsmd_utils_remove_call_vendor_id(modem,vendor_id,TRUE);
                break;
                //Someone's calling us
        case ECAM_ALERTING:
                g_debug("%s: ECAM_ALERTING",__func__);
                gsmd_utils_set_unknown_vendor_id(modem, vendor_id);


                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if (call) {
                        gsmd_utils_table_insert_int(call->vendor_properties,GSMD_VENDOR_KEY_ECAM,state);
                        gsmd_utils_call_send_status(modem,
                                                    call,
                                                    "incoming");
                }

                break;

                //Remote end is busy
        case ECAM_BUSY:
                g_debug("%s: ECAM_BUSY",__func__);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if (call) {

                        //Remove the call
                        if ( call->status == CALL_IDLE ) {
                                gsmd_utils_remove_call_vendor_id(modem,vendor_id,TRUE);
                        }
                }

                break;
                //We are calling someone
        case ECAM_CALL_CALLING:
                g_debug("%s: ECAM_CALL_CALLING",__func__);
                gsmd_utils_set_unknown_vendor_id(modem,vendor_id);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if (call) {
                        gsmd_utils_table_insert_int(call->vendor_properties,GSMD_VENDOR_KEY_ECAM,state);
                        gsmd_utils_call_send_status(modem,
                                                    call,
                                                    "outgoing");
                }

                break;
                //We are waiting on hold
        case ECAM_CALL_WAITING:
                g_debug("%s: ECAM_CALL_WAITING",__func__);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if (call) {
                        gsmd_utils_call_send_status(modem,
                                                    call,
                                                    "outgoing");
                        call->status = CALL_WAITING;
                }
                break;
        case ECAM_CONNECTING:
                g_debug("%s: ECAM_CONNECTING",__func__);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                break;

        case ECAM_CALL_HOLD:
                g_debug("%s: ECAM_CALL_HOLD",__func__);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if (call) {
                        gsmd_utils_call_send_status(modem,
                                                    call,
                                                    "held");
                }

                break;
        }
}

static
AtCommandHandlerStatus telit_call_initiate_handler(ModemInterface *modem,
                                                   AtCommandContext *at,
                                                   GString *response)
{
        gint vendor_id = -1;
        GScanner* scanner = modem->scanner;
        EcamState state = ECAM_CALL_IDLE;
        CallType call_type = CALLTYPE_VOICE;
        g_debug("%s", __func__);
        switch (scanner->token) {
        case '#':
                g_scanner_get_next_token(scanner);

                switch (scanner->token) {
                case SYMBOL_ECAM://line status
                        gsm_ecam_parse(scanner,&vendor_id,&state,&call_type);
                        g_debug("%s : call vendor id: %d", __func__, vendor_id);

                        if (call_type == CALLTYPE_VOICE) {
                                vendor_handle_voice_ecam(modem,state,vendor_id);
                        }
                        return AT_HANDLER_NEED_MORE;
                default:
                        g_scanner_input_text (scanner, response->str, response->len);
                        g_scanner_get_next_token (scanner);
                        g_scanner_peek_next_token (scanner);
                        break;
                }
                break;
        default:
                break;
        }
        return vendor_priv->orig_call_initiate_handler(modem,at,response);
}


AtCommandHandlerStatus vendor_unsolicite_handler (ModemInterface *modem,
                                                  AtCommandContext *at,
                                                  GString *response)
{
// only reponse for parse the vendor token such as #ECAM
        gint vendor_id = -1;
        GScanner* scanner = modem->scanner;
        EcamState state = ECAM_CALL_IDLE;
        CallType call_type = CALLTYPE_VOICE;
        g_debug("%s",__func__);

        switch (scanner->token) {

                //Override busy and no carrier and don't do anything
                //instead wait for the same information to come from ecam along with
                //call id
                /*         case SYMBOL_BUSY: */
                /*                 return AT_HANDLER_DONE; */
                /*         case SYMBOL_NO: */
                /*                 g_scanner_get_next_token(scanner); */
                /*                 if (scanner->token == SYMBOL_CARRIER) */
                /*                         return AT_HANDLER_DONE; */

        case '#':
                g_scanner_get_next_token(scanner);

                switch (scanner->token) {
                case SYMBOL_ECAM://line status
                        //#ECAM: 0,3,,,: call start
                        gsm_ecam_parse(scanner,&vendor_id,&state,&call_type);
                        g_debug("%s : call vendor id: %d", __func__, vendor_id);
                        if (call_type == CALLTYPE_VOICE) {
                                vendor_handle_voice_ecam(modem,state,vendor_id);
                        } else {
                                g_message ("data call\n");
                        }
                        return AT_HANDLER_DONE;
                default:
                        break;
                }
        default:
                break;
        }
        return AT_HANDLER_DONT_UNDERSTAND;
};
