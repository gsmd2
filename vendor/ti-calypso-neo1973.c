/*
 *  ti-calypso-neo1973.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <glib.h>
#include <glib/gstdio.h>

#include "../src/modem.h"
#include "../src/utils.h"
#include "../src/call_interface.h"

static void neo1973_unsolicite_handler_init (ModemInterface* modem,
                                             VendorInterface *vendor);

static AtCommandHandlerStatus neo1973_unsolicite_handler (ModemInterface *modem,
                                                          AtCommandContext *at,
                                                          GString *response);

static void neo1973_data_in (ModemInterface *modem,
                             AtCommandContext *at,
                             GString *data);

static gboolean neo1973_at_init_sim_ready (VendorInterface* vendor);
static gboolean neo1973_at_init (VendorInterface* vendor);
static gboolean neo1973_wakeup_modem (VendorInterface* vendor);
static AtCommandContext* neo1973_command_prepare ( VendorInterface *vendor,
                                                   ModemInterface *modem,
                                                   AtCommandContext *command);
static AtCommandHandlerStatus neo1973_handler_wakeup (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response);
/* static void neo1973_get_dtmf_duration(CallInterface *call, */
/*                                       gpointer ipc_data); */

enum {
        NEO_INIT = VENDOR_COMMAND,
        NEO_WAKEUP,
        NEO_COPS,
        NEO_ENABLE_CALL_PROGRESS
    };
static const AtCommand
commands[] = {
        //Vendor command
        {  NEO_INIT, "AT S7=45 S0=0 L1 V1 X4 &c1 E0 Q0\r\n" , 5000,
           TRUE, 0, NULL,NULL, SIM_UNKNOWN,NULL},
        //Vendor command
        {  NEO_WAKEUP, "\x1a\r\n" , 100,
           TRUE, 3,
           neo1973_handler_wakeup, NULL,SIM_UNKNOWN   },
        //Vendor command
        {  NEO_COPS, "AT+COPS\r\n" , 180000,
           TRUE, 3,
           gsmd_utils_handler_ok, NULL,SIM_UNKNOWN,NULL   },
        //Vendor command
        {  NEO_ENABLE_CALL_PROGRESS, "AT%CPI=4\r\n" , 5000,
           TRUE, 3,
           gsmd_utils_handler_ok,NULL, SIM_UNKNOWN,NULL   },
        { 0,                    NULL,                100,
          TRUE,  1, NULL, NULL, SIM_UNKNOWN,NULL },
}, *command_p = commands;

enum {
    SYMBOL_CPI = VENDOR_SYMBOL

    };
static const SymbolTable
symbols[] = {
        { "CPI", SYMBOL_CPI, },//vendor command
        { NULL, 0, },
}, *symbol_p = symbols;

typedef struct _Neo1973 {
        ModemInterface *modem;
        gboolean need_wakeup;
        gboolean control_device;
        GTimeVal last_data_in;
} Neo1973;

#define GSMD_VENDOR_KEY_CPI "cpi"

typedef enum {
        CPI_SETUP             = 0,
        CPI_DISCONNECT        = 1,
        CPI_ALERT             = 2,
        CPI_CALL_PROCEED      = 3,
        CPI_SYNC              = 4,
        CPI_PROGRESS          = 5,
        CPI_CALL_CONNECTED    = 6,
        CPI_RELEASE           = 7,
        CPI_REJECT            = 8,
        CPI_UNKNOWN           = 9,
} CpiState;



static Neo1973 *neo1973_priv = NULL;

/** vendor_handle_init should be called from modem.c when the lib loaded*/
gboolean vendor_handle_init (VendorInterface *vendor, ModemInterface* modem)
{
        g_debug("%s", __func__);
        if ( neo1973_priv ) {
                g_warning("%s : Already initialized!", __func__);
                return FALSE;
        }
        neo1973_priv = g_new0(Neo1973, 1);
        neo1973_priv->need_wakeup = FALSE;
        neo1973_priv->control_device = TRUE;
        neo1973_priv->modem = modem;
        vendor->init_at_sim_ready = neo1973_at_init_sim_ready;
        vendor->init_at = neo1973_at_init;
        vendor->data_in = neo1973_data_in;
        vendor->wakeup_modem = neo1973_wakeup_modem;
        vendor->command_prepare = neo1973_command_prepare;
        vendor->priv = (gpointer)neo1973_priv;
        neo1973_unsolicite_handler_init (modem, vendor);

        // %CPI provides all necessary information
        modem->no_call_monitoring_needed = TRUE;
/*         modem->call->get_dtmf_duration = &neo1973_get_dtmf_duration; */
        return TRUE;
}
/*************************************/
/* if need replace default at_command, vendor at_init bellow*/

static gboolean neo1973_wakeup (gpointer data)
{
        Neo1973 *priv = (Neo1973*)data;
        g_debug("%s",__func__);
        if(gsmd_modem_serial_write_simple(priv->modem, "\x1a", INTERFACE_GENERAL)) {
                g_usleep(0.2*G_USEC_PER_SEC);
                gsmd_modem_post_alive_test(priv->modem, TRUE);
        }
        return FALSE;
}
static gboolean neo1973_wakeup_modem (VendorInterface* vendor)
{
        g_debug("%s", __func__);
        g_timeout_add(100,
                      neo1973_wakeup,
                      neo1973_priv);
        return FALSE;
}
static AtCommandHandlerStatus neo1973_handler_wakeup (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response)
{
        g_debug("%s", __func__);
        GScanner* scanner = modem->scanner;
        if (scanner->token == SYMBOL_OK) {
                neo1973_priv->need_wakeup = FALSE;
               return AT_HANDLER_DONE;
        } else if(scanner->token == SYMBOL_ERROR) {
                neo1973_priv->need_wakeup = FALSE;
                return AT_HANDLER_DONE_ERROR;
        } else if(scanner->token == SYMBOL_TIME_OUT) {
                g_warning("%s : TIMEOUT", __func__);
                neo1973_priv->need_wakeup = TRUE;
/*                 gsmd_modem_reset(modem); */
                return AT_HANDLER_RETRY;
        }
        return AT_HANDLER_DONT_UNDERSTAND;
}

static AtCommandContext* neo1973_command_prepare ( VendorInterface *vendor,
                                                   ModemInterface *modem,
                                                   AtCommandContext *command)
{
        GTimeVal time = {0,};
        Neo1973 *priv = (Neo1973*)vendor->priv;
        g_get_current_time(&time);
        if( priv->last_data_in.tv_sec == 0
            && command->command->cmd_id != NEO_WAKEUP) {
                g_debug("%s : First wakeup", __func__);
                gsmd_modem_serial_write_simple(modem, "\x1a", INTERFACE_GENERAL);
                g_usleep(0.2*G_USEC_PER_SEC);
                return gsmd_at_command_context_new_from_id(  modem,
                                                             NEO_WAKEUP,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL );

        }
        if( time.tv_sec - priv->last_data_in.tv_sec >= 5) {
                g_debug("%s : Need wakeup", __func__);
                gsmd_modem_serial_write_simple(modem, "\x1a", INTERFACE_GENERAL);
                g_usleep(0.2*G_USEC_PER_SEC);
        }
        return NULL;
}

static gboolean neo1973_at_init (VendorInterface* vendor)
{
        ModemInterface *modem = neo1973_priv->modem;
        g_assert(modem);
        g_debug("%s", __func__);

        // POWER_ON here is need because, for some odd reason,
        // sequence
        //    at+cpin?
        //    +CPIN: SIM PIN
        //    OK
        //    at+cpin="<pin>"
        //    OK
        //    at+cfun=1
        //    +CME ERROR: 11 (PIN required)
        // but sequence
        //    at+cfun=1
        //    +CME ERROR: 11 (PIN required)
        //    at+cpin?
        //    +CPIN: SIM PIN
        //    OK
        //    at+cpin="<pin>"
        //    OK
        //    at+cfun=1
        //    OK

        gsmd_modem_post_at_command_id (modem,
                                       POWER_ON,
                                       NULL, NULL, NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
        gsmd_modem_post_at_command_id (modem,
                                       NEO_INIT,
                                       NULL, NULL, NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
        gsmd_modem_post_at_command_id (modem,
                                       ENABLE_CMEE,
                                       NULL, NULL, NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
        gsmd_modem_post_at_command_id (modem,
                                       NETWORK_REGISTER,
                                       NULL, NULL, NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
        gsmd_modem_general_at_init (modem);

        return TRUE;
}

static gboolean neo1973_at_init_sim_ready (VendorInterface* vendor)
{
        ModemInterface *modem = neo1973_priv->modem;
        g_assert(modem);
        g_debug("%s", __func__);

        gsmd_modem_post_at_command_id (modem,
                                       SMS_INIT,
                                       NULL, NULL, NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
        gsmd_modem_post_at_command_id (modem,
                                       POWER_ON,
                                       NULL, NULL, NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
        gsmd_modem_post_at_command_id (modem,
                                       NEO_COPS,
                                       NULL, NULL, NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
        gsmd_modem_post_at_command_id (modem,
                                       NEO_ENABLE_CALL_PROGRESS,
                                       NULL, NULL, NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
        gsmd_modem_general_at_init_sim_ready (modem);

        return TRUE;
}

/********************************/

static void neo1973_data_in (ModemInterface *modem,
                             AtCommandContext *at,
                             GString *data)
{
        g_get_current_time(&neo1973_priv->last_data_in);
}

/* static void neo1973_get_dtmf_duration(CallInterface *call, */
/*                                       gpointer ipc_data) */
/* { */
/*         // It seems that Neo1973 modem does not support AT+VTD command */
/*         GError *error = g_error_new(GSMD_ERROR, */
/*                                     GSMD_ERROR_NOTSUPPORTED, */
/*                                     "Not supported"); */
/*         neo1973_priv->modem->call_ipc->send_dtmf_error(neo1973_priv->modem->call_ipc, */
/*                                                        ipc_data, */
/*                                                        error); */
/*         g_error_free(error); */
/* } */
static void neo1973_unsolicite_handler_init(ModemInterface *modem,
                                            VendorInterface *vendor)
{
        g_debug("%s", __func__);
        //add the symbol to the modem scanner
        while (symbol_p->symbol_name) {
                g_scanner_add_symbol (modem->scanner, symbol_p->symbol_name,
                                      GINT_TO_POINTER(symbol_p->symbol_token));
                symbol_p ++;
        }

        //add the command to the modem command table
        while (command_p->command) {
                gsmd_modem_register_command(modem, command_p);
                command_p++;
        }
        vendor->unsolicite_handler = neo1973_unsolicite_handler;
}

static void neo1973_cpi_parse (GScanner* scanner,
                               gint* call_id,
                               CpiState *state)
{
/* From http://svn.openmoko.org/trunk/src/target/gsm/src/gsmd/vendor_ti.c (revision 4442) */
/* Format: cId, msgType, ibt, tch, dir,[mode],[number],[type],[alpha],[cause],line */
        g_scanner_get_next_token (scanner);
        if (scanner->token == ':') {
                g_scanner_get_next_token (scanner);
                *call_id = scanner->value.v_int;

                g_scanner_get_next_token (scanner);//get ;
                g_scanner_get_next_token (scanner);
                *state = scanner->value.v_int;
                // TODO handle rest of the data
        } else {
                g_warning ("%s : unexpexted symbol", __func__);
        }
}

void neo1973_handle_voice_cpi(ModemInterface *modem,
                              CpiState state,
                              gint vendor_id)
{
/*
at%cpi=4
atd1234567890;
OK

%CPI: 1,9,0,0,0,0,"1234567890",129,,0xc5ff,0 (unknown)

%CPRI: 1,2

%CPI: 1,3,0,0,0,0,"1234567890",129,,0xc5ff,0 (call proceed)

%CPI: 1,4,0,1,0,0,"1234567890",129,,0xc506,0 (sync)

%CPI: 1,2,1,1,0,0,"1234567890",129,,0xc5ff,0 (alert)
<ring>

<answer>
%CPI: 1,6,0,1,0,0,"1234567890",129,,0xc500,0 (connected)
<hangup>
%CPI: 1,1,1,1,0,0,"1234567890",129,,0x 510,0 (disconnect)

%CPI: 1,1,1,0,0,0,"1234567890",129,,0x 510,0 (disconnect)

NO CARRIER

%CPI: 1,7,1,0,0,0,"1234567890",129,,0x 510,0 (release)

---------------------------------------------
<call>
%CPRI: 1,2

%CPI: 1,0,0,0,1,0,"1234567890",129,,0xc5ff,0 (setup)

%CPI: 1,0,0,1,1,0,"1234567890",129,,0xc506,0 (setup)

RING

+CLIP: "1234567890",129,,,,0

%CPI: 1,4,0,1,1,0,"1234567890",129,,0xc506,0 (sync)

RING

+CLIP: "1234567890",129,,,,0

RING

+CLIP: "1234567890",129,,,,0
ata
OK

%CPI: 1,6,0,1,1,0,"1234567890",129,,0xc500,0 (connected)
ath
OK

%CPI: 1,1,0,0,1,0,"1234567890",129,,0x85ff,0 (disconnect)

%CPI: 1,7,0,0,1,0,"1234567890",129,,0x85ff,0 (release)

-------------------------------------------------------------
atd1234567890;
OK

%CPI: 1,9,0,0,0,0,"1234567890",129,,0xc5ff,0 (unknown)

%CPRI: 1,2

%CPI: 1,3,0,0,0,0,"1234567890",129,,0xc5ff,0 (call proceed)

%CPI: 1,4,0,1,0,0,"1234567890",129,,0xc506,0 (sync)

%CPI: 1,2,1,1,0,0,"1234567890",129,,0xc5ff,0 (alert)
<ring>
<hangup>
%CPI: 1,1,1,1,0,0,"1234567890",129,,0x 511,0 (disconnect)

%CPI: 1,1,1,0,0,0,"1234567890",129,,0x 511,0 (disconnect)

BUSY

%CPI: 1,7,1,0,0,0,"1234567890",129,,0x 511,0 (release)


 */

        Call *call = NULL;
        g_debug("%s : call vendor id: %d, state: %d", __func__, vendor_id,state);


        switch (state) {
        case CPI_CALL_CONNECTED://Phone call established
                g_debug("%s: CPI_CALL_CONNECTED",__func__);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if( !call ) {
                        g_debug("%s : No call found?", __func__);
                        break;
                }

                gsmd_utils_call_send_status(modem,
                                            call,
                                            "active");

                call->status = CALL_CONNECTED;
                break;

        case CPI_REJECT:
        case CPI_DISCONNECT:
        case CPI_RELEASE://Got disconnected
                g_debug("%s: CPI_RELEASE, CPI_DISCONNECT or CPI_REJECT",__func__);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if( call ) {
                        gsmd_utils_remove_call_vendor_id(modem,vendor_id,TRUE);
                } else {
                        g_debug("%s : Cannot find call with vendor id %d", __func__, vendor_id);
                }
                break;
        case CPI_CALL_PROCEED://We are calling someone
                g_debug("%s: CPI_CALL_PROCEED",__func__);
                gsmd_utils_set_unknown_vendor_id(modem,vendor_id);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if( !call ) {
                        g_debug("%s : No call found?", __func__);
                        break;
                }
                gsmd_utils_table_insert_int(call->vendor_properties,GSMD_VENDOR_KEY_CPI,state);
                gsmd_utils_call_send_status(modem,
                                            call,
                                            "outgoing");
                break;
        case CPI_UNKNOWN:
                g_debug("%s: CPI_UNKNOWN",__func__);
                break;
        case CPI_PROGRESS:
                g_debug("%s: CPI_PROGRESS",__func__);
                break;
        case CPI_SYNC:
                g_debug("%s: CPI_SYNC",__func__);
                call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                if( !call ) {
                        gsmd_utils_set_unknown_vendor_id(modem,vendor_id);
                        call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
                        if( !call ) {
                                g_debug("%s : No call found?", __func__);
                                break;
                        }
                        gsmd_utils_table_insert_int(call->vendor_properties,GSMD_VENDOR_KEY_CPI,state);
                }
                break;
        case CPI_ALERT:
                g_debug("%s: CPI_ALERT",__func__);
                break;
        case CPI_SETUP:
                g_debug("%s: CPI_SETUP",__func__);
                break;
        }

}
static AtCommandHandlerStatus neo1973_unsolicite_handler (ModemInterface *modem,
                                                          AtCommandContext *at,
                                                          GString *response)
{
// only reponse for parse the vendor token such as %CPI
        gint vendor_id = -1;
        GScanner* scanner = modem->scanner;
        CpiState state = CPI_UNKNOWN;
        g_debug("%s",__func__);

        switch (scanner->token) {

        case '%':
                g_scanner_get_next_token(scanner);

                switch (scanner->token) {
                case SYMBOL_CPI:
                        //%CPRI: 1,2
                        neo1973_cpi_parse(scanner,&vendor_id,&state);
                        g_debug("%s : call vendor id: %d", __func__, vendor_id);
                        neo1973_handle_voice_cpi(modem,state,vendor_id);
                        return AT_HANDLER_DONE;
                default:
                        break;
                }
        default:
                break;
        }
        return AT_HANDLER_DONT_UNDERSTAND;
};
