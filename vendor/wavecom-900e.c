/*
 *  wavecom-900e.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include <glib.h>
#include <glib/gstdio.h>

#include "../src/modem.h"
#include "../src/utils.h"
#include "../src/call_interface.h"
#include "../src/at_error.h"
#include "../src/sim.h"

enum {
    ENABLE_WIND = VENDOR_COMMAND
    };
enum {
    SYMBOL_WIND = VENDOR_SYMBOL

    };

static void wavecom_unsolicite_handler_init (ModemInterface* modem,
                                             VendorInterface *vendor);
static AtCommandHandlerStatus wavecom_unsolicite_handler (ModemInterface* modem,
                                                          AtCommandContext *at,
                                                          GString *response);

static gboolean vendor_at_init_sim_ready (VendorInterface* vendor);

static AtCommandHandlerStatus wavecom_call_initiate_handler(ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response);
static const AtCommand
commands[] = {
        //Vendor command
        { ENABLE_WIND,        "AT+WIND=50\r\n",   100,    TRUE, 1,
                NULL,NULL }
}, *command_p = commands;

static const SymbolTable
symbols[] = {
        { "WIND", SYMBOL_WIND, },//vendor command
        { NULL, 0, },
}, *symbol_p = symbols;

typedef struct _Wavecom900e {
        ModemInterface *modem;
        AtCommandHandlerStatus (*orig_call_initiate_handler) (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response);
} Wavecom900e;

static Wavecom900e *vendor_priv = NULL;

static
AtCommandHandlerStatus wavecom_handler_query_pin_status (ModemInterface *modem,
                                                         AtCommandContext *at,
                                                         GString *response);

/** vendor_handle_init should be called from modem.c when the lib loaded*/
gboolean vendor_handle_init (VendorInterface *vendor, ModemInterface* modem)
{
        if ( vendor_priv ) {
                g_warning("%s : Already initialized!", __func__);
                return FALSE;
        }
        vendor_priv = g_new0(Wavecom900e, 1);
        g_debug("%s", __func__);
        vendor->init_at = NULL;
        vendor->init_at_sim_ready = vendor_at_init_sim_ready;
        vendor->priv = (gpointer)vendor_priv;
        wavecom_unsolicite_handler_init (modem, vendor);
        vendor_priv->modem = modem;

        AtCommand *call_initiate = gsmd_modem_get_command(modem, INITIATE_CALL);
        vendor_priv->orig_call_initiate_handler = call_initiate->handler;
        call_initiate->handler = &wavecom_call_initiate_handler;

        AtCommand *query_pin = gsmd_modem_get_command(modem, PIN_QUERY);
        query_pin->handler = &wavecom_handler_query_pin_status;


        return TRUE;
}
/*************************************/


/**
 * @brief Handler to modems response from queryPinStatus command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
static
AtCommandHandlerStatus wavecom_handler_query_pin_status (ModemInterface *modem,
                                                         AtCommandContext *at,
                                                         GString *response)
{
        g_debug("%s",__func__);
        GScanner* scanner = modem->scanner;



        if (gsmd_utils_send_error_from_response(at,
                                                response,
                                                "ERROR READING PIN STATUS",
                                                "TIMEOUT READING PIN STATUS"))
                return AT_HANDLER_DONE_ERROR;

        if (scanner->token != '+') {
                return AT_HANDLER_DONT_UNDERSTAND;
        }

        g_scanner_get_next_token (scanner);
        switch (scanner->token) {

        case SYMBOL_CME:
                g_scanner_get_next_token (scanner);
                //g_scanner_get_next_token (scanner);
                if (scanner->token == SYMBOL_ERROR) {
                        g_scanner_peek_next_token(scanner);
                        if (scanner->next_token == ':') {
                                g_scanner_get_next_token (scanner);
                                g_scanner_get_next_token(scanner);//arrive error value
                                if ( ERR_NO_SIM_CARD == scanner->value.v_int ) {
                                        gsmd_sim_change_auth_status(modem,
                                                                    at->ipc_data,
                                                                    SIM_MISSING,
                                                                    "SIM MISSING",
                                                                    TRUE);
                                } else {
                                        gsmd_sim_change_auth_status(modem,
                                                                    at->ipc_data,
                                                                    SIM_ERROR,
                                                                    "SIM ERROR",
                                                                    TRUE);
                                }

                                return AT_HANDLER_DONE;
                        } else {
                                g_scanner_unexp_token (scanner, ':', NULL, "symbol",
                                                       NULL, NULL, TRUE);

                        };
                } else {
                        g_scanner_unexp_token (scanner, SYMBOL_ERROR, NULL, "symbol",
                                               NULL, NULL, TRUE);
                }
                break;
        case SYMBOL_CPIN:
                g_scanner_get_next_token (scanner);//get ':'
                g_scanner_get_next_token (scanner);
                if (scanner->token == SYMBOL_READY) {
                        g_debug("%s: READY",__func__);
                        gsmd_sim_change_auth_status(modem,
                                                    at->ipc_data,
                                                    SIM_READY,
                                                    "READY",
                                                    TRUE);


                        return AT_HANDLER_DONE;

                } else if (scanner->token == SYMBOL_SIM) {

                        g_scanner_get_next_token (scanner);
                        g_scanner_peek_next_token(scanner);
                        if (scanner->token == SYMBOL_PIN) {
                                gsmd_sim_change_auth_status(modem,
                                                            at->ipc_data,
                                                            SIM_NEED_PIN,
                                                            "SIM PIN",
                                                            TRUE);
                        } else if (scanner->token == SYMBOL_PUK) {
                                g_debug("%s: SIM PUK",__func__);
                                gsmd_sim_change_auth_status(modem,
                                                            at->ipc_data,
                                                            SIM_NEED_PUK,
                                                            "SIM PUK",
                                                            TRUE);
                        }

                        return AT_HANDLER_DONE;
                } else {
                        g_scanner_unexp_token (scanner, SYMBOL_READY, NULL, "symbol",
                                               NULL, NULL, TRUE);
                }
                break;
        default:
                g_warning("No match after getting + \n");
                break;
        }





        return AT_HANDLER_DONT_UNDERSTAND;
}


static gboolean vendor_at_init_sim_ready (VendorInterface* vendor)
{
        ModemInterface *modem = vendor_priv->modem;
        g_assert(modem);
        g_debug("%s", __func__);

        gsmd_modem_general_at_init_sim_ready (modem);

        gsmd_modem_post_at_command_id(modem,
                                      ENABLE_WIND,
                                      NULL,NULL,NULL,NULL,
                                      INTERFACE_GENERAL,
                                      NULL);

        return TRUE;
}

/********************************/



static void wavecom_unsolicite_handler_init(ModemInterface *modem,
                                            VendorInterface *vendor)
{
        g_debug("%s", __func__);
        //add the symbol to the modem scanner
        while (symbol_p->symbol_name) {
                g_scanner_add_symbol (modem->scanner, symbol_p->symbol_name,
                                      GINT_TO_POINTER(symbol_p->symbol_token));
                symbol_p ++;
        }

        //add the command to the modem command table
        while (command_p->command) {
                gsmd_modem_register_command(modem, command_p);
                command_p++;
        }
        vendor->unsolicite_handler = wavecom_unsolicite_handler;

}

static
AtCommandHandlerStatus wavecom_handle_wind(ModemInterface *modem,
                                           AtCommandContext *at,
                                           gint value,
                                           gint callid)
{
        g_debug("%s",__func__);
        Call *call = NULL;

        switch (value) {
        case 2:
                call = gsmd_utils_find_first_call_status(modem,CALL_CALLING);
                if (call) {
                        call->status = CALL_WAITING;


                } else {
                        gsmd_utils_print_calls(modem);
                }
                return AT_HANDLER_NEED_MORE;
                break;
        case 5:
                g_debug("%s : Call created with id %d",__func__,callid);
                g_debug("%s : Current calls: ",__func__);

                gsmd_utils_print_calls(modem);
                call = gsmd_utils_find_first_call_status(modem,CALL_IDLE);
                if (call) {
                        call->vendor_id = callid;
                        call->status = CALL_CALLING;
                        if (modem->call_ipc->initiate_reply && at)
                                modem->call_ipc->initiate_reply(modem->call_ipc,
                                                                at->ipc_data,
                                                                call->id);

                        gsmd_utils_call_send_status(modem,
                                                    call,
                                                    "outgoing");
                        at->ipc_data = NULL;
                }
                return AT_HANDLER_NEED_MORE;
                break;
        case 6:
                gsmd_utils_remove_calls(modem,TRUE);
                return AT_HANDLER_NEED_MORE;
                break;
        }

        return AT_HANDLER_DONT_UNDERSTAND;
}


AtCommandHandlerStatus wavecom_parse_wind(ModemInterface *modem,
                                          AtCommandContext *at,
                                          GString *response)
{
        g_debug("%s",__func__);
        AtCommandHandlerStatus result = AT_HANDLER_DONT_UNDERSTAND;
        GError **error = NULL;
        GRegex *regex = g_regex_new ("\\+WIND:\\s(?<value>\\d+),?(?<call>\\d+)?", 0, 0, error);
        GMatchInfo *match_info;
        gchar *match = NULL;

        if (g_regex_match (regex, response->str, 0, &match_info)) {
                if (gsmd_utils_match_exists(match_info,"value")) {
                        match = g_match_info_fetch_named(match_info,"value");
                        gint value = atoi(match);
                        g_free(match);
                        gint id = 0;

                        if (gsmd_utils_match_exists(match_info,"call")) {
                                match = g_match_info_fetch_named(match_info,"call");
                                id = atoi(match);
                                g_free(match);
                        }

                        result = wavecom_handle_wind(modem,at,value,id);
                }
        }

        g_free(error);
        g_match_info_free (match_info);
        g_regex_unref (regex);

        return result;
}

static
AtCommandHandlerStatus wavecom_call_initiate_handler(ModemInterface *modem,
                                                     AtCommandContext *at,
                                                     GString *response)
{

        AtCommandHandlerStatus status = wavecom_parse_wind(modem,at,response);
        GScanner* scanner = modem->scanner;


        if (scanner->token == SYMBOL_OK) {

                Call *call = gsmd_utils_find_first_call_status(modem,CALL_WAITING);


                if (!call) {
                        call = gsmd_utils_find_first_call_status(modem,CALL_CALLING);
                }

                if (call) {
                        call->status = CALL_CONNECTED;
                        gsmd_utils_call_send_status(modem,
                                                    call,
                                                    "active");

                } else {
                        g_warning("%s : OK reply to a call that doesn't existst. Current calls:",__func__);
                        gsmd_utils_print_calls(modem);
                }

                return AT_HANDLER_DONE;
        } else if (scanner->token == SYMBOL_BUSY || scanner->token == SYMBOL_NO) {
                Call *call = gsmd_utils_get_first_call(modem);
                if (call) {
                        gsmd_utils_remove_call(modem,call->id, TRUE);
                }

                return AT_HANDLER_DONE;
        }


        if (status != AT_HANDLER_NEED_MORE)
                return vendor_priv->orig_call_initiate_handler(modem,at,response);
        else
                return status;
}


AtCommandHandlerStatus wavecom_unsolicite_handler (ModemInterface *modem,
                                                   AtCommandContext *at,
                                                   GString *response)
{
        g_debug("%s",__func__);
        return wavecom_parse_wind(modem,at,response);
};
