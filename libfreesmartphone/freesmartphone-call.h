/*
 *  freesmartphone-call.h
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#ifndef FREESMARTPHONE_CALL_H
#define FREESMARTPHONE_CALL_H

#include <glib.h>
#include <dbus/dbus-glib.h>

#include "freesmartphone.h"

/* typedef enum { */
/*         FSO_GSM_CALL_TYPE_VOICE = 0 */
/* } FSGSMCallType; */

/* typedef enum { */
/*         FSO_GSM_CALL_STATUS_UNKNOWN = 0, */
/*         FSO_GSM_CALL_STATUS_DISCONNECT, */
/*         FSO_GSM_CALL_STATUS_CALL_CONNECTED */
/* } FSGSMCallStatus; */



typedef void (*FSGSMCallInitiateReplyFunc) ( gint id,
                                             GError *error,
                                             gpointer userdata );

gboolean fso_gsm_call_initiate( FreeSmartphone *fs,
                                const gchar *number,
                                const gchar *type,
                                FSGSMCallInitiateReplyFunc cb,
                                gpointer userdata );



typedef void (*FSGSMCallActivateReplyFunc) (  GError *error,
                                              gpointer userdata );

gboolean fso_gsm_call_activate( FreeSmartphone *fs,
                                gint id,
                                FSGSMCallActivateReplyFunc cb,
                                gpointer userdata);

typedef void (*FSGSMCallActivateConferenceReplyFunc) (  GError *error,
                                                        gpointer userdata );

gboolean fso_gsm_call_activate_conference( FreeSmartphone *fs,
                                           gint id,
                                           FSGSMCallActivateConferenceReplyFunc cb,
                                           gpointer userdata);

typedef void (*FSGSMCallReleaseReplyFunc) ( GError *error,
                                            gpointer userdata );

gboolean fso_gsm_call_release( FreeSmartphone *fs,
                               const gchar *message,
                               gint id,
                               FSGSMCallReleaseReplyFunc cb,
                               gpointer userdata );

typedef void (*FSGSMCallReleaseHeldReplyFunc) ( GError *error,
                                                gpointer userdata );

gboolean fso_gsm_call_release_held( FreeSmartphone *fs,
                                    const gchar *message,
                                    FSGSMCallReleaseHeldReplyFunc cb,
                                    gpointer userdata );

typedef void (*FSGSMCallReleaseAllReplyFunc) ( GError *error,
                                               gpointer userdata );

gboolean fso_gsm_call_release_all( FreeSmartphone *fs,
                                   const gchar *message,
                                   FSGSMCallReleaseAllReplyFunc cb,
                                   gpointer userdata );


typedef void (*FSGSMCallListReplyFunc) ( GPtrArray *calls,
                                         GError *error,
                                         gpointer userdata );


gboolean fso_gsm_call_list_calls( FreeSmartphone *fs,
                                  FSGSMCallListReplyFunc cb,
                                  gpointer userdata);

/* Signals */

typedef void (*FSGSMCallStatusSignalFunc) (gint id,
                                           gchar *status,
                                           GHashTable *properties,
                                           gpointer userdata);

gboolean fso_gsm_call_status_signal(FreeSmartphone *fs,
                                    FSGSMCallStatusSignalFunc cb,
                                    gpointer userdata);

gboolean fso_gsm_call_status_signal_remove(FreeSmartphone *fs,
                                           FSGSMCallStatusSignalFunc cb,
                                           gpointer userdata);




#endif /* FREESMARTPHONE_CALL_H */

