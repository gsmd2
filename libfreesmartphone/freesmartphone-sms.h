/*
 *  freesmartphone-sms.h
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#ifndef FREESMARTPHONE_SMS_H
#define FREESMARTPHONE_SMS_H

#include <glib.h>
#include <dbus/dbus-glib.h>

#include "freesmartphone.h"

typedef void (*FSGSMSMSReply) (GError *error,
                               gpointer userdata);


typedef void (*FSGSMSMSMessageSent) (gboolean success,
                                     gchar *reason,
                                     gpointer userdata);
typedef void (*FSGSMSMSIncomingMessage) (gint index,
                                         gpointer userdata);

gboolean fso_gsm_sms_send_message(FreeSmartphone *fs,
                                  const gchar *message,
                                  const gchar *number,
                                  const gboolean want_report,
                                  FSGSMSMSReply cb,
                                  gpointer userdata);


gboolean fso_gsm_sms_incoming_message_signal_remove(FreeSmartphone *fs,
                                                    FSGSMSMSIncomingMessage cb,
                                                    gpointer userdata);

gboolean fso_gsm_sms_incoming_message_signal(FreeSmartphone *fs,
                                             FSGSMSMSIncomingMessage cb,
                                             gpointer userdata);

gboolean fso_gsm_sms_message_sent_signal_remove(FreeSmartphone *fs,
                                                FSGSMSMSMessageSent cb,
                                                gpointer userdata);

gboolean fso_gsm_sms_message_sent_signal(FreeSmartphone *fs,
                                         FSGSMSMSMessageSent cb,
                                         gpointer userdata);

gboolean fso_gsm_sms_modify_message(FreeSmartphone *fs,
                                    gint index,
                                    GHashTable *message,
                                    FSGSMSMSReply cb,
                                    gpointer userdata);

#endif /* FREESMARTPHONE_CALL_H */

