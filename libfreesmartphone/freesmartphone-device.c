/*
 *  freesmartphone-device.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */
/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#include "freesmartphone-internal.h"
#include "freesmartphone-device.h"
#include "freesmartphone-device-binding.h"
#include "fso-marshallers.h"



/**
* @brief Initializes device interface, it is called automatically
* when necessary
*
* @param fs pointer to freesmartphone struct
*/
void fso_gsm_device_init(FreeSmartphone *fs)
{
        g_debug("%s", __func__);
}

/**
* @brief Reply function to get device information function
* Calls given callback with device information.
*
* @param proxy dbus proxy used to call get device information
* @param vendor vendor name
* @param model model name
* @param revision device revision
* @param imei imei code
* @param error pointer to error struct
* @param userdata userdata passed when get device information was called
*/
static
void fso_gsm_device_get_info_reply(DBusGProxy *proxy,
                                   GHashTable *info,
                                   GError *error,
                                   gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMDeviceGetInfoReplyFunc reply_cb = (FSGSMDeviceGetInfoReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(info,error,data);
}


/**
* @brief Gets device information
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when information is gotten
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_device_get_info(FreeSmartphone *fs,
                                 FSGSMDeviceGetInfoReplyFunc cb,
                                 gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_DEVICE);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_DEVICE);

        org_freesmartphone_GSM_Device_get_info_async(proxy,
                                                     &fso_gsm_device_get_info_reply,
                                                     (gpointer)replydata);
        return TRUE;
}


/* This is in the current API, but we have a bit older version of it in use */
/* TODO uncomment when a newer API specification is used */

static
void fso_gsm_device_get_antenna_power_reply(DBusGProxy *proxy,
                                            gboolean antenna_power,
                                            GError *error,
                                            gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMDeviceGetAntennaPowerReplyFunc reply_cb = (FSGSMDeviceGetAntennaPowerReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(antenna_power,error,data);
}

gboolean fso_gsm_device_get_antenna_power(FreeSmartphone *fs,
                                          FSGSMDeviceGetAntennaPowerReplyFunc cb,
                                          gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_DEVICE);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_DEVICE);

        org_freesmartphone_GSM_Device_get_antenna_power_async(proxy,
                                                   &fso_gsm_device_get_antenna_power_reply,
                                                   (gpointer)replydata);
        return TRUE;
}


/**
* @brief Reply function to set antenna power
*
* @param proxy proxy used to call set antenna power
* @param error pointer to errur struct
* @param userdata user data
*/
static
void fso_gsm_device_set_antenna_power_reply(DBusGProxy *proxy,
                                            GError *error,
                                            gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMDeviceReplyFunc reply_cb = (FSGSMDeviceReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error,data);
}

/**
* @brief Sets antenna power (on or off)
*
* @param fs pointer to freesmartphone struct
* @param antenna_power on = TRUE, off = FALSE
* @param cb callback function to call when antenna power has been set
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_device_set_antenna_power(FreeSmartphone *fs,
                                          gboolean antenna_power,
                                          FSGSMDeviceReplyFunc cb,
                                          gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_DEVICE);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_DEVICE);

        org_freesmartphone_GSM_Device_set_antenna_power_async(proxy,
                                                              antenna_power,
                                                              &fso_gsm_device_set_antenna_power_reply,
                                                              (gpointer)replydata);
        return TRUE;
}

/**
* @brief Reply function to get features
*
* @param proxy proxy used to call get features
* @param features hash table containing device features
* @param error pointer to errur struct
* @param userdata user data
*/
static
void fso_gsm_device_get_features_reply(DBusGProxy *proxy,
                                       GHashTable *features,
                                       GError *error,
                                       gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMDeviceGetFeaturesReplyFunc reply_cb = (FSGSMDeviceGetFeaturesReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(features,error,data);
}

/**
* @brief Gets device features
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when antenna power has been set
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_device_get_features(FreeSmartphone *fs,
                                     FSGSMDeviceGetFeaturesReplyFunc cb,
                                     gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_DEVICE);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_DEVICE);

        org_freesmartphone_GSM_Device_get_features_async(proxy,
                                                         &fso_gsm_device_get_features_reply,
                                                         (gpointer)replydata);
        return TRUE;
}


/**
* @brief Reply function to prepare to suspend
*
* @param proxy proxy used
* @param error pointer to errur struct
* @param userdata user data
*/
static
void fso_gsm_device_prepare_to_suspend_reply(DBusGProxy *proxy,
                                             GError *error,
                                             gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMDeviceReplyFunc reply_cb = (FSGSMDeviceReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error,data);
}

/**
* @brief Prepare to suspend
*
* @param fs pointer to freesmartphone struct
* @param cb callback function
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_device_prepare_to_suspend(FreeSmartphone *fs,
                                           FSGSMDeviceReplyFunc cb,
                                           gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_DEVICE);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_DEVICE);

        org_freesmartphone_GSM_Device_prepare_to_suspend_async(proxy,
                                                               &fso_gsm_device_prepare_to_suspend_reply,
                                                               (gpointer)replydata);
        return TRUE;
}

/**
* @brief Reply function to prepare to suspend
*
* @param proxy proxy used
* @param error pointer to errur struct
* @param userdata user data
*/
static
void fso_gsm_device_recover_from_suspend_reply(DBusGProxy *proxy,
                                               GError *error,
                                               gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMDeviceReplyFunc reply_cb = (FSGSMDeviceReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error,data);
}

/**
* @brief Recover from suspend
*
* @param fs pointer to freesmartphone struct
* @param cb callback function
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_device_recover_from_suspend(FreeSmartphone *fs,
                                             FSGSMDeviceReplyFunc cb,
                                             gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_DEVICE);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_DEVICE);

        org_freesmartphone_GSM_Device_recover_from_suspend_async(proxy,
                                                                 &fso_gsm_device_recover_from_suspend_reply,
                                                                 (gpointer)replydata);
        return TRUE;
}
