/*
 *  freesmartphone-sim.h
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#ifndef FREESMARTPHONE_SIM_H
#define FREESMARTPHONE_SIM_H

#include <glib.h>
#include <dbus/dbus-glib.h>

#include "freesmartphone.h"

/******************************Method******************************************/
typedef void (*FSGSMSIMReplyFunc) (GError *error,
                                   gpointer userdata );

typedef void (*FSGSMSIMStoreMessageReplyFunc) (gint index,
                                               GError *error,
                                               gpointer userdata );

typedef void (*FSGSMSIMGetSIMInfoReplyFunc) (GHashTable *info,
                                             GError *error,
                                             gpointer userdata );

gboolean fso_gsm_sim_get_sim_info(FreeSmartphone *fs,
                                  FSGSMSIMGetSIMInfoReplyFunc cb,
                                  gpointer userdata);

typedef void (*FSGSMSIMGetPhonebookInfoReplyFunc) (GHashTable *info,
                                                   GError *error,
                                                   gpointer userdata );

gboolean fso_gsm_sim_get_phonebook_info(FreeSmartphone *fs,
                                        FSGSMSIMGetPhonebookInfoReplyFunc cb,
                                        gpointer userdata);

gboolean fso_gsm_sim_change_auth_code(FreeSmartphone *fs,
                                      gchar *old_pin,
                                      gchar *new_pin,
                                      FSGSMSIMReplyFunc cb,
                                      gpointer userdata);

gboolean fso_gsm_sim_unlock(FreeSmartphone *fs,
                            gchar *puk,
                            gchar *new_pin,
                            FSGSMSIMReplyFunc cb,
                            gpointer userdata);

gboolean fso_gsm_sim_send_auth_code(FreeSmartphone *fs,
                                    gchar *pin,
                                    FSGSMSIMReplyFunc cb,
                                    gpointer userdata);

typedef void (*FSGSMSIMGetAuthStatusReplyFunc) (gchar *status,
                                                GError *error,
                                                gpointer userdata );

gboolean fso_gsm_sim_get_auth_status(FreeSmartphone *fs,
                                     FSGSMSIMGetAuthStatusReplyFunc cb,
                                     gpointer userdata);

typedef void (*FSGSMSIMRetrieveMessageReplyFunc) (gchar *number,
                                                  gchar *message,
                                                  GError *error,
                                                  gpointer userdata );

gboolean fso_gsm_sim_retrieve_message(FreeSmartphone *fs,
                                      gint index,
                                      FSGSMSIMRetrieveMessageReplyFunc cb,
                                      gpointer userdata);

typedef void (*FSGSMSIMretrieve_messagebookReplyFunc) (GPtrArray *messagebook,
                                                       GError *error,
                                                       gpointer userdata );

typedef void (*FSGSMSIMSendStoredMessageReplyFunc) (gint transaction_index,
                                                    GError *error,
                                                    gpointer userdata );

gboolean fso_gsm_sim_retrieve_messagebook(FreeSmartphone *fs,
                                          gchar *category,
                                          FSGSMSIMretrieve_messagebookReplyFunc cb,
                                          gpointer userdata);

gboolean fso_gsm_sim_delete_message(FreeSmartphone *fs,
                                    gint index,
                                    FSGSMSIMReplyFunc cb,
                                    gpointer userdata);

gboolean fso_gsm_sim_store_message(FreeSmartphone *fs,
                                   gchar *message,
                                   gchar *number,
                                   FSGSMSIMStoreMessageReplyFunc cb,
                                   gpointer userdata);

gboolean fso_gsm_sim_send_stored_message(FreeSmartphone *fs,
                                         gint index,
                                         FSGSMSIMSendStoredMessageReplyFunc cb,
                                         gpointer userdata);

gboolean fso_gsm_sim_delete_entry(FreeSmartphone *fs,
                                  gint index,
                                  FSGSMSIMReplyFunc cb,
                                  gpointer userdata);

gboolean fso_gsm_sim_store_entry(FreeSmartphone *fs,
                                 gint index,
                                 gchar *name,
                                 gchar *number,
                                 FSGSMSIMReplyFunc cb,
                                 gpointer userdata);

typedef void (*FSGSMSIMRetrieveEntryReplyFunc) (gchar *name,
                                                gchar *number,
                                                GError *error,
                                                gpointer userdata );

gboolean fso_gsm_sim_retrieve_entry(FreeSmartphone *fs,
                                    gint index,
                                    FSGSMSIMRetrieveEntryReplyFunc cb,
                                    gpointer userdata);

/******************************Signal******************************************/
typedef void (*FSGSMSIMAuthStatusSignalFunc) (gchar *status,
                                              gpointer userdata);

gboolean fso_gsm_sim_auth_status_signal_remove(FreeSmartphone *fs,
                                               FSGSMSIMAuthStatusSignalFunc cb,
                                               gpointer userdata);

gboolean fso_gsm_sim_auth_status_signal(FreeSmartphone *fs,
                                        FSGSMSIMAuthStatusSignalFunc cb,
                                        gpointer userdata);

#endif /* FREESMARTPHONE_SIM_H */

