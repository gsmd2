/*
 *  freesmartphone-pdp.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#include "freesmartphone-internal.h"
#include "freesmartphone-pdp.h"
#include "freesmartphone-pdp-binding.h"
#include "fso-marshallers.h"


#define FSO_PDP_CONTEXT_ACTIVATED_SIGNAL_NAME   "ContextActivated"
#define FSO_PDP_CONTEXT_DEACTIVATED_SIGNAL_NAME "ContextDeactivated"
#define FSO_PDP_CONTEXT_CHANGED_SIGNAL_NAME     "ContextChanged"


/**
* @brief Initializes PDP interface, it is called automatically
* when necessary
*
* @param fs pointer to freesmartphone struct
*/
void fso_gsm_pdp_init(FreeSmartphone *fs)
{
        g_debug("%s", __func__);
}


/**
* @brief Reply function to fso_gsm_pdp_list_gprs_classes function call
*
* @param proxy dbus proxy used to call fso_gsm_pdp_list_gprs_classes function
* @param error pointer to error struct
* @param userdata userdata passed when fso_gsm_pdp_list_gprs_classes function was
* called
*/
static
void fso_gsm_pdp_list_gprs_classes_reply(DBusGProxy *proxy,
                                         gchar **classes,
                                         GError *error,
                                         gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMPDPListGprsClassesReplyFunc reply_cb = (FSGSMPDPListGprsClassesReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(classes,error,data);
}


/**
* @brief Lists gprs classes
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_pdp_list_gprs_classes(FreeSmartphone *fs,
                                       FSGSMPDPListGprsClassesReplyFunc cb,
                                       gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        org_freesmartphone_GSM_PDP_list_gprs_classes_async(proxy,
                                                           &fso_gsm_pdp_list_gprs_classes_reply,
                                                           (gpointer)replydata);
        return TRUE;
}

/**
* @brief Generic reply function for callbacks that have no parameters
*
* @param proxy dbus proxy in use
* @param error pointer to error struct
* @param userdata userdata passed calling the command
*/
static
void fso_gsm_pdp_reply(DBusGProxy *proxy,
                       GError *error,
                       gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMPDPReplyFunc reply_cb = (FSGSMPDPReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error,data);
}


/**
* @brief Selects gprs class
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_pdp_select_gprs_class(FreeSmartphone *fs,
                                       gchar *klass,
                                       FSGSMPDPReplyFunc cb,
                                       gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        org_freesmartphone_GSM_PDP_select_gprs_class_async(proxy,
                                                           klass,
                                                           &fso_gsm_pdp_reply,
                                                           (gpointer)replydata);
        return TRUE;
}



/**
* @brief Activates ?
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_pdp_activate(FreeSmartphone *fs,
                              gint index,
                              FSGSMPDPReplyFunc cb,
                              gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        org_freesmartphone_GSM_PDP_activate_async(proxy,
                                                  index,
                                                  &fso_gsm_pdp_reply,
                                                  (gpointer)replydata);
        return TRUE;
}



/**
* @brief Activates ?
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_pdp_deactivate(FreeSmartphone *fs,
                                gint index,
                                FSGSMPDPReplyFunc cb,
                                gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        org_freesmartphone_GSM_PDP_deactivate_async(proxy,
                                                    index,
                                                    &fso_gsm_pdp_reply,
                                                    (gpointer)replydata);
        return TRUE;
}




/**
* @brief Activates ?
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_pdp_select_context(FreeSmartphone *fs,
                                    gchar *context,
                                    FSGSMPDPReplyFunc cb,
                                    gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        org_freesmartphone_GSM_PDP_select_context_async(proxy,
                                                        context,
                                                        &fso_gsm_pdp_reply,
                                                        (gpointer)replydata);
        return TRUE;
}


/**
* @brief Reply function to fso_gsm_pdp_add_context function call
*
* @param proxy dbus proxy used to call fso_gsm_pdp_add_context function
* @param error pointer to error struct
* @param userdata userdata passed when fso_gsm_pdp_add_context function was
* called
*/
static
void fso_gsm_pdp_add_context_reply(DBusGProxy *proxy,
                                   gint index,
                                   GError *error,
                                   gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMPDPAddContextReplyFunc reply_cb = (FSGSMPDPAddContextReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(index,error,data);
}


/**
* @brief Adds context
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_pdp_add_context(FreeSmartphone *fs,
                                 gchar *context,
                                 FSGSMPDPAddContextReplyFunc cb,
                                 gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        org_freesmartphone_GSM_PDP_add_context_async(proxy,
                                                     context,
                                                     &fso_gsm_pdp_add_context_reply,
                                                     (gpointer)replydata);
        return TRUE;
}

/**
* @brief Deletes context
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_pdp_delete_context(FreeSmartphone *fs,
                                    gint index,
                                    FSGSMPDPReplyFunc cb,
                                    gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        org_freesmartphone_GSM_PDP_delete_context_async(proxy,
                                                        index,
                                                        &fso_gsm_pdp_reply,
                                                        (gpointer)replydata);
        return TRUE;
}


/**
* @brief Reply function to fso_gsm_pdp_list_contexts function call
*
* @param proxy dbus proxy used to call fso_gsm_pdp_list_contexts function
* @param error pointer to error struct
* @param userdata userdata passed when fso_gsm_pdp_list_contexts function was
* called
*/
static
void fso_gsm_pdp_list_contexts_reply(DBusGProxy *proxy,
                                   gchar **contexts,
                                   GError *error,
                                   gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMPDPListContextsReplyFunc reply_cb = (FSGSMPDPListContextsReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(contexts,error,data);
}


/**
* @brief Lists contexts
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_pdp_list_contexts(FreeSmartphone *fs,
                                    FSGSMPDPListContextsReplyFunc cb,
                                    gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        org_freesmartphone_GSM_PDP_list_contexts_async(proxy,
                                                        &fso_gsm_pdp_list_contexts_reply,
                                                        (gpointer)replydata);
        return TRUE;
}

/**
* @brief Handler to context activated signal
* Calls all registered callbacks.
*
* @param proxy proxy to the connection that signal was sent through
* @param provider_name provider's name
* @param status network status
* @param strength signal strength
* @param data user data
*/
static
void fso_gsm_pdp_context_activated_handler(DBusGProxy *proxy,
                                    gint id,
                                    gpointer data)
{
        g_debug("%s", __func__);
        FreeSmartphone *fs = (FreeSmartphone*)data;
        g_return_if_fail(fs);
        GList *list = fso_get_signal_handler(fs,FSO_SIGNAL_CONTEXT_ACTIVATED);

        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                FSGSMPDPContextSignalFunc status_cb = (FSGSMPDPContextSignalFunc)rd->cb;
                status_cb (id,rd->data);
                list = g_list_next(list);
        }
}


/**
* @brief Adds a callback to context activated signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when signal is received
* @param userdata user data passed along with callback
* @return true if callback was succesfully added
*/
gboolean fso_gsm_pdp_context_activated_signal(FreeSmartphone *fs,
                                       FSGSMPDPContextSignalFunc cb,
                                       gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        if ( fso_append_data_to_signal_handler(fs,
                                               FSO_SIGNAL_CONTEXT_ACTIVATED,
                                               replydata) ) {
                dbus_g_proxy_add_signal(proxy,
                                        FSO_PDP_CONTEXT_ACTIVATED_SIGNAL_NAME,
                                        G_TYPE_INT,
                                        G_TYPE_INVALID);
                dbus_g_proxy_connect_signal (proxy, FSO_PDP_CONTEXT_ACTIVATED_SIGNAL_NAME,
                                             G_CALLBACK(fso_gsm_pdp_context_activated_handler),
                                             fs, NULL);
        }

        return TRUE;
}

/**
* @brief Removes callback from context activated signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to remove
* @param userdata userdata assosicated with callback
* @return true if callback was removed
*/
gboolean fso_gsm_pdp_context_activated_signal_remove(FreeSmartphone *fs,
                                           FSGSMPDPContextSignalFunc cb,
                                           gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);


        return fso_signal_remove(fs,
                                 FSO_NETWORK,
                                 FSO_PDP_CONTEXT_ACTIVATED_SIGNAL_NAME,
                                 FSO_SIGNAL_CONTEXT_ACTIVATED,
                                 G_CALLBACK(fso_gsm_pdp_context_activated_handler),
                                 (gpointer)fs,
                                 G_CALLBACK(cb),
                                 userdata);
}


/**
* @brief Handler to context deactivated signal
* Calls all registered callbacks.
*
* @param proxy proxy to the connection that signal was sent through
* @param provider_name provider's name
* @param status network status
* @param strength signal strength
* @param data user data
*/
static
void fso_gsm_pdp_context_deactivated_handler(DBusGProxy *proxy,
                                    gint id,
                                    gpointer data)
{
        g_debug("%s", __func__);
        FreeSmartphone *fs = (FreeSmartphone*)data;
        g_return_if_fail(fs);
        GList *list = fso_get_signal_handler(fs,FSO_SIGNAL_CONTEXT_DEACTIVATED);

        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                FSGSMPDPContextSignalFunc status_cb = (FSGSMPDPContextSignalFunc)rd->cb;
                status_cb (id,rd->data);
                list = g_list_next(list);
        }
}


/**
* @brief Adds a callback to context activated signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when signal is received
* @param userdata user data passed along with callback
* @return true if callback was succesfully added
*/
gboolean fso_gsm_pdp_context_deactivated_signal(FreeSmartphone *fs,
                                       FSGSMPDPContextSignalFunc cb,
                                       gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        if ( fso_append_data_to_signal_handler(fs,
                                               FSO_SIGNAL_CONTEXT_DEACTIVATED,
                                               replydata) ) {
                dbus_g_proxy_add_signal(proxy,
                                        FSO_PDP_CONTEXT_DEACTIVATED_SIGNAL_NAME,
                                        G_TYPE_INT,
                                        G_TYPE_INVALID);
                dbus_g_proxy_connect_signal (proxy, FSO_PDP_CONTEXT_DEACTIVATED_SIGNAL_NAME,
                                             G_CALLBACK(fso_gsm_pdp_context_deactivated_handler),
                                             fs, NULL);
        }

        return TRUE;
}

/**
* @brief Removes callback from context deactivated signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to remove
* @param userdata userdata assosicated with callback
* @return true if callback was removed
*/
gboolean fso_gsm_pdp_context_deactivated_signal_remove(FreeSmartphone *fs,
                                           FSGSMPDPContextSignalFunc cb,
                                           gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);


        return fso_signal_remove(fs,
                                 FSO_NETWORK,
                                 FSO_PDP_CONTEXT_DEACTIVATED_SIGNAL_NAME,
                                 FSO_SIGNAL_CONTEXT_DEACTIVATED,
                                 G_CALLBACK(fso_gsm_pdp_context_deactivated_handler),
                                 (gpointer)fs,
                                 G_CALLBACK(cb),
                                 userdata);
}


/**
* @brief Handler to context changed signal
* Calls all registered callbacks.
*
* @param proxy proxy to the connection that signal was sent through
* @param provider_name provider's name
* @param status network status
* @param strength signal strength
* @param data user data
*/
static
void fso_gsm_pdp_context_changed_handler(DBusGProxy *proxy,
                                    gint id,
                                    gpointer data)
{
        g_debug("%s", __func__);
        FreeSmartphone *fs = (FreeSmartphone*)data;
        g_return_if_fail(fs);
        GList *list = fso_get_signal_handler(fs,FSO_SIGNAL_CONTEXT_CHANGED);

        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                FSGSMPDPContextSignalFunc status_cb = (FSGSMPDPContextSignalFunc)rd->cb;
                status_cb (id,rd->data);
                list = g_list_next(list);
        }
}


/**
* @brief Adds a callback to context changed signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when signal is received
* @param userdata user data passed along with callback
* @return true if callback was succesfully added
*/
gboolean fso_gsm_pdp_context_changed_signal(FreeSmartphone *fs,
                                       FSGSMPDPContextSignalFunc cb,
                                       gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_PDP);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_PDP);

        if ( fso_append_data_to_signal_handler(fs,
                                               FSO_SIGNAL_CONTEXT_CHANGED,
                                               replydata) ) {
                dbus_g_proxy_add_signal(proxy,
                                        FSO_PDP_CONTEXT_CHANGED_SIGNAL_NAME,
                                        G_TYPE_INT,
                                        G_TYPE_INVALID);
                dbus_g_proxy_connect_signal (proxy, FSO_PDP_CONTEXT_CHANGED_SIGNAL_NAME,
                                             G_CALLBACK(fso_gsm_pdp_context_changed_handler),
                                             fs, NULL);
        }

        return TRUE;
}

/**
* @brief Removes callback from context changed signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to remove
* @param userdata userdata assosicated with callback
* @return true if callback was removed
*/
gboolean fso_gsm_pdp_context_changed_signal_remove(FreeSmartphone *fs,
                                           FSGSMPDPContextSignalFunc cb,
                                           gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);


        return fso_signal_remove(fs,
                                 FSO_NETWORK,
                                 FSO_PDP_CONTEXT_CHANGED_SIGNAL_NAME,
                                 FSO_SIGNAL_CONTEXT_CHANGED,
                                 G_CALLBACK(fso_gsm_pdp_context_changed_handler),
                                 (gpointer)fs,
                                 G_CALLBACK(cb),
                                 userdata);
}
