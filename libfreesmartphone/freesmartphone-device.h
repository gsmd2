/*
 *  freesmartphone-device.h
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#ifndef FREESMARTPHONE_DEVICE_H
#define FREESMARTPHONE_DEVICE_H

#include <glib.h>
#include <dbus/dbus-glib.h>

#include "freesmartphone.h"

typedef void (*FSGSMDeviceReplyFunc) (GError *error,
                                      gpointer userdata );
typedef void (*FSGSMDeviceGetFeaturesReplyFunc) (GHashTable *features,
                                                 GError *error,
                                                 gpointer userdata );
typedef void (*FSGSMDeviceGetInfoReplyFunc) (GHashTable *info,
                                             GError *error,
                                             gpointer userdata );

typedef void (*FSGSMDeviceGetAntennaPowerReplyFunc) (gboolean antenna_power,
                                                     GError *error,
                                                     gpointer userdata );

gboolean fso_gsm_device_get_info(FreeSmartphone *fs,
                                 FSGSMDeviceGetInfoReplyFunc cb,
                                 gpointer userdata);

gboolean fso_gsm_device_set_antenna_power(FreeSmartphone *fs,
                                          gboolean antenna_power,
                                          FSGSMDeviceReplyFunc cb,
                                          gpointer userdata);

gboolean fso_gsm_device_get_features(FreeSmartphone *fs,
                                     FSGSMDeviceGetFeaturesReplyFunc cb,
                                     gpointer userdata);

gboolean fso_gsm_device_prepare_to_suspend(FreeSmartphone *fs,
                                           FSGSMDeviceReplyFunc cb,
                                           gpointer userdata);

gboolean fso_gsm_device_recover_from_suspend(FreeSmartphone *fs,
                                             FSGSMDeviceReplyFunc cb,
                                             gpointer userdata);

#endif /* FREESMARTPHONE_DEVICE_H */

