/*
 *  freesmartphone-sms.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */


#include "freesmartphone-internal.h"
#include "freesmartphone-sms.h"
#include "freesmartphone-sms-binding.h"
#include "fso-marshallers.h"

#define FSO_MESSAGE_SENT_SIGNAL_NAME "MessageSent"
#define FSO_CALL_INCOMING_MESSAGE_SIGNAL_NAME "IncomingMessage"


/**
* @brief Initializes SMS interface, it is called automatically
* when necessary
*
* @param fs pointer to freesmartphone struct
*/
void fso_gsm_sms_init(FreeSmartphone *fs)
{
        g_debug("%s", __func__);

        /* Message Sent */
        dbus_g_object_register_marshaller (fso_marshaller_VOID__BOOLEAN_STRING,
                                           G_TYPE_NONE,
                                           G_TYPE_BOOLEAN,
                                           G_TYPE_STRING,
                                           G_TYPE_INVALID);
}

static
void fso_gsm_sms_send_message_reply(DBusGProxy *proxy,
                                    GError *error,
                                    gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMSMSReply reply_cb = (FSGSMSMSReply)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error, data);
}



gboolean fso_gsm_sms_send_message(FreeSmartphone *fs,
                                  const gchar *message,
                                  const gchar *number,
                                  const gboolean want_report,
                                  FSGSMSMSReply cb,
                                  gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s : '%s' to %s", __func__, message, number);


        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_SMS);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_SMS);

        org_freesmartphone_GSM_SMS_send_message_async (proxy,
                                                       message,
                                                       number,
                                                       want_report,
                                                       &fso_gsm_sms_send_message_reply,
                                                       (gpointer)replydata);

        return TRUE;
}


static
void fso_gsm_sms_message_sent_handler(DBusGProxy *proxy,
                                      gboolean success,
                                      gchar *reason,
                                      gpointer data)
{
        g_debug("%s", __func__);
        FreeSmartphone *fs = (FreeSmartphone*)data;
        g_return_if_fail(fs);
        GList *list = NULL;

        list = fso_get_signal_handler(fs,FSO_SIGNAL_MESSAGE_SENT);
        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                FSGSMSMSMessageSent status_cb = (FSGSMSMSMessageSent)rd->cb;
                status_cb (success,reason,rd->data);
                list = g_list_next(list);
        }
}

gboolean fso_gsm_sms_message_sent_signal(FreeSmartphone *fs,
                                         FSGSMSMSMessageSent cb,
                                         gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_SMS);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_SMS);

        if ( fso_append_data_to_signal_handler(fs,
                                               FSO_SIGNAL_MESSAGE_SENT,
                                               replydata) ) {
                dbus_g_proxy_add_signal(proxy,
                                        FSO_MESSAGE_SENT_SIGNAL_NAME,
                                        G_TYPE_BOOLEAN,
                                        G_TYPE_STRING,
                                        G_TYPE_INVALID);
                dbus_g_proxy_connect_signal (proxy, FSO_MESSAGE_SENT_SIGNAL_NAME,
                                             G_CALLBACK(fso_gsm_sms_message_sent_handler),
                                             fs, NULL);
        }


        return TRUE;
}

gboolean fso_gsm_sms_message_sent_signal_remove(FreeSmartphone *fs,
                                                FSGSMSMSMessageSent cb,
                                                gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        return fso_signal_remove(fs,
                                 FSO_SMS,
                                 FSO_MESSAGE_SENT_SIGNAL_NAME,
                                 FSO_SIGNAL_MESSAGE_SENT,
                                 G_CALLBACK(fso_gsm_sms_message_sent_handler),
                                 (gpointer)fs,
                                 G_CALLBACK(cb),
                                 userdata);
}



static
void fso_gsm_sms_incoming_message_handler(DBusGProxy *proxy,
                                          gint index,
                                          gpointer data)
{
        g_debug("%s", __func__);
        FreeSmartphone *fs = (FreeSmartphone*)data;
        g_return_if_fail(fs);
        GList *list = NULL;

        list = fso_get_signal_handler(fs,FSO_SIGNAL_CALL_INCOMING_MESSAGE);
        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                FSGSMSMSIncomingMessage status_cb = (FSGSMSMSIncomingMessage)rd->cb;
                status_cb (index,rd->data);
                list = g_list_next(list);
        }
}

gboolean fso_gsm_sms_incoming_message_signal(FreeSmartphone *fs,
                                             FSGSMSMSIncomingMessage cb,
                                             gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_SMS);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_SMS);

        if ( fso_append_data_to_signal_handler(fs,
                                               FSO_SIGNAL_CALL_INCOMING_MESSAGE,
                                               replydata) ) {
                dbus_g_proxy_add_signal(proxy,
                                        FSO_CALL_INCOMING_MESSAGE_SIGNAL_NAME,
                                        G_TYPE_INT,
                                        G_TYPE_INVALID);
                dbus_g_proxy_connect_signal (proxy, FSO_CALL_INCOMING_MESSAGE_SIGNAL_NAME,
                                             G_CALLBACK(fso_gsm_sms_incoming_message_handler),
                                             fs, NULL);
        }

        return TRUE;
}

gboolean fso_gsm_sms_incoming_message_signal_remove(FreeSmartphone *fs,
                                                    FSGSMSMSIncomingMessage cb,
                                                    gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        return fso_signal_remove(fs,
                                 FSO_SMS,
                                 FSO_CALL_INCOMING_MESSAGE_SIGNAL_NAME,
                                 FSO_SIGNAL_CALL_INCOMING_MESSAGE,
                                 G_CALLBACK(fso_gsm_sms_incoming_message_handler),
                                 (gpointer)fs,
                                 G_CALLBACK(cb),
                                 userdata);
}
