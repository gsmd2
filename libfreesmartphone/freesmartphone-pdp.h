/*
 *  freesmartphone-pdp.h
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#ifndef FREESMARTPHONE_PDP_H
#define FREESMARTPHONE_PDP_H

#include <glib.h>
#include <dbus/dbus-glib.h>

#include "freesmartphone.h"

/*********************************Methods**************************************/
typedef void (*FSGSMPDPReplyFunc) (GError *error,
                                   gpointer userdata );


typedef void (*FSGSMPDPListGprsClassesReplyFunc) (gchar **classes,
                                                  GError *error,
                                                  gpointer userdata );

typedef void (*FSGSMPDPAddContextReplyFunc) (gint index,
                                             GError *error,
                                             gpointer userdata );

typedef void (*FSGSMPDPListContextsReplyFunc) (gchar **contexts,
                                               GError *error,
                                               gpointer userdata );

gboolean fso_gsm_pdp_list_contexts(FreeSmartphone *fs,
                                   FSGSMPDPListContextsReplyFunc cb,
                                   gpointer userdata);

gboolean fso_gsm_pdp_delete_context(FreeSmartphone *fs,
                                    gint index,
                                    FSGSMPDPReplyFunc cb,
                                    gpointer userdata);

gboolean fso_gsm_pdp_add_context(FreeSmartphone *fs,
                                 gchar *context,
                                 FSGSMPDPAddContextReplyFunc cb,
                                 gpointer userdata);

gboolean fso_gsm_pdp_select_context(FreeSmartphone *fs,
                                    gchar *context,
                                    FSGSMPDPReplyFunc cb,
                                    gpointer userdata);

gboolean fso_gsm_pdp_deactivate(FreeSmartphone *fs,
                                gint index,
                                FSGSMPDPReplyFunc cb,
                                gpointer userdata);

gboolean fso_gsm_pdp_activate(FreeSmartphone *fs,
                              gint index,
                              FSGSMPDPReplyFunc cb,
                              gpointer userdata);

gboolean fso_gsm_pdp_select_gprs_class(FreeSmartphone *fs,
                                       gchar *klass,
                                       FSGSMPDPReplyFunc cb,
                                       gpointer userdata);

gboolean fso_gsm_pdp_list_gprs_classes(FreeSmartphone *fs,
                                       FSGSMPDPListGprsClassesReplyFunc cb,
                                       gpointer userdata);

/*********************************Signals**************************************/

typedef void (*FSGSMPDPContextSignalFunc) (gint id,
                                           gpointer userdata);
gboolean fso_gsm_pdp_context_changed_signal_remove(FreeSmartphone *fs,
                                                   FSGSMPDPContextSignalFunc cb,
                                                   gpointer userdata);

gboolean fso_gsm_pdp_context_changed_signal(FreeSmartphone *fs,
                                            FSGSMPDPContextSignalFunc cb,
                                            gpointer userdata);

gboolean fso_gsm_pdp_context_deactivated_signal_remove(FreeSmartphone *fs,
                                                       FSGSMPDPContextSignalFunc cb,
                                                       gpointer userdata);

gboolean fso_gsm_pdp_context_deactivated_signal(FreeSmartphone *fs,
                                                FSGSMPDPContextSignalFunc cb,
                                                gpointer userdata);

gboolean fso_gsm_pdp_context_activated_signal_remove(FreeSmartphone *fs,
                                                     FSGSMPDPContextSignalFunc cb,
                                                     gpointer userdata);

gboolean fso_gsm_pdp_context_activated_signal(FreeSmartphone *fs,
                                              FSGSMPDPContextSignalFunc cb,
                                              gpointer userdata);


#endif /* FREESMARTPHONE_PDP_H */

