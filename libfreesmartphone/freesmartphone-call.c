/*
 *  freesmartphone-call.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#include <string.h>

#include "freesmartphone-internal.h"
#include "freesmartphone-call.h"
#include "freesmartphone-call-binding.h"
#include "fso-marshallers.h"



#define FSO_STATUS_SIGNAL_NAME        "CallStatus"
#define GSMD_CALL_STATUS_PROPERTIES_TYPE (dbus_g_type_get_map ("GHashTable", \
      G_TYPE_STRING, G_TYPE_VALUE))

/**
* @brief Initializes Call interface, it is called automatically
* when necessary
*
* @param fs pointer to freesmartphone struct
*/
void fso_gsm_call_init(FreeSmartphone *fs)
{
        g_debug("%s", __func__);

        /* Released call */
        dbus_g_object_register_marshaller (fso_marshaller_VOID__BOOLEAN_STRING_INT,
                                           G_TYPE_NONE,
                                           G_TYPE_INT,
                                           G_TYPE_STRING,
                                           GSMD_CALL_STATUS_PROPERTIES_TYPE,
                                           G_TYPE_INVALID);
}


static
void fso_gsm_call_initiate_reply(DBusGProxy *proxy,
                                 gint id,
                                 GError *error,
                                 gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMCallInitiateReplyFunc reply_cb = (FSGSMCallInitiateReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(id, error, data);
}

gboolean fso_gsm_call_initiate(FreeSmartphone *fs,
                               const gchar *number,
                               const gchar *type,
                               FSGSMCallInitiateReplyFunc cb,
                               gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_CALL);
        if ( !replydata )
                return FALSE;
        DBusGProxy *proxy = fso_get_proxy(fs,FSO_CALL);

        org_freesmartphone_GSM_Call_initiate_async(proxy,
                                                   number,
                                                   type,
                                                   &fso_gsm_call_initiate_reply,
                                                   (gpointer)replydata);
        return TRUE;
}




static
void fso_gsm_call_activate_reply (DBusGProxy *proxy,
                                  GError *error,
                                  gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMCallActivateReplyFunc reply_cb = (FSGSMCallActivateReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error, data);
}

gboolean fso_gsm_call_activate(FreeSmartphone *fs,
                               gint id,
                               FSGSMCallActivateReplyFunc cb,
                               gpointer userdata)
{
        g_debug("%s", __func__);
        g_return_val_if_fail(fs, FALSE);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_CALL);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_CALL);


        org_freesmartphone_GSM_Call_activate_async(proxy,
                                                   id,
                                                   &fso_gsm_call_activate_reply,
                                                   (gpointer)replydata);
        return TRUE;
}

static
void fso_gsm_call_activate_conference_reply (DBusGProxy *proxy,
                                             GError *error,
                                             gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMCallActivateConferenceReplyFunc reply_cb = (FSGSMCallActivateConferenceReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error, data);
}

gboolean fso_gsm_call_activate_conference( FreeSmartphone *fs,
                                           gint id,
                                           FSGSMCallActivateConferenceReplyFunc cb,
                                           gpointer userdata)
{
        g_debug("%s", __func__);
        g_return_val_if_fail(fs, FALSE);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_CALL);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_CALL);


        org_freesmartphone_GSM_Call_activate_conference_async(proxy,
                                                              id,
                                                              &fso_gsm_call_activate_conference_reply,
                                                              (gpointer)replydata);
        return TRUE;
}
static
void fso_gsm_call_release_reply (DBusGProxy *proxy,
                                 GError *error,
                                 gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMCallReleaseReplyFunc reply_cb = (FSGSMCallReleaseReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error, data);
}

gboolean fso_gsm_call_release( FreeSmartphone *fs,
                               const gchar *message,
                               gint id,
                               FSGSMCallReleaseReplyFunc cb,
                               gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_CALL);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_CALL);


        org_freesmartphone_GSM_Call_release_async( proxy,
                                                   message,
                                                   id,
                                                   &fso_gsm_call_release_reply,
                                                   (gpointer)replydata );
        return TRUE;
}
static
void fso_gsm_call_release_held_reply (DBusGProxy *proxy,
                                      GError *error,
                                      gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMCallReleaseHeldReplyFunc reply_cb = (FSGSMCallReleaseHeldReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error, data);
}
gboolean fso_gsm_call_release_held( FreeSmartphone *fs,
                                    const gchar *message,
                                    FSGSMCallReleaseHeldReplyFunc cb,
                                    gpointer userdata )
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_CALL);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_CALL);


        org_freesmartphone_GSM_Call_release_held_async( proxy,
                                                        message,
                                                        &fso_gsm_call_release_held_reply,
                                                        (gpointer)replydata );
        return TRUE;
}

static
void fso_gsm_call_release_all_reply (DBusGProxy *proxy,
                                      GError *error,
                                      gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMCallReleaseAllReplyFunc reply_cb = (FSGSMCallReleaseAllReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error, data);
}
gboolean fso_gsm_call_release_all( FreeSmartphone *fs,
                                    const gchar *message,
                                    FSGSMCallReleaseAllReplyFunc cb,
                                    gpointer userdata )
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_CALL);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_CALL);


        org_freesmartphone_GSM_Call_release_all_async( proxy,
                                                        message,
                                                        &fso_gsm_call_release_all_reply,
                                                        (gpointer)replydata );
        return TRUE;
}



static
void fso_gsm_call_list_calls_reply (DBusGProxy *proxy,
                                    GPtrArray *calls,
                                    GError *error,
                                    gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMCallListReplyFunc reply_cb = (FSGSMCallListReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(calls,error, data);
}

gboolean fso_gsm_call_list_calls( FreeSmartphone *fs,
                                  FSGSMCallListReplyFunc cb,
                                  gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_CALL);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_CALL);


        org_freesmartphone_GSM_Call_list_calls_async( proxy,
                                                      &fso_gsm_call_list_calls_reply,
                                                      (gpointer)replydata );
        return TRUE;
}




/* Signals */





static
void fso_gsm_call_status_handler(DBusGProxy *proxy,
                                 gint id,
                                 gchar *status,
                                 GHashTable *properties,
                                 gpointer data)
{
        g_debug("%s", __func__);
        FreeSmartphone *fs = (FreeSmartphone*)data;
        g_return_if_fail(fs);
        GList *list = fso_get_signal_handler(fs,FSO_SIGNAL_CALL_STATUS);

        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                FSGSMCallStatusSignalFunc status_cb = (FSGSMCallStatusSignalFunc)rd->cb;
                status_cb (id,status,properties,rd->data);
                list = g_list_next(list);
        }
}

gboolean fso_gsm_call_status_signal(FreeSmartphone *fs,
                                    FSGSMCallStatusSignalFunc cb,
                                    gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_CALL);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_CALL);

        if ( fso_append_data_to_signal_handler(fs,
                                               FSO_SIGNAL_CALL_STATUS,
                                               replydata) ) {
                dbus_g_proxy_add_signal(proxy,
                                        FSO_STATUS_SIGNAL_NAME,
                                        G_TYPE_INT,
                                        G_TYPE_STRING,
                                        GSMD_CALL_STATUS_PROPERTIES_TYPE,
                                        G_TYPE_INVALID);
                dbus_g_proxy_connect_signal (proxy, FSO_STATUS_SIGNAL_NAME,
                                             G_CALLBACK(fso_gsm_call_status_handler),
                                             fs, NULL);
        }

        return TRUE;
}

gboolean fso_gsm_call_status_signal_remove(FreeSmartphone *fs,
                                           FSGSMCallStatusSignalFunc cb,
                                           gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);


        return fso_signal_remove(fs,
                                 FSO_CALL,
                                 FSO_STATUS_SIGNAL_NAME,
                                 FSO_SIGNAL_CALL_STATUS,
                                 G_CALLBACK(fso_gsm_call_status_handler),
                                 (gpointer)fs,
                                 G_CALLBACK(cb),
                                 userdata);
}
