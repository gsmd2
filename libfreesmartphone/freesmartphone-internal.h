/*
 *  freesmartphone-internal.h
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#include <dbus/dbus-glib.h>

#ifndef FREESMARTPHONE_INTERNAL_H
#define FREESMARTPHONE_INTERNAL_H

#include "freesmartphone.h"

#define FSO_GSM_SERVICE_NAME "org.freesmartphone.GSM"

typedef enum {
        FSO_CALL=0,
        FSO_DEVICE,
        FSO_NETWORK,
        FSO_PDP,
        FSO_SIM,
        FSO_SMS
}FSOInterface;

typedef enum {
        FSO_SIGNAL_CALL_STATUS=0,
        FSO_SIGNAL_MESSAGE_SENT,
        FSO_SIGNAL_CALL_INCOMING_MESSAGE,
        FSO_SIGNAL_NETWORK_STATUS,
        FSO_SIGNAL_SUBSCRIBER_NUMBERS,
        FSO_SIGNAL_CONTEXT_ACTIVATED,
        FSO_SIGNAL_CONTEXT_DEACTIVATED,
        FSO_SIGNAL_CONTEXT_CHANGED,
        FSO_SIGNAL_AUTH_STATUS,
        FSO_SIGNAL_LAST,
}FSOSignal;


typedef void (*InterfaceInitFunction)(FreeSmartphone *fs);

typedef struct _InterfaceDetails InterfaceDetails;

struct _InterfaceDetails {
    FSOInterface key;
    const char *path;
    const char *interface;
    InterfaceInitFunction init;
};

struct _FreeSmartphone {
        DBusGConnection *conn;
        GHashTable *proxies;
        GList *signal_handlers[FSO_SIGNAL_LAST];

};


typedef struct _ReplyData ReplyData;

struct _ReplyData {

        GCallback cb;

        gpointer data;

};


DBusGProxy *fso_get_proxy(FreeSmartphone *fs,FSOInterface interface);
ReplyData *fso_prepare_method(FreeSmartphone *fs,
                              GCallback cb,
                              gpointer userdata,
                              FSOInterface interface);

ReplyData *fso_reply_data_new(GCallback cb,
                              gpointer data);

gboolean fso_append_data_to_signal_handler(FreeSmartphone *fs,
                                           FSOSignal signal,
                                           ReplyData *replydata);
void fso_reply_data_free(ReplyData *rd);

GList *fso_get_signal_handler(FreeSmartphone *fs, FSOSignal signal);


gboolean fso_signal_remove(FreeSmartphone *fs,
                           FSOInterface interface,
                           const gchar *signal_name,
                           FSOSignal signal,
                           GCallback signal_handler,
                           gpointer signal_handler_data,
                           GCallback usercb,
                           gpointer userdata);


void fso_gsm_call_init(FreeSmartphone *fs);
void fso_gsm_device_init(FreeSmartphone *fs);
void fso_gsm_network_init(FreeSmartphone *fs);
void fso_gsm_pdp_init(FreeSmartphone *fs);
void fso_gsm_sim_init(FreeSmartphone *fs);
void fso_gsm_sms_init(FreeSmartphone *fs);

#endif /* FREESMARTPHONE_INTERNAL_H */
