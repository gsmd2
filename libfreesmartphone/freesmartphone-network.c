/*
 *  freesmartphone-network.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#include "freesmartphone-internal.h"
#include "freesmartphone-network.h"
#include "freesmartphone-network-binding.h"
#include "fso-marshallers.h"

#define FSO_NETWORK_STATUS_SIGNAL_NAME "Status"
#define FSO_SUBSCRIBER_NUMBERS_SIGNAL_NAME "SubscriberNumbers"


/**
* @brief Initializes network interface, it is called automatically
* when necessary
*
* @param fs pointer to freesmartphone struct
*/
void fso_gsm_network_init(FreeSmartphone *fs)
{
        g_debug("%s", __func__);

        /* Status */
        dbus_g_object_register_marshaller (fso_marshaller_VOID__STRING_STRING_INT,
                                           G_TYPE_NONE,
                                           G_TYPE_STRING,
                                           G_TYPE_STRING,
                                           G_TYPE_INT,
                                           G_TYPE_INVALID);
}

/**
* @brief Reply function to fso_gsm_network_register function call
*
* @param proxy dbus proxy used to call fso_gsm_network_register function
* @param error pointer to error struct
* @param userdata userdata passed when fso_gsm_network_register function was
* called
*/
static
void fso_gsm_network_register_reply(DBusGProxy *proxy,
                                    GError *error,
                                    gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMNetworkReplyFunc reply_cb = (FSGSMNetworkReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error,data);
}


/**
* @brief Registers to network
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when registration command has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_network_register(FreeSmartphone *fs,
                                  FSGSMNetworkReplyFunc cb,
                                  gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_NETWORK);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_NETWORK);

        org_freesmartphone_GSM_Network_register_async(proxy,
                                                      &fso_gsm_network_register_reply,
                                                      (gpointer)replydata);
        return TRUE;
}

/**
* @brief Reply function to get status
*
* @param proxy dbus proxy used to call fso_gsm_network_get_status function
* @param error pointer to error struct
* @param userdata userdata passed when fso_gsm_network_get_status function was
* called
*/
static
void fso_gsm_network_get_status_reply(DBusGProxy *proxy,
                                      GHashTable *status,
                                      GError *error,
                                      gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMNetworkGetStatusReplyFunc reply_cb = (FSGSMNetworkGetStatusReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(status,error,data);
}


/**
* @brief Gets network status
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when get status command has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_network_get_status(FreeSmartphone *fs,
                                    FSGSMNetworkGetStatusReplyFunc cb,
                                    gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_NETWORK);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_NETWORK);

        org_freesmartphone_GSM_Network_get_status_async(proxy,
                                                        &fso_gsm_network_get_status_reply,
                                                        (gpointer)replydata);
        return TRUE;
}



/**
* @brief Reply function to get status
*
* @param proxy dbus proxy used to call fso_gsm_network_list_providers function
* @param error pointer to error struct
* @param userdata userdata passed when fso_gsm_network_list_providers function
* was called
*/
static
void fso_gsm_network_list_providers_reply(DBusGProxy *proxy,
                                          GPtrArray *providers,
                                          GError *error,
                                          gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMNetworkListProvidersReplyFunc reply_cb = (FSGSMNetworkListProvidersReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(providers,error,data);
}


/**
* @brief Lists available network providers
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when list providers command has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_network_list_providers(FreeSmartphone *fs,
                                        FSGSMNetworkListProvidersReplyFunc cb,
                                        gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_NETWORK);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_NETWORK);

        org_freesmartphone_GSM_Network_list_providers_async(proxy,
                                                            &fso_gsm_network_list_providers_reply,
                                                            (gpointer)replydata);
        return TRUE;
}

/**
* @brief Reply function to get status
*
* @param proxy dbus proxy used to call fso_gsm_network_register_with_provider
* function
* @param error pointer to error struct
* @param userdata userdata passed when fso_gsm_network_register_with_provider
* function was called
*/
static
void fso_gsm_network_register_with_provider_reply(DBusGProxy *proxy,
                                                  GError *error,
                                                  gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMNetworkReplyFunc reply_cb = (FSGSMNetworkReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(error,data);
}


/**
* @brief Lists available network providers
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when list providers command has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_network_register_with_provider(FreeSmartphone *fs,
                                                gint operator_code,
                                                FSGSMNetworkReplyFunc cb,
                                                gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_NETWORK);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_NETWORK);

        org_freesmartphone_GSM_Network_register_with_provider_async(proxy,
                                                                    operator_code,
                                                                    &fso_gsm_network_register_with_provider_reply,
                                                                    (gpointer)replydata);
        return TRUE;
}

///**
//* @brief Reply to get subscriber numbers function
//*
//* @param proxy dbus proxy used to call fso_gsm_network_get_subscriber_numbers
//* function
//* @param numbers subscriber numbers
//* @param error pointer to error struct
//* @param userdata userdata passed when fso_gsm_network_get_subscriber_numbers
//* function was called
//*/
//static
//void fso_gsm_network_get_subscriber_numbers_reply(DBusGProxy *proxy,
//                                                  gchar **numbers,
//                                                  GError *error,
//                                                  gpointer userdata)
//{
//        g_debug("%s", __func__);
//        ReplyData *reply = (ReplyData*)userdata;
//        FSGSMNetworkGetSubscriberNumbersReplyFunc reply_cb = (FSGSMNetworkGetSubscriberNumbersReplyFunc)reply->cb;
//        gpointer data = reply->data;
//        fso_reply_data_free(reply);
//        reply_cb(numbers,error,data);
//}


///**
//* @brief Get subscriber numbers
//*
//* @param fs pointer to freesmartphone struct
//* @param cb callback function to call when the command has finished
//* @param userdata user data
//* @return true if function was succesfully executed
//*/
//gboolean fso_gsm_network_get_subscriber_numbers(FreeSmartphone *fs,
//                                                FSGSMNetworkGetSubscriberNumbersReplyFunc cb,
//                                                gpointer userdata)
//{
//        g_return_val_if_fail(fs, FALSE);
//        g_debug("%s", __func__);
//        ReplyData *replydata = fso_prepare_method(fs,
//                                                  G_CALLBACK(cb),
//                                                  userdata,
//                                                  FSO_NETWORK);
//        if ( !replydata )
//                return FALSE;
//
//        DBusGProxy *proxy = fso_get_proxy(fs,FSO_NETWORK);
//
//        org_freesmartphone_GSM_Network_get_subscriber_numbers_async(proxy,
//                                                                    &fso_gsm_network_get_subscriber_numbers_reply,
//                                                                    (gpointer)replydata);
//        return TRUE;
//}

/**
* @brief Reply function to get country code
*
* @param proxy dbus proxy used to call fso_gsm_network_get_country_code function
* @param error pointer to error struct
* @param userdata userdata passed when fso_gsm_network_get_country_code function
* was called
*/
static
void fso_gsm_network_get_country_code_reply(DBusGProxy *proxy,
                                            gchar *dial_code,
                                            GError *error,
                                            gpointer userdata)
{
        g_debug("%s", __func__);
        ReplyData *reply = (ReplyData*)userdata;
        FSGSMNetworkGetCountryCodeReplyFunc reply_cb = (FSGSMNetworkGetCountryCodeReplyFunc)reply->cb;
        gpointer data = reply->data;
        fso_reply_data_free(reply);
        reply_cb(dial_code,error,data);
}


/**
* @brief Gets country code
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_network_get_country_code(FreeSmartphone *fs,
                                          FSGSMNetworkGetCountryCodeReplyFunc cb,
                                          gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);
        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_NETWORK);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_NETWORK);

        org_freesmartphone_GSM_Network_get_country_code_async(proxy,
                                                              &fso_gsm_network_get_country_code_reply,
                                                              (gpointer)replydata);
        return TRUE;
}



/**
* @brief Handler to network status signal
* Calls all registered callbacks.
*
* @param proxy proxy to the connection that signal was sent through
* @param provider_name provider's name
* @param status network status
* @param strength signal strength
* @param data user data
*/
static
void fso_gsm_network_status_handler(DBusGProxy *proxy,
                                    GHashTable *status,
                                    gpointer data)
{
        g_debug("%s", __func__);
        FreeSmartphone *fs = (FreeSmartphone*)data;
        g_return_if_fail(fs);
        GList *list = fso_get_signal_handler(fs,FSO_SIGNAL_NETWORK_STATUS);

        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                FSGSMNetworkStatusSignalFunc status_cb = (FSGSMNetworkStatusSignalFunc)rd->cb;
                status_cb (status, rd->data);
                list = g_list_next(list);
        }
}


/**
* @brief Adds a callback to network status signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when signal is received
* @param userdata user data passed along with callback
* @return true if callback was succesfully added
*/
gboolean fso_gsm_network_status_signal(FreeSmartphone *fs,
                                       FSGSMNetworkStatusSignalFunc cb,
                                       gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_NETWORK);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_NETWORK);

        if ( fso_append_data_to_signal_handler(fs,
                                               FSO_SIGNAL_NETWORK_STATUS,
                                               replydata) ) {
                dbus_g_proxy_add_signal(proxy,
                                        FSO_NETWORK_STATUS_SIGNAL_NAME,
                                        dbus_g_type_get_map ("GHashTable", \
                                                             G_TYPE_STRING, G_TYPE_VALUE),
                                        G_TYPE_INVALID);
                dbus_g_proxy_connect_signal (proxy, FSO_NETWORK_STATUS_SIGNAL_NAME,
                                             G_CALLBACK(fso_gsm_network_status_handler),
                                             fs, NULL);
        }

        return TRUE;
}

/**
* @brief Removes callback from network status signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to remove
* @param userdata userdata assosicated with callback
* @return true if callback was removed
*/
gboolean fso_gsm_network_status_signal_remove(FreeSmartphone *fs,
                                           FSGSMNetworkStatusSignalFunc cb,
                                           gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);


        return fso_signal_remove(fs,
                                 FSO_NETWORK,
                                 FSO_NETWORK_STATUS_SIGNAL_NAME,
                                 FSO_SIGNAL_NETWORK_STATUS,
                                 G_CALLBACK(fso_gsm_network_status_handler),
                                 (gpointer)fs,
                                 G_CALLBACK(cb),
                                 userdata);
}

/**
* @brief Handler to network status signal
* Calls all registered callbacks.
*
* @param proxy proxy to the connection that signal was sent through
* @param provider_name provider's name
* @param status network status
* @param strength signal strength
* @param data user data
*/
static
void fso_gsm_network_subscriber_numbers_signal_handler(DBusGProxy *proxy,
                                    gchar **numbers,
                                    gpointer data)
{
        g_debug("%s", __func__);
        FreeSmartphone *fs = (FreeSmartphone*)data;
        g_return_if_fail(fs);
        GList *list = fso_get_signal_handler(fs,FSO_SIGNAL_SUBSCRIBER_NUMBERS);

        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                FSGSMNetworkSubscriberNumbersSignalFunc status_cb = (FSGSMNetworkSubscriberNumbersSignalFunc)rd->cb;
                status_cb (numbers,rd->data);
                list = g_list_next(list);
        }
}


/**
* @brief Adds a callback to subscriber numbers signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when signal is received
* @param userdata user data passed along with callback
* @return true if callback was succesfully added
*/
gboolean fso_gsm_network_subscriber_numbers_signal(FreeSmartphone *fs,
                                       FSGSMNetworkSubscriberNumbersSignalFunc cb,
                                       gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);

        ReplyData *replydata = fso_prepare_method(fs,
                                                  G_CALLBACK(cb),
                                                  userdata,
                                                  FSO_NETWORK);
        if ( !replydata )
                return FALSE;

        DBusGProxy *proxy = fso_get_proxy(fs,FSO_NETWORK);

        if ( fso_append_data_to_signal_handler(fs,
                                               FSO_SIGNAL_SUBSCRIBER_NUMBERS,
                                               replydata) ) {
                dbus_g_proxy_add_signal(proxy,
                                        FSO_SUBSCRIBER_NUMBERS_SIGNAL_NAME,
                                        G_TYPE_STRING,
                                        G_TYPE_STRING,
                                        G_TYPE_INT,
                                        G_TYPE_INVALID);
                dbus_g_proxy_connect_signal (proxy, FSO_SUBSCRIBER_NUMBERS_SIGNAL_NAME,
                                             G_CALLBACK(fso_gsm_network_subscriber_numbers_signal_handler),
                                             fs, NULL);
        }

        return TRUE;
}

/**
* @brief Removes callback from subscriber numbers signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to remove
* @param userdata userdata assosicated with callback
* @return true if callback was removed
*/
gboolean fso_gsm_network_subscriber_numbers_signal_remove(FreeSmartphone *fs,
                                           FSGSMNetworkSubscriberNumbersSignalFunc cb,
                                           gpointer userdata)
{
        g_return_val_if_fail(fs, FALSE);
        g_debug("%s", __func__);


        return fso_signal_remove(fs,
                                 FSO_NETWORK,
                                 FSO_SUBSCRIBER_NUMBERS_SIGNAL_NAME,
                                 FSO_SIGNAL_SUBSCRIBER_NUMBERS,
                                 G_CALLBACK(fso_gsm_network_subscriber_numbers_signal_handler),
                                 (gpointer)fs,
                                 G_CALLBACK(cb),
                                 userdata);
}
