/*
 *  freesmartphone-sim.h
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

#ifndef FREESMARTPHONE_SIM_H
#define FREESMARTPHONE_SIM_H

#include <glib.h>
#include <dbus/dbus-glib.h>

#include "freesmartphone.h"

/*********************************Methods**************************************/
typedef void (*FSGSMNetworkReplyFunc) (GError *error,
                                       gpointer userdata );
typedef void (*FSGSMNetworkGetStatusReplyFunc) (GHashTable *status,
                                                GError *error,
                                                gpointer userdata);
typedef void (*FSGSMNetworkGetSubscriberNumbersReplyFunc) (gchar **numbers,
                                                           GError *error,
                                                           gpointer userdata );

typedef void (*FSGSMNetworkListProvidersReplyFunc) (GPtrArray *providers,
                                                    GError *error,
                                                    gpointer userdata );

typedef void (*FSGSMNetworkGetCountryCodeReplyFunc) (gchar *dial_code,
                                                     GError *error,
                                                     gpointer userdata );
typedef void (*FSGSMNetworkGetHomeCountryCodeReplyFunc) (gchar *dial_code,
                                                         GError *error,
                                                         gpointer userdata );

gboolean fso_gsm_network_get_home_country_code(FreeSmartphone *fs,
                                               FSGSMNetworkGetHomeCountryCodeReplyFunc cb,
                                               gpointer userdata);

gboolean fso_gsm_network_get_country_code(FreeSmartphone *fs,
                                          FSGSMNetworkGetCountryCodeReplyFunc cb,
                                          gpointer userdata);

gboolean fso_gsm_network_get_subscriber_numbers(FreeSmartphone *fs,
                                                FSGSMNetworkGetSubscriberNumbersReplyFunc cb,
                                                gpointer userdata);
gboolean fso_gsm_network_register_with_provider(FreeSmartphone *fs,
                                                gint operator_code,
                                                FSGSMNetworkReplyFunc cb,
                                                gpointer userdata);

gboolean fso_gsm_network_list_providers(FreeSmartphone *fs,
                                        FSGSMNetworkListProvidersReplyFunc cb,
                                        gpointer userdata);

gboolean fso_gsm_network_get_status(FreeSmartphone *fs,
                                    FSGSMNetworkGetStatusReplyFunc cb,
                                    gpointer userdata);

gboolean fso_gsm_network_register(FreeSmartphone *fs,
                                  FSGSMNetworkReplyFunc cb,
                                  gpointer userdata);



/*****************************************************Signals******************/
typedef void (*FSGSMNetworkStatusSignalFunc) (GHashTable *status,
                                              gpointer userdata);

typedef void (*FSGSMNetworkSubscriberNumbersSignalFunc) (gchar **numbers,
                                                         gpointer userdata);

gboolean fso_gsm_network_subscriber_numbers_signal_remove(FreeSmartphone *fs,
                                                          FSGSMNetworkSubscriberNumbersSignalFunc cb,
                                                          gpointer userdata);
gboolean fso_gsm_network_subscriber_numbers_signal(FreeSmartphone *fs,
                                                   FSGSMNetworkSubscriberNumbersSignalFunc cb,
                                                   gpointer userdata);
gboolean fso_gsm_network_status_signal_remove(FreeSmartphone *fs,
                                              FSGSMNetworkStatusSignalFunc cb,
                                              gpointer userdata);
gboolean fso_gsm_network_status_signal(FreeSmartphone *fs,
                                       FSGSMNetworkStatusSignalFunc cb,
                                       gpointer userdata);


#endif /* FREESMARTPHONE_SIM_H */

