/*
 *  freesmartphone.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */

/**
 * @mainpage Libfreesmartphone
 *
 * \section intro Introduction
 *
 * This is the documentation of libfreesmartphone, a library to access
 * GSMD2's dbus methods conveniently.
 *
 *
 */

#include <dbus/dbus-glib.h>
#include <glib.h>
#include "freesmartphone.h"
#include "freesmartphone-internal.h"
#include "freesmartphone-call.h"
#include "freesmartphone-sms.h"

static InterfaceDetails
details[]
= {
        {FSO_CALL,
                "/org/freesmartphone/GSM/Call",
                "org.freesmartphone.GSM.Call",
                fso_gsm_call_init
        },

        {FSO_DEVICE,
         "/org/freesmartphone/GSM/Device",
         "org.freesmartphone.GSM.Device",
         fso_gsm_device_init},

        {FSO_NETWORK,
         "/org/freesmartphone/GSM/Network",
         "org.freesmartphone.GSM.Network",
         fso_gsm_network_init},

        {FSO_PDP,
         "/org/freesmartphone/GSM/PDP",
         "org.freesmartphone.GSM.PDP",
         fso_gsm_pdp_init},

        {FSO_SIM,
         "/org/freesmartphone/GSM/SIM",
         "org.freesmartphone.GSM.SIM",
         fso_gsm_sim_init},

        {FSO_SMS,
         "/org/freesmartphone/GSM/SMS",
         "org.freesmartphone.GSM.SMS",
         fso_gsm_sms_init}
};


/**
 * Initialize libfreesmartphone library
 *
 *
 * @return Handle for the library or NULL if initialization fails
 */
FreeSmartphone* fso_init()
{
        g_debug("%s", __func__);
        g_type_init();

        FreeSmartphone *fs = g_try_new0(FreeSmartphone, 1);
        if ( !fs ) {
                g_warning("%s : Out of memory", __func__);
                return NULL;
        }
        GError *error = NULL;
        fs->conn = dbus_g_bus_get(DBUS_BUS_SYSTEM, &error);
        if ( !fs->conn || error) {
                g_warning("%s : Failed connect to system bus: %s",
                          __func__, error->message);
                g_error_free( error );
                error = NULL;
                fso_free(fs);
                return NULL;
        }

        fs->proxies = g_hash_table_new_full(g_int_hash,
                                            g_int_equal,
                                            NULL,
                                            g_object_unref);


        dbus_g_connection_ref (fs->conn);

        return fs;
}
/**
 *
 *
 * @param fs
 */
void fso_reply_data_free(ReplyData *rd)
{
        g_free( rd );
}

void fso_free_signal_handler(FreeSmartphone *fs,FSOSignal signal)
{
    GList *list = fso_get_signal_handler(fs,signal);

    while ( list ) {
                fso_reply_data_free((ReplyData*)list->data);
                list = g_list_next(list);
    }

    list = fso_get_signal_handler(fs,signal);

    if (list)
        g_list_free(list);
}

void fso_free(FreeSmartphone *fs)
{
        if ( !fs ) {
                return;
        }
        if ( fs->conn ) {
                dbus_g_connection_unref (fs->conn);
        }

        g_hash_table_destroy(fs->proxies);
        fs->conn = NULL;

        FSOSignal signal = FSO_SIGNAL_CALL_STATUS;

        for(;signal<FSO_SIGNAL_LAST;signal++) {
            fso_free_signal_handler(fs,signal);
        }

        g_free(fs);
}




ReplyData *fso_reply_data_new(GCallback cb,
                              gpointer data)
{
        ReplyData *rd = g_try_new0(ReplyData, 1);
        if ( rd ) {
                rd->cb = cb;
                rd->data = data;
        }
        return rd;
}



gboolean fso_signal_remove(FreeSmartphone *fs,
                           FSOInterface interface,
                           const gchar *signal_name,
                           FSOSignal signal,
                           GCallback signal_handler,
                           gpointer signal_handler_data,
                           GCallback usercb,
                           gpointer userdata)
{
        GList *list = fso_get_signal_handler(fs,signal);
        DBusGProxy *proxy = fso_get_proxy(fs,interface);

        ReplyData *match = NULL;
        while ( list ) {
                ReplyData *rd = (ReplyData*)list->data;
                if ( rd->cb == G_CALLBACK(usercb) && rd->data == userdata ) {
                        match= rd;
                        break;
                }
                list = g_list_next(list);
        }
        if ( match ) {
                fs->signal_handlers[signal] = g_list_remove_all(fs->signal_handlers[signal], match);
                if ( fs->signal_handlers[signal] == NULL) {
                        g_debug("%s : '%s'", __func__, signal_name);
                        dbus_g_proxy_disconnect_signal(proxy, signal_name,
                                                       G_CALLBACK(signal_handler),
                                                       signal_handler_data);
                }
                fso_reply_data_free( match );
        }
        return TRUE;
}

/**
* @brief Returns and ensures that specific interface's proxy exists,
* creates it if necessary
*
* @param fs pointer to freesmartphone strucht
* @param interface interface whose proxy to ensure
* @return true if proxy exists, false if it couldn't be created
*/
DBusGProxy *fso_get_proxy(FreeSmartphone *fs,FSOInterface interface)
{
        g_assert(fs);
        g_assert(fs->proxies);

        //Try to find the proxy
        DBusGProxy* proxy = g_hash_table_lookup(fs->proxies,
                                                &details[interface].key);

        if (!proxy) {
                //If no proxy found, create
                proxy = dbus_g_proxy_new_for_name(fs->conn,
                                                  FSO_GSM_SERVICE_NAME,
                                                  details[interface].path,
                                                  details[interface].interface);

                if (!proxy)
                        return NULL;

                //and add it
                g_hash_table_insert(fs->proxies,
                                    &details[interface].key,
                                    proxy);

                if (details[interface].init)
                        details[interface].init(fs);
        }

        return proxy;
}

/**
* @brief Appends replydata to signal handler
*
* @param fs pointer to freesmartphone struct
* @param signal signal handler to append replydata to
* @param replydata replydata to append
* @return true if signal handler needs to be initialized
*/
gboolean fso_append_data_to_signal_handler(FreeSmartphone *fs,
                                           FSOSignal signal,
                                           ReplyData *replydata)
{
        g_return_val_if_fail(fs, FALSE);
        gboolean result = FALSE;

        if (!fs->signal_handlers[signal])
                result = TRUE;

        fs->signal_handlers[signal] = g_list_append(fs->signal_handlers[signal],
                                                    replydata);
        return result;
}

GList *fso_get_signal_handler(FreeSmartphone *fs, FSOSignal signal)
{
        g_return_val_if_fail(fs, NULL);
        return fs->signal_handlers[signal];
}




/**
* @brief Prepares replydata for a method call
* Ensures that a proper proxy is present.
*
* @param fs pointer to freesmartphone data
* @param cb Callback function
* @param userdata user data
* @param interface interface to which the method (we are preparing for) belongs
* @return new replydata (that has to be freed) or null if failed
*/
ReplyData *fso_prepare_method(FreeSmartphone *fs,
                              GCallback cb,
                              gpointer userdata,
                              FSOInterface interface)
{

        if ( !cb ) {
                g_warning("%s : No callback function provided!", __func__);
                return NULL;
        }

        ReplyData *replydata = fso_reply_data_new(cb, userdata);
        if ( !replydata ) {
                g_warning("%s : Out of memory", __func__);
                return NULL;
        }


        DBusGProxy *proxy = fso_get_proxy(fs,interface);

        if ( !proxy ) {
                g_free( replydata );
                return NULL;
        }

        return replydata;
}
