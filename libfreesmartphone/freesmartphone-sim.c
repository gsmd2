/*
 *  freesmartphone-sim.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 *   Vesa Pikki
 */


#include "freesmartphone-internal.h"
#include "freesmartphone-sim.h"
#include "freesmartphone-sim-binding.h"
#include "fso-marshallers.h"

#define FSO_SIM_AUTH_STATUS_SIGNAL_NAME "AuthStatus"

/**
* @brief Initializes SIM interface, it is called automatically
* when necessary
*
* @param fs pointer to freesmartphone struct
*/
void fso_gsm_sim_init(FreeSmartphone *fs)
{
    g_debug("%s", __func__);
}

/**
* @brief Reply function to fso_gsm_sim_get_auth_status function call
*
* @param proxy dbus proxy used to call fso_gsm_sim_get_auth_status
* function
* @param status auth status
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_get_auth_status
*/
static
void fso_gsm_sim_get_auth_status_reply(DBusGProxy *proxy,
                                       gchar *status,
                                       GError *error,
                                       gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMGetAuthStatusReplyFunc reply_cb = (FSGSMSIMGetAuthStatusReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(status,error,data);
}


/**
* @brief Get auth status
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_get_auth_status(FreeSmartphone *fs,
                                     FSGSMSIMGetAuthStatusReplyFunc cb,
                                     gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_get_auth_status_async(proxy,
                                                     &fso_gsm_sim_get_auth_status_reply,
                                                     (gpointer)replydata);
    return TRUE;
}


/**
* @brief General reply function
*
* @param proxy dbus proxy used
* @param error pointer to error struct
* @param userdata user data
*/
static
void fso_gsm_sim_reply(DBusGProxy *proxy,
                       GError *error,
                       gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMReplyFunc reply_cb = (FSGSMSIMReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(error,data);
}


/**
* @brief Send auth code
*
* @param fs pointer to freesmartphone struct
* @param pin auth code
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_send_auth_code(FreeSmartphone *fs,
                                    gchar *pin,
                                    FSGSMSIMReplyFunc cb,
                                    gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if (!replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_send_auth_code_async(proxy,
                                                    pin,
                                                    &fso_gsm_sim_reply,
                                                    (gpointer)replydata);
    return TRUE;
}


/**
* @brief Unlocks sim
*
* @param fs pointer to freesmartphone struct
* @param puk puk code
* @param new_pin new pin code
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_unlock(FreeSmartphone *fs,
                            gchar *puk,
                            gchar *new_pin,
                            FSGSMSIMReplyFunc cb,
                            gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_unlock_async(proxy,
                                            puk,
                                            new_pin,
                                            &fso_gsm_sim_reply,
                                            (gpointer)replydata);
    return TRUE;
}


/**
* @brief Changes pin code
*
* @param fs pointer to freesmartphone struct
* @param old_pin old pin code
* @param new_pin new pin code
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_change_auth_code(FreeSmartphone *fs,
                                      gchar *old_pin,
                                      gchar *new_pin,
                                      FSGSMSIMReplyFunc cb,
                                      gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_change_auth_code_async(proxy,
                                                      old_pin,
                                                      new_pin,
                                                      &fso_gsm_sim_reply,
                                                      (gpointer)replydata);
    return TRUE;
}

/**
* @brief Reply function to fso_gsm_sim_retrieve_message_reply function call
*
* @param proxy dbus proxy used
* function
* @param number message sender's/recipient's number
* @param message message's contents
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_get_auth_status
*/
static
void fso_gsm_sim_retrieve_message_reply(DBusGProxy *proxy,
                                        gchar *number,
                                        gchar *message,
                                        GError *error,
                                        gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMRetrieveMessageReplyFunc reply_cb = (FSGSMSIMRetrieveMessageReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(number,message,error,data);
}


/**
* @brief Retrieves message
*
* @param fs pointer to freesmartphone struct
* @param index index of the message to retrieve
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_retrieve_message(FreeSmartphone *fs,
                                      gint index,
                                      FSGSMSIMRetrieveMessageReplyFunc cb,
                                      gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_retrieve_message_async(proxy,
                                                      index,
                                                      &fso_gsm_sim_retrieve_message_reply,
                                                      (gpointer)replydata);
    return TRUE;
}

/**
* @brief Reply function to fso_gsm_sim_delete_message function call
*
* @param proxy dbus proxy used to call fso_gsm_sim_get_auth_status
* function
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_delete_message
*/
static
void fso_gsm_sim_delete_message_reply(DBusGProxy *proxy,
                                      GError *error,
                                      gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMReplyFunc reply_cb = (FSGSMSIMReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(error,data);
}


/**
* @brief Deletes message
*
* @param fs pointer to freesmartphone struct
* @param index index of the message to delete
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_delete_message(FreeSmartphone *fs,
                                    gint index,
                                    FSGSMSIMReplyFunc cb,
                                    gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_delete_message_async(proxy,
                                                    index,
                                                    &fso_gsm_sim_delete_message_reply,
                                                    (gpointer)replydata);
    return TRUE;
}

/**
* @brief Reply function to fso_gsm_sim_store_message function call
*
* @param proxy dbus proxy used to call fso_gsm_sim_get_auth_status
* function
* @param index index of the stored message
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_delete_message
*/
static
void fso_gsm_sim_store_message_reply(DBusGProxy *proxy,
                                     gint index,
                                     GError *error,
                                     gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMStoreMessageReplyFunc reply_cb = (FSGSMSIMStoreMessageReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(index,error,data);
}


/**
* @brief Stores message
*
* @param fs pointer to freesmartphone struct
* @param message contents of the message
* @param number message sender's/recipient's number
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_store_message(FreeSmartphone *fs,
                                   gchar *message,
                                   gchar *number,
                                   FSGSMSIMStoreMessageReplyFunc cb,
                                   gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_store_message_async(proxy,
                                                   message,
                                                   number,
                                                   &fso_gsm_sim_store_message_reply,
                                                   (gpointer)replydata);
    return TRUE;
}

/**
* @brief Reply function to fso_gsm_sim_send_message function call
*
* @param proxy dbus proxy used to call fso_gsm_sim_get_auth_status
* function
* @param number message sender's/recipient's number
* @param indices message indices
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_delete_message
*/
static
void fso_gsm_sim_send_stored_message_reply(DBusGProxy *proxy,
                                           gint transaction_index,
                                           GError *error,
                                           gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMReplyFunc reply_cb = (FSGSMSIMReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(error,data);
}


/**
* @brief Sends a stored message
*
* @param fs pointer to freesmartphone struct
* @param index index of the message to send
* @param want_report
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_send_stored_message(FreeSmartphone *fs,
                                         gint index,
                                         FSGSMSIMStoreMessageReplyFunc cb,
                                         gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_send_stored_message_async(proxy,
                                                         index,
                                                         &fso_gsm_sim_send_stored_message_reply,
                                                         (gpointer)replydata);
    return TRUE;
}
/**
* @brief Reply function to fso_gsm_sim_list_messages function call
*
* @param proxy dbus proxy used to call fso_gsm_sim_get_auth_status
* function
* @param number message sender's/recipient's number
* @param indices message indices
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_get_auth_status
*/
static
void fso_gsm_sim_retrieve_messagebook_reply(DBusGProxy *proxy,
                                     GPtrArray *messagebook,
                                     GError *error,
                                     gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMretrieve_messagebookReplyFunc reply_cb = (FSGSMSIMretrieve_messagebookReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    /*
    gint i = 0;
    for (i=0; i < indices->len; i++)
    {
        g_debug("%s : %d", __func__, (gint)indices->data[i]);
    }
    */
    reply_cb(messagebook,error,data);
}


/**
* @brief Retrieves message
*
* @param fs pointer to freesmartphone struct
* @param status status of messages to list (new,read,sending,sent,all)
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_retrieve_messagebook(FreeSmartphone *fs,
                                          gchar *category,
                                          FSGSMSIMretrieve_messagebookReplyFunc cb,
                                          gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_retrieve_messagebook_async(proxy,
                                                          category,
                                                          &fso_gsm_sim_retrieve_messagebook_reply,
                                                          (gpointer)replydata);
    return TRUE;
}

/**
* @brief Reply function to fso_gsm_sim_get_sim_info
*
* @param proxy dbus proxy used
* function
* @param status auth status
* @param error pointer to error struct
* @param userdata userdata
*/
static
void fso_gsm_sim_get_sim_info_reply(DBusGProxy *proxy,
                                    GHashTable *info,
                                    GError *error,
                                    gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMGetSIMInfoReplyFunc reply_cb = (FSGSMSIMGetSIMInfoReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(info,error,data);
}


/**
* @brief gets IMSI code
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_get_sim_info(FreeSmartphone *fs,
                                  FSGSMSIMGetSIMInfoReplyFunc cb,
                                  gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_get_sim_info_async(proxy,
                                                  &fso_gsm_sim_get_sim_info_reply,
                                                  (gpointer)replydata);
    return TRUE;
}
/**
 * @brief Reply function to fso_gsm_sim_get_phonebook_info
 *
 * @param proxy dbus proxy used
 * function
 * @param status auth status
 * @param error pointer to error struct
 * @param userdata userdata
 */
static
void fso_gsm_sim_get_phonebook_info_reply(DBusGProxy *proxy,
                                          GHashTable *info,
                                          GError *error,
                                          gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMGetPhonebookInfoReplyFunc reply_cb = (FSGSMSIMGetPhonebookInfoReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(info,error,data);
}


/**
* @brief Gets SIM phonebook info
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_get_phonebook_info(FreeSmartphone *fs,
                                        FSGSMSIMGetPhonebookInfoReplyFunc cb,
                                        gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_get_phonebook_info_async(proxy,
                                                        &fso_gsm_sim_get_phonebook_info_reply,
                                                        (gpointer)replydata);
    return TRUE;
}

/**
* @brief Reply function to fso_gsm_sim_delete_entry function call
*
* @param proxy dbus proxy used to call fso_gsm_sim_get_auth_status
* function
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_delete_entry
*/
static
void fso_gsm_sim_delete_entry_reply(DBusGProxy *proxy,
                                    GError *error,
                                    gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMReplyFunc reply_cb = (FSGSMSIMReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(error,data);
}


/**
* @brief Deletes entry
*
* @param fs pointer to freesmartphone struct
* @param index index of the entry to delete
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_delete_entry(FreeSmartphone *fs,
                                  gint index,
                                  FSGSMSIMReplyFunc cb,
                                  gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_delete_entry_async(proxy,
                                                  index,
                                                  &fso_gsm_sim_delete_entry_reply,
                                                  (gpointer)replydata);
    return TRUE;
}
/**
* @brief Reply function to fso_gsm_sim_store_entry function call
*
* @param proxy dbus proxy used
* function
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_delete_entry
*/
static
void fso_gsm_sim_store_entry_reply(DBusGProxy *proxy,
                                   GError *error,
                                   gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMReplyFunc reply_cb = (FSGSMSIMReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(error,data);
}


/**
* @brief Stores an entry
*
* @param fs pointer to freesmartphone struct
* @param index entry index
* @param name entry's name
* @param number entry's number
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_store_entry(FreeSmartphone *fs,
                                 gint index,
                                 gchar *name,
                                 gchar *number,
                                 FSGSMSIMReplyFunc cb,
                                 gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_store_entry_async(proxy,
                                                 index,
                                                 name,
                                                 number,
                                                 &fso_gsm_sim_store_entry_reply,
                                                 (gpointer)replydata);
    return TRUE;
}

/**
* @brief Reply function to fso_gsm_sim_retrieve_entry_reply function call
*
* @param proxy dbus proxy used
* function
* @param name entry's name
* @param number entry's number
* @param error pointer to error struct
* @param userdata userdata passed to fso_gsm_sim_get_auth_status
*/
static
void fso_gsm_sim_retrieve_entry_reply(DBusGProxy *proxy,
                                      gchar *name,
                                      gchar *number,
                                      GError *error,
                                      gpointer userdata)
{
    g_debug("%s", __func__);
    ReplyData *reply = (ReplyData*)userdata;
    FSGSMSIMRetrieveEntryReplyFunc reply_cb = (FSGSMSIMRetrieveEntryReplyFunc)reply->cb;
    gpointer data = reply->data;
    fso_reply_data_free(reply);
    reply_cb(name,number,error,data);
}


/**
* @brief Retrieves entry
*
* @param fs pointer to freesmartphone struct
* @param index index of the entry to retrieve
* @param cb callback function to call when the command
* has finished
* @param userdata user data
* @return true if function was succesfully executed
*/
gboolean fso_gsm_sim_retrieve_entry(FreeSmartphone *fs,
                                    gint index,
                                    FSGSMSIMRetrieveEntryReplyFunc cb,
                                    gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);
    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    org_freesmartphone_GSM_SIM_retrieve_entry_async(proxy,
                                                    index,
                                                    &fso_gsm_sim_retrieve_entry_reply,
                                                    (gpointer)replydata);
    return TRUE;
}
/**
* @brief Handler to auth status signal
* Calls all registered callbacks.
*
* @param proxy proxy to the connection that signal was sent through
* @param status auth status
* @param data user data
*/
static
void fso_gsm_sim_auth_status_handler(DBusGProxy *proxy,
                                     gchar *status,
                                     gpointer data)
{
    g_debug("%s", __func__);
    FreeSmartphone *fs = (FreeSmartphone*)data;
    g_return_if_fail(fs);
    GList *list = fso_get_signal_handler(fs,FSO_SIGNAL_AUTH_STATUS);

    while ( list )
    {
        ReplyData *rd = (ReplyData*)list->data;
        FSGSMSIMAuthStatusSignalFunc status_cb = (FSGSMSIMAuthStatusSignalFunc)rd->cb;
        status_cb (status,rd->data);
        list = g_list_next(list);
    }
}


/**
* @brief Adds a callback to auth status signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to call when signal is received
* @param userdata user data passed along with callback
* @return true if callback was succesfully added
*/
gboolean fso_gsm_sim_auth_status_signal(FreeSmartphone *fs,
                                        FSGSMSIMAuthStatusSignalFunc cb,
                                        gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);

    ReplyData *replydata = fso_prepare_method(fs,
                                              G_CALLBACK(cb),
                                              userdata,
                                              FSO_SIM);
    if ( !replydata )
        return FALSE;

    DBusGProxy *proxy = fso_get_proxy(fs,FSO_SIM);

    if ( fso_append_data_to_signal_handler(fs,
                                           FSO_SIGNAL_AUTH_STATUS,
                                           replydata) )
    {
        dbus_g_proxy_add_signal(proxy,
                                FSO_SIM_AUTH_STATUS_SIGNAL_NAME,
                                G_TYPE_STRING,
                                G_TYPE_INVALID);
        dbus_g_proxy_connect_signal (proxy, FSO_SIM_AUTH_STATUS_SIGNAL_NAME,
                                     G_CALLBACK(fso_gsm_sim_auth_status_handler),
                                     fs, NULL);
    }

    return TRUE;
}

/**
* @brief Removes callback from auth status signal
*
* @param fs pointer to freesmartphone struct
* @param cb callback function to remove
* @param userdata userdata assosicated with callback
* @return true if callback was removed
*/
gboolean fso_gsm_sim_auth_status_signal_remove(FreeSmartphone *fs,
                                               FSGSMSIMAuthStatusSignalFunc cb,
                                               gpointer userdata)
{
    g_return_val_if_fail(fs, FALSE);
    g_debug("%s", __func__);


    return fso_signal_remove(fs,
                             FSO_SIM,
                             FSO_SIM_AUTH_STATUS_SIGNAL_NAME,
                             FSO_SIGNAL_AUTH_STATUS,
                             G_CALLBACK(fso_gsm_sim_auth_status_handler),
                             (gpointer)fs,
                             G_CALLBACK(cb),
                             userdata);
}
