#include "modem.h"


#ifndef GSMD_SMS_IMPL_H
#define GSMD_SMS_IMPL_H

#define SMS_SEND_DATA "sms_data"
#define SMS_WRITE_INDEX "sms_write_id"

#define GSM_7BIT	0
#define GSM_8BIT	4
#define GSM_UCS2	8
#define MAX_7BITS_LEN 160
#define MAX_UCS2_LEN 70
#define MAX_SMS_RETRY 3
#define PDU_BUF_SIZE	512

/**
* Sms data
*/
typedef struct {
        gboolean pdu_sent;
        /**
        * pdu data
        */
        guchar pdu [PDU_BUF_SIZE];

        /**
        * Segment id
        */
        gint seg_id;

        /**
        * Message id
        */
        gint msg_id;

        /**
        * Are we sending (FALSE) or storing (TRUE) this sms data
        */
        gboolean storing;

} sms_data;


/**
* @brief SMS parameters
*/
typedef struct {
        /**
        * Service center address
        */
        guchar SCA[16];

        /**
        * Remote tel number
        */
        guchar TPA[16];

        /**
        * Type of address, tel number format 91 is international,
        * 81 is network defualt.
        */
        guchar TOA;

        /**
        * User info identifier
        */
        guchar TP_PID;

        /**
        * Code mode
        */
        guchar TP_DCS;

        /**
        * Timestamp
        */
        guchar TP_SCTS[16];

        /**
        * Sms message content
        */
        guchar TP_UD[161];

        /**
        * Message length
        */
        gint   TP_UD_LEN;

        /**
        * Sms index
        */
        guchar index;

        /**
        * @brief Is this storing sms parameter (TRUE) or submit (FALSE)
        */
        gboolean storing;

        /**
        * @brief Multipart index
        */
        guchar multipart_index;

        /**
        * @brief Multipart count
        */
        guchar multipart_count;

        /**
        * @brief Multipart reference id
        */
        guchar multipart_ref;

        /**
        * @brief Is this message multipart
        */
        gboolean multipart;

        /**
        * @brief Timestamp of the message
        */
        time_t timestamp;

        /**
        * @brief Message
        */
        gchar *message;

        /**
        * @brief Phone number
        */
        gchar *number;

} SmsParam;

void gsmd_sms_sms_param_free(SmsParam *pdu);

gint gsmd_sms_invert_number(const guchar* src,
                            guchar* dst,
                            gint src_length,
                            gboolean is_encode);

gboolean gsmd_sms_string_to_bytes (const guchar* src,
                                   guchar* dst,
                                   int src_length,
                                   int *dst_length);

void gsmd_sms_send_pdu (ModemInterface *modem,
                        gpointer ipc_data,
                        const gchar *message,
                        const gchar *number,
                        AtCommandID command_id,
                        InterfaceType interface,
                        ErrorFunction error_function,
                        gpointer error_data,
                        gboolean store);

AtCommandHandlerStatus gsmd_sms_handler_send_pdu (ModemInterface *modem,
                                                  AtCommandContext *at,
                                                  GString *response);

gboolean gsmd_sms_command_send_prepare(ModemInterface *modem);

void gsmd_sms_sms_data_free(AtCommandContext *at);


AtCommandHandlerStatus gsmd_sms_process_sms_error_response (ModemInterface* modem,
                                                            AtCommandContext *at,
                                                            GScanner* scanner);

#endif
