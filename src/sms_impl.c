#include "sms_impl.h"
#include "utils.h"
#include <stdlib.h>
#include <string.h>


void gsmd_sms_sms_param_free(SmsParam *pdu)
{
        if (pdu) {
                if (pdu->message) {
                        g_free(pdu->message);
                        pdu->message = NULL;
                }

                if (pdu->number) {
                        g_free(pdu->number);
                        pdu->number = NULL;
                }
        }

        g_free(pdu);
}


/**
 * @brief Converts bytes to string
 *
 * @param src source string
 * @param dst destination string
 * @param src_length length of the source string
 * @return resulting destination's string
 */
static gint gsmd_sms_bytes_to_string (const guchar* src, guchar* dst, gint src_length)
{
        const gchar tab[]="0123456789ABCDEF";
        gint i=0;
        for (i=0; i < src_length;i++) {
                //low 4 bits
                *dst++ = tab[(*src>>4)&0xF];
                //high 4 bits
                *dst++ = tab[*src&0xF];
                src++;
        }
        *dst=0;
        return src_length*2;//*2
}


void gsmd_sms_sms_data_free(AtCommandContext *at)
{
        g_debug("%s",__func__);
        GValue *val = g_hash_table_lookup(at->handler_data,SMS_SEND_DATA);
        if (val) {
                sms_data *pdata = g_value_get_pointer(val);
                if (pdata)
                        g_free(pdata);

                g_hash_table_remove(at->handler_data,SMS_SEND_DATA);
        }
}


/**
 * @brief 7 bit encode
 *
 * @param src source message
 * @param dst destination message
 * @param src_length length of the source string
 * @return resulting destination's string
 */
static gint gsmd_sms_encode_7bits (const guchar* src,
                                   guchar* dst,
                                   gint src_length)
{
        //divid src in groups, each group have 8 bytes
        //after encode one group became 7 bytes

        gint src_p = 0;//processed bytes in source gourp
        gint dst_p = 0;//processed bytes in dest group
        gint cur_index;// char index of current processing group
        gint cur_left = 0;// the left bits from last byte

        while (src_p < src_length) {
                cur_index = src_p & 7;
                if (cur_index == 0) { //the first byte
                        cur_left = *src;
                } else {
                        *dst = (*src << (8-cur_index))|cur_left;
                        cur_left = *src >> cur_index;
                        dst_p++;
                        dst++;
                }
                src_p++;
                src++;
                g_assert(dst_p < 512);
        }
        if (cur_left) {
                *dst = cur_left;
                dst_p++;
        }
        return dst_p;
}







/**
 * @brief Checks if sms is coded in 7bits
 *
 * @param str string to check
 * @return true if sms is 7bits
 */
static gboolean gsmd_sms_is_7bits (const gchar* str)
{
        gint len = strlen (str);
        gint i=0;
        for (i=0; i< len; i++) {
                if ((str[i]&0x80) != 0)
                        return FALSE;
        }
        return TRUE;
}


/**
 * @brief Pdu encoder
 *
 * @param pdu sms parameters
 * @param destination string
 * @return length of the destination string
 */
static gint gsmd_sms_encoder_pdu (const SmsParam* pdu, guchar* dst)
{
        gint length=0;
        gint dst_length=0;
        guchar buffer[256];
        gint i=0;
        length = strlen((char*)pdu->SCA);
        g_assert (length == 0);
        if (length) {
                buffer[0] = (gchar)(((length&1)==0)?length:length+1)/2+1;//the length of SMCS info
                buffer[1] = 0;//use net default value
                dst_length = gsmd_sms_bytes_to_string (buffer,dst,2);//convert buffer[0] buffer [1]
                dst_length += gsmd_sms_invert_number (pdu->SCA, &dst[dst_length], length, TRUE);
        } else {
                dst[dst_length++]='0';
                dst[dst_length++]='0';
        }
        g_assert(dst_length == 2);
        length = strlen((gchar*)pdu->TPA);
        buffer[0] = 0x11; //TP_MTI=01
        buffer[1] = 0;//TP-MR=0
        buffer[2] = (gchar)length;//telNo bytes number
        buffer[3] = pdu->TOA;//use netwrk default 81 or 91(internaltional format)
        dst_length += gsmd_sms_bytes_to_string ( buffer, &dst[dst_length], 4);

        dst_length += gsmd_sms_invert_number (pdu->TPA, &dst[dst_length], length, TRUE);

        buffer[0] = pdu->TP_PID;
        buffer[1] = pdu->TP_DCS;
        buffer[2] = 0xAA;//TP_VP 0xAA is 4 days

        length = pdu->TP_UD_LEN;
//	length = strlen (pdu->TP_UD);
        if (pdu->TP_DCS == GSM_7BIT) {
                buffer[3] = length;
                length = gsmd_sms_encode_7bits(pdu->TP_UD,&buffer[4],length+1)+4;
        } else if (pdu->TP_DCS == GSM_UCS2) {
                buffer[3] = length;//the number of bytes
                memcpy (&buffer[4], pdu->TP_UD, length);
                for (i=0;i<length;i++) {
                        g_debug ("byte %d is %x\n",i,buffer[i+4]);
                }
                length += 4;
        } else {
                g_warning("unsupport code\n");
        }
        dst_length += gsmd_sms_bytes_to_string( buffer,&dst[dst_length], length);
        return dst_length;
}



/**
 * @brief Normalizes telephonenumber we're about to send.
 *
 * Tries to convert telephonenumber to international
 * format.
 *
 * @param pdu sms parameters
 */
static void gsmd_sms_normalize_telnumber_of_send (SmsParam* pdu)
{
        gchar* p = (gchar*)pdu->TPA;
        gint i;
        gint len = strlen ((const char*)pdu->TPA);
        if ((*p) == '+') {
                pdu->TOA = 0x91;//international format
                for (i=1; i <= len; i++) {
                        pdu->TPA[i-1] = pdu->TPA[i];
                }
        } else if ((*p) == '0') {
                if (*(p+1) == '0') {
                        pdu->TOA = 0x91;//international format
                        for (i=2; i <= len; i++) {
                                pdu->TPA[i-2] = pdu->TPA[i];
                        }
                } else {
                        pdu->TOA = 0x81;//network default
                }
        } else {
                pdu->TOA = 0x81;//network default
        }
}


/**
 * @brief Initializes sms_data struct from sms message.
 *
 * @param msg contents of the mssage
 * @param length length of the message
 * @param phoneNo message's phone number
 * @param pdu sms parameters
 * @param code coding mode
 */
static void gsmd_sms_build_pdu (const char* msg,
                                gint length,
                                const char* phoneNo,
                                SmsParam* pdu,
                                gint code)
{
        pdu->SCA[0]=0;
        g_stpcpy ((gchar*)pdu->TPA, phoneNo);
        pdu->TP_PID = 0;
        pdu->TP_DCS = code;
        //g_stpcpy ((gchar*)pdu->TP_UD, msg);
        memset (pdu->TP_UD, 0, sizeof(pdu->TP_UD));
        memcpy (pdu->TP_UD, msg, length);
        pdu->TP_UD_LEN = length;
        gsmd_sms_normalize_telnumber_of_send (pdu);
}



/**
 * @brief Creates a new sms data
 *
 * @param seq_id segment id
 * @param storing are we creating sms data for sms to be stored(TRUE) or
 * sent (FALSE)
 * @return new sms_data
 */
static sms_data* gsmd_sms_new_sms_data (gint seq_id, gboolean storing)
{
        sms_data* pdata = g_try_new0(sms_data, sizeof(sms_data));
        pdata->seg_id = seq_id;
        pdata->msg_id = -1;
        pdata->pdu_sent = FALSE;
        pdata->storing = storing;

        return pdata;
}



/**
 * @brief Switches the order of number by groups of two.
 *
 * For example:
 * 34 56 ---> 43 65. If the count is odd, the last one uses F to compensate
 *
 *
 * @param psrc the source string
 * @param pdst the destination string
 * @param src_length the length of source string
 * @param is_encode TRUE: normal format->pdu format, 0: pdu->normal
 * @return length of the resulting destination string
 */
gint gsmd_sms_invert_number(const guchar* src,
                            guchar* dst,
                            gint src_length,
                            gboolean is_encode)
{
        gint dst_length;
        gchar ch;
        dst_length = src_length;
        gint i=0;
	for (i=0; i < ((src_length+1)/2);i++) {
                ch = *src++;
                *dst++ = *src++;
                *dst++ = ch;
        }

        if (is_encode) {
                if (src_length & 1) {
                        *(dst-2) = 'F';
                        dst_length++;
                }
        } else {
                if (*(dst-1)=='F') {
                        dst--;
                        *dst = 0;
                        dst_length--;
                }
        }

        *dst = '\0';
        return dst_length;
}


/**
 * @brief Converts string to bytes
 *
 * @param src source string
 * @param dst destination string
 * @param src_length length of the source string
 * @param dst_length Place to put length of the destination string, or NULL
 * @return FALSE if pdu is invalid
 */
gboolean gsmd_sms_string_to_bytes (const guchar* src,
                                   guchar* dst,
                                   int src_length,
                                   int *dst_length)
{

        gint i=0;
        guchar ch=0;
        gint len = src_length/2;
        /*
                g_debug("%s : src %s",__func__,src);
                g_debug("%s : strlen(src) %d",__func__,strlen((char*)src));
                g_debug("%s : src_length %d",__func__,src_length);
                g_debug("%s : len %d",__func__,len);
        */
        if ( src_length & 1 ) {
                g_critical("%s : Invalid pdu, wrong length %d, pdu: %s",
                           __func__,
                           src_length,src);

                return FALSE;
        }
        for (i=0; i<len; i++) {
                //g_debug("%s : %s",__func__,src);
                //high 4 bits
                ch = 0;
                if ((*src >= '0') && (*src <= '9')) {
                        ch = (*src - '0') << 4;
                } else if ((*src >= 'A') && (*src <= 'F')) {
                        ch = (*src-'A'+10) << 4;
                } else {
                        g_critical("%s : Invalid pdu (high 4 bits), pdu: %s",
                                   __func__,
                                   src);

                        return FALSE;
                }
                src++;
                //low 4 bits
                if ((*src >= '0') && (*src <= '9'))
                        ch |= *src-'0';
                else if ((*src >= 'A') && (*src <= 'F')) {
                        ch |= (*src-'A'+10)&0xFF;
                } else {
                        g_critical("%s : Invalid pdu (low 4 bits), pdu: %s",
                                   __func__,
                                   src);
                        return FALSE;
                }
                *dst = ch ;
                dst++;
                src++;
        }
        if ( dst_length ) {
                *dst_length = len;
        }
        return TRUE;
}

/**
 * @brief Processes sms error responses.
 *
 * @param modem modem whose error to process
 * @param scanner to get error from
 * @return true if next command can be prosessed, false if command should be retried
 */
AtCommandHandlerStatus gsmd_sms_process_sms_error_response (ModemInterface* modem,
                                                            AtCommandContext *at,
                                                            GScanner* scanner)
{
        GString* reason=NULL;
        g_assert( at );
        g_debug( "sms error process \n");
        GValue *val = g_hash_table_lookup(at->handler_data,SMS_SEND_DATA);

        sms_data *data = g_value_get_pointer(val);


        switch (scanner->value.v_int) {
        case 41://check network status
        case 331://No network services
                reason = g_string_new("No network service");
                break;
        case 42://network congestion retry 3 times
                data->pdu_sent = FALSE;
                //
                return AT_HANDLER_RETRY;
        case 1:
        case 96:
                reason = g_string_new("Destination address is uncorrect or missing");
                //need check destinate address
                break;
        case 330://SMSC centre No uncorrect
                reason = g_string_new("SMS service centre number is uncorrect or missing");
                break;
        case 304:
                data->pdu_sent = FALSE;
                return AT_HANDLER_RETRY;
        default:
                reason = g_string_new ("");
                g_string_printf (reason, "Error code is %ld\n", scanner->value.v_int);
        }



        gsmd_utils_send_error(at,
                              GSMD_ERROR_UNKNOWN,
                              reason->str);



        if (modem->sms_ipc->message_sent) {
                modem->sms_ipc->message_sent(modem->sms_ipc,FALSE, reason->str);
        }

        g_string_free (reason, TRUE);
        return AT_HANDLER_DONE_ERROR;
};



/**
 * @brief Creates the at command to send a sms message
 *
 * @param modem modem whose message to send
 * @param ipc_data pointer to ipc data
 * @param msg message contents
 * @param length length of the message
 * @param phoneno recipient's phonenumber
 * @param msgid id of the message to send
 * @param seqid the sequence id of the message. If there is only on seqment of the last
 *        seqment of the message, the id is 0
 * @param code coding mode
 * @param interface interface to write data
 * @param error_function error function to call if an error is occurred
 * @param error_data error data to pass with error_function
 * @param store are we storing sms (TRUE) or sending sms (FALSE)
 */
static void gsmd_sms_command_send (ModemInterface *modem,
                                   gpointer ipc_data,
                                   gchar* msg,
                                   gint length,
                                   GString* phoneNo,
                                   gint seqid,
                                   gint code,
                                   AtCommandID command_id,
                                   InterfaceType interface,
                                   ErrorFunction error_function,
                                   gpointer error_data,
                                   gboolean store)
{
        GString* command = g_string_new ("");
        gchar smsc_length=0;
        gint pdu_length=0;
        gint len=0;
        guchar* pdu_data_buf = NULL;
        sms_data* pdata = gsmd_sms_new_sms_data (seqid,store);
        pdu_data_buf = pdata->pdu;
        SmsParam pdu;
        g_debug("%s: message: %s, phonenumber: %s\n",__func__,msg,phoneNo->str);
        gsmd_sms_build_pdu (msg, length, phoneNo->str, &pdu, code);

        pdu_length = gsmd_sms_encoder_pdu (&pdu, pdu_data_buf);
        len = strlen ((const char*)pdu_data_buf);
        g_debug("%s : PDU is %s",pdu_data_buf,__func__);

        pdu_data_buf [len] = 0x1A;
        pdu_data_buf [len+1] = 0;

        gsmd_sms_string_to_bytes (pdu_data_buf, (guchar*)&smsc_length, 2, NULL);//get the smsc length
        smsc_length++;//add the length byte itself
        pdu_length = pdu_length/2 - smsc_length;
        g_string_printf (command,"%d", pdu_length);

        GHashTable *data = gsmd_utils_create_hash_table();
        gsmd_utils_table_insert_pointer(data,SMS_SEND_DATA,pdata);

        gsmd_modem_post_at_command_id( modem,
                                       command_id,
                                       command->str,
                                       ipc_data,
                                       error_function,
                                       error_data,
                                       interface,
                                       data);

        g_string_free (command,TRUE);
}


/**
 * @brief Sends pdu to gsm modem (used for storing and sending sms)
 *
 * @param modem pointer to modem interface
 * @param ipc_data ipc data pointer
 * @param message message's contents
 * @param number recipient's number
 * @param interface interface to write commands to
 * @param error_function error function to call if an error is occurred
 * @param error_data data to pass with error_function's call
 * @param store are we storing sms (TRUE) or sending (FALSE)
 */
void gsmd_sms_send_pdu (ModemInterface *modem,
                        gpointer ipc_data,
                        const gchar *message,
                        const gchar *number,
                        AtCommandID command_id,
                        InterfaceType interface,
                        ErrorFunction error_function,
                        gpointer error_data,
                        gboolean store)
{
        gchar* str_msg;
        gchar* seg_msg;
        gint   str_len=0;
        gint   left_len;
        GString* str_phoneNo;
        gint seqid = 0;
        gint code = 0;
        gint seg_len = 0;
        gint seg_num = 0;
        gint i =0;
        GError* error;
        glong read;
        glong write;
        gchar* q;
        gchar temp;

        //read message from sms_db
        if (!message || !number || strlen(number) == 0) {
                g_warning ("%s : Message needs contents and a number",__func__);
                gsmd_utils_send_error_full(error_function,
                                           error_data,
                                           ipc_data,
                                           GSMD_ERROR_UNKNOWN,
                                           "MESSAGE NEEDS CONTENTS AND A NUMBER.");
                return;
        }
        //TODO: scan message
        //if it is not asccii 7bits
        if (gsmd_sms_is_7bits (message)) {
                g_debug ("sms is 7 bits\n");
                code = GSM_7BIT;
                seg_len = MAX_7BITS_LEN;
                str_len = strlen (message);
        } else {
                //convert to utf 16
                g_debug ("sms is ucs2\n");
                code =  GSM_UCS2;
                seg_len = MAX_UCS2_LEN;
                str_len = g_utf8_strlen (message, -1);
        }
        //split message into segment
        g_debug ("msg length is %d\n", str_len);
        seg_num = strlen (message)/seg_len;
        if ( strlen (message) % seg_len)
                seg_num++;
        if (code == GSM_UCS2) {
                str_msg = (gchar*) g_utf8_to_utf16 (message, -1,&read,&write,&error);
                if ( str_msg == NULL) {
                        //free msg and return
                        g_warning ("utf8_to_utf16 failed\n");
                        return;
                }
                str_len = write*2;//utf2 use 2 bytes for each character
                q = str_msg;
                /*
                 * The intel CPU is little-endian, but the ucs2 in sms is big-endian,
                 * so the usc_string byte order need to be converted
                 * For example chinese hello in utf16 is 4F60 597D
                 * but usc_string is 604F 7D59
                 * Following while() is to convert the byte order
                 */
                for (i=0; i < str_len; i += 2) {
                        temp = *q;
                        *q = *(q+1);
                        *(q+1) = temp;
                        q += 2;
                }
        } else {
                str_msg = g_strdup (message);
                str_len = strlen (str_msg);
        }

        //check phone number format
        str_phoneNo = g_string_new (number);
        //for each segment call sms send
        //after every segmant sent , move sms to sent box
        left_len = str_len;
        seg_msg = str_msg;
        while (left_len > seg_len) {
                seg_msg +=  seg_len;
                gsmd_sms_command_send (modem,
                                       ipc_data,
                                       seg_msg,seg_len,
                                       str_phoneNo,
                                       seqid,
                                       code,
                                       command_id,
                                       interface,
                                       error_function,
                                       error_data,
                                       store);
                left_len -= seg_len;
        }

        gsmd_sms_command_send (modem,
                               ipc_data,
                               seg_msg,
                               left_len,
                               str_phoneNo,
                               seqid,
                               code,
                               command_id,
                               interface,
                               error_function,
                               error_data,
                               store);

        g_string_free (str_phoneNo, TRUE);
        g_free (str_msg);

}


/**
 * @brief General PDU handler for sms related reply handling
 *
 * @param modem whose replies to handle
 * @return true if replies were recognized
 */
static AtCommandHandlerStatus gsmd_sms_handler_pdu (ModemInterface *modem,
                                                    AtCommandContext *at,
                                                    GString *response)
{
        g_debug("%s",__func__);
        GScanner* scanner = modem->scanner;
        GString* reason=NULL;
        GValue *val = g_hash_table_lookup(at->handler_data,SMS_SEND_DATA);
        if (!val) {
                g_warning("%s : Invalid handler data. sms send data: %p",
                          __func__,
                          val);
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "INVALID HANDLER DATA");
                return AT_HANDLER_DONE_ERROR;

        }

        sms_data *data = g_value_get_pointer(val);

        //sms_data* data = (sms_data*) gsmd_modem_peek_command_data (modem);
        guint symbol;
        gint seq_id;
        symbol = scanner->token;
        switch (symbol) {
        case SYMBOL_OK:
                seq_id = data->seg_id;

                //sms_free_sms_data (data);
                if (seq_id > 0)//it is not the last one
                        return AT_HANDLER_DONE;


                //Send storing smsreply
                if (data->storing) {
                        //Check that we have a valid message index returned
                        //with +CMGW
                        if (data->msg_id < 0) {
                                gsmd_utils_send_error(at,
                                                      GSMD_ERROR_UNKNOWN,
                                                      "FAILED TO GET WRITTEN MESSAGE'S INDEX");
                        } else {
                                modem->sim_ipc->store_message_reply(modem->sim_ipc,
                                                                    at->ipc_data,
                                                                    data->msg_id);
                        }
                } else
                        //Send sending sms reply
                {
                        if (modem->sms_ipc->send_message_reply) {
                                modem->sms_ipc->send_message_reply(modem->sms_ipc,
                                                                   at->ipc_data);
                        }


                        if (modem->sms_ipc->message_sent)
                                modem->sms_ipc->message_sent(modem->sms_ipc,TRUE,NULL);
                }

                return AT_HANDLER_DONE;
                break;
        case SYMBOL_ERROR:
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "FAILED TO SEND MESSAGE");


                if (modem->sms_ipc->message_sent) {
                        // appedn error code
                        reason = g_string_new("SOME ERROR OCCURRED");
                        modem->sms_ipc->message_sent(modem->sms_ipc,
                                                     FALSE,
                                                     reason->str);
                        g_string_free (reason, TRUE);
                }
                return AT_HANDLER_DONE_ERROR;
                break;
        case '+':
                g_scanner_get_next_token (scanner);
                if (scanner->token == SYMBOL_CMGW) {
                        g_scanner_get_next_token (scanner); //Get :
                        g_scanner_get_next_token (scanner); //Get index
                        data->msg_id = scanner->value.v_int;
                        return AT_HANDLER_NEED_MORE;
                }

                if (scanner->token == SYMBOL_CMS) {
                        g_debug("%s : got +CMS\n",__func__);
                        g_scanner_get_next_token (scanner);
                        switch (scanner->token) {
                        case SYMBOL_ERROR:
                                g_scanner_get_next_token (scanner);//get :
                                g_scanner_get_next_token (scanner);//get ERROR Code
                                return gsmd_sms_process_sms_error_response (modem,
                                                                            at,
                                                                            scanner);

                        default:
                                g_scanner_unexp_token (scanner,
                                                       SYMBOL_ERROR,
                                                       NULL,
                                                       "symbol",
                                                       NULL,
                                                       NULL,
                                                       TRUE);

                        }

                        gsmd_utils_send_error(at,
                                              GSMD_ERROR_UNKNOWN,
                                              "FAILED TO SEND MESSAGE");
                        return AT_HANDLER_DONE_ERROR;//trigger next command
                }
        }
        return AT_HANDLER_DONT_UNDERSTAND;
}


/**
 * @brief Handler for sending sms at command's replies.
 *
 * Sending an SMS message is a two part operation. First AT+CMGS is sent to
 * tell gsm modem that we want to send a message. Modem replies by sending ">"
 * indicating that it is ready to receive PDU. Then the PDU is sent and
 * modem should reply with OK.
 *
 * To accomplish this SMS_SEND command first sends AT+CMGS, then when handler
 * receives ">" it sends the pdu data and marks in the data that it has been sent
 * and returns "AT_HANDLER_NEED_MORE" to get the OK for the data.
 *
 *
 * @param modem whose responses to handle
 * @return true if replies were recognized
 */
AtCommandHandlerStatus gsmd_sms_handler_send_pdu (ModemInterface *modem,
                                                  AtCommandContext *at,
                                                  GString *response)
{
        GScanner* scanner = modem->scanner;
        GString* reason = NULL;
        guint symbol;


        g_assert ( at );
        GValue *val = g_hash_table_lookup(at->handler_data,SMS_SEND_DATA);

        if (!val) {
                g_warning("%s : Invalid handler data. sms send data: %p",
                          __func__,
                          val);
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "INVALID HANDLER DATA");
                return AT_HANDLER_ERROR;
        }

        sms_data *data = g_value_get_pointer(val);


        //If pdu was sent, use a different handler to handle reply to pdu
        if ( data->pdu_sent ) {
                return gsmd_sms_handler_pdu( modem, at, response );
        }
        do {
                symbol = scanner->token;
                switch (symbol) {
                case '>': { //Modem is ready to receive pdu
                        //Directly write pdu to modem
                        g_debug("%s : Writing pdu '%s'",
                                __func__,
                                (gchar*)data->pdu);

                        gsmd_modem_serial_write_simple( modem,
                                                        (gchar*)data->pdu,
                                                        INTERFACE_SMS);
                        //Mark that pdu was sent
                        data->pdu_sent = TRUE;
                        //Wait for ok to the pdu
                        return AT_HANDLER_NEED_MORE;
                }
                case SYMBOL_ERROR:
                        if (modem->sms_ipc->message_sent) {
                                reason = g_string_new("ERROR");
                                gsmd_utils_send_error(at,
                                                      GSMD_ERROR_UNKNOWN,
                                                      reason->str);

                                modem->sms_ipc->message_sent(modem->sms_ipc,
                                                             FALSE,
                                                             reason->str);
                                g_string_free (reason, TRUE);
                        }

                        return AT_HANDLER_DONE_ERROR;
                }
                g_scanner_get_next_token (scanner);
        } while (scanner->token != G_TOKEN_EOF);
        g_warning ("%s: didn't get the correct symbol, got '%s' instead",
                   __func__,
                   response->str);

        return AT_HANDLER_DONT_UNDERSTAND;
}

gboolean gsmd_sms_command_send_prepare(ModemInterface *modem)
{
        Trigger *trigger = gsmd_modem_serial_register_trigger( modem,
                                                               "> ",
                                                               NULL,
                                                               NULL,
                                                               TRUE,
                                                               INTERFACE_SMS);
        return trigger != NULL;
}
