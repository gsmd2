/*
 *  sim_inteface.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef SIM_INTERFACE
#define SIM_INTERFACE

typedef struct _SIMInterface SIMInterface;

/**
 * @brief SIM interface
 */
struct _SIMInterface
{

    gpointer priv;

    /***************************Method calls*******************************/

    /**
     * @brief Gets authorization status
     * @param sim sim interface pointer
     * @param ipc_data data for ipc
     */
    void (*get_auth_status) (SIMInterface *sim,
                             gpointer ipc_data);

    /**
     * @brief Sends pin code
     * @param sim sim interface pointer
     * @param ipc_data data for ipc
     * @param pin pin code
     */
    void (*send_auth_code) (SIMInterface *sim,
                            gpointer ipc_data,
                            const char* pin);

    /**
     * @brief Unlocks sim card with puk code and sets a new pin code
     * @param sim sim interface pointer
     * @param ipc_data data for ipc
     * @param puk puk code
     * @param new_pin new pin code
     */
    void (*unlock) (SIMInterface *sim,
                    gpointer ipc_data,
                    const char* puk,
                    const char* new_pin);

    /**
     * @brief Changes pin code
     * @param sim sim interface pointer
     * @param ipc_data data for ipc
     * @param old_pin old pin code
     * @param new_pin new pin code
     */
    void (*change_auth_code) (SIMInterface *sim,
                              gpointer ipc_data,
                              const char* old_pin,
                              const char* new_pin);

    /**
     * @brief Gets SIM info: IMSI(International Mobile Subscriber
     * Identity), subscriber number and country code
     *
     * @param sim sim interface
     * pointer @param ipc_data data for ipc
     */
    void (*get_sim_info) (SIMInterface *sim,
                          gpointer ipc_data);
    /**
    * @brief Gets service center number
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*get_service_center_number) (SIMInterface *sim,
                                       gpointer ipc_data);
    /**
    * @brief Sets service center number
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param number service center number
    */
    void (*set_service_center_number) (SIMInterface *sim,
                                       gpointer ipc_data,
                                       const char *number);


    /**
    * @brief Stores a message
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param recipient messages recipient
    * @param contents messages contents
    */
    void (*store_message) (SIMInterface *sim,
                           gpointer ipc_data,
                           const gchar *message,
                           const gchar *number);

    /**
    * @brief Retrieves message
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param index message's index
    */
    void (*retrieve_message) (SIMInterface *sim,
                              gpointer ipc_data,
                              gint index);

    /**
    * @brief Gets phonebook info
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    */
    void (*get_phonebook_info) (SIMInterface *sim,
                                gpointer ipc_data);

    /**
     * @brief Retrieves the whole phonebook
     *
     * @param sim pointer to sim interface
     * @param ipc_data ipc data
     */
    void (*retrieve_phonebook) (SIMInterface *sim,
                                gpointer ipc_data);
    /**
    * @brief Gets messagebook info
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    */
    void (*get_messagebook_info) (SIMInterface *sim,
                                  gpointer ipc_data);

    /**
    * @brief Deletes a phonebook entry
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param index of the message to sent
    */
    void (*delete_entry) (SIMInterface *sim,
                          gpointer ipc_data,
                          gint index);

    /**
    * @brief Stores a phonebook entry
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param index of the message to sent
    */
    void (*store_entry) (SIMInterface *sim,
                         gpointer ipc_data,
                         gint index,
                         const gchar *name,
                         const gchar *number);

    /**
    * @brief Retrieves a phonebook entry
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param index of the message to sent
    */
    void (*retrieve_entry) (SIMInterface *sim,
                            gpointer ipc_data,
                            gint index);

    /**
    * @brief Deletes a message
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param index of the message to delete
    */
    void (*delete_message) (SIMInterface *sim,
                            gpointer ipc_data,
                            gint index);

    /**
    * @brief Sends a message from sim card
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param index of the message to delete
    */
    void (*send_stored_message) (SIMInterface *sim,
                                 gpointer ipc_data,
                                 gint index);



    /**
    * @brief Lists messages from sim card
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param status status of messages to list
    * "all" = all messages,
    * "read" = message you have received and read,
    * "unread" = new messages you have received,
    * "sent" = messages you have sent,
    * "unsent" = message you have not sent yet.
    */
    void (*retrieve_messagebook) (SIMInterface *sim,
                                  gpointer ipc_data,
                                  const gchar *category);

    /**
    * @brief Send a generic SIM command to the SIM card.
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param command The command to send
    */
    void (*send_generic_sim_command) (SIMInterface *sim,
                                      gpointer ipc_data,
                                      const gchar *command);

    /**
    * @brief Send a restricted SIM command to the SIM card
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param command The command to send. Valid values are:
    * 176 = READ BINARY,
    * 178 = READ RECORD,
    * 192 = GET RESPONSE,
    * 214 = UPDATE BINARY,
    * 220 = UPDATE RECORD,
    * 242 = STATUS.
    *
    * @param fileid The identifier of an elementary datafile on SIM.
    * Mandatory for every command except STATUS.
    * @param p1 Parameter 1 passed to the SIM. Mandatory for every command
    * except STATUS and GET RESPONSE.
    * @param p2 Parameter 1 passed to the SIM. Mandatory for every command
    * except STATUS and GET RESPONSE.
    * @param p3 Parameter 1 passed to the SIM. Mandatory for every command
    * except STATUS and GET RESPONSE.
    * @param data
    * TODO documentation had identical comments on parameters 1-3, fix this
    * comment when documentation is updated
    */
    void (*send_restricted_sim_command) (SIMInterface *sim,
                                         gpointer ipc_data,
                                         gint command,
                                         gint fileid,
                                         gint p1,
                                         gint p2,
                                         gint p3,
                                         const gchar *data);

    /**
    * @brief Retrieve the homezones coordinates, if stored on SIM
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    */
    void (*get_home_zones) (SIMInterface *sim, gpointer ipc_data);
};

typedef struct _SIMIPCInterface SIMIPCInterface;


struct _SIMIPCInterface
{

    gpointer priv;

    /**************************Replies to method calls*************************/

    /**
      * @brief Reply to "get_auth_status" method call
      * @param sim_ipc sim IPC interface pointer
      * @param ipc_data data for ipc
      * @param status authorization status
      */
    void (*get_auth_status_reply) (SIMIPCInterface *sim_ipc,
                                   gpointer ipc_data,
                                   const char *status);


    /**
     * @brief Reply to "send_auth_code" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*send_auth_code_reply) (SIMIPCInterface *sim_ipc,
                                  gpointer ipc_data);

    /**
     * @brief Reply to "unlock" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*unlock_reply) (SIMIPCInterface *sim_ipc,
                          gpointer ipc_data);

    /**
     * @brief Reply to "change_auth_code" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*change_auth_code_reply) (SIMIPCInterface *sim_ipc,
                                    gpointer ipc_data);

    /**
     * @brief Reply to "get_sim_info" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     * @param info information hash table
     */
    void (*get_sim_info_reply) (SIMIPCInterface *sim_ipc,
                                gpointer ipc_data,
                                GHashTable *info);
    /**
    * @brief Reply to "get service center number" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param number service center number
    */
    void (*get_service_center_number_reply) (SIMIPCInterface *sim_ipc,
                                             gpointer ipc_data,
                                             const char *number);

    /**
    * @brief Reply to "set service center number" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*set_service_center_number_reply) (SIMIPCInterface *sim_ipc,
                                             gpointer ipc_data);


    /**
    * @brief Reply to "store a message" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param index stored message's index
    */
    void (*store_message_reply) (SIMIPCInterface *sim_ipc,
                                 gpointer ipc_data,
                                 const int index);

    /**
    * @brief Reply to "retrieve message" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param sender_number message sender's number
    * @param content messages contents
    */
    void (*retrieve_message_reply) (SIMIPCInterface *sim_ipc,
                                    gpointer ipc_data,
                                    const char *sender_number,
                                    const char *contents);

    /**
    * @brief Reply to "retrieve phonebook" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*retrieve_phonebook_reply) (SIMIPCInterface *sim_ipc,
                                      gpointer ipc_data,
                                      GArray *phonebook);
    /**
    * @brief Reply to "get phonebook info" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*get_phonebook_info_reply) (SIMIPCInterface *sim_ipc,
                                      gpointer ipc_data,
                                      GHashTable *info);


    /**
    * @brief Reply to "get messagebook info" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*get_messagebook_info_reply) (SIMIPCInterface *sim_ipc,
                                        gpointer ipc_data,
                                        GHashTable *info);

    /**
    * @brief Reply to "delete entry" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*delete_entry_reply) (SIMIPCInterface *sim_ipc,
                                gpointer ipc_data);

    /**
    * @brief Reply to "store entry" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*store_entry_reply) (SIMIPCInterface *sim_ipc,
                               gpointer ipc_data);
    /**
    * @brief Reply to "retrieve entry" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*retrieve_entry_reply) (SIMIPCInterface *sim_ipc,
                                  gpointer ipc_data,
                                  const gchar *number,
                                  const gchar *content);

    /**
    * @brief Reply to "delete entry" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*delete_message_reply) (SIMIPCInterface *sim_ipc,
                                  gpointer ipc_data);

    /**
    * @brief Reply to "list messages" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param messages array containing all messages that matched the criteria
    */
    void (*retrieve_messagebook_reply) (SIMIPCInterface *sim_ipc,
                                        gpointer ipc_data,
                                        GArray *messages);

    /**
    * @brief Reply to "send messages" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    */
    void (*send_stored_message_reply) (SIMIPCInterface *sim_ipc,
                                       gpointer ipc_data,
                                       gint transaction_index);

    /**
    * @brief Reply to "send generic sim command" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param result The result of the command
    */
    void (*send_generic_sim_command_reply) (SIMIPCInterface *sim_ipc,
                                            gpointer ipc_data,
                                            const gchar *result);

    /**
    * @brief Reply to "send restricted sim command" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param result The result of the command
    */
    void (*send_restricted_sim_command_reply) (SIMIPCInterface *sim_ipc,
                                               gpointer ipc_data,
                                               const gchar *result);

    /**
    * @brief Reply to "get home zones" method call
    *
    * @param sim pointer to sim interface
    * @param ipc_data ipc data
    * @param zones GArray containing homezone_entrys
    */
    void (*get_home_zones_reply) (SIMIPCInterface *sim_ipc,
                                  gpointer ipc_data,
                                  GArray *zones);

    /*********************Error replies to method calls********************/

    /**
    * @brief Error reply to "get service center number" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*get_service_center_number_error) (SIMIPCInterface *sim_ipc,
                                             gpointer ipc_data,
                                             GError *error);
    /**
    * @brief Error reply to "set service center number" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*set_service_center_number_error) (SIMIPCInterface *sim_ipc,
                                             gpointer ipc_data,
                                             GError *error);

    /**
     * @brief Error reply to "get_auth_status" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*get_auth_status_error) (SIMIPCInterface *sim_ipc,
                                   gpointer ipc_data,
                                   GError *error);



    /**
     * @brief Error reply to "send_auth_code" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*send_auth_code_error) (SIMIPCInterface *sim_ipc,
                                  gpointer ipc_data,
                                  GError *error);

    /**
     * @brief Error reply to "unlock" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*unlock_error) (SIMIPCInterface *sim_ipc,
                          gpointer ipc_data,
                          GError *error);

    /**
     * @brief Error reply to "change_auth_code" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*change_auth_code_error) (SIMIPCInterface *sim_ipc,
                                    gpointer ipc_data,
                                    GError *error);

    /**
     * @brief Error reply to "get_sim_info" method call
     * @param sim_ipc sim IPC interface pointer
     * @param ipc_data data for ipc
     * @param error error to be sent
     */
    void (*get_sim_info_error) (SIMIPCInterface *sim_ipc,
                                gpointer ipc_data,
                                GError *error);


    /**
    * @brief Error reply to "store a message" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*store_message_error) (SIMIPCInterface *sim_ipc,
                                 gpointer ipc_data,
                                 GError *error);

    /**
    * @brief Error reply to "retrieve message" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*retrieve_message_error) (SIMIPCInterface *sim_ipc,
                                    gpointer ipc_data,
                                    GError *error);

    /**
    * @brief Error reply to "get phonebook info" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*get_phonebook_info_error) (SIMIPCInterface *sim_ipc,
                                      gpointer ipc_data,
                                      GError *error);

    /**
    * @brief Error reply to "retrieve phonebook" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*retrieve_phonebook_error) (SIMIPCInterface *sim_ipc,
                                      gpointer ipc_data,
                                      GError *error);

    /**
    * @brief Error reply to "get messagebook info" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*get_messagebook_info_error) (SIMIPCInterface *sim_ipc,
                                        gpointer ipc_data,
                                        GError *error);

    /**
    * @brief Error reply to "delete entry" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*delete_entry_error) (SIMIPCInterface *sim_ipc,
                                gpointer ipc_data,
                                GError *error);

    /**
    * @brief Error reply to "store entry" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*store_entry_error) (SIMIPCInterface *sim_ipc,
                               gpointer ipc_data,
                               GError *error);

    /**
    * @brief Error reply to "retreve entry" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*retrieve_entry_error) (SIMIPCInterface *sim_ipc,
                                  gpointer ipc_data,
                                  GError *error);

    /**
    * @brief Error reply to "delete entry" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*delete_message_error) (SIMIPCInterface *sim_ipc,
                                  gpointer ipc_data,
                                  GError *error);

    /**
    * @brief Error reply to "send stored message" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*send_stored_message_error) (SIMIPCInterface *sim_ipc,
                                       gpointer ipc_data,
                                       GError *error);

    /**
    * @brief Error reply to "retrieve messagebook" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*retrieve_messagebook_error) (SIMIPCInterface *sim_ipc,
                                        gpointer ipc_data,
                                        GError *error);

    /**
    * @brief Error reply to "send restricted sim command" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*send_restricted_sim_command_error) (SIMIPCInterface *sim_ipc,
                                               gpointer ipc_data,
                                               GError *error);

    /**
    * @brief Error reply to "send generic sim command" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*send_generic_sim_command_error) (SIMIPCInterface *sim_ipc,
                                            gpointer ipc_data,
                                            GError *error);

    /**
    * @brief Error reply to "get home zones" method call
    *
    * @param modem pointer to modem struct
    * @param ipc_data ipc data
    * @param error pointer to GError describing error
    */
    void (*get_home_zones_error) (SIMIPCInterface *sim_ipc,
                                  gpointer ipc_data,
                                  GError *error);


    /********************************Signals***********************************/


    /**
     * @brief Authorization status signal
     * @param sim_ipc sim IPC interface pointer
     * @param status authorization stauts
     */
    void (*auth_status) (SIMIPCInterface *sim_ipc,
                         const char *status);
    /**
    * @brief Message has arrived
    *
    * @param modem pointer to modem struct
    * @param message's index
    */
    void (*incoming_message) (SIMIPCInterface *sim_ipc,
                              gint index);


};

#endif

