/*
 *  network.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#ifndef GSMD_NETWORK_IMPL_H
#define GSMD_NETWORK_IMPL_H

#include "modem.h"
#include "network_interface.h"

AtCommandHandlerStatus gsmd_network_handler_query_network_registration (ModemInterface *modem,
                                                                        AtCommandContext *at,
                                                                        GString *response);

AtCommandHandlerStatus gsmd_network_handler_query_signal_strength (ModemInterface *modem,
                                                                   AtCommandContext *at,
                                                                   GString *response);

AtCommandHandlerStatus gsmd_network_handler_query_current_operator (ModemInterface *modem,
                                                                    AtCommandContext *at,
                                                                    GString *response);

AtCommandHandlerStatus gsmd_network_handler_query_available_operators (ModemInterface *modem,
                                                                       AtCommandContext *at,
                                                                       GString *response);

AtCommandHandlerStatus gsmd_network_handler_set_operator (ModemInterface *modem,
                                                          AtCommandContext *at,
                                                          GString *response);

AtCommandHandlerStatus gsmd_network_handler_query_signal_strength (ModemInterface *modem,
                                                                   AtCommandContext *at,
                                                                   GString *response);




void gsmd_network_deinitialize(ModemInterface *modem);
void gsmd_network_init(ModemInterface *modem);

#endif
