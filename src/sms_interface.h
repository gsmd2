/*
 *  sms_interface.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef SMS_INTERFACE
#define SMS_INTERFACE

typedef struct _SMSInterface SMSInterface;
#include <glib-object.h>

/**
* @brief SMS interface
*/
struct _SMSInterface {

        gpointer priv;


        /**
        * @brief Sends a message
        *
        * @param sms pointer to sms interface
        * @param ipc_data ipc data
        * @param message message's contents
        * @param number recipient's number
        * @param want_report should gsm be told that user wants a notification
        * when the message has been sent succesfully
        */
        void (*send_message) (SMSInterface *sms,
                              gpointer ipc_data,
                              const gchar *message,
                              const gchar *number,
                              gboolean want_report);


};

typedef struct _SMSIPCInterface SMSIPCInterface;

struct _SMSIPCInterface {
        gpointer priv;

        /*********************Replies to methods*****************/


        /**
        * @brief Reply to "send message" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*send_message_reply) (SMSIPCInterface *sms_ipc,
                                    gpointer ipc_data);


        /*********************Method errors**********************************/


        /**
        * @brief Error reply to "send message" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param error pointer to GError describing error
        */
        void (*send_message_error) (SMSIPCInterface *sms_ipc,
                                    gpointer ipc_data,
                                    GError *error);




        /**********************Signals************************************/
        /* serial port  ---->   service interface */

        /**
        * @brief Message has been sent
        *
        * @param modem pointer to modem struct
        * @param success has message been succesfully sent
        * @param reason reason why message might have not been sent
        */
        void (*message_sent) (SMSIPCInterface *sms_ipc,
                              gboolean success,
                              const gchar* reason);



};

#endif

