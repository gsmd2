/*
 *  serial.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#ifndef GSMD_SERIAL_H
#define GSMD_SERIAL_H

typedef struct _Trigger Trigger;

typedef struct _Serial Serial;

struct _Serial {
        GIOChannel *io;
        GList *triggers;
        GString *buffer;
        gint fd;
        guint watch;
        gboolean error;
};

typedef struct _WriteData WriteData;

typedef void (*SerialWriteComplete)(WriteData *write, gpointer data);

struct _WriteData {
        gchar *command;
        gint *timeoutid;
        guint timeout;
        gpointer timeout_data;
        GSourceFunc timeout_function;
        gpointer write_complete_data;
        SerialWriteComplete write_complete_function;
};

typedef void (*SerialDataIn)( GString *buffer, gpointer data );


Serial* gsmd_serial_open_port ( const gint fd );

gboolean gsmd_serial_queue_write ( Serial *serial, WriteData *data);

gboolean gsmd_serial_write ( Serial *serial,
                             WriteData *write );

void gsmd_serial_free( Serial *serial );

Trigger* gsmd_serial_register_trigger( Serial *serial,
                                       const gchar *trigger_str,
                                       SerialDataIn handler,
                                       gpointer handler_data,
                                       gboolean onetime );

void gsmd_serial_deregister_trigger( Serial *serial, Trigger *trigger );

void gsmd_serial_reset_line_term( Serial *serial );

#endif
