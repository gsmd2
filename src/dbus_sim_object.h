/*
 *  dbus_sim_object.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef DBUS_SIM_OBJECT_H
#define DBUS_SIM_OBJECT_H

#include "modem.h"
#include <glib-object.h>
#include "dbus_objects.h"
#include "sim_interface.h"


typedef struct DBusSimObject DBusSimObject;
typedef struct DBusSimObjectClass DBusSimObjectClass;

GType dbus_sim_object_get_type (void);


struct DBusSimObject
{
    GObject parent;
    SIMInterface *sim;
};

struct DBusSimObjectClass
{
    GObjectClass parent;
    guint incoming_message;
    guint auth_status;
};

/* Macros that are needed for making the proper GObject */
#define DBUS_SIM_TYPE_OBJECT              (dbus_sim_object_get_type ())
#define DBUS_SIM_OBJECT(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), DBUS_SIM_TYPE_OBJECT, DBusSimObject))
#define DBUS_SIM_OBJECT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), DBUS_SIM_TYPE_OBJECT, DBusSimObjectClass))
#define DBUS_IS_SIM_OBJECT(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), DBUS_SIM_TYPE_OBJECT))
#define DBUS_IS_SIM_OBJECT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), DBUS_SIM_TYPE_OBJECT))
#define DBUS_SIM_OBJECT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), DBUS_SIM_TYPE_OBJECT, DBusSimObjectClass))

DBusSimObject *gsmd_dbus_sim_initialize (SIMIPCInterface *sim_ipc,
                                         SIMInterface *sim);

void gsmd_dbus_sim_uninitialize(DBusSimObject *obj);

gboolean smartphone_sim_set_auth_code_required(DBusSimObject *obj,
                                               gboolean check,
                                               DBusGMethodInvocation *method_context);

gboolean smartphone_sim_get_auth_code_required(DBusSimObject *obj,
                                               DBusGMethodInvocation *method_context);

gboolean smartphone_sim_get_auth_status(DBusSimObject *obj,
                                        DBusGMethodInvocation *method_context);

gboolean smartphone_sim_change_auth_code(DBusSimObject *obj,
                                         const gchar *old_pin,
                                         const gchar *new_pin,
                                         DBusGMethodInvocation *method_context);

gboolean smartphone_sim_get_sim_info(DBusSimObject *obj,
                                     DBusGMethodInvocation *method_context);

gboolean smartphone_sim_send_auth_code(DBusSimObject *obj,
                                       const gchar *pin,
                                       DBusGMethodInvocation *method_context);

gboolean smartphone_sim_unlock(DBusSimObject *obj,
                               const gchar *puk,
                               const gchar *new_pin,
                               DBusGMethodInvocation *method_context);

gboolean smartphone_sim_get_service_center_number(DBusSimObject *obj,
                                                  DBusGMethodInvocation *method_context);

gboolean smartphone_sim_set_service_center_number(DBusSimObject *obj,
                                                  const gchar *number,
                                                  DBusGMethodInvocation *method_context);

gboolean smartphone_sim_get_home_country_code(DBusSimObject *obj,
                                              DBusGMethodInvocation *method_context);

gboolean smartphone_sim_store_message(DBusSimObject *obj,
                                      const gchar *recipient_number,
                                      const gchar *contents,
                                      DBusGMethodInvocation *method_context);


gboolean smartphone_sim_retrieve_message(DBusSimObject *obj,
                                         const gint number,
                                         DBusGMethodInvocation *method_context);

gboolean smartphone_sim_get_phonebook_info(DBusSimObject *obj,
                                           DBusGMethodInvocation *method_context);


gboolean smartphone_sim_send_generic_sim_command(DBusSimObject *obj,
                                                 const gchar *command,
                                                 DBusGMethodInvocation *method_context);

gboolean smartphone_sim_send_restricted_sim_command(DBusSimObject *obj,
                                                    gint command,
                                                    gint fileid,
                                                    gint p1,
                                                    gint p2,
                                                    gint p3,
                                                    const gchar *data,
                                                    DBusGMethodInvocation *method_context);

gboolean smartphone_sim_retrieve_phonebook(DBusSimObject *obj,
                                           DBusGMethodInvocation *method_context);

gboolean smartphone_sim_delete_entry(DBusSimObject *obj,
                                     gint index,
                                     DBusGMethodInvocation *method_context);

gboolean smartphone_sim_store_entry(DBusSimObject *obj,
                                    gint index,
                                    const gchar *name,
                                    const gchar *number,
                                    DBusGMethodInvocation *method_context);

gboolean smartphone_sim_retrieve_entry(DBusSimObject *obj,
                                       gint index,
                                       DBusGMethodInvocation *method_context);

gboolean smartphone_sim_get_messagebook_info(DBusSimObject *obj,
                                             DBusGMethodInvocation *method_context);

gboolean smartphone_sim_delete_message(DBusSimObject *obj,
                                       const gint index,
                                       DBusGMethodInvocation *method_context);

gboolean smartphone_sim_retrieve_messagebook(DBusSimObject *obj,
                                             const gchar *category,
                                             DBusGMethodInvocation *method_context);

gboolean smartphone_sim_send_stored_message(DBusSimObject *obj,
                                            gint index,
                                            DBusGMethodInvocation *method_context);

gboolean smartphone_sim_get_home_zones(DBusSimObject *obj,
                                       DBusGMethodInvocation *method_context);
#endif
