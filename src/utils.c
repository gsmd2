/*
 *  utils.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include "utils.h"
#include <string.h>
#include <stdlib.h>


#include <fcntl.h>
#include <unistd.h>

#include <dbus/dbus.h>
#include <dbus/dbus-glib-lowlevel.h>

#define ERROR_PREFIX "org.freesmartphone.GSM."

const gchar* call_status_names[] = {"CALL_IDLE",
                                    "CALL_CALLING",
                                    "CALL_CONNECTED",
                                    "CALL_WAITING",
                                    "CALL_INCOMING",
                                    "CALL_HOLD"
                                   };
/**
* @brief Converts call status to a string and returns it
*
* @param status status to convert
* @return const gchar* containing status' name
*/
const gchar *gsmd_utils_call_status_to_string(CallStatus status)
{
        return call_status_names[status];
}

/**
* @brief Converts callstatus to a status string understood by
* freesmartphone.org's otapi
*
* @param status call's status to convert
* @return const gchar* containing status
*/
const gchar *gsmd_utils_call_status_to_fso(CallStatus status)
{
        switch (status) {
        case CALL_IDLE:
                return "unknown";
                break;
        case CALL_WAITING:
        case CALL_CALLING:
                return "outgoing";
                break;
        case CALL_CONNECTED:
                return "active";
                break;
        case CALL_INCOMING:
                return "incoming";
                break;
        case CALL_HOLD:
                return "held";
                break;
        }
        return NULL;
}


/**
 * @brief Check if the scanner has standard end token "OK" or "ERROR"
 *
 * @param scanner the input scanner
 *
 * @return if the scanner is string "OK" or "ERROR" return TRUE, otherwise FALSE
 */
gboolean gsmd_utils_check_end_token(GScanner* scanner)
{
        if ((scanner->token == SYMBOL_OK) || (scanner->token == SYMBOL_ERROR)) {
                return TRUE;
        } else {
                return FALSE;
        }
}


/**
* @brief Helper function to check if a regular expression match exists
*
* @param match_info match info to check
* @param name name to match
* @return TRUE if match exists
*/
gboolean gsmd_utils_match_exists(GMatchInfo *match_info,const char *name)
{
        gboolean result = FALSE;
        gchar *match = g_match_info_fetch_named(match_info,name);

        if (match && strlen(match) > 0)
                result = TRUE;

        g_free(match);

        return result;
}


static
const gchar* gsmd_utils_get_error_name(GsmdErrorCode code)
{
        const gchar *name = NULL;
        switch(code)
        {
        case GSMD_ERROR_DEVICE_TIMEOUT:
                name = ERROR_PREFIX "Device.Timeout";
                break;
        case GSMD_ERROR_NOTSUPPORTED:
                name = ERROR_PREFIX "UnsupportedCommand";
                break;
        case GSMD_ERROR_NETWORK_NOT_PRESENT:
                name = ERROR_PREFIX "Network.NotPresent";
                break;
        case GSMD_ERROR_SIM_UNAUTHORIZED:
                name = ERROR_PREFIX "SIM.Unauthorized";
                break;
        case GSMD_ERROR_INTERNAL:
                name = ERROR_PREFIX "Unknown.Internal";
                break;
        case GSMD_ERROR_CALL_NOTFOUND:
                name = ERROR_PREFIX "Call.NotFound";
                break;
        case GSMD_ERROR_CALL_INVALIDSTATE:
                name = ERROR_PREFIX "Call.InvalidState";
                break;
        case GSMD_ERROR_UNKNOWN:
        case GSMD_ERROR_RESET:
        default:
                name = ERROR_PREFIX "Device.Failed";
                break;
        }
        return name;
}

/**
* @brief Creates a GError with given data and calls error_function
*
* @param error_function error function to call
* @param ipc_data ipc data for the error function
* @param code error code for GError
* @param message error message
*/
void gsmd_utils_send_error_full(ErrorFunction error_function,
                                gpointer error_data,
                                gpointer ipc_data,
                                GsmdErrorCode code,
                                const char *message)
{
        if (error_function && error_data) {
                GError *err = NULL;
                DBusError error = {0,};
                error.name = gsmd_utils_get_error_name(code);
                error.message = message;
                dbus_set_g_error(&err, &error);
                error_function(error_data,ipc_data,err);
                g_error_free( err );
        }
}


/**
* @brief Send error response according to scanner's data
*
* @param at current command's context
* @param response modem's reply
* @param errormsg error message to send if scanner has ERROR token
* @param timeoutmsg error message to send if scanner has TIMEOUT token
* @return TRUE if ERROR or TIMEOUT found and response was sent
*/
gboolean gsmd_utils_send_error_from_response(AtCommandContext *at,
                                             GString* response,
                                             const gchar *errormsg,
                                             const gchar *timeoutmsg)
{
        g_assert(at);

        static GRegex *regex_error = NULL;
        static GRegex *regex_timeout = NULL;
        if( !regex_error ) {
                GError *error = NULL;
                regex_error = g_regex_new ("^ERROR$", 0, 0, &error);
                if ( error ) {
                        g_error("%s: %s", __func__, error->message);
                }
                regex_timeout = g_regex_new ("^TIME_OUT$", 0, 0, &error);
                if ( error ) {
                        g_error("%s: %s", __func__, error->message);
                }
        }
        GMatchInfo *match_info;

        if (g_regex_match (regex_error, response->str, 0, &match_info)) {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      errormsg);
                return TRUE;
        }

        if (g_regex_match (regex_timeout, response->str, 0, &match_info)) {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_DEVICE_TIMEOUT,
                                      timeoutmsg);
                return TRUE;
        }
        gint code = 0;
        if( gsmd_utils_parse_cms_error(response, &code) ) {
                g_warning("%s : CMS ERROR: %d", __func__, code);
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      errormsg);
                return TRUE;
        }
        if( gsmd_utils_parse_cme_error(response, &code) ) {
                g_warning("%s : CME ERROR: %d", __func__, code);
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      errormsg);
                return TRUE;
        }
        return FALSE;
}

/**
* @brief Sends error for given at command context with given message
*
* @param at at command context whose error function to call
* @param code error's code
* @param message error message to send
*/
void gsmd_utils_send_error(AtCommandContext *at,
                           GsmdErrorCode code,
                           const char *message)
{
        gsmd_utils_send_error_full(at->error_function,
                                   at->error_data,
                                   at->ipc_data,
                                   code,
                                   message);
}

/**
* @brief Prints information on all current calls
*
* @param modem pointer to modem interface
*/
void gsmd_utils_print_calls(ModemInterface *modem)
{
        GList *list = modem->calls;
        Call *call = NULL;
        gint index = 0;
        if( list ) {
                list = g_list_nth(list, g_list_length(list)-1);
        }
        while ( list ) {
                call = (Call*)list->data;
                g_debug("Call %d",index);
                g_debug("---");
                g_debug("Status: %s",gsmd_utils_call_status_to_string(call->status));
                g_debug("Number: %s",call->number->str);
                g_debug("Type: %s",call->type->str);
                g_debug("Id: %d",call->id);
                g_debug("Index: %d",call->index);
                g_debug("Vendor id: %d",call->vendor_id);

                g_debug("\n");

                list = g_list_previous( list );
                index++;
        }
}

/**
* @brief Finds first call with given status
*
* @param modem pointer to modem interface
* @param status call status to find
* @return call pointer or NULL if none found
*/
Call* gsmd_utils_find_first_call_status(ModemInterface *modem,
                                        CallStatus status)
{
        g_debug("%s", __func__);
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                if (call->status == status)
                        return call;
                list = g_list_next( list );
        }

        return NULL;
}

/**
* @brief Finds call with given phone number
*
* @param modem pointer to modem interface
* @param number call's number to find
* @return call or NULL if none found
*/
Call* gsmd_utils_find_call_number(ModemInterface *modem, const gchar *number)
{
        g_debug("%s", __func__);
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                if (strcmp(call->number->str, number) == 0) {
                        return call;
                }
                g_debug("%s : call (%p) id: %d, number %s != %s", __func__,call,
                        call->id, call->number->str, number);
                list = g_list_next( list );
        }
        return NULL;
}

/**
 * @brief Finds call with given id
 *
 * @param modem pointer to modem interface
 * @param id id to find
 * @return call or NULL if none found
 */
Call* gsmd_utils_find_call_id(ModemInterface *modem, guint id)
{
        g_debug("%s", __func__);
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                if (call->id == id) {
                        return call;
                }
                g_debug("%s : call (%p) id: %d != %d", __func__,call, call->id, id);
                list = g_list_next( list );
        }
        return NULL;
}


/**
 * @brief Returns the first call
 *
 * @param modem pointer to modem interface
 * @return call or NULL if none found
 */
Call* gsmd_utils_get_first_call(ModemInterface *modem)
{
        GList *list = modem->calls;
        if ( list ) {
                return list->data;
        }
        return NULL;
}

/**
 * @brief Finds call with given vendor id
 *
 * @param modem pointer to modem interface
 * @param vendor_id vendor id to find
 * @return call or NULL if none found
 */
Call* gsmd_utils_find_call_vendor_id(ModemInterface *modem, gint vendor_id)
{
        g_debug("%s", __func__);
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                if (call->vendor_id == vendor_id) {
                        return call;
                }
                g_debug("%s : Call vendor id: %d, wanted %d", __func__, call->vendor_id, vendor_id);
                list = g_list_next( list );
        }

        return NULL;
}

/**
* @brief Gets specified call's status
*
* @param modem pointer to modem interface
* @param id call id of the call whose status to get
* @return call's status or CALL_IDLE if none found
*/
CallStatus gsmd_utils_get_call_status(ModemInterface *modem, gint id)
{
        g_debug("%s", __func__);
        Call *call = gsmd_utils_find_call_id(modem,id);
        if (call)
                return call->status;

        return CALL_IDLE;
}


/**
* @brief Returns the number of calls with specified status
*
* @param modem pointer to modem interface
* @param status calls with this status to count
* @return count of calls with specified status
*/
gint gsmd_utils_get_call_status_count(ModemInterface *modem,CallStatus status)
{
        gint result = 0;
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                if (call->status == status)
                        result++;
                list = g_list_next( list );
        }

        return result;
}

/**
* @brief Finds first unknown modem id and sets given id to it
*
* @param modem pointer to modem struct
* @param vendor_id new call id to be set
*/
void gsmd_utils_set_unknown_vendor_id(ModemInterface *modem, gint vendor_id)
{
        g_debug("%s", __func__);
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                if (call->vendor_id == -1) {
                        call->vendor_id = vendor_id;
                        g_debug("%s : Setting call vendor id %d for call %d", __func__, call->vendor_id, call->id);
                        return;
                }
                list = g_list_next( list );
        }

        gsmd_utils_print_calls(modem);
        g_warning("%s : No unknown calls!", __func__);
}


/**
* @brief Free's call's data
*
* @param call Call whose data to free
*/
void gsmd_utils_call_free(Call *call)
{
        g_assert(call);
        if (call->number)
                g_string_free(call->number,TRUE);
        if (call->type)
                g_string_free(call->type,TRUE);
        g_hash_table_destroy(call->vendor_properties);
        call->type = NULL;
        call->number = NULL;
        g_free(call);
}

/**
* @brief Helper function to get call propeties for status signal/reply
*
* @param call call which properties to get
*/
GHashTable* gsmd_utils_call_get_properties(Call *call)
{
        GHashTable *properties = gsmd_utils_create_hash_table();
        GList *keys = g_hash_table_get_keys(call->vendor_properties);
        GList *list = keys;
        while(list ) {
                gchar *key  = (gchar*)list->data;
                GValue *val = g_hash_table_lookup(call->vendor_properties, key);
                gsmd_utils_table_insert_copy(properties, key, val);
                list = g_list_next(list);
        }
        gsmd_utils_table_insert_string(properties,
                                       GSMD_MODEM_KEY_PHONE_NUMBER,
                                       call->number->str);
        return properties;
}

/**
* @brief Helper function to send a call status signal with given status
*
* @param modem Modem who owns call
* @param call call which status signal to send
* @param status status to be sent
* @param phonenumber should phone number be added to properties
*/
void gsmd_utils_call_send_status(ModemInterface *modem,
                                 Call *call,
                                 const gchar *status)
{
        g_debug("%s", __func__);
        if (modem && modem->call_ipc->call_status && call) {
                GHashTable *properties = gsmd_utils_call_get_properties(call);

                gsmd_utils_table_insert_string(properties,
                                       GSMD_MODEM_KEY_PHONE_NUMBER,
                                       call->number->str);

                modem->call_ipc->call_status(modem->call_ipc,
                                             call->id,
                                             status,
                                             properties);
                g_hash_table_destroy(properties);
        }

}


/**
* @brief Removes given call from modem's list
*
* @param modem pointer to modem interface to remove call from
* @param call pointer to call to remove
* @param send_released_call should signal be sent that a call was released
*/
void gsmd_utils_remove_call_direct(ModemInterface *modem,
                                   Call *call,
                                   gboolean send_released_call)
{
        if ( call ) {
                if ( send_released_call)
                        gsmd_utils_call_send_status(modem,call,"released");


                modem->calls = g_list_remove(modem->calls, call);
                gsmd_utils_call_free(call);
        }
}

/**
* @brief Removes all calls that have a specific status
*
* @param modem pointer to modem interface to remove calls from
* @param status status of the calls to be removed
* @param send_released_call should signal be sent that a call was released for
* all calls
*/
void gsmd_utils_remove_calls_with_status(ModemInterface *modem,
                                         CallStatus status,
                                         gboolean send_released_call)
{
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                if (call->status == status) {
                        gsmd_utils_remove_call_direct(modem,call,send_released_call);
                        break;
                }
                list = g_list_next( list );
        }
}


/**
* @brief Removes all calls
*
* @param modem pointer to modem interface
* @param send_released_call If true, send ReleasedCall signal
*/
void gsmd_utils_remove_calls(ModemInterface *modem,
                             gboolean send_released_call)
{
        g_debug("%s", __func__);
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                g_debug("%s : Removing call id %d (vendor id %d)", __func__, call->id, call->vendor_id);
                gsmd_utils_call_send_status(modem,call,"released");
                gsmd_utils_call_free(call);
                list = g_list_next( list );
        }

        g_list_free(modem->calls);
        modem->calls = NULL;
}


/**
* @brief Removes a call from call array
*
* @param modem pointer to modem interface
* @param id id of the call to remove
* @param send_released_call If true, send ReleasedCall signal
*/
void gsmd_utils_remove_call(ModemInterface *modem,
                            guint id,
                            gboolean send_released_call)
{
        g_debug("%s", __func__);
        GList *list = modem->calls;
        Call *call = NULL;
        while ( list ) {
                call = (Call*)list->data;
                if (call->id == id) {
                        g_debug("%s : Removing call id %d (vendor id %d)", __func__, id, call->vendor_id);
                        gsmd_utils_remove_call_direct(modem,call,send_released_call);
                        break;
                }
                list = g_list_next( list );
                call = NULL;
        }

}

/**
 * @brief Removes a call with given vendor id from call array
 *
 * @param modem pointer to modem interface
 * @param vendor_id vendor id of the call to remove
 */
void gsmd_utils_remove_call_vendor_id(ModemInterface *modem,
                                      gint vendor_id,
                                      gboolean send_released_call)
{
        g_debug("%s", __func__);
        Call *call = gsmd_utils_find_call_vendor_id(modem,vendor_id);
        gsmd_utils_remove_call(modem,
                               call->id,
                               send_released_call);
}


/**
 * @brief Get latest call that was made or received
 *
 * @param modem pointer to modem interface
 *
 * @return Latest call or NULL if none
 */
Call* gsmd_utils_call_get_current(ModemInterface *modem)
{
        g_debug("%s", __func__);
        return modem->calls ? (Call*)modem->calls->data : NULL;
}

/**
* @brief Creates a new call and and adds it to call array
*
* @param modem pointer to modem struct
* @param number phone number of the remote end
* @param type call type
* @param status Call status
* @return the newly created call
*/
Call* gsmd_utils_new_call(ModemInterface *modem,
                          const gchar *number,
                          const gchar *type,
                          CallStatus status)
{
        // FIXME conflict possible after 2^sizeof(guint) calls ;)
        static guint call_id = 0;
        Call *call = g_new0(Call,1);
        g_debug("%s : New call %p", __func__, call);
        call->status = status;
        call->type = g_string_new(type);
        call->number = g_string_new(number);
        call->id = call_id++;
        call->vendor_id = -1;
        call->index = -1;
        call->vendor_properties = gsmd_utils_create_hash_table();
        modem->calls = g_list_prepend(modem->calls, call);
        return call;
}
/**
* @brief Helper function to create a gvalue from given int
* and insert it to given table
*
* @param table GHashTable to insert gvalue to
* @param key key to insert value with
* @param value value to insert
*/
void gsmd_utils_table_insert_int(GHashTable *table,
                                 gchar *key,
                                 const gint value)
{
        GValue *val = g_new0(GValue,1);
        g_value_init(val,G_TYPE_INT);
        g_value_set_int(val,value);
        g_hash_table_insert(table,
                            key,
                            val);
}

/**
* @brief Helper function to create a gvalue from given string
* and insert it to given table
*
* @param table GHashTable to insert gvalue to
* @param key key to insert value with
* @param value value to insert
*/
void gsmd_utils_table_insert_string(GHashTable *table,
                                    gchar *key,
                                    const gchar *value)
{
        GValue *val = g_new0(GValue,1);
        g_value_init(val,G_TYPE_STRING);
        g_value_set_string(val,value);
        g_hash_table_insert(table,
                            key,
                            val);
}

/**
* @brief Helper function to create a gvalue from given boolean
* and insert it to given table
*
* @param table GHashTable to insert gvalue to
* @param key key to insert value with
* @param value value to insert
*/
void gsmd_utils_table_insert_boolean(GHashTable *table,
                                     gchar *key,
                                     const gboolean value)
{
        GValue *val = g_new0(GValue,1);
        g_value_init(val,G_TYPE_BOOLEAN);
        g_value_set_boolean(val,value);
        g_hash_table_insert(table,
                            key,
                            val);
}


/**
* @brief Helper function to get string value from hashtable
*
* @param table GHashTable to get string value from
* @param key key to find value with
* @param value default to return if no value found
*/
const gchar *gsmd_utils_table_get_string(GHashTable *table,
                                         gchar *key,
                                         const gchar* value)
{
        GValue *val = g_hash_table_lookup(table,key);
        if (!val)
                return value;
        return g_value_get_string(val);
}

/**
* @brief Helper function to get boolean value from hashtable
*
* @param table GHashTable to get boolean value from
* @param key key to find value with
* @param value default to return if no value found
*/
gboolean gsmd_utils_table_get_boolean(GHashTable *table,
                                      gchar *key,
                                      gboolean value)
{
        GValue *val = g_hash_table_lookup(table,key);
        if (!val)
                return value;
        return g_value_get_boolean(val);
}

/**
* @brief Helper function to get int value from hashtable
*
* @param table GHashTable to get int value from
* @param key key to find value with
* @param value default to return if no value found
*/
gint gsmd_utils_table_get_int(GHashTable *table,
                              gchar *key,
                              gint value)
{
        GValue *val = g_hash_table_lookup(table,key);
        if (!val)
                return value;
        return g_value_get_int(val);
}

/**
* @brief Helper function to get pointer value from hashtable
*
* @param table GHashTable to get pointer value from
* @param key key to find value with
* @param value default to return if no value found
*/
gpointer gsmd_utils_table_get_pointer(GHashTable *table,
                                      gchar *key,
                                      gpointer value)
{
        GValue *val = g_hash_table_lookup(table,key);
        if (!val)
                return value;
        return g_value_get_pointer(val);
}

/**
* @brief Helper function to create a gvalue from given pointer
* and insert it to given table
*
* @param table GHashTable to insert value to
* @param key key to insert value with
* @param value value to insert
*/
void gsmd_utils_table_insert_pointer(GHashTable *table,
                                     gchar *key,
                                     const gpointer value)
{
        GValue *val = g_new0(GValue,1);
        g_value_init(val,G_TYPE_POINTER);
        g_value_set_pointer(val,value);
        g_hash_table_insert(table,
                            key,
                            val);
}


/**
* @brief Helper function to create a copy of gvalue and insert it
* to given hash table
*
* @param table ghashtable to insert copy of given value
* @param key value's key
* @param value value to copy and insert
*/
void gsmd_utils_table_insert_copy(GHashTable *table,
                                  gchar *key,
                                  GValue *value)
{
        g_assert(table);
        g_assert(value);

        GValue *copy = g_new0(GValue,1);
        g_value_init(copy,G_VALUE_TYPE(value));
        g_value_copy(value,copy);
        g_hash_table_insert(table,key,copy);
}

/**
* @brief Fetches a matching text, converts it to int and returns it
*
* @param matc_info match info to fetch value from
* @param match key to fetch
* @param value default value to return if matching value was not found
* @return matching text converted to int
*/
gint gsmd_utils_fetch_match_int(GMatchInfo *match_info,
                                gchar *match,
                                gint value)
{
        gchar *str = g_match_info_fetch_named(match_info,match);
        gint result = atoi(str);
        g_free(str);
        return result;
}

/**
* @brief Fetches a matching text, converts it to GString and returns it
*
* @param matc_info match info to fetch value from
* @param match key to fetch
* @param value default value to return if matching value was not found
* @return matching text converted to GString
*/
GString *gsmd_utils_fetch_match_string(GMatchInfo *match_info,
                                       gchar *match,
                                       GString *value)
{
        gchar *str = g_match_info_fetch_named(match_info,match);
        GString *result = g_string_new(str);
        g_free(str);
        return result;
}

/**
* @brief Function to free gvalue
*
* @param data gvalue pointer
*/
void gsmd_utils_gvalue_free(gpointer data)
{
        GValue *val = data;
        g_value_unset(val);
        g_free(val);
}

/**
* @brief Defaults serial port initializer
*
* @param device device
* @param cflag control mode flag
* @param speed serial port's speed
* @return handle
*/
gint gsmd_utils_default_serial_init (const gchar *device,
                                     tcflag_t cflag,
                                     speed_t speed)
{
        g_debug("%s", __func__);
        struct termios t;
        gint fd = g_open (device, O_RDWR | O_APPEND | O_NOCTTY | O_NONBLOCK, 0);
        if ( !fd ) {
                g_warning("%s : open failed: '%s'", __func__, device);
                return 0;
        }
        if (tcgetattr(fd, &t) < 0) {
                g_warning("%s : get attr failed '%s'", __func__, device);
                goto error;
        }

        cfsetispeed(&t, speed);
        cfsetospeed(&t, speed);

        t.c_cflag = cflag;
        t.c_lflag &= ~(ICANON|ECHO);
        t.c_iflag &= ~(IXON|ICRNL);
        t.c_oflag &= ~(ONLCR);
        t.c_cc[VMIN] = 0;
        t.c_cc[VTIME] = 0;
//        t.c_cc[VSUSP] = 0;

        if (tcsetattr (fd, TCSANOW, &t) < 0) {
                g_warning("%s : setattr failed", __func__);
                goto error;
        }
        return fd;
error:
        close( fd );
        return 0;
}

/**
* @brief Creates a new hash table with string keys and GValue values
* This is to be used throughout gsmd2 and with ipc mechanisms aswell.
*
* @return new hash table
*/
GHashTable *gsmd_utils_create_hash_table()
{
        return g_hash_table_new_full(NULL,
                                     g_str_equal,
                                     NULL,
                                     gsmd_utils_gvalue_free);
}

/**
 * @brief Handler to scan for OK/ERROR/TIMEOUT message
 * Returns AT_HANDLER_DONE on OK
 * Returns AT_HANDLER_DONE_ERROR on ERROR
 * Returns AT_HANDLER_RETRY on TIMEOUT
 * Or returns AT_HANDLER_DONT_UNDERSTAND
 *
 *
 * @param modem modem to scan
 * @param at at command's context
 * @param response string modem sent
 * @return true if OK/ERROR scanned
 */
AtCommandHandlerStatus gsmd_utils_handler_ok (ModemInterface *modem,
                                              AtCommandContext *at,
                                              GString *response)
{
        GScanner* scanner = modem->scanner;
        switch (scanner->token) {
        case SYMBOL_OK:
                return AT_HANDLER_DONE;
        case SYMBOL_ERROR:
                return AT_HANDLER_DONE_ERROR;
        case SYMBOL_TIME_OUT:
                return AT_HANDLER_RETRY;
        default:
                return AT_HANDLER_DONT_UNDERSTAND;
        }
}

/**
* @brief Helper utility that prints command or data along with special characters
* @param func function's name to prepend to the text
* @param prepend text to prepend before command
* @param data data to write
*/
void gsmd_utils_print_data(const gchar *func,
                           const gchar *prepend,
                           const gchar *data)
{

        GString *debug_str = g_string_new(func);
        if (prepend)
                g_string_append_printf(debug_str," : %s",prepend);
        else
                g_string_append_printf(debug_str," : ");

        gint i;
        for (i=0; i < strlen(data); i++) {
                switch (data[i]) {

                case '\r':
                        g_string_append(debug_str, "\\r");
                        break;
                case '\n':
                        g_string_append(debug_str, "\\n");
                        break;
                case '%':
                        g_string_append(debug_str, "%%");
                        break;
                case 26:
                        g_string_append(debug_str, "<EOF>");
                        break;
                default:
                        g_string_append_printf(debug_str, "%c", data[i]);
                        break;
                }
        }

        g_string_append(debug_str, "<end>");
        g_debug(debug_str->str);
        g_string_free( debug_str, TRUE);
}

/**
* @brief returns integer from first 2 characters
*
* @param str str whose first 2 characters to convert
* @return integer
*/
int gsmd_utils_convert_to_int(const gchar *str)
{
        gchar two[3] = {0};
        memcpy(two,str,2);
        return atoi(two);
}

/**
* @brief Converts timestamp information from pdu to time_t
*
* @param str string containing timestamp information
* @return timestamp converted to time_t
*/
time_t gsmd_utils_convert_to_timestamp(const gchar *str)
{
        if (strlen(str) < 14)
                return 0;

        struct tm timeinfo;
        //We'll be having problems with this..
        timeinfo.tm_year = 100 + gsmd_utils_convert_to_int(str);
        str+=2;

        timeinfo.tm_mon = gsmd_utils_convert_to_int(str) - 1;
        str+=2;

        timeinfo.tm_mday = gsmd_utils_convert_to_int(str);
        str+=2;

        timeinfo.tm_hour = gsmd_utils_convert_to_int(str);
        str+=2;

        timeinfo.tm_min = gsmd_utils_convert_to_int(str);
        str+=2;

        timeinfo.tm_sec = gsmd_utils_convert_to_int(str);
        str+=2;


        timeinfo.tm_isdst = -1;


        char tz = (char)gsmd_utils_convert_to_int(str);

        return mktime ( &timeinfo ) + (900 * tz);
}

/**
* @brief Parses cms error and stoes it to given variable
*
* @param response string to parse cms error from
* @param code code to store cms code
* @return true if cms error was found and parsed from response
*/
gboolean gsmd_utils_parse_cms_error(GString *response,
                                    gint *code)
{
        static GRegex *regex = NULL;
        if( !regex ) {
                GError *error = NULL;
                regex = g_regex_new ("^\\+CMS ERROR:\\s(?<code>\\d+)$.*$", 0, 0, &error);
                if ( error ) {
                        g_error("%s: %s", __func__, error->message);
                }
        }
        GMatchInfo *match_info;


        if (g_regex_match (regex, response->str, 0, &match_info)) {
                if ( code ) {
                        gchar *match = g_match_info_fetch_named(match_info,"code");
                        *code = atoi(match);
                        g_free(match);
                }
                return TRUE;
        }
        return FALSE;
}

/**
* @brief Parses cme error and stoes it to given variable
*
* @param response string to parse cms error from
* @param code code to store cms code
* @return true if cms error was found and parsed from response
*/
gboolean gsmd_utils_parse_cme_error(GString *response,
                                    gint *code)
{
        static GRegex *regex = NULL;
        if( !regex ) {
                GError *error = NULL;
                regex = g_regex_new ("^\\+CME ERROR:\\s(?<code>\\d+)$.*$", 0, 0, &error);
                if ( error ) {
                        g_error("%s: %s", __func__, error->message);
                }
        }
        GMatchInfo *match_info;


        if (g_regex_match (regex, response->str, 0, &match_info)) {
                if ( code ) {
                        gchar *match = g_match_info_fetch_named(match_info,"code");
                        *code = atoi(match);
                        g_free(match);
                }
                return TRUE;
        }
        return FALSE;
}
