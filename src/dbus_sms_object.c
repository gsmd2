/*
 *  dbus_sms_object.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include "dbus_sms_object.h"
#include "dbus_sms_glue.h"
#include "dbus_marshal.h"
#include "sms_interface.h"
#include <stdlib.h>
#include <stdio.h>

G_DEFINE_TYPE(DBusSmsObject, dbus_sms_object, G_TYPE_OBJECT)



static void
dbus_sms_object_class_init(DBusSmsObjectClass *klass)
{
        /** Register our signals so that they can be emitted */

        klass->message_sent = g_signal_new ("message_sent",
                                            G_OBJECT_CLASS_TYPE (klass),
                                            G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                                            0, NULL, NULL, gsmd_smartphone_marshaller_VOID__BOOLEAN_STRING,
                                            G_TYPE_NONE, 2, G_TYPE_BOOLEAN,G_TYPE_STRING);


}


static void dbus_sms_object_init(DBusSmsObject *gsm)
{
        dbus_g_object_type_install_info (DBUS_SMS_TYPE_OBJECT,
                                         &dbus_glib_smartphone_sms_object_info);
}

void dbus_sms_object_finalize(GObject *obj)
{
        //G_OBJECT_CLASS (parent_class)->finalize (obj);
        //DBusSmsObject *gsm = (DBusSmsObject *) obj;
}



void gsmd_dbus_sms_uninitialize(DBusSmsObject *obj)
{
        g_object_unref(obj);
}



/*********************Signal functions***********************/
void smartphone_sms_message_sent (  SMSIPCInterface *sms_ipc,
                                    gboolean success,
                                    const gchar* reason)
{
        DBusSmsObject *obj = (DBusSmsObject*)sms_ipc->priv;
        g_signal_emit (obj,
                       DBUS_SMS_OBJECT_GET_CLASS(obj)->message_sent,
                       0, success,reason);
}




/*************************Methods**********************************************/

gboolean smartphone_sms_get_service_bearer(DBusSmsObject *obj,
                                           DBusGMethodInvocation *method_context)
{
        g_assert(FALSE);
        return TRUE;
}

gboolean smartphone_sms_set_service_bearer(DBusSmsObject *obj,
                                           const gchar *mode,
                                           DBusGMethodInvocation *method_context)
{
        g_assert(FALSE);
        return TRUE;
}

gboolean smartphone_sms_send_message(DBusSmsObject *obj,
                                     const gchar *message,
                                     const gchar *number,
                                     const gboolean want_report,
                                     DBusGMethodInvocation *method_context)
{
        obj->sms->send_message(  obj->sms,
                                 method_context,
                                 message,
                                 number,
                                 want_report);
        return TRUE;
}


/*************************Method replies***************************************/


void smartphone_sms_send_message_reply(SMSIPCInterface *sms_ipc,
                                       gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}


/*****************Method errors***************************************/


void smartphone_sms_send_message_error( SMSIPCInterface *sms_ipc,
                                        gpointer ipc_data,
                                        GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                           error);
}


DBusSmsObject *gsmd_dbus_sms_initialize (SMSIPCInterface *sms_ipc,
                                         SMSInterface *sms)
{
        DBusSmsObject *obj = g_object_new (DBUS_SMS_TYPE_OBJECT, NULL);
        sms_ipc->priv = obj;
        obj->sms = sms;

        //Signals

        sms_ipc->message_sent = &smartphone_sms_message_sent;




        //Method replies
        sms_ipc->send_message_reply = &smartphone_sms_send_message_reply;

        //Method errors
        sms_ipc->send_message_error = &smartphone_sms_send_message_error;



        return obj;
}

