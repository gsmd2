/*
 *  device.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include <string.h>

#include "device.h"

#include "utils.h"
#include "modem.h"

static
AtCommandHandlerStatus gsmd_device_handler_query_date_time (ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response);
static
AtCommandHandlerStatus gsmd_device_handler_set_date_time (ModemInterface *modem,
                                                          AtCommandContext *at,
                                                          GString *response);
static
AtCommandHandlerStatus gsmd_device_handler_query_imei (ModemInterface *modem,
                                                       AtCommandContext *at,
                                                       GString *response);
static
AtCommandHandlerStatus gsmd_device_handler_query_revision (ModemInterface *modem,
                                                           AtCommandContext *at,
                                                           GString *response);
static
AtCommandHandlerStatus gsmd_device_handler_query_model (ModemInterface *modem,
                                                        AtCommandContext *at,
                                                        GString *response);
static
AtCommandHandlerStatus gsmd_device_handler_query_manufacturer (ModemInterface *modem,
                                                               AtCommandContext *at,
                                                               GString *response);
static
AtCommandHandlerStatus gsmd_device_handler_get_antenna_power (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response);
static
AtCommandHandlerStatus gsmd_device_handler_get_info (ModemInterface *modem,
                                                     AtCommandContext *at,
                                                     GString *response);

static const AtCommand
device_commands[]
= {
        //Query datetime
        { DATE_TIME_QUERY,              "AT+CCLK?\r\n",             100,
                TRUE,  0,
                gsmd_device_handler_query_date_time, NULL, SIM_READY, NULL},

        //Set datetime
        { SET_DATE_TIME,                "AT+CCLK=\"%s\"\r\n",       100000,
          FALSE,  0,
          gsmd_device_handler_set_date_time, NULL, SIM_READY, NULL},

        //Get IMEI
        { IMEI_QUERY,                   "AT+CGSN\r\n",              100,
          FALSE,  0,
          gsmd_device_handler_query_imei, NULL, SIM_UNKNOWN, NULL},

        //Get SW revision
        { SW_REVISION_QUERY,            "AT+CGMR\r\n",              100,
          FALSE,  0,
          gsmd_device_handler_query_revision, NULL, SIM_UNKNOWN, NULL},

        //Get model
        { MODEL_QUERY,                  "AT+CGMM\r\n",              100,
          FALSE,  0,
          gsmd_device_handler_query_model, NULL, SIM_UNKNOWN, NULL},

        //Get manufacturer
        { MANUFACTURER_QUERY,           "AT+CGMI\r\n",              100,
          FALSE,  0,
          gsmd_device_handler_query_manufacturer, NULL, SIM_UNKNOWN, NULL},

        //Get antenna power
        { ANTENNA_POWER_GET,            "AT+CFUN?\r\n",            100,
          FALSE,  0,
          gsmd_device_handler_get_antenna_power, NULL, SIM_UNKNOWN, NULL},

        /*         //Get capabilities */
        /*         { CAPABILITIES_GET,             "AT+GCAP\r\n",            100, */
        /*           FALSE,  0, */
        /*           gsmd_device_handler_get_features, NULL, SIM_UNKNOWN}, */

        { 0,                    NULL,                100,    TRUE,  1,
          NULL, NULL, SIM_UNKNOWN },

}, *device_command_p = device_commands;

static const SymbolTable
device_symbols[] = {
        { "CCLK",       SYMBOL_CCLK,    },
        { "CFUN",       SYMBOL_CFUN,    },
        { "CGSN",       SYMBOL_CGSN,    },
        { "CGMR",       SYMBOL_CGMR,    },
        { "CGMM",       SYMBOL_CGMM,    },
        { "CGMI",       SYMBOL_CGMI,    },
        { NULL, 0,                      },
}, *device_symbol_p = device_symbols;


void gsmd_device_init_at_handler(ModemInterface* modem)
{
        while (device_symbol_p->symbol_name) {
                g_scanner_add_symbol (modem->scanner, device_symbol_p->symbol_name,
                                      GINT_TO_POINTER(device_symbol_p->symbol_token));
                device_symbol_p++;
        }
        while (device_command_p->command) {
                gsmd_modem_register_command (modem,device_command_p);
                device_command_p++;
        }
}

/**
 * @brief AtCommand to query datetime
 *
 * @param modem modem whose response to handle
 */
static void gsmd_device_command_query_date_time (DeviceInterface *device,
                                                 gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)device->priv;
        gsmd_modem_post_at_command_id( modem,
                                       DATE_TIME_QUERY,
                                       NULL,
                                       ipc_data,
                                       NULL, NULL,
                                       INTERFACE_DEVICE,
                                       NULL);
}


/**
 * @brief AtCommand to set datetime
 *
 * @param modem modem whose response to handle
 * @param date_time datetime to set. Format "yy/MM/dd,hh:mm:ss±zz"
 */
static void gsmd_device_command_set_date_time (DeviceInterface *device,
                                               gpointer ipc_data,
                                               const char* date_time)
{
        ModemInterface *modem = (ModemInterface*)device->priv;
        gsmd_modem_post_at_command_id( modem,
                                       SET_DATE_TIME,
                                       date_time,
                                       ipc_data,
                                       NULL, NULL,
                                       INTERFACE_DEVICE,
                                       NULL);
}


/**
 * @brief Handler to modems response from setDateTime command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
static
AtCommandHandlerStatus gsmd_device_handler_set_date_time (ModemInterface *modem,
                                                          AtCommandContext *at, GString *response)
{
        GScanner* scanner = modem->scanner;
//	g_message ("Set datatime handler\n");
        if (scanner->token == SYMBOL_OK) {
                //TODO smartphone doesn't have datetime methods so they need
                //to be specified or this code moved elsewhere
                /*
                  if (modem->modem_interface->setDateTimeResp)
                  modem->modem_interface->setDateTimeResp (modem,TRUE);
                */
                return AT_HANDLER_DONE;
        } else if (scanner->token == SYMBOL_ERROR) { //if format uncorrect,
                //TODO smartphone doesn't have datetime methods so they need to
                //be specified or this code moved elsewhere
                /*
                  if (modem->modem_interface->setDateTimeResp)
                  modem->modem_interface->setDateTimeResp (modem,FALSE);
                */
                return AT_HANDLER_DONE;
        }
        if (scanner->token == SYMBOL_TIME_OUT) {
                return AT_HANDLER_DONE;
        }
        return AT_HANDLER_DONT_UNDERSTAND;
}

/**
 * @brief Handler to modems response from queryDateTime command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
static
AtCommandHandlerStatus gsmd_device_handler_query_date_time (ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response)
{
        GScanner* scanner = modem->scanner;
//	g_message ("Query datatime handler\n");
        //AT+CCLK=?
        // +CCLK: "02/09/07,22:30:25"
        if (gsmd_utils_check_end_token(scanner)) {
                return AT_HANDLER_DONE;
        }
        if (scanner->token == SYMBOL_TIME_OUT) {
                return AT_HANDLER_DONE;
        }
        if (scanner->token != '+')
                return AT_HANDLER_DONT_UNDERSTAND;
        g_scanner_get_next_token (scanner);
        if (scanner->token == SYMBOL_CCLK) {
                g_scanner_get_next_token (scanner);//get :
                g_scanner_get_next_token (scanner);//get string
                GString* date_time_str = g_string_new (scanner->value.v_string);
                /* //TODO datetime methods arent specified in freesmartphone's api
                   if (modem->device->dateTime)
                   modem->device->dateTime (modem,date_time_str);
                */
                g_string_free (date_time_str, TRUE);
                return AT_HANDLER_DONE;
        }
        return AT_HANDLER_DONT_UNDERSTAND;
}

static
AtCommandHandlerStatus gsmd_device_get_info_handler(ModemInterface* modem,
                                                    AtCommandContext *at,
                                                    GString *response,
                                                    gchar *key,
                                                    const gchar *errormsg,
                                                    const gchar *timeoutmsg)
{
        GScanner* scanner = modem->scanner;
        GValue *val = NULL;

        if (scanner->token == SYMBOL_OK) {
                val = g_hash_table_lookup(at->handler_data,key);
                if (!val) {
                        gsmd_utils_send_error(at,GSMD_ERROR_UNKNOWN,"OK WITHOUT RESPONSE");
                        g_warning("%s : Invalid handler data: %p",
                                  __func__,
                                  val);

                        return AT_HANDLER_DONE_ERROR;
                }

                return AT_HANDLER_DONE;
        } else if (scanner->token == G_TOKEN_EOF) {
                return AT_HANDLER_NEED_MORE;
        } else if (gsmd_utils_send_error_from_response(at,
                                                       response,
                                                       errormsg,
                                                       timeoutmsg)) {
                return AT_HANDLER_DONE_ERROR;
        }

        return AT_HANDLER_DONT_UNDERSTAND;
}

static
AtCommandHandlerStatus gsmd_device_handler_query_imei ( ModemInterface* modem,
                                                        AtCommandContext *at,
                                                        GString *response)
{

        AtCommandHandlerStatus status = gsmd_device_get_info_handler(modem,
                                                                     at,
                                                                     response,
                                                                     GSMD_DEVICE_KEY_IMEI,
                                                                     "ERROR OCCURRED WHEN QUERYING IMEI",
                                                                     "TIMEOUT OCCURRED WHEN QUERYING IMEI");
        if (status != AT_HANDLER_DONT_UNDERSTAND) {
                return status;
        }
        GScanner* scanner = modem->scanner;
        gchar *res = NULL;
        if ( scanner->token == '+' ) {
                g_scanner_get_next_token (scanner);
                if ( scanner->token == SYMBOL_CGSN ) {
                        g_scanner_get_next_token (scanner);
                        if (scanner->token == ':') {
                                g_scanner_peek_next_token (scanner);
                                if ( scanner->next_token == G_TOKEN_INT) {
                                        res = g_strdup(response->str+g_scanner_cur_position(scanner));
                                        res = g_strstrip(res);
                                }
                        }
                }
        } else {
                res = g_strdup(response->str);
                res = g_strstrip(res);
        }
        if ( res ) {
                gboolean is_imei = TRUE;
                gint i = 0;
                if ( strlen(res) == 15 ) {
                        for (i=0; i < 15;i++) {
                                if ( !g_ascii_isdigit(res[i]) ) {
                                        is_imei = FALSE;
                                        break;
                                }
                        }
                } else {
                        is_imei = FALSE;
                }
                if ( !is_imei ) {
                        g_debug("%s : %s is not IMEI",__func__, res);
                        g_free( res );
                        return AT_HANDLER_DONT_UNDERSTAND;
                }

                g_debug("%s : IMEI: %s",__func__, res);

                gsmd_utils_table_insert_string(at->handler_data,GSMD_DEVICE_KEY_IMEI,res);

                g_free( res );
        }
        return AT_HANDLER_NEED_MORE;
}
static
AtCommandHandlerStatus gsmd_device_handler_query_revision (ModemInterface *modem,
                                                           AtCommandContext *at,
                                                           GString *response)
{
        g_debug("%s : %s",__func__, response->str);
        AtCommandHandlerStatus status = gsmd_device_get_info_handler(modem,
                                                                     at,
                                                                     response,
                                                                     GSMD_DEVICE_KEY_MODEM_REVISION,
                                                                     "ERROR OCCURRED WHEN QUERYING REVISION",
                                                                     "TIMEOUT OCCURRED WHEN QUERYING REVISION");
        if (status != AT_HANDLER_DONT_UNDERSTAND)
                return status;


        GScanner* scanner = modem->scanner;
        gchar *res = NULL;;
        if ( scanner->token == '+' ) {
                g_scanner_get_next_token (scanner);
                if ( scanner->token == SYMBOL_CGMR ) {
                        g_scanner_get_next_token (scanner);
                        if (scanner->token == ':') {
                                g_scanner_peek_next_token (scanner);
                                if ( scanner->next_token == G_TOKEN_STRING ) {
                                        g_scanner_get_next_token (scanner);
                                        res = g_strdup(scanner->value.v_string);
                                } else {
                                        res = g_strdup(response->str+g_scanner_cur_position(scanner));
                                        res = g_strstrip(res);
                                }
                                g_debug("%s : manu: '%s'", __func__, res);
                        }
                }
        } else {
                res = g_strdup(response->str);
                res = g_strstrip(res);
        }
        if ( res && strlen(res) > 0 ) {
                g_debug("%s : SW revision: %s",__func__, res);
                gsmd_utils_table_insert_string(at->handler_data,
                                               GSMD_DEVICE_KEY_MODEM_REVISION,
                                               res);
        }

        g_free(res);
        return AT_HANDLER_NEED_MORE;
}
static
AtCommandHandlerStatus gsmd_device_handler_query_manufacturer (ModemInterface *modem,
                                                               AtCommandContext *at,
                                                               GString *response)
{
/*         g_debug("%s : %s",__func__, response->str); */
        AtCommandHandlerStatus status = gsmd_device_get_info_handler(modem,
                                                                     at,
                                                                     response,
                                                                     GSMD_DEVICE_KEY_MODEM_VENDOR,
                                                                     "ERROR OCCURRED WHEN QUERYING MANUFACTURE",
                                                                     "TIMEOUT OCCURRED WHEN QUERYING MANUFACTURER");
        if (status != AT_HANDLER_DONT_UNDERSTAND)
                return status;

        GScanner* scanner = modem->scanner;
        gchar *res = NULL;;
        if ( scanner->token == '+' ) {
                g_scanner_get_next_token (scanner);
                if ( scanner->token == SYMBOL_CGMI ) {
                        g_scanner_get_next_token (scanner);
                        if (scanner->token == ':') {
                                g_scanner_peek_next_token (scanner);
                                if ( scanner->next_token == G_TOKEN_STRING ) {
                                        g_scanner_get_next_token (scanner);
                                        res = g_strdup(scanner->value.v_string);
                                } else {
                                        res = g_strdup(response->str+g_scanner_cur_position(scanner));
                                        res = g_strstrip(res);
                                }
                                g_debug("%s : manu: '%s'", __func__, res);
                        }
                }
        } else {
                res = g_strdup(response->str);
                res = g_strstrip(res);
        }
        if ( res && strlen(res) > 0 ) {
                g_debug("%s : manufacturer: %s",__func__, res);
                gsmd_utils_table_insert_string(at->handler_data,
                                               GSMD_DEVICE_KEY_MODEM_VENDOR,
                                               res);

        }

        g_free(res);
        return AT_HANDLER_NEED_MORE;
}
static
AtCommandHandlerStatus gsmd_device_handler_query_model (ModemInterface *modem,
                                                        AtCommandContext *at,
                                                        GString *response)
{
        g_debug("%s : %s",__func__, response->str);
        AtCommandHandlerStatus status = gsmd_device_get_info_handler(modem,
                                                                     at,
                                                                     response,
                                                                     GSMD_DEVICE_KEY_MODEM_MODEL,
                                                                     "ERROR OCCURRED WHEN QUERYING MODEL",
                                                                     "TIMEOUT OCCURRED WHEN QUERYING MODEL");
        if (status != AT_HANDLER_DONT_UNDERSTAND)
                return status;

        GScanner* scanner = modem->scanner;
        gchar *res = NULL;;
        if ( scanner->token == '+' ) {
                g_scanner_get_next_token (scanner);
                if ( scanner->token == SYMBOL_CGMM ) {
                        g_scanner_get_next_token (scanner);
                        if (scanner->token == ':') {
                                g_scanner_peek_next_token (scanner);
                                if ( scanner->next_token == G_TOKEN_STRING ) {
                                        g_scanner_get_next_token (scanner);
                                        res = g_strdup(scanner->value.v_string);
                                } else {
                                        res = g_strdup(response->str+g_scanner_cur_position(scanner));
                                        res = g_strstrip(res);
                                }
                                g_debug("%s : manu: '%s'", __func__, res);
                        }
                }
        } else {
                res = g_strdup(response->str);
                res = g_strstrip(res);
        }
        if ( res && strlen(res) > 0 ) {
                g_debug("%s : model: %s",__func__, res);
                gsmd_utils_table_insert_string(at->handler_data,
                                               GSMD_DEVICE_KEY_MODEM_MODEL,
                                               res);
        }
        g_free(res);

        return AT_HANDLER_NEED_MORE;
}


static gboolean gsmd_device_command_get_info_reply(ModemInterface *modem,
                                                   gpointer ipc_data,
                                                   GHashTable *info)
{
        if( !modem->no_cache ) {
                GValue *vendor = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_VENDOR);
                GValue *model = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_MODEL);
                GValue *revision = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_REVISION);
                GValue *imei = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_IMEI);

                if (vendor) {
                        gsmd_utils_table_insert_copy(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_VENDOR,vendor);
                }
                if (model) {
                        gsmd_utils_table_insert_copy(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_MODEL,model);
                }
                if (revision) {
                        gsmd_utils_table_insert_copy(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_REVISION,revision);
                }
                if (imei) {
                        gsmd_utils_table_insert_copy(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_IMEI,imei);
                }
        }
        modem->device_ipc->get_info_reply( modem->device_ipc,
                                           ipc_data,
                                           info);
        return TRUE;
}

static
AtCommandHandlerStatus gsmd_device_handler_get_info (ModemInterface *modem,
                                                     AtCommandContext *at,
                                                     GString *response)
{
        AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
        AtCommandContext *cmd = NULL;
        switch (at->command->cmd_id) {
        case IMEI_QUERY:
                g_debug("%s : IMEI_QUERY", __func__);
                /*                 status = gsmd_device_handler_query_imei(modem,at,response); */
                status = at->command->handler(modem,at,response);
                switch (status) {
                case AT_HANDLER_DONE_ERROR:
                case AT_HANDLER_ERROR:
                        return AT_HANDLER_DONE_ERROR;
                        break;
                case AT_HANDLER_DONE:
                        cmd = gsmd_at_command_context_new_from_id( modem,
                                                                   MANUFACTURER_QUERY,
                                                                   NULL,/*param*/
                                                                   at->ipc_data,
                                                                   at->error_function,
                                                                   at->error_data,
                                                                   at->handler_data);
                        at->handler_data = NULL;
                        cmd->handler = &gsmd_device_handler_get_info;
                        gsmd_modem_post_at_command(modem,cmd,INTERFACE_DEVICE);
                        status = AT_HANDLER_DONE;
                        break;
                case AT_HANDLER_DONT_UNDERSTAND:
                case AT_HANDLER_NEED_MORE:
                case AT_HANDLER_RETRY:
                        return status;
                }
                break;
        case MANUFACTURER_QUERY:
                g_debug("%s : MANUFACTURER_QUERY", __func__);
                status = at->command->handler(modem,at,response);
                switch (status) {
                case AT_HANDLER_DONE_ERROR:
                case AT_HANDLER_ERROR:
                        return AT_HANDLER_DONE_ERROR;
                case AT_HANDLER_DONE:
                        cmd = gsmd_at_command_context_new_from_id( modem,
                                                                   SW_REVISION_QUERY,
                                                                   NULL,/*param*/
                                                                   at->ipc_data,
                                                                   at->error_function,
                                                                   at->error_data,
                                                                   at->handler_data);
                        at->handler_data = NULL;
                        cmd->handler = &gsmd_device_handler_get_info;
                        gsmd_modem_post_at_command(modem,cmd,INTERFACE_DEVICE);
                        status = AT_HANDLER_DONE;
                        break;
                case AT_HANDLER_DONT_UNDERSTAND:
                case AT_HANDLER_NEED_MORE:
                case AT_HANDLER_RETRY:
                        return status;
                }
                break;
        case SW_REVISION_QUERY:
                g_debug("%s : SW_REVISION_QUERY", __func__);
                status = at->command->handler(modem,at,response);
                switch (status) {
                case AT_HANDLER_DONE_ERROR:
                case AT_HANDLER_ERROR:
                        return AT_HANDLER_DONE_ERROR;
                case AT_HANDLER_DONE:
                        cmd = gsmd_at_command_context_new_from_id( modem,
                                                                   MODEL_QUERY,
                                                                   NULL,/*param*/
                                                                   at->ipc_data,
                                                                   at->error_function,
                                                                   at->error_data,
                                                                   at->handler_data);
                        at->handler_data = NULL;
                        cmd->handler = &gsmd_device_handler_get_info;
                        gsmd_modem_post_at_command(modem,cmd,INTERFACE_DEVICE);
                        status = AT_HANDLER_DONE;
                        break;
                case AT_HANDLER_DONT_UNDERSTAND:
                case AT_HANDLER_NEED_MORE:
                case AT_HANDLER_RETRY:
                        return status;
                }
                break;
        case MODEL_QUERY:
                g_debug("%s : MODEL_QUERY", __func__);
                status = gsmd_device_handler_query_model(modem,at,response);
                switch (status) {
                case AT_HANDLER_DONE_ERROR:
                case AT_HANDLER_ERROR:
                        return AT_HANDLER_DONE_ERROR;
                        break;
                case AT_HANDLER_DONE:
                        gsmd_device_command_get_info_reply(modem,at->ipc_data, at->handler_data);

                        break;
                case AT_HANDLER_DONT_UNDERSTAND:
                case AT_HANDLER_NEED_MORE:
                case AT_HANDLER_RETRY:
                        return status;
                }
                break;
        }
        return status;
}



void gsmd_device_command_get_info( DeviceInterface *device, gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)device->priv;
        GValue *vendor = NULL;
        GValue *model =  NULL;
        GValue *revision = NULL;
        GValue *imei = NULL;
        if ( !modem->no_cache ) {
                vendor = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_VENDOR);
                model = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_MODEL);
                revision = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_MODEM_REVISION);
                imei = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_IMEI);
        }
        if (vendor && model && revision && imei) {
                g_debug("%s: info is cached",__func__);
                GHashTable *info = gsmd_utils_create_hash_table();
                gsmd_utils_table_insert_copy(info,GSMD_DEVICE_KEY_MODEM_VENDOR,vendor);
                gsmd_utils_table_insert_copy(info,GSMD_DEVICE_KEY_MODEM_MODEL,model);
                gsmd_utils_table_insert_copy(info,GSMD_DEVICE_KEY_MODEM_REVISION,revision);
                gsmd_utils_table_insert_copy(info,GSMD_DEVICE_KEY_IMEI,imei);
                modem->device_ipc->get_info_reply(modem->device_ipc,
                                               ipc_data,
                                               info);
                g_hash_table_destroy(info);

        } else {
                AtCommandContext *cmd = NULL;
                cmd = gsmd_at_command_context_new_from_id( modem,
                                                           IMEI_QUERY,
                                                           NULL,/*param*/
                                                           ipc_data,
                                                           (ErrorFunction)modem->device_ipc->get_info_error,
                                                           modem->device_ipc,
                                                           NULL);
                cmd->handler = &gsmd_device_handler_get_info;
                gsmd_modem_post_at_command(modem,cmd,INTERFACE_DEVICE);

        }
}

void gsmd_device_command_set_antenna_power( DeviceInterface *device,
                                            gpointer ipc_data,
                                            gboolean antenna_power)
{
        //TODO implement

}

static
AtCommandHandlerStatus gsmd_device_handler_get_antenna_power (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response)
{
        GScanner* scanner = modem->scanner;
        GValue *val = NULL;
        //	g_message ("Query datatime handler\n");
        //AT+CCLK=?
        // +CCLK: "02/09/07,22:30:25"
        if (scanner->token == SYMBOL_OK) {
                val = g_hash_table_lookup(at->handler_data,GSMD_DEVICE_KEY_POWER);


                if (!val) {
                        g_warning("%s : Invalid handler data. power: %p",
                                  __func__,
                                  val);

                        gsmd_utils_send_error(at,
                                              GSMD_ERROR_UNKNOWN,
                                              "OK WITHOUT RESPONSE");
                }


                if (!val)
                        return AT_HANDLER_DONE_ERROR;


                modem->device_ipc->get_antenna_power_reply(modem->device_ipc,
                                                           at->ipc_data,
                                                           g_value_get_boolean(val));

                gsmd_utils_table_insert_copy(modem->caches[INTERFACE_DEVICE],
                                             GSMD_DEVICE_KEY_POWER,
                                             val);
                return AT_HANDLER_DONE;
        }


        if (gsmd_utils_send_error_from_response(at,
                                                response,
                                                "FAILED TO GET ANTENNA POWER",
                                                "TIMEOUT WHEN GETTING ANTENNA POWER")) {
                return AT_HANDLER_DONE_ERROR;
        }

        if (scanner->token != '+') {
                return AT_HANDLER_DONT_UNDERSTAND;
        }
        g_scanner_get_next_token (scanner);
        if (scanner->token == SYMBOL_CFUN) {
                g_scanner_get_next_token (scanner);//get :
                g_scanner_get_next_token (scanner);//get string

                gsmd_utils_table_insert_boolean(at->handler_data,
                                                GSMD_DEVICE_KEY_POWER,
                                                scanner->value.v_int);
                return AT_HANDLER_NEED_MORE;
        }
        return AT_HANDLER_DONT_UNDERSTAND;
}

void gsmd_device_command_get_antenna_power( DeviceInterface *device,
                                            gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)device->priv;
        if (modem->no_cache || !g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_POWER)) {
                gsmd_modem_post_at_command_id( modem,
                                               ANTENNA_POWER_GET,
                                               NULL,
                                               ipc_data,
                                               (ErrorFunction)modem->device_ipc->get_antenna_power_error,
                                               (gpointer)modem->device_ipc,
                                               INTERFACE_DEVICE,
                                               NULL);
        } else {
                GValue *val = g_hash_table_lookup(modem->caches[INTERFACE_DEVICE],GSMD_DEVICE_KEY_POWER);
                if (val) {
                        modem->device_ipc->get_antenna_power_reply(modem->device_ipc,
                                                                   ipc_data,
                                                                   g_value_get_boolean(val));
                }

        }

}

void gsmd_device_command_prepare_to_suspend( DeviceInterface *device,
                                             gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)device->priv;
        modem->device_ipc->prepare_to_suspend_reply(modem->device_ipc,
                                                    ipc_data);
}

void gsmd_device_command_recover_from_suspend( DeviceInterface *device,
                                               gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)device->priv;
        modem->device_ipc->recover_from_suspend_reply(modem->device_ipc,
                                                      ipc_data);
}

void gsmd_device_command_get_features( DeviceInterface *device, gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)device->priv;
        GHashTable *table = gsmd_utils_create_hash_table();

        modem->device_ipc->get_features_reply( modem->device_ipc,
                                               ipc_data,
                                               table);
        g_hash_table_destroy(table);

}

void gsmd_device_init(ModemInterface *modem)
{
        gsmd_device_init_at_handler(modem);

        modem->device->priv = (gpointer)modem;
        modem->device->prepare_to_suspend = &gsmd_device_command_prepare_to_suspend;
        modem->device->recover_from_suspend = &gsmd_device_command_recover_from_suspend;
        modem->device->get_date_time = &gsmd_device_command_query_date_time;
        modem->device->set_date_time = &gsmd_device_command_set_date_time;
        modem->device->get_info = &gsmd_device_command_get_info;
        modem->device->set_antenna_power = &gsmd_device_command_set_antenna_power;
        modem->device->get_antenna_power = &gsmd_device_command_get_antenna_power;
        modem->device->get_features = &gsmd_device_command_get_features;
}
