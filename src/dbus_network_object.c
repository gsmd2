/*
 *  dbus_network_object.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include "dbus_network_object.h"
#include "dbus_network_glue.h"
#include "dbus_marshal.h"
#include "dbus_objects.h"
#include "network_interface.h"
#include <stdlib.h>
#include <stdio.h>
#include <dbus/dbus-glib-bindings.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>


#define GSMD_NETWORK_LISTPROVIDER_TYPE (dbus_g_type_get_struct ("GValueArray", \
      G_TYPE_INT, \
      G_TYPE_STRING, \
      G_TYPE_STRING, \
      G_TYPE_STRING, \
      G_TYPE_INVALID))

G_DEFINE_TYPE(DBusNetworkObject, dbus_network_object, G_TYPE_OBJECT)



static void
dbus_network_object_class_init(DBusNetworkObjectClass *klass)
{
        /* Make the local system aware of our kind */

        /** Register our signals so that they can be emitted */
        klass->status = g_signal_new("status",
                                     G_OBJECT_CLASS_TYPE (klass),
                                     G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                                     0, NULL, NULL, g_cclosure_marshal_VOID__POINTER,
                                     G_TYPE_NONE, 1, dbus_g_type_get_map ("GHashTable",
                                                                          G_TYPE_STRING, G_TYPE_VALUE));


        klass->subscriber_numbers = g_signal_new ("subscriber_numbers",
                                                  G_OBJECT_CLASS_TYPE (klass),
                                                  G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                                                  0, NULL, NULL, g_cclosure_marshal_VOID__STRING,
                                                  G_TYPE_NONE, 1, G_TYPE_STRING);
}


static void dbus_network_object_init(DBusNetworkObject *gsm)
{
        dbus_g_object_type_install_info (DBUS_NETWORK_TYPE_OBJECT,
                                         &dbus_glib_smartphone_network_object_info);
}


void dbus_network_object_finalize(GObject *obj)
{
        //G_OBJECT_CLASS (parent_class)->finalize (obj);
        //DBusNetworkObject *gsm = (DBusNetworkObject *) obj;
}



void gsmd_dbus_network_uninitialize(DBusNetworkObject *obj)
{
        g_object_unref(obj);
}



/*********************Signal functions***********************/


void smartphone_network_status (NetworkIPCInterface *network_ipc,
                                GHashTable *status)
{
        DBusNetworkObject *obj = (DBusNetworkObject*)network_ipc->priv;
        g_signal_emit (obj,
                       DBUS_NETWORK_OBJECT_GET_CLASS(obj)->status,
                       0, status);
}

void smartphone_network_subscriber_numbers (NetworkIPCInterface *network_ipc,
                                            const gchar** number)
{
        DBusNetworkObject *obj = (DBusNetworkObject*)network_ipc->priv;
        g_signal_emit (obj,
                       DBUS_NETWORK_OBJECT_GET_CLASS(obj)->subscriber_numbers,
                       0, number);


}


/*****************************Methods******************************************/

gboolean smartphone_network_get_country_code(DBusNetworkObject *obj,
                                             DBusGMethodInvocation *method_context)
{
        obj->network->get_country_code(obj->network, method_context);
        return TRUE;
}


gboolean smartphone_network_register(DBusNetworkObject *obj,
                                     DBusGMethodInvocation *method_context)
{
        obj->network->register_network(obj->network, method_context);
        return TRUE;
}

gboolean smartphone_network_unregister(DBusNetworkObject *obj,
                                       DBusGMethodInvocation *method_context)
{
        obj->network->unregister(obj->network, method_context);
        return TRUE;
}


gboolean smartphone_network_get_status(DBusNetworkObject *obj,
                                       DBusGMethodInvocation *method_context)
{
        obj->network->get_status(obj->network,
                                 method_context);
        return TRUE;
}

gboolean smartphone_network_list_providers(DBusNetworkObject *obj,
                                           DBusGMethodInvocation *method_context)
{
        obj->network->list_providers(obj->network,
                                     method_context);

        return TRUE;
}

gboolean smartphone_network_register_with_provider(DBusNetworkObject *obj,
                                                   const gint index,
                                                   DBusGMethodInvocation *method_context)
{
        obj->network->register_with_provider(obj->network,
                                             method_context,
                                             index);
        return TRUE;
}




/*****************************Method replies***********************************/
void smartphone_network_get_country_code_reply(NetworkIPCInterface *network_ipc,
                                               gpointer ipc_data,
                                               const char *dial_code)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     dial_code);
}

void smartphone_network_unregister_reply(NetworkIPCInterface *network_ipc,
                                         gpointer ipc_data)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_network_get_home_country_code_reply(NetworkIPCInterface *network_ipc,
                                                    gpointer ipc_data,
                                                    const char *dial_code)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     dial_code);
}

void smartphone_network_register_reply(NetworkIPCInterface *network_ipc, gpointer ipc_data)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_network_get_status_reply(NetworkIPCInterface *network_ipc,
                                         gpointer ipc_data,
                                         GHashTable *status)
{

        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     status);
}




void smartphone_network_list_providers_reply(NetworkIPCInterface *network_ipc,
                                             gpointer ipc_data,
                                             GArray *providers)
{
        g_debug("%s", __func__);
        GPtrArray *result = NULL;
        guint i;
        NetworkProvider *provider = NULL;

        result = g_ptr_array_sized_new (providers->len);

        for (i = 0; i < providers->len; i++) {
                provider = g_array_index(providers,NetworkProvider*,i);
                GValue entry = { 0, };


                g_value_init (&entry, GSMD_NETWORK_LISTPROVIDER_TYPE);
                g_value_take_boxed (&entry,
                                    dbus_g_type_specialized_construct (GSMD_NETWORK_LISTPROVIDER_TYPE));

                dbus_g_type_struct_set (&entry,
                                        0, provider->index,
                                        1, provider->status,
                                        2, provider->name,
                                        3, provider->nickname,
                                        G_MAXUINT);

                g_ptr_array_add (result, g_value_get_boxed (&entry));
        }

        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data, result);
        g_ptr_array_free (result, TRUE);

}

void smartphone_network_register_with_provider_reply(NetworkIPCInterface *network_ipc,
                                                     gpointer ipc_data)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}



/**************************Method error replies********************************/
void smartphone_network_get_country_code_error(NetworkIPCInterface *network_ipc,
                                               gpointer ipc_data,
                                               GError *error)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_network_get_home_country_code_error(NetworkIPCInterface *network_ipc,
                                                    gpointer ipc_data,
                                                    GError *error)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_network_register_error(NetworkIPCInterface *network_ipc,
                                       gpointer ipc_data,
                                       GError *error)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_network_unregister_error(NetworkIPCInterface *network_ipc,
                                         gpointer ipc_data,
                                         GError *error)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_network_get_status_error(NetworkIPCInterface *network_ipc,
                                         gpointer ipc_data,
                                         GError *error)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_network_list_providers_error(NetworkIPCInterface *network_ipc,
                                             gpointer ipc_data,
                                             GError *error)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_network_register_with_provider_error(NetworkIPCInterface *network_ipc,
                                                     gpointer ipc_data,
                                                     GError *error)
{
        g_debug("%s", __func__);
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}





DBusNetworkObject *gsmd_dbus_network_initialize (   NetworkIPCInterface *network_ipc,
                                                    NetworkInterface *network)
{
        DBusNetworkObject *obj = g_object_new (DBUS_NETWORK_TYPE_OBJECT, NULL);
        network_ipc->priv = obj;
        obj->network = network;

        //Signals
        network_ipc->status = &smartphone_network_status;
        network_ipc->subscriber_numbers = &smartphone_network_subscriber_numbers;

        //Replies
        network_ipc->register_network_reply = &smartphone_network_register_reply;
        network_ipc->unregister_reply = &smartphone_network_unregister_reply;
        network_ipc->get_status_reply = &smartphone_network_get_status_reply;
        network_ipc->list_providers_reply = &smartphone_network_list_providers_reply;
        network_ipc->register_with_provider_reply = &smartphone_network_register_with_provider_reply;
        network_ipc->get_country_code_reply = &smartphone_network_get_country_code_reply;

        //Errors
        network_ipc->register_network_error = &smartphone_network_register_error;
        network_ipc->unregister_error = &smartphone_network_unregister_error;
        network_ipc->get_status_error = &smartphone_network_get_status_error;
        network_ipc->list_providers_error = &smartphone_network_list_providers_error;
        network_ipc->register_with_provider_error = &smartphone_network_register_with_provider_error;
        network_ipc->get_country_code_error = &smartphone_network_get_country_code_error;





        return obj;
}
