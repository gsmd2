/*
 *  main.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

/**
 * @mainpage GSMD2
 *
 * \section intro Introduction
 *
 * This is the documentation of GSM daemon called GSMD2.
 *
 * \section install_sec Installation
 *
 * ...
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <syslog.h>
#include <dbus/dbus-glib-bindings.h>
#include <glib-object.h>
#include <glib/gquark.h>
#include <gmodule.h>
#include "modem_internal.h"
#include "dbus_objects.h"

#define USE_SYSLOG 	1
#define RUNNING_DIR	"/"
#define LOCK_FILE	"/var/lock/LCK..gsmd2.lock"

#define DEFAULT_VENDOR "generic"
#define DEFAULT_SERIAL_PLUGIN "generic"
#define DEFAULT_DEVICE "/dev/ttyS2"
#define DEFAULT_CONFFILE "/etc/gsmd2/gsmd2.conf"
static gchar **devices = NULL;
static gchar *vendor = DEFAULT_VENDOR;
static gchar *serial_plugin = DEFAULT_SERIAL_PLUGIN;
static gchar *conffile = DEFAULT_CONFFILE;
static gboolean no_conf  = FALSE;               //Should the conf file be read
static gboolean no_cache  = FALSE;               //Should we cache values
static gboolean no_daemon = FALSE;  //Should the process be daemon (FALSE)
                                        //or a normal process (TRUE)
static gint interface_devices[INTERFACE_LAST] = {0};

//Command line parameters
static GOptionEntry options[] = {
        { "conf", 'c',G_OPTION_FLAG_FILENAME , G_OPTION_ARG_STRING, &conffile,
          "The configuration file to use. "
          "The configuration file defaults to '" DEFAULT_CONFFILE "'.", NULL },
        { "no-conf", 0, 0, G_OPTION_ARG_NONE, &no_conf,
          "Do not try to read configuration file. "
          "The configuration file is read by default.", NULL },
        { "no-cache", 0, 0, G_OPTION_ARG_NONE, &no_cache,
          "Do not cache any responses."
          "The configuration file is read by default.", NULL },
        { "device", 'd', 0, G_OPTION_ARG_STRING_ARRAY, &devices,
          "The device of the modem. "
          "The device defaults to '" DEFAULT_DEVICE "'.", NULL },
        { "vendor", 'v', 0, G_OPTION_ARG_STRING, &vendor,
          "The modem vendor. "
          "The vendor defaults to '" DEFAULT_VENDOR "'.", NULL },
        { "serial", 's', 0, G_OPTION_ARG_STRING, &serial_plugin,
          "The serial plugin. "
          "The vendor defaults to '" DEFAULT_SERIAL_PLUGIN "'.", NULL },
        { "no-daemon", 'n', 0, G_OPTION_ARG_NONE, &no_daemon,
          "Run without making this process a daemon"
          "", NULL},
        { "general_index",0, 0, G_OPTION_ARG_INT, &interface_devices[INTERFACE_GENERAL],
          "Specify index of the the device to be used in general communication"
          "", NULL},
        { "call_index",0, 0, G_OPTION_ARG_INT, &interface_devices[INTERFACE_CALL],
          "Specify the index of the device used in call interface's communication"
          "", NULL},
        { "device_index",0, 0, G_OPTION_ARG_INT, &interface_devices[INTERFACE_DEVICE],
          "Specify the index of the device used in device interface's communication"
          "", NULL},
        { "network_index",0, 0, G_OPTION_ARG_INT, &interface_devices[INTERFACE_NETWORK],
          "Specify the index of the device used in network interface's communication"
          "", NULL},
        { "pdp_index",0, 0, G_OPTION_ARG_INT, &interface_devices[INTERFACE_PDP],
          "Specify the index of the device used in pdp interface's communication"
          "", NULL},
        { "sim_index",0, 0, G_OPTION_ARG_INT, &interface_devices[INTERFACE_SIM],
          "Specify the index of the device used in sim interface's communication"
          "", NULL},
        { "sms_index",0, 0, G_OPTION_ARG_INT, &interface_devices[INTERFACE_SMS],
          "Specify the index of the device used in sms interface's communication"
          "", NULL},
        { NULL }
};

GMainLoop *loop = NULL;


/**
* @brief Wrapper to the syslog function
*
* @param priority priority of the message to log
* @param message message to log
*/
void syslog_message(int priority, const char *message)
{
#ifdef USE_SYSLOG
        syslog(priority, message);
#endif
}

/**
* @brief Custom signal handler to SIGHUP,SIGTERM,SIGQUIT and SIGINT.
*
* - SIGHUP writes to syslog
* - SIGTEMR/SIGQUIT/SIGINT quits main loop and closes the log.
*
* @param sig signal to handle
*/
void signal_handler(int sig)
{
        switch (sig) {
        case SIGHUP:
                syslog_message(LOG_INFO, "SIGHUP signal catched");
                break;
        case SIGTERM:
        case SIGQUIT:
        case SIGINT: {
                closelog();
                if (loop && g_main_loop_is_running(loop))
                        g_main_loop_quit( loop );
                syslog_message(LOG_INFO, "SIGTERM, SIGINT or SIGQUIT signal catched. Daemon stopped.");
                exit(0);
        }
        break;
        }
}

static
GKeyFile* gsmd_read_configuration_file(const gchar *file)
{
        GKeyFile *conf = g_key_file_new();
        GError *error = NULL;
        gchar **string_list = NULL;
        gchar *string = NULL;
        gboolean bool = FALSE;
        if( !g_key_file_load_from_file(conf, conffile, G_KEY_FILE_NONE, &error) ) {
                g_warning("%s : %s", __func__, error->message);
                g_error_free( error );
                g_key_file_free( conf );
                return NULL;
        }
        string_list = g_key_file_get_string_list(conf, "Default", "devices", NULL, NULL);
        if( string_list ) {
                if(  !devices ) {
                        g_debug("%s : Using devices from %s", __func__, conffile);
                        devices = string_list;
                        string_list = NULL;
                } else {
                        g_strfreev( string_list );
                }
        }
        string = g_key_file_get_string(conf, "Default", "vendor",NULL);
        if( string ) {
                if( (gpointer)vendor == (gpointer)DEFAULT_VENDOR) {
                        g_debug("%s : Using vendor %s from %s", __func__, string, conffile);
                        vendor = string;
                } else {
                        g_free( string );
                }
        }

        string = g_key_file_get_string(conf, "Default", "serial",NULL);
        if( string ) {
                if( (gpointer)serial_plugin == (gpointer)DEFAULT_SERIAL_PLUGIN) {
                        g_debug("%s : Using serial plugin %s from %s", __func__, string, conffile);
                        serial_plugin = string;
                } else {
                        g_free( string );
                }
        }

        bool = g_key_file_get_boolean(conf, "Default", "no-daemon",&error);
        if( error == NULL) {
                g_debug("%s : no-daemon: %s", __func__, bool ? "yes": "no");
                no_daemon = bool;
        } else {
                g_debug("%s : no-daemon error: %s", __func__, error->message);
                g_error_free( error );
                error = NULL;
        }
        bool = g_key_file_get_boolean(conf, "Default", "no-cache",&error);
        if( error == NULL) {
                g_debug("%s : no-cache: %s", __func__, bool ? "yes": "no");
                no_cache = bool;
        } else {
                g_debug("%s : no-cache error: %s", __func__, error->message);
                g_error_free( error );
                error = NULL;
        }
        return conf;
}

/**
* @brief Daemonizes the process.
*
*/
void daemonize(void)
{
        int i = 0;
        int lock_file = 0;
        char str[10];

#ifdef USE_SYSLOG
        openlog("gsmd2", (LOG_CONS|LOG_PERROR|LOG_PID), LOG_DAEMON);
#endif

        i = fork();

        /* error in fork */
        if (i < 0) {
                syslog_message(LOG_ERR, "Can not fork process! Daemon not started.");
                exit(1);
        }

        /* parent exits */
        if (i > 0)
                exit(0);

        /* daemon must have own process group */
        setsid();

        /* close all open descriptors */
        for (i=getdtablesize(); i>=0; --i)
                close(i);

        /* Redirect standard files to /dev/null */
        freopen( "/dev/null", "r", stdin);
        freopen( "/dev/null", "w", stdout);
        freopen( "/dev/null", "w", stderr);

        /* change running directory to "/" */
        chdir(RUNNING_DIR);
        lock_file = open(LOCK_FILE, O_RDWR|O_CREAT, 0640);

        /* can't open lock file*/
        if (lock_file < 0) {
                syslog_message(LOG_ERR, "Can't open lock file");
                exit(1);
        }

        /* can't lock */
        if (lockf(lock_file, F_TLOCK, 0) < 0) {
                syslog_message(LOG_ERR, "Can't set F_TLOCK to the lock file.");
                syslog_message(LOG_ERR, "Daemon is probably already running!");
                exit(0);
        }

        /* record program ID to lockfile */
        sprintf(str, "%d\n", getpid());
        write(lock_file, str, strlen(str));

        /* set signal handlers */
        signal(SIGCHLD, SIG_IGN);
        signal(SIGTSTP, SIG_IGN);
        signal(SIGTTOU, SIG_IGN);
        signal(SIGTTIN, SIG_IGN);
        signal(SIGHUP, signal_handler);
        signal(SIGTERM, signal_handler);
        signal(SIGQUIT, signal_handler);
        signal(SIGINT, signal_handler);

        syslog_message(LOG_INFO, "daemon is running successfully");
}

int main(int argc, char**argv)
{
        GOptionContext *ctx = NULL;
        GError *error = NULL;
        ModemInterface* modem = NULL;
        GKeyFile *conf = NULL;
        if(!g_module_supported()) {
                g_critical("%s : Current platform does not support GModule",__func__);
                exit(1);
        }

        /* To be able to use GObjects or GType's */
        g_type_init ();

        //Parse commandline parameters
        ctx = g_option_context_new(NULL);
        g_option_context_add_main_entries (ctx, options, NULL);

        if (!g_option_context_parse (ctx, &argc, &argv, &error)) {
                g_print ("%s\n", error->message);
                exit(1);
        }
        g_option_context_free(ctx);

        if( !no_conf ) {
                conf = gsmd_read_configuration_file(conffile);
                if( !conf ) {
                        g_warning("%s : Failed to read configuration file '%s'", __func__, conffile);
                }
        }

        //Check if we need to daemonize the process
        if (!no_daemon) {
                g_debug("demonizing");
                daemonize();
        }

        //Create a main loop
        loop = g_main_loop_new (NULL, FALSE);
        if (!loop) {
                g_warning("Loop was NULL");
                exit (1);
        }

        //Create a new modem device
        if (devices == NULL) {
            devices = g_new0(char*,2);
            devices[0] = DEFAULT_DEVICE;
        }

        modem = gsmd_modem_open (devices,
                                 vendor,
                                 serial_plugin,
                                 no_cache,
                                 conf,
                                 &gsmd_dbus_initialize,
                                 interface_devices);
        g_free(devices);


        if (!modem) {
                g_warning ("cannot open modem\n");
                exit (1);
        }

        //Start the main loop
        g_main_loop_run (loop);
        gsmd_modem_free (modem);
        gsmd_dbus_uninitialize((DBusObjects*)modem->ipc_data);


        return 0;
}

