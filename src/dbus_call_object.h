/*
 *  dbus_call_object.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef DBUS_CALL_OBJECT_H
#define DBUS_CALL_OBJECT_H


#include <glib-object.h>
#include "dbus_objects.h"

typedef struct DBusCallObject DBusCallObject;
typedef struct DBusCallObjectClass DBusCallObjectClass;

GType dbus_call_object_get_type (void);


struct DBusCallObject {
        GObject parent;
        CallInterface *call;
};

struct DBusCallObjectClass {
        GObjectClass parent;
        guint call_status;
};

/* Macros that are needed for making the proper GObject */
#define DBUS_CALL_TYPE_OBJECT              (dbus_call_object_get_type ())
#define DBUS_CALL_OBJECT(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), DBUS_CALL_TYPE_OBJECT, DBusCallObject))
#define DBUS_CALL_OBJECT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), DBUS_CALL_TYPE_OBJECT, DBusCallObjectClass))
#define DBUS_IS_CALL_OBJECT(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), DBUS_CALL_TYPE_OBJECT))
#define DBUS_IS_CALL_OBJECT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), DBUS_CALL_TYPE_OBJECT))
#define DBUS_CALL_OBJECT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), DBUS_CALL_TYPE_OBJECT, DBusCallObjectClass))

DBusCallObject *gsmd_dbus_call_initialize ( CallIPCInterface *call_ipc,
                                            CallInterface *call );
void gsmd_dbus_call_uninitialize( DBusCallObject *obj );



/* Declarations for the API methods */
gboolean smartphone_call_emergency( DBusCallObject *obj,
                                    DBusGMethodInvocation *method_context);

gboolean smartphone_call_activate( DBusCallObject *obj,
                                   gint id,
                                   DBusGMethodInvocation *method_context);

gboolean smartphone_call_activate_conference(DBusCallObject *obj,
                                             gint id,
                                             DBusGMethodInvocation *method_context);

gboolean smartphone_call_hold_active(DBusCallObject *obj,
                                     DBusGMethodInvocation *method_context);

gboolean smartphone_call_join(DBusCallObject *obj,
                              DBusGMethodInvocation *method_context);

gboolean smartphone_call_transfer(DBusCallObject *obj,
                                  const gchar *number,
                                  DBusGMethodInvocation *method_context);

gboolean smartphone_call_release( DBusCallObject *obj,
                                  const gchar *message,
                                  const gint id,
                                  DBusGMethodInvocation *method_context);

gboolean smartphone_call_initiate( DBusCallObject *obj,
                                   const gchar *number,const gchar *type,
                                   DBusGMethodInvocation *method_context);

gboolean smartphone_call_get_status(DBusCallObject *obj,
                                    const gint status,
                                    DBusGMethodInvocation *method_context);

gboolean smartphone_call_send_dtmf( DBusCallObject *obj,
                                    DBusGMethodInvocation *method_context);

gboolean smartphone_call_release_all(DBusCallObject *obj,
                                     const gchar *message,
                                     DBusGMethodInvocation *method_context);

gboolean smartphone_call_list_active(DBusCallObject *obj,
                                     DBusGMethodInvocation *method_context);

gboolean smartphone_call_get_subscriber_numbers(DBusCallObject *obj,
                                                DBusGMethodInvocation *method_context);

gboolean smartphone_call_release_held(DBusCallObject *obj,
                                      const gchar *message,
                                      DBusGMethodInvocation *method_context);

gboolean smartphone_call_list_calls(DBusCallObject *obj,
                                    DBusGMethodInvocation *method_context);

#endif
