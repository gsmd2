/*
 *  dbus_pdp_object.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include "dbus_pdp_object.h"
#include "dbus_pdp_glue.h"
#include "dbus_marshal.h"
#include "dbus_objects.h"
#include "pdp_interface.h"
#include <stdlib.h>
#include <stdio.h>

G_DEFINE_TYPE(DBusPDPObject, dbus_pdp_object, G_TYPE_OBJECT)



static void
dbus_pdp_object_class_init(DBusPDPObjectClass *klass)
{
        /* Make the local system aware of our kind */

        /** Register our signals so that they can be emitted */
        klass->context_activated = g_signal_new("context_activated",
                                                G_OBJECT_CLASS_TYPE (klass),
                                                G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                                                0, NULL, NULL, g_cclosure_marshal_VOID__INT,
                                                G_TYPE_NONE, 1, G_TYPE_INT);

        klass->context_deactivated = g_signal_new("context_deactivated",
                                     G_OBJECT_CLASS_TYPE (klass),
                                     G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                                     0, NULL, NULL, g_cclosure_marshal_VOID__INT,
                                     G_TYPE_NONE, 1, G_TYPE_INT);

        klass->context_changed = g_signal_new("context_changed",
                                              G_OBJECT_CLASS_TYPE (klass),
                                              G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                                              0, NULL, NULL, g_cclosure_marshal_VOID__INT,
                                              G_TYPE_NONE, 1, G_TYPE_INT);
}


static void dbus_pdp_object_init(DBusPDPObject *gsm)
{
        dbus_g_object_type_install_info (DBUS_PDP_TYPE_OBJECT,
                                         &dbus_glib_smartphone_pdp_object_info);
}


void dbus_pdp_object_finalize(GObject *obj)
{
        //G_OBJECT_CLASS (parent_class)->finalize (obj);
        //DBusPDPObject *gsm = (DBusPDPObject *) obj;
}



void gsmd_dbus_pdp_uninitialize(DBusPDPObject *obj)
{
        g_object_unref(obj);
}



/*********************Signal functions***********************/
void smartphone_pdp_context_activated (PDPIPCInterface *pdp_ipc,
                                       const gint id)
{
        DBusPDPObject *obj = (DBusPDPObject *) pdp_ipc->priv;
        g_signal_emit (obj,
                       DBUS_PDP_OBJECT_GET_CLASS(obj)->context_activated,
                       0, id);
}

void smartphone_pdp_context_deactivated (PDPIPCInterface *pdp_ipc,
                const gint id)
{

        DBusPDPObject *obj = (DBusPDPObject *) pdp_ipc->priv;
        g_signal_emit (obj,
                       DBUS_PDP_OBJECT_GET_CLASS(obj)->context_deactivated,
                       0, id);
}

void smartphone_pdp_context_changed (PDPIPCInterface *pdp_ipc,
                                     const gint id)
{

        DBusPDPObject *obj = (DBusPDPObject *) pdp_ipc->priv;
        g_signal_emit (obj,
                       DBUS_PDP_OBJECT_GET_CLASS(obj)->context_changed,
                       0, id);
}





/***********************Methods**************************************/
gboolean smartphone_pdp_list_gprs_classes(  DBusPDPObject *obj,
                DBusGMethodInvocation *method_context)
{
        obj->pdp->list_gprs_classes(obj->pdp,
                                           method_context);
        return TRUE;
}

gboolean smartphone_pdp_select_gprs_class(  DBusPDPObject *obj,
                const gchar *klass,
                DBusGMethodInvocation *method_context)
{
        obj->pdp->select_gprs_class(obj->pdp,
                                           method_context,
                                           klass);
        return TRUE;
}

gboolean smartphone_pdp_activate(   DBusPDPObject *obj,
                                    const gint index,
                                    DBusGMethodInvocation *method_context)
{
        obj->pdp->activate(obj->pdp,
                                  method_context,
                                  index);
        return TRUE;
}

gboolean smartphone_pdp_deactivate( DBusPDPObject *obj,
                                    const gint index,
                                    DBusGMethodInvocation *method_context)
{
        obj->pdp->deactivate(obj->pdp,
                                    method_context,
                                    index);
        return TRUE;
}

gboolean smartphone_pdp_select_context( DBusPDPObject *obj,
                                        const gchar *context,
                                        DBusGMethodInvocation *method_context)
{
        obj->pdp->select_context(obj->pdp,
                                        method_context,
                                        context);
        return TRUE;
}

gboolean smartphone_pdp_add_context(DBusPDPObject *obj,
                                    const gchar *context,
                                    DBusGMethodInvocation *method_context)
{
        obj->pdp->add_context(obj->pdp,
                                     method_context,
                                     context);

        return TRUE;
}

gboolean smartphone_pdp_delete_context( DBusPDPObject *obj,
                                        const gint index,
                                        DBusGMethodInvocation *method_context)
{
        obj->pdp->delete_context(obj->pdp,
                                        method_context,
                                        index);

        return TRUE;
}

gboolean smartphone_pdp_list_contexts(  DBusPDPObject *obj,
                                        DBusGMethodInvocation *method_context)
{
        obj->pdp->list_contexts(obj->pdp,
                                       method_context);
        return TRUE;
}


/*****************************Method replies***********************************/
void smartphone_pdp_list_gprs_classes_reply(PDPIPCInterface *pdp_ipc,
                gpointer ipc_data,
                const char **classes)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     classes);
}

void smartphone_pdp_select_gprs_class_reply(PDPIPCInterface *pdp_ipc,
                gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_pdp_activate_reply(PDPIPCInterface *pdp_ipc,
                                   gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_pdp_deactivate_reply(PDPIPCInterface *pdp_ipc,
                                     gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_pdp_select_context_reply(PDPIPCInterface *pdp_ipc,
                gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_pdp_add_context_reply(PDPIPCInterface *pdp_ipc,
                                      gpointer ipc_data,
                                      int index)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     index);
}

void smartphone_pdp_delete_context_reply(PDPIPCInterface *pdp_ipc,
                gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_pdp_list_contexts_reply(PDPIPCInterface *pdp_ipc,
                                        gpointer ipc_data,
                                        const char **contexts)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     contexts);
}


/*****************************Method error replies*****************************/
void smartphone_pdp_list_gprs_classes_error(PDPIPCInterface *pdp_ipc,
                gpointer ipc_data,
                GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_pdp_select_gprs_class_error(PDPIPCInterface *pdp_ipc,
                gpointer ipc_data,
                GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_pdp_activate_error(PDPIPCInterface *pdp_ipc,
                                   gpointer ipc_data,
                                   GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_pdp_deactivate_error(PDPIPCInterface *pdp_ipc,
                                     gpointer ipc_data,
                                     GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_pdp_select_context_error(PDPIPCInterface *pdp_ipc,
                gpointer ipc_data,
                GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_pdp_add_context_error(PDPIPCInterface *pdp_ipc,
                                      gpointer ipc_data,
                                      GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_pdp_delete_context_error(PDPIPCInterface *pdp_ipc,
                gpointer ipc_data,
                GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_pdp_list_contexts_error(PDPIPCInterface *pdp_ipc,
                                        gpointer ipc_data,
                                        GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}



DBusPDPObject *gsmd_dbus_pdp_initialize (PDPIPCInterface *pdp_ipc, PDPInterface *pdp)
{
        DBusPDPObject *obj = g_object_new (DBUS_PDP_TYPE_OBJECT, NULL);
        pdp_ipc->priv = obj;
        obj->pdp = pdp;

        pdp_ipc->list_gprs_classes_reply = &smartphone_pdp_list_gprs_classes_reply;
        pdp_ipc->select_gprs_class_reply = &smartphone_pdp_select_gprs_class_reply;
        pdp_ipc->activate_reply = &smartphone_pdp_activate_reply;
        pdp_ipc->deactivate_reply = &smartphone_pdp_deactivate_reply;
        pdp_ipc->select_context_reply = &smartphone_pdp_select_context_reply;
        pdp_ipc->add_context_reply = &smartphone_pdp_add_context_reply;
        pdp_ipc->delete_context_reply = &smartphone_pdp_delete_context_reply;
        pdp_ipc->list_contexts_reply = &smartphone_pdp_list_contexts_reply;

        pdp_ipc->list_gprs_classes_error = &smartphone_pdp_list_gprs_classes_error;
        pdp_ipc->select_gprs_class_error = &smartphone_pdp_select_gprs_class_error;
        pdp_ipc->activate_error = &smartphone_pdp_activate_error;
        pdp_ipc->deactivate_error = &smartphone_pdp_deactivate_error;
        pdp_ipc->select_context_error = &smartphone_pdp_select_context_error;
        pdp_ipc->add_context_error = &smartphone_pdp_add_context_error;
        pdp_ipc->delete_context_error = &smartphone_pdp_delete_context_error;
        pdp_ipc->list_contexts_error = &smartphone_pdp_list_contexts_error;


        return obj;
}
