/*
 *  network_interface.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef NETWORK_INTERFACE
#define NETWORK_INTERFACE

typedef struct _NetworkInterface NetworkInterface;


typedef struct _NetworkProvider {
        gint index;
        gchar *status;
        gchar *name;
        gchar *nickname;
} NetworkProvider;

/**
* @brief Network interface
*/
struct _NetworkInterface {
        gpointer priv;

        /**
        * @brief Provider's name
        */
        GString *provider_name;

        /**
        * @brief Provider status
        */
        GString *status;



        //TODO remove these/modify them to comply with freesmartphone's xml/modify freesmartphone's xml
        void (*set_operator) (NetworkInterface *network,
                              gpointer ipc_data,
                              const char* operator,
                              gboolean locked);
        void (*query_current_operator) (NetworkInterface *network,
                                        gpointer ipc_data);
        void (*query_signal_strength) (NetworkInterface *network,
                                       gpointer ipc_data);

        /**
        * @brief Registers to network
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*register_network) (NetworkInterface *network,
                                  gpointer ipc_data);

        /**
        * @brief Unregisters from network
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*unregister) (NetworkInterface *network,
                            gpointer ipc_data);

        /**
        * @brief Gets network status
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*get_status) (NetworkInterface *network,
                            gpointer ipc_data);

        /**
        * @brief Lists providers
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*list_providers) (NetworkInterface *network,
                                gpointer ipc_data);

        /**
        * @brief Registers with provider
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param index provider's index
        */
        void (*register_with_provider) (NetworkInterface *network,
                                        gpointer ipc_data,
                                        int index);


        /**
        * @brief Get country code
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param index provider's index
        */
        void (*get_country_code) (NetworkInterface *network,
                                  gpointer ipc_data);





};

typedef struct _NetworkIPCInterface NetworkIPCInterface;

struct _NetworkIPCInterface {

        gpointer priv;


        /*********************Replies to methods*****************/

        /**
        * @brief Reply to "register_network" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*register_network_reply) (NetworkIPCInterface *network_ipc,
                                        gpointer ipc_data);

        /**
        * @brief Reply to "unregister" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*unregister_reply) (NetworkIPCInterface *network_ipc,
                                  gpointer ipc_data);

        /**
        * @brief Reply to "get_status" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param provider_name
        * @param status
        * @param strength
        */
        void (*get_status_reply) (NetworkIPCInterface *network_ipc,
                                  gpointer ipc_data,
                                  GHashTable *status);

        /**
        * @brief Reply to "list_providers" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param providers list of network providers
        */
        void (*list_providers_reply) (NetworkIPCInterface *network_ipc,
                                      gpointer ipc_data,
                                      GArray *providers);

        /**
        * @brief Reply to "register_with_provider" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*register_with_provider_reply) (NetworkIPCInterface *network_ipc,
                                              gpointer ipc_data);


        /**
        * @brief Reply to "get_country_code" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*get_country_code_reply) (NetworkIPCInterface *network_ipc,
                                        gpointer ipc_data,
                                        const gchar *dial_code);





        /*********************Method errors**********************************/

        /**
        * @brief Error reply to "register_network" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*register_network_error) (NetworkIPCInterface *network_ipc,
                                        gpointer ipc_data,
                                        GError *error);

        /**
        * @brief Error reply to "unregister" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*unregister_error) (NetworkIPCInterface *network_ipc,
                                  gpointer ipc_data,
                                  GError *error);

        /**
        * @brief Error reply to "get_status" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*get_status_error) (NetworkIPCInterface *network_ipc,
                                  gpointer ipc_data,
                                  GError *error);

        /**
        * @brief Error reply to "list_providers" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*list_providers_error) (NetworkIPCInterface *network_ipc,
                                      gpointer ipc_data,
                                      GError *error);

        /**
        * @brief Error reply to "register_with_provider" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*register_with_provider_error) (NetworkIPCInterface *network_ipc,
                                              gpointer ipc_data,
                                              GError *error);

        /**
        * @brief Error reply to "get_country_code" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*get_country_code_error) (NetworkIPCInterface *network_ipc,
                                        gpointer ipc_data,
                                        GError *error);




        /*********************Signals**********************************/
        /**
        * @brief Status signal
        *
        * @param modem pointer to modem struct
        * @param provider_name
        * @param status
        * @param strength signal strength
        */
        void (*status) (NetworkIPCInterface *network_ipc,
                        GHashTable *status);

        /**
        * @brief Subscriber numbers
        *
        * @param modem pointer to modem struct
        * @param number
        */
        void (*subscriber_numbers) (NetworkIPCInterface *network_ipc,
                                    const char **number);


};

#endif



