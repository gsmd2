/*
 *  dbus_call_object.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include "dbus_call_object.h"
#include "dbus_call_glue.h"
#include "dbus_marshal.h"
#include "call_interface.h"
#include <stdlib.h>
#include <stdio.h>

G_DEFINE_TYPE(DBusCallObject, dbus_call_object, G_TYPE_OBJECT)

#define GSMD_CALL_STATUS_PROPERTIES_TYPE (dbus_g_type_get_map ("GHashTable", \
      G_TYPE_STRING, G_TYPE_VALUE))

#define GSMD_CALL_LIST_CALLS_TYPE (dbus_g_type_get_struct ("GValueArray", \
      G_TYPE_INT, \
      G_TYPE_STRING, \
      G_TYPE_HASH_TABLE, \
      G_TYPE_INVALID))





static void
dbus_call_object_class_init(DBusCallObjectClass *klass)
{
    /* Make the local system aware of our kind */

    /** Register our signals so that they can be emitted */
    klass->call_status =
        g_signal_new("call_status",
                     G_OBJECT_CLASS_TYPE (klass),
                     G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                     0, NULL, NULL, gsmd_smartphone_marshaller_VOID__INT_STRING_POINTER,
                     G_TYPE_NONE, 3, G_TYPE_INT,G_TYPE_STRING,GSMD_CALL_STATUS_PROPERTIES_TYPE);
}


static void dbus_call_object_init(DBusCallObject *gsm)
{
    dbus_g_object_type_install_info (DBUS_CALL_TYPE_OBJECT,
                                     &dbus_glib_smartphone_call_object_info);
}


void dbus_call_object_finalize(GObject *obj)
{
    //G_OBJECT_CLASS (parent_class)->finalize (obj);
    //DBusCallObject *gsm = (DBusCallObject *) obj;
}






/*********************Signal functions***********************/


void smartphone_call_status (CallIPCInterface *call_ipc,
                             const gint id,
                             const gchar *status,
                             GHashTable *properties)
{
    g_debug("%s: id: %d, status: %s, properties: %p",
            __func__,
            id,
            status,
            properties);


    DBusCallObject *obj = (DBusCallObject *) call_ipc->priv;
    g_signal_emit (obj,
                   DBUS_CALL_OBJECT_GET_CLASS(obj)->call_status,
                   0,id,status,properties);
}



/*************************Methods**********************************************/
gboolean smartphone_call_activate(DBusCallObject *obj,
                                  gint id,
                                  DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->activate(obj->call,method_context, id);
    return TRUE;
}

gboolean smartphone_call_activate_conference(DBusCallObject *obj,
                                             gint id,
                                             DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->activate_conference(obj->call,method_context, id);
    return TRUE;
}

gboolean smartphone_call_initiate(DBusCallObject *obj,
                                  const gchar *number,
                                  const gchar *type,
                                  DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->initiate(obj->call,method_context, number,type);
    return TRUE;
}

gboolean smartphone_call_emergency(DBusCallObject *obj,
                                   DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->emergency(obj->call,method_context);
    return TRUE;
}

gboolean smartphone_call_release(DBusCallObject *obj,
                                 const gchar *message, const gint id,
                                 DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->release(obj->call,method_context,message,id);
    return TRUE;
}

gboolean smartphone_call_hold_active(DBusCallObject *obj,
                                     DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->hold_active(obj->call,method_context);
    return TRUE;
}

gboolean smartphone_call_join(DBusCallObject *obj,
                              DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->join(obj->call,method_context);
    return TRUE;
}

gboolean smartphone_call_transfer(DBusCallObject *obj,
                                  const gchar *number,
                                  DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->transfer(obj->call,method_context,number);
    return TRUE;
}

gboolean smartphone_call_release_all(DBusCallObject *obj,
                                     const gchar *message,
                                     DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->release_all(obj->call,method_context,message);
    return TRUE;
}

gboolean smartphone_call_release_held(DBusCallObject *obj,
                                      const gchar *message,
                                      DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->release_held(obj->call,method_context,message);
    return TRUE;
}



gboolean smartphone_call_list_calls(DBusCallObject *obj,
                                    DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);


    obj->call->list_calls(obj->call,method_context);
    return TRUE;
}



gboolean smartphone_call_send_dtmf(DBusCallObject *obj,
                                   DBusGMethodInvocation *method_context)
{
    g_debug("%s", __func__);
    obj->call->send_dtmf(obj->call,method_context);
    return TRUE;
}

/**************************Replies to method calls*************************/
/**
     * @brief sends a reply to ipc that phone call has been made
     * @param call_ipc pointer to call IPC interface
     * @param ipc_data data for ipc
     * @param id newly initiated phone call's id
     */
void smartphone_call_initiate_reply(CallIPCInterface *call_ipc,
                                    gpointer ipc_data,
                                    const gint id)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,id);
}



/**
 * @brief sends a reply to ipc that phone call has been activated
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param id newly initiated phone call's id
 */
void smartphone_call_activate_reply (CallIPCInterface *call_ipc,
                                     gpointer ipc_data)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

/**
 * @brief sends a reply to ipc that active and held calls have been joined and released
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param id newly initiated phone call's id
 */
void smartphone_call_join_reply (CallIPCInterface *call_ipc,
                                 gpointer ipc_data)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

/**
 * @brief sends a reply to ipc that phone call has been added to
 * conference
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param id newly initiated phone call's id
 */
void smartphone_call_activate_conference_reply (CallIPCInterface *call_ipc,
                                                gpointer ipc_data)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

/**
 * @brief sends a reply to ipc that a phone call has been released (hung up)
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 */
void smartphone_call_release_reply (CallIPCInterface *call_ipc,
                                    gpointer ipc_data)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
    g_debug("%s : Done",__func__);
}

/**
 * @brief sends a reply to ipc that all phone call have been released (hung up)
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 */
void smartphone_call_release_all_reply (CallIPCInterface *call_ipc,
                                        gpointer ipc_data)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

/**
 * @brief sends a reply to ipc that all held calls have been released
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 */
void smartphone_call_release_held_reply (CallIPCInterface *call_ipc,
                                         gpointer ipc_data)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

/**
 * @brief sends a reply to ipc that dtmf has been sent
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 */
void smartphone_call_send_dtmf_reply (CallIPCInterface *call_ipc,
                                      gpointer ipc_data,
                                      const char *mode)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

/**
 * @brief sends a reply to ipc with the emergency number
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param number emergency number
 */
void smartphone_call_emergency_reply(CallIPCInterface *call_ipc,
                                     gpointer ipc_data,
                                     const char* number)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,number);
}



/**
 * @brief send a reply to set DTMF duration function
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 */
void smartphone_call_set_dtmf_duration_reply (CallIPCInterface *call_ipc,
                                              gpointer ipc_data)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

/**
 * @brief send a reply to get DTMF duration function
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param duration dtmf duration
 */
void smartphone_call_get_dtmf_duration_reply (CallIPCInterface *call_ipc,
                                              gpointer ipc_data,
                                              const gint duration)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,duration);
}



/**
* @brief send a reply to list calls function
* @param call_ipc call IPC interface pointer
* @param ipc_data data for ipc
* @param calls active calls
*/
void smartphone_call_list_calls_reply (CallIPCInterface *call_ipc,
                                       gpointer ipc_data,
                                       GArray *calls)
{
    g_debug("%s", __func__);
    /*
    GPtrArray *result = NULL;
    guint i;
    gint index = 0;

    result = g_ptr_array_sized_new (calls->len);

    for (i = 0; i < calls->len; i++) {
            index = g_array_index(calls,gint,i);
            GValue entry = { 0, };


            g_value_init (&entry, GSMD_CALL_LIST_CALLS_TYPE);
            g_value_take_boxed (&entry,
                                dbus_g_type_specialized_construct (GSMD_CALL_LIST_CALLS_TYPE));

            dbus_g_type_struct_set (&entry,
                                    0, index,
                                    G_MAXUINT);

            g_ptr_array_add (result, g_value_get_boxed (&entry));
    }


    */

    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                             calls);
}

/**
* @brief send a reply to set send identification function
* @param call_ipc call IPC interface pointer
* @param ipc_data data for ipc
*/
void smartphone_call_set_send_identification_reply (CallIPCInterface *call_ipc,
                                                    gpointer ipc_data)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

/**
* @brief send a reply to get send identification function
* @param call_ipc call IPC interface pointer
* @param ipc_data data for ipc
*/
void smartphone_call_get_send_identification_reply (CallIPCInterface *call_ipc,
                                                    gpointer ipc_data,
                                                    gboolean mode)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                             mode);
}
/*********************Error replies to method calls************************/

/**
 * @brief sends an error reply to ipc as a response to join command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_join_error (CallIPCInterface *call_ipc,
                                 gpointer ipc_data,
                                 GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}


/**
 * @brief sends an error reply to ipc as a response to initiate command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_initiate_error (CallIPCInterface *call_ipc,
                                     gpointer ipc_data,
                                     GError *error)
{
    g_debug("%s", __func__);
    if (error)
        g_debug("%s : %s", __func__,error->message);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to activate command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_activate_error (CallIPCInterface *call_ipc,
                                     gpointer ipc_data,
                                     GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to activate conference command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_activate_conference_error (CallIPCInterface *call_ipc,
                                                gpointer ipc_data,
                                                GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to release command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_release_error (CallIPCInterface *call_ipc,
                                    gpointer ipc_data,
                                    GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}


/**
 * @brief sends an error reply to ipc as a response to release all command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_release_all_error (CallIPCInterface *call_ipc,
                                        gpointer ipc_data,
                                        GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to release held command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_release_held_error (CallIPCInterface *call_ipc,
                                         gpointer ipc_data,
                                         GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to send dtmf command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_send_dtmf_error (CallIPCInterface *call_ipc,
                                      gpointer ipc_data,
                                      GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to emergency command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_emergency_error (CallIPCInterface *call_ipc,
                                      gpointer ipc_data,
                                      GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data
                                   ,error);
}




/**
 * @brief sends an error reply to ipc as a response to set dtmf duration command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_set_dtmf_duration_error (CallIPCInterface *call_ipc,
                                              gpointer ipc_data,
                                              GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to get dtmf duration command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_get_dtmf_duration_error (CallIPCInterface *call_ipc,
                                              gpointer ipc_data,
                                              GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}


/**
 * @brief sends an error reply to ipc as a response to list active command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_list_active_error (CallIPCInterface *call_ipc,
                                        gpointer ipc_data,
                                        GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to list calls command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_list_calls_error (CallIPCInterface *call_ipc,
                                       gpointer ipc_data,
                                       GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

/**
 * @brief sends an error reply to ipc as a response to set send identification
 * command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_set_send_identification_error (CallIPCInterface *call_ipc,
                                                    gpointer ipc_data,
                                                    GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}


/**
 * @brief sends an error reply to ipc as a response to get send identification
 * command
 * @param call_ipc pointer to call IPC interface
 * @param ipc_data data for ipc
 * @param error pointer to GError describing error
*/
void smartphone_call_get_send_identification_error (CallIPCInterface *call_ipc,
                                                    gpointer ipc_data,
                                                    GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}








DBusCallObject *gsmd_dbus_call_initialize ( CallIPCInterface *call_ipc,
                                            CallInterface *call)
{
    DBusCallObject *obj = g_object_new (DBUS_CALL_TYPE_OBJECT, NULL);
    call_ipc->priv = obj;
    obj->call = call;

    //Signals
    call_ipc->call_status = &smartphone_call_status;

    //Method replies
    call_ipc->initiate_reply = &smartphone_call_initiate_reply;

    call_ipc->activate_reply = &smartphone_call_activate_reply;
    call_ipc->activate_conference_reply = &smartphone_call_activate_conference_reply;
    call_ipc->release_reply = &smartphone_call_release_reply;
    call_ipc->release_all_reply = &smartphone_call_release_all_reply;
    call_ipc->release_held_reply = &smartphone_call_release_held_reply;
    call_ipc->send_dtmf_reply = &smartphone_call_send_dtmf_reply;
    call_ipc->emergency_reply = &smartphone_call_emergency_reply;
    call_ipc->set_dtmf_duration_reply = &smartphone_call_set_dtmf_duration_reply;
    call_ipc->get_dtmf_duration_reply = &smartphone_call_get_dtmf_duration_reply;
    call_ipc->list_calls_reply = &smartphone_call_list_calls_reply;
    call_ipc->set_send_identification_reply = &smartphone_call_set_send_identification_reply;
    call_ipc->get_send_identification_reply = &smartphone_call_get_send_identification_reply;
    call_ipc->join_reply = &smartphone_call_join_reply;

    //Method errors
    call_ipc->initiate_error = &smartphone_call_initiate_error;
    call_ipc->activate_error = &smartphone_call_activate_error;
    call_ipc->activate_conference_error = &smartphone_call_activate_conference_error;
    call_ipc->release_error = &smartphone_call_release_error;
    call_ipc->send_dtmf_error = &smartphone_call_send_dtmf_error;
    call_ipc->emergency_error = &smartphone_call_emergency_error;
    call_ipc->set_dtmf_duration_error = &smartphone_call_set_dtmf_duration_error;
    call_ipc->get_dtmf_duration_error = &smartphone_call_get_dtmf_duration_error;
    call_ipc->list_calls_error = &smartphone_call_list_calls_error;
    call_ipc->release_all_error = &smartphone_call_release_all_error;
    call_ipc->join_error = &smartphone_call_join_error;
    call_ipc->release_held_error = &smartphone_call_release_held_error;
    call_ipc->set_send_identification_error = &smartphone_call_set_send_identification_error;
    call_ipc->get_send_identification_error = &smartphone_call_get_send_identification_error;




    return obj;
}

void gsmd_dbus_call_uninitialize(DBusCallObject *obj)
{
    g_object_unref(obj);
}




