/*
 *  call.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#ifndef GSMD_CALL_IMPL_INTERFACE
#define GSMD_CALL_IMPL_INTERFACE


#define GSMD_CALL_KEY_CALL_STATUS_NUMBER "number"
#define GSMD_CALL_KEY_SEND_IDENTIFICATION "send_identification"
#define GSMD_CALL_KEY_ID "id"
#define GSMD_CALL_KEY_DTMF_DURATION "dtmf_duration"
#define GSMD_CALL_KEY_CONFERENCE "conference"
#define GSMD_CALL_KEY_MODEM_STATUS "status"
#define GSMD_CALL_KEY_CALL_STATUS "status"

#include "call_interface.h"

void gsmd_call_init(ModemInterface* modem);

typedef struct _CallStatusStruct CallStatusStruct;

struct _CallStatusStruct {
    gint id;
    GString *status;
    GHashTable *properties;
};
#endif /* GSMD_CALL_IMPL_INTERFACE */
