/*
 *  sim.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include "utils.h"
#include "sim.h"
#include "gsmd-error.h"
#include "modem.h"
#include <string.h>
#include <stdlib.h>

static void gsmd_sms_handler_read_free(AtCommandContext *at);

static gboolean gsmd_sim_decoder_pdu (guchar* src,SmsParam* pdu);

static
AtCommandHandlerStatus gsmd_sim_handler_query_pin_status (ModemInterface *modem,
                                                          AtCommandContext *at,
                                                          GString *response);

static
AtCommandHandlerStatus gsmd_sim_handler_query_imsi( ModemInterface *modem,
                                                    AtCommandContext *at,
                                                    GString *response);
static
AtCommandHandlerStatus gsmd_sim_handler_pin(ModemInterface *modem,
                                            AtCommandContext *at,
                                            GString *response);
static
AtCommandHandlerStatus gsmd_sim_handler_read (ModemInterface *modem,
                                              AtCommandContext *at,
                                              GString *response);
static
AtCommandHandlerStatus gsmd_sim_handler_delete(ModemInterface *modem,
                                               AtCommandContext *at,
                                               GString *response);

static
AtCommandHandlerStatus gsmd_sim_handler_query_service_center (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response);

static
AtCommandHandlerStatus gsmd_sim_handler_set_service_center (ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response);

static
AtCommandHandlerStatus gsmd_sim_handler_get_subscriber_numbers (ModemInterface *modem,
                                                                AtCommandContext *at,
                                                                GString *response);

static
AtCommandHandlerStatus gsmd_sim_handler_retrieve_messagebook (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response);

static
AtCommandHandlerStatus gsmd_sim_handler_send_message (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response);

static
void gsmd_sim_handler_retrieve_messagebook_free (AtCommandContext *at);


/* static */
/* void gsmd_sim_storage_spaces_free(AtCommandContext *at); */

/* AtCommandHandlerStatus gsmd_sim_handler_get_storage_spaces( */
/*         ModemInterface *modem, */
/*         AtCommandContext *at, */
/*         GString *response); */

AtCommandHandlerStatus gsmd_sim_handler_get_phonebook_status(
    ModemInterface *modem,
    AtCommandContext *at,
    GString *response);

/* AtCommandHandlerStatus gsmd_sim_handler_set_default_storage_space( */
/*         ModemInterface *modem, */
/*         AtCommandContext *at, */
/*         GString *response); */

AtCommandHandlerStatus gsmd_sim_handler_store_entry(
    ModemInterface *modem,
    AtCommandContext *at,
    GString *response);

AtCommandHandlerStatus gsmd_sim_handler_delete_entry(
    ModemInterface *modem,
    AtCommandContext *at,
    GString *response);

static
AtCommandHandlerStatus gsmd_sim_handler_get_index_boundaries(
    ModemInterface *modem,
    AtCommandContext *at,
    GString *response);

AtCommandHandlerStatus gsmd_sim_handler_retrieve_entry(
    ModemInterface *modem,
    AtCommandContext *at,
    GString *response);


static const AtCommand
sim_commands[]
=
{
    //Query pin status
    { PIN_QUERY,                    "AT+CPIN?\r\n",             20000,
        TRUE,  0,
        gsmd_sim_handler_query_pin_status, NULL, SIM_UNKNOWN, NULL},
    //Query subscriber numbers
    { QUERY_SUBSCRIBER_NUMBERS,     "AT+CNUM\r\n",             20000,
      TRUE,  0,
      gsmd_sim_handler_get_subscriber_numbers, NULL, SIM_UNKNOWN, NULL},

    //Setup pin
    { PIN_SETUP,                    "AT+CPIN=\"%s\"\r\n",           20000,
      FALSE, 0,
      gsmd_sim_handler_pin, NULL, SIM_UNKNOWN, NULL},

    //Change pin
    { PIN_CHANGE,                   "AT+CPIN=%s\r\n",           20000,
      FALSE, 0,
      gsmd_sim_handler_pin, NULL, SIM_UNKNOWN, NULL},

    //Setup puk
    { PUK_SETUP,                    "AT+CPIN=%s\r\n",           20000,
      FALSE, 0,
      gsmd_sim_handler_pin, NULL, SIM_UNKNOWN, NULL},

    //Get IMSI
    { IMSI_QUERY,                   "AT+CIMI\r\n",              5000,
      FALSE,  0,
      gsmd_sim_handler_query_imsi, NULL, SIM_READY},
    //Read sms
    { SMS_READ,                     "AT+CMGR=%s\r\n",           5000,
      FALSE,  0,
      gsmd_sim_handler_read, NULL, SIM_READY, gsmd_sms_handler_read_free},

    //Delete sms
    { SMS_DELETE,                   "AT+CMGD=%s\r\n",           5000,
      FALSE,  0,
      gsmd_sim_handler_delete, NULL, SIM_READY, NULL},

    //Query sms service center
    { SMS_CENTER_QUERY,             "AT+CSCA?\r\n",             20000,
      TRUE,   0,
      gsmd_sim_handler_query_service_center, NULL, SIM_READY, NULL},

    //Set sms service center
    { SMS_SET_CENTER,               "AT+CSCA=%s\r\n",           20000,
      TRUE,   0,
      gsmd_sim_handler_set_service_center, NULL, SIM_READY, NULL},

    //List messages
    { SMS_LIST,                     "AT+CMGL=%s\r\n",           30000,
      FALSE,  0, gsmd_sim_handler_retrieve_messagebook,
      NULL, SIM_READY,gsmd_sim_handler_retrieve_messagebook_free},

    //Send message from storage
    { SIM_SEND,                     "AT+CMSS=%s\r\n",           30000,
      FALSE,  0, gsmd_sim_handler_send_message,
      NULL, SIM_READY,NULL},

    //Get available storage space
    /*         { PHONEBOOK_SPACES_QUERY,         "AT+CPBS=?\r\n",            100, */
    /*           FALSE,  0, */
    /*           gsmd_sim_handler_get_storage_spaces, NULL, SIM_READY, */
    /*           gsmd_sim_storage_spaces_free}, */

    //Get current storage space status
    { PHONEBOOK_STATUS_QUERY,      "AT+CPBS?\r\n",             100,
      FALSE,  0,
      gsmd_sim_handler_get_phonebook_status, NULL, SIM_READY,
      NULL},

    //Set storage space
    /*         { PHONEBOOK_SPACE_SET,            "AT+CPBS=%s\r\n",           100, */
    /*           FALSE,  0, */
    /*           gsmd_sim_handler_set_default_storage_space, NULL, SIM_READY, */
    /*           NULL}, */

    //Get storage item's index boundaries
    { PHONEBOOK_GET_INDEX_BOUNDARIES, "AT+CPBR=?\r\n",            100,
      FALSE,  0,
      gsmd_sim_handler_get_index_boundaries, NULL, SIM_READY, NULL},

    //Get entry from storage
    { PHONEBOOK_ENTRY_GET,            "AT+CPBR=%s\r\n",           100,
      FALSE,  0,
      gsmd_sim_handler_retrieve_entry, NULL, SIM_READY, NULL},

    //Set storage entry
    { PHONEBOOK_ENTRY_SET,            "AT+CPBW=%s\r\n",           100,
      FALSE,  0,
      gsmd_sim_handler_store_entry, NULL, SIM_READY, NULL},

    //Delete storage entry
    { PHONEBOOK_ENTRY_DELETE,         "AT+CPBW=%s\r\n",           100,
      FALSE,  0,
      gsmd_sim_handler_delete_entry, NULL, SIM_READY, NULL},

    { 0,                    NULL,                100,    TRUE,  1,
      NULL, NULL, SIM_UNKNOWN },

}, *sim_command_p = sim_commands;

static const SymbolTable
sim_symbols[] =
{
    { "SIM",        SYMBOL_SIM,     },
    { NULL, 0,                      },
}, *sim_symbol_p = sim_symbols;
void gsmd_sim_init_at_handler(ModemInterface* modem)
{
    while (sim_symbol_p->symbol_name)
    {
        g_scanner_add_symbol (modem->scanner, sim_symbol_p->symbol_name,
                              GINT_TO_POINTER(sim_symbol_p->symbol_token));
        sim_symbol_p++;
    }
    while (sim_command_p->command)
    {
        gsmd_modem_register_command (modem,sim_command_p);
        sim_command_p++;
    }
}
/**
 * @brief Changes auth (pin) status, sends auth_status signal if
 * status was changed.
 *
 * @param modem modem whose auth status to change
 * @param status new pin status
 * @param send_reply should reply to get_auth_status ipc method
 * be sent aswll
 */
void gsmd_sim_change_auth_status( ModemInterface *modem,
                                  gpointer ipc_data,
                                  SIMStatus status,
                                  const gchar *message,
                                  gboolean send_reply)
{
    if ( status != modem->sim_status )
    {
        g_debug("Changing auth status from %d to %d.",modem->sim_status, status);
    }


    gsmd_utils_table_insert_string(modem->caches[INTERFACE_SIM],
                                   GSMD_SIM_KEY_AUTH_STATUS,
                                   message);



    //Send a reply to get auth_status ipc method if necessary
    if (send_reply && modem->sim_ipc->get_auth_status_reply && ipc_data)
        modem->sim_ipc->get_auth_status_reply(modem->sim_ipc,
                                              ipc_data,
                                              message);

    //If sim status has changed, send a signal
    if (modem->sim_ipc->auth_status && status != modem->sim_status)
    {
        modem->sim_ipc->auth_status(modem->sim_ipc,
                                    message);
    }

    //finally change modem's sim status
    gsmd_modem_change_sim_status(modem,status);

}


/**
 * Sends a reply to all pin related ipc method calls
 *
 * @param modem modem pointer
 * @param cmd_id command/method call id
 */
void gsmd_sim_send_pin_reply(ModemInterface *modem, AtCommandContext *at)
{
    switch (at->command->cmd_id)
    {
    case PIN_SETUP:
        modem->sim_ipc->send_auth_code_reply(modem->sim_ipc,at->ipc_data);
        break;
    case PIN_CHANGE:
        modem->sim_ipc->change_auth_code_reply(modem->sim_ipc,at->ipc_data);
        break;
    case PUK_SETUP:
        modem->sim_ipc->unlock_reply(modem->sim_ipc,at->ipc_data);
        break;
    }
}

/**
* @brief Free's messagebook entry
* @param msg entry to free
*/
void gsmd_sim_handler_retrieve_messagebook_free_entry(messagebook_msg *msg)
{
    if (msg->status)
        g_string_free(msg->status,TRUE);
    if (msg->number)
        g_string_free(msg->number,TRUE);
    if (msg->content)
        g_string_free(msg->content,TRUE);
    g_free(msg);
}

/**
* @brief Free's the whole messagebook
* @param at AtCommandContext context containing the messagebook to free
*/
void gsmd_sim_handler_retrieve_messagebook_free (AtCommandContext *at)
{
    GValue *val = g_hash_table_lookup(at->handler_data,
                                      GSMD_SIM_KEY_MESSAGE_BOOK);
    GArray *list = g_value_get_pointer(val);
    if (list)
    {
        gint i=0;

        for (i=0;i<list->len;i++)
        {
            messagebook_msg *msg = g_array_index(list,messagebook_msg*,i);
            gsmd_sim_handler_retrieve_messagebook_free_entry(msg);
        }
        g_array_free(list,TRUE);
    }
}

/**
* @brief Removes last message from the list
* @param table table containing a list of messages
*/
static gboolean gsmd_sim_handler_retrieve_messagebook_remove_last(GHashTable *table)
{
    g_debug("%s",__func__);
    GValue *val = g_hash_table_lookup(table,GSMD_SIM_KEY_MESSAGE_BOOK);
    if (!val)
        return FALSE;

    GArray *list = g_value_get_pointer(val);

    if (!list)
        return FALSE;

    messagebook_msg *msg = g_array_index(list,messagebook_msg*,list->len-1);
    gsmd_sim_handler_retrieve_messagebook_free_entry(msg);
    g_array_remove_index(list,list->len-1);

    return TRUE;
}

/**
* @brief Fills details from SMSParam to last entry in messagebook list in garray
* When parsing output from cmgl (list sms messages command) messages are listed
* index first, contents second. Parser function that collects all messages
* first adds a new entry to a list with the index. When parser receives message's
* contents (pdu) it calls this function to add (or append) contents to the message.
*
* @param sms parsed pdu to set as last message's contents
* @param handler_data GHashTable that contains a list of messages
* @param append should this message's contents be only applied to last message
*/
static gboolean gsmd_sim_handler_retrieve_messagebook_fill_last_message(SmsParam *sms,
                                                                        GHashTable *handler_data,
                                                                        gboolean append)
{
    if (!sms || !handler_data)
        return FALSE;

    g_debug("%s",__func__);
    GValue *val = g_hash_table_lookup(handler_data,GSMD_SIM_KEY_MESSAGE_BOOK);
    if (!val)
        return FALSE;

    GArray *list = g_value_get_pointer(val);

    if (!list)
        return FALSE;

    messagebook_msg *msg = g_array_index(list,messagebook_msg*,list->len-1);

    if (append)
    {
        g_string_append_len(msg->content,(gchar*)sms->TP_UD, (gint)sms->TP_UD_LEN);
        return TRUE;
    }

    msg->status = g_string_new(gsmd_utils_table_get_string(handler_data,
                                                           GSMD_SIM_KEY_MESSAGE_BOOK_TYPE,
                                                           NULL));

    msg->number = g_string_new((gchar*)sms->TPA);
    msg->content = g_string_new_len((gchar*)sms->TPA,sms->TP_UD_LEN);

    return TRUE;
}


AtCommandHandlerStatus gsmd_sim_handler_retrieve_messagebook (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response)
{
    /*
    at+cmgl=1
    +CMGL: 1,1,"",145
    <pdu>
    +CMGL: 2,1,"",148
    <pdu>
    +CMGL: 3,1,"",158

    Regular expression:
    \+CMGL:\s(\d+),.*
    */

    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    gint cms_code = 0;
    if ( gsmd_utils_parse_cms_error(response,&cms_code) )
    {
        switch ( cms_code )
        {
        case CMS_ERROR_INVALID_MESSAGE_INDEX:
        {
            /**
             * At least on Neo 1973 this means that there
             * are no message with given status
             */
            GValue *val = g_hash_table_lookup(at->handler_data,
                                              GSMD_SIM_KEY_MESSAGE_BOOK);
            modem->sim_ipc->retrieve_messagebook_reply(modem->sim_ipc,
                                                       at->ipc_data,
                                                       g_value_get_pointer(val));
            return AT_HANDLER_DONE;
        }
        default:
            g_warning("%s : CMS ERROR: %d", __func__, cms_code);
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "UNABLE TO LIST MESSAGES");
            return AT_HANDLER_DONE_ERROR;
        }
    }
    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CMGL:\\s(?<id>\\d+),.*|(?<error>ERROR)|(?<ok>OK)|(?<pdu>[0-9A-F]+)", 0, 0, error);

    GMatchInfo *match_info;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {



        if (gsmd_utils_match_exists(match_info,"id"))
        {
            gchar *match = g_match_info_fetch_named(match_info,"id");

            GValue *val = g_hash_table_lookup(at->handler_data,
                                              GSMD_SIM_KEY_MESSAGE_BOOK);
            if (!val)
            {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "INVALID HANDLER DATA");
                status = AT_HANDLER_DONE_ERROR;
            }

            GArray *list = g_value_get_pointer(val);
            messagebook_msg *msg = g_try_new0(messagebook_msg,1);
            msg->index = atoi(match);
            g_free(match);
            g_array_append_val(list,msg);

            status = AT_HANDLER_NEED_MORE;
        }

        if (gsmd_utils_match_exists(match_info,"error"))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "UNABLE TO LIST MESSAGES");
            status = AT_HANDLER_DONE_ERROR;
        }
        else if (gsmd_utils_match_exists(match_info,"ok"))
        {
            GValue *val = g_hash_table_lookup(at->handler_data,
                                              GSMD_SIM_KEY_MESSAGE_BOOK);
            if (!val)
            {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "OK WITHOUT RESPONSE");
                status = AT_HANDLER_DONE_ERROR;
            }
            else
            {
                modem->sim_ipc->retrieve_messagebook_reply(modem->sim_ipc,
                                                           at->ipc_data,
                                                           g_value_get_pointer(val));
            }

            status = AT_HANDLER_DONE;
        }
        else
            //Check pdu, if this pdu is from a multipart message, but it's
            //not the first part, then remove it from the list
            if (gsmd_utils_match_exists(match_info,"pdu"))
            {
                gchar *match = g_match_info_fetch_named(match_info,"pdu");
                SmsParam *sms = g_try_new0(SmsParam,1);
                if (gsmd_sim_decoder_pdu((guchar*)match,sms))
                {

                    if (sms->multipart && sms->multipart_index != 1)
                    {
                        //If it's multipart message and we have any of the
                        //latter parts, then remove their indexes
                        //and append their texts to the previous message

                        gsmd_sim_handler_retrieve_messagebook_remove_last(at->handler_data);
                        gsmd_sim_handler_retrieve_messagebook_fill_last_message(sms,
                                                                                at->handler_data,
                                                                                TRUE);

                    }
                    else //Otherwise create a new entry
                        gsmd_sim_handler_retrieve_messagebook_fill_last_message(sms,
                                                                                at->handler_data,
                                                                                FALSE);

                }


                gsmd_sms_sms_param_free(sms);

                status = AT_HANDLER_NEED_MORE;
                g_free(match);

            }

    }

    g_free(error);
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;
}

static
AtCommandHandlerStatus gsmd_sim_handler_send_message (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response)
{
    g_debug("%s",__func__);

    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    if (gsmd_utils_send_error_from_response(at,
                                            response,
                                            "UNKNOWN ERROR OCCURRED WHEN SENDING MESSAGE",
                                            "TIMEOUT OCCURRED WHEN SENDING MESSAGE"))
        return AT_HANDLER_DONE_ERROR;


    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CMSS:\\s(?<id>\\d+)|(?<cms>\\+CMS\\sERROR).*|(?<ok>OK)", 0, 0, error);

    GMatchInfo *match_info;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        //Got +CMSS <id>
        if (gsmd_utils_match_exists(match_info,"id"))
        {
            gsmd_utils_table_insert_int(at->handler_data,
                                        GSMD_SIM_KEY_CMSS_ID,
                                        gsmd_utils_fetch_match_int(match_info,
                                                                   "id",0));
            status = AT_HANDLER_NEED_MORE;
        }
        else
            //Got ok
            if (gsmd_utils_match_exists(match_info,"ok"))
            {
                GValue *val = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_CMSS_ID);

                if (!val)
                {
                    g_warning("%s : Invalid handler data. Didn't get CMSS id",
                              __func__);

                    gsmd_utils_send_error(at,
                                          GSMD_ERROR_UNKNOWN,
                                          "OK WITHOUT RESPONSE");
                    status = AT_HANDLER_DONE_ERROR;
                }
                else
                {
                    modem->sim_ipc->send_stored_message_reply(modem->sim_ipc,at->ipc_data,0);
                    if (modem->sms_ipc->message_sent)
                    {
                        modem->sms_ipc->message_sent(modem->sms_ipc,TRUE,NULL);
                    }
                    status = AT_HANDLER_DONE;
                }
            }
            else
                //Got +CMS ERROR
                if (gsmd_utils_match_exists(match_info,"cms"))
                {
                    status = gsmd_sms_process_sms_error_response (modem,
                                                                  at,
                                                                  modem->scanner);
                }

    }

    g_free(error);
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;
}

/**
 * @brief Handler to modems response from setupPin command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
static
AtCommandHandlerStatus gsmd_sim_handler_pin(ModemInterface *modem,
                                            AtCommandContext *at,
                                            GString *response)
{

    AtCommandHandlerStatus ret = AT_HANDLER_DONT_UNDERSTAND;

    GScanner* scanner = modem->scanner;

    if (scanner != 0)
        g_debug("gsmd_sim_handler_setup_pin %d", scanner->token);
    else
    {
        g_debug("gsmd_sim_handler_setup_pin error - no scanner");
        return AT_HANDLER_DONE;
    }



    switch (scanner->token)
    {
    case SYMBOL_OK:
        gsmd_sim_send_pin_reply(modem, at);
        gsmd_sim_change_auth_status(modem,NULL,SIM_READY,"READY",FALSE);

        ret = AT_HANDLER_DONE;
        break;
    case SYMBOL_ERROR:
        gsmd_utils_send_error(at,
                              GSMD_ERROR_UNKNOWN,
                              "FAILED TO CHANGE AUTH CODE");

        if (modem->sim_status == SIM_NEED_PUK)
            gsmd_sim_change_auth_status(modem,NULL,SIM_READY,"READY",FALSE);


        /* What should be done on each status
           SIM_UNKNOWN=0 -> we're still at unknown state
           SIM_MISSING_SIM -> we still don't have a sim card
           SIM_NEED_PIN -> pin was wrong, we still need pin
           SIM_NEED_PUK -> puk code was wrong, we still need puk
           SIM_READY -> we never needed any code so nothing changed
        */
        ret = AT_HANDLER_DONE_ERROR;
        break;
    default:
        g_warning("%s : No match to setupPin handler", __func__);
        break;
    }
    return ret;
}


void gsmd_sim_cache_auth_status(ModemInterface *modem,
                                AtCommandContext *at,
                                SIMStatus status,
                                const gchar *message)
{
    g_debug("%s: status: %d, message: %s",__func__,status,message);
    gsmd_utils_table_insert_string(at->handler_data,GSMD_SIM_KEY_AUTH_MESSAGE,message);
    gsmd_utils_table_insert_int(at->handler_data,GSMD_SIM_KEY_AUTH_STATUS,status);
}

/**
 * @brief Handler to modems response from queryPinStatus command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
static
AtCommandHandlerStatus gsmd_sim_handler_query_pin_status (ModemInterface *modem,
                                                          AtCommandContext *at,
                                                          GString *response)
{
    g_debug("%s",__func__);
    GScanner* scanner = modem->scanner;



    if (scanner->token == SYMBOL_OK)
    {

        GValue *status = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_AUTH_STATUS);
        GValue *message = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_AUTH_MESSAGE);

        if (!status || !message)
        {
            g_warning("%s : Invalid handler data. status: %p, message: %p",
                      __func__,
                      status,
                      message);
            gsmd_utils_send_error(at,GSMD_ERROR_UNKNOWN,"INVALID HANDLER DATA");
            return AT_HANDLER_DONE_ERROR;
        }

        gsmd_sim_change_auth_status(modem,
                                    at->ipc_data,
                                    g_value_get_int(status),
                                    g_value_get_string(message),
                                    TRUE);

        return AT_HANDLER_DONE;
    }

    if (scanner->token == SYMBOL_ERROR)
    {
        gsmd_utils_send_error(at,
                              GSMD_ERROR_UNKNOWN,
                              "ERROR READING PIN STATUS");
        return AT_HANDLER_DONE_ERROR;
    }
    else if ( scanner->token == SYMBOL_TIME_OUT)
    {
        gsmd_utils_send_error(at,
                              GSMD_ERROR_DEVICE_TIMEOUT,
                              "TIMEOUT READING PIN STATUS");
        return AT_HANDLER_DONE_ERROR;
    }

    if (scanner->token != '+')
    {
        return AT_HANDLER_DONT_UNDERSTAND;
    }

    g_scanner_get_next_token (scanner);
    switch (scanner->token)
    {

    case SYMBOL_CME:
        g_scanner_get_next_token (scanner);
        //g_scanner_get_next_token (scanner);
        if (scanner->token == SYMBOL_ERROR)
        {
            g_scanner_peek_next_token(scanner);
            if (scanner->next_token == ':')
            {
                g_scanner_get_next_token (scanner);
                g_scanner_get_next_token(scanner);//arrive error value
                if ( ERR_NO_SIM_CARD == scanner->value.v_int )
                {
                    gsmd_sim_change_auth_status(modem,
                                                at->ipc_data,
                                                SIM_MISSING,
                                                "SIM MISSING",
                                                TRUE);
                }
                else
                {
                    gsmd_sim_change_auth_status(modem,
                                                at->ipc_data,
                                                SIM_ERROR,
                                                "SIM ERROR",
                                                TRUE);
                }

                return AT_HANDLER_DONE;
            }
            else
            {
                g_scanner_unexp_token (scanner, ':', NULL, "symbol",
                                       NULL, NULL, TRUE);

            };
        }
        else
        {
            g_scanner_unexp_token (scanner, SYMBOL_ERROR, NULL, "symbol",
                                   NULL, NULL, TRUE);
        }
        break;
    case SYMBOL_CPIN:
        g_scanner_get_next_token (scanner);//get ':'
        g_scanner_get_next_token (scanner);
        if (scanner->token == SYMBOL_READY)
        {
            gsmd_sim_cache_auth_status(modem,
                                       at,
                                       SIM_READY,
                                       "READY");


            return AT_HANDLER_NEED_MORE;

        }
        else if (scanner->token == SYMBOL_SIM)
        {

            g_scanner_get_next_token (scanner);
            g_scanner_peek_next_token(scanner);
            if (scanner->token == SYMBOL_PIN)
            {
                gsmd_sim_cache_auth_status(modem,
                                           at,
                                           SIM_NEED_PIN,
                                           "SIM PIN");


            }
            else if (scanner->token == SYMBOL_PUK)
            {
                gsmd_sim_cache_auth_status(modem,
                                           at,
                                           SIM_NEED_PUK,
                                           "SIM PUK");
            }

            return AT_HANDLER_NEED_MORE;
        }
        else
        {
            g_scanner_unexp_token (scanner, SYMBOL_READY, NULL, "symbol",
                                   NULL, NULL, TRUE);
        }
        break;
    default:
        g_warning("No match after getting + \n");
        break;
    }



    return AT_HANDLER_DONT_UNDERSTAND;
}



/**
 * @brief AtCommand to setup pin value(s)
 *
 * @param modem modem whose response to handle
 * @param pin_code pin code(s) to set
 */
static void gsmd_sim_command_setup_pin (SIMInterface *sim,
                                        gpointer ipc_data,
                                        const char* pin_code)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    gsmd_modem_post_at_command_id( modem,
                                   PIN_SETUP,
                                   pin_code,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->send_auth_code_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
}


/**
 * @brief AtCommand to query pin status
 *
 * @param modem modem whose response to handle
 */
static void gsmd_sim_command_query_pin_status (SIMInterface *sim,
                                               gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GValue *val = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_AUTH_STATUS);
    if (!modem->no_cache && val)
    {
        modem->sim_ipc->get_auth_status_reply(modem->sim_ipc,
                                              ipc_data,
                                              g_value_get_string(val));
    }
    else
    {
        gsmd_modem_post_at_command_id( modem,
                                       PIN_QUERY,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->sim_ipc->get_auth_status_error,
                                       (gpointer)modem->sim_ipc,
                                       INTERFACE_SIM,
                                       NULL);
    }
}



void gsmd_sim_command_unlock(SIMInterface *sim,
                             gpointer ipc_data,
                             const char* puk,
                             const char* new_pin)
{
    g_debug("%s",__func__);
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GString* param = g_string_new ("");
    g_string_printf(param, "\"%s\",\"%s\"",puk,new_pin);

    gsmd_modem_post_at_command_id( modem,
                                   PUK_SETUP,
                                   param->str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->unlock_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
    g_string_free(param,TRUE);
}


void gsmd_sim_command_change_auth_code(SIMInterface *sim,
                                       gpointer ipc_data,
                                       const char* old_pin,
                                       const char* new_pin)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GString* param = g_string_new ("");
    g_string_printf(param, "\"%s\",\"%s\"",old_pin,new_pin);

    gsmd_modem_post_at_command_id( modem,
                                   PIN_CHANGE,
                                   param->str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->change_auth_code_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
    g_string_free(param,TRUE);
}


/**
 * Handler for reply to query_imsi command.
 *
 * @param modem pointer to modem struct
 * @param response response sent by gsm modem
 * @return AT_HANDLER_DONE when response is recognized
 */
static
AtCommandHandlerStatus gsmd_sim_handler_query_imsi( ModemInterface *modem,
                                                    AtCommandContext *at,
                                                    GString *response)
{
    /*
      at+cimi
      +CIMI: 244917070350971

      OK
    */

    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GError *error = NULL;
    GRegex *regex = g_regex_new ("^\\+CIMI:\\s(?<imsi>\\d+)$|^(?<imsi2>\\d+)$|^(?<error>ERROR)$|^(?<ok>OK)$",
                                 0, 0, &error);
    if ( error )
    {
        g_debug("%s : '%s'", __func__, error->message);
        g_error_free( error );
        return AT_HANDLER_ERROR;
    }
    GMatchInfo *match_info;
    GValue *val = NULL;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"imsi"))
        {
            gchar *match = g_match_info_fetch_named(match_info,
                                                    "imsi");
            g_debug("%s : imsi : %s", __func__, match);

            gsmd_utils_table_insert_string(at->handler_data,
                                           GSMD_SIM_KEY_IMSI,
                                           match);

            g_free(match);
            status = AT_HANDLER_NEED_MORE;
        }
        else if (gsmd_utils_match_exists(match_info,"imsi2"))
        {
            gchar *match = g_match_info_fetch_named(match_info,
                                                    "imsi2");

            g_debug("%s : imsi2 : %s", __func__, match);
            gsmd_utils_table_insert_string(at->handler_data,
                                           GSMD_SIM_KEY_IMSI,
                                           match);

            g_free(match);
            status = AT_HANDLER_NEED_MORE;
        }
        else if (gsmd_utils_match_exists(match_info,"error"))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "FAILED TO READ IMSI");
            status = AT_HANDLER_DONE_ERROR;
        }
        else if (gsmd_utils_match_exists(match_info,"ok"))
        {
            val = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_IMSI);
            g_debug("%s : ok : %p", __func__, val);
            if (val)
            {
                status = AT_HANDLER_DONE;
            }
            else
            {
                // If we didn't get the response,
                //  then this OK is not for us
                status = AT_HANDLER_DONT_UNDERSTAND;
            }
        }

    }

    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;
}

static
gboolean gsmd_sim_command_get_sim_info_reply(ModemInterface *modem,
                                             gpointer ipc_data,
                                             GHashTable *info)
{
    g_debug("%s", __func__);
    if ( !modem->no_cache )
    {
        GValue *imsi = g_hash_table_lookup(info,GSMD_SIM_KEY_IMSI);
        if ( imsi )
        {
            gsmd_utils_table_insert_copy(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_IMSI,imsi);
        }
    }
    modem->sim_ipc->get_sim_info_reply( modem->sim_ipc,
                                        ipc_data,
                                        info);
    return TRUE;
}
static
AtCommandHandlerStatus gsmd_sim_handler_get_sim_info( ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response)
{
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    switch (at->command->cmd_id)
    {
    case IMSI_QUERY:
        g_debug("%s : IMSI_QUERY", __func__);
        status = at->command->handler(modem,at,response);
        switch (status)
        {
        case AT_HANDLER_DONE_ERROR:
        case AT_HANDLER_ERROR:
            return AT_HANDLER_DONE_ERROR;
            break;
        case AT_HANDLER_DONE:
            if ( !g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_IMSI) )
            {
                gsmd_utils_send_error(at,GSMD_ERROR_UNKNOWN,"UNABLE TO READ IMSI");
                return AT_HANDLER_DONE_ERROR;
            }
            gsmd_sim_command_get_sim_info_reply(modem, at->ipc_data, at->handler_data);
            /*                         gsmd_modem_post_at_command_id( modem, */
            /*                                                        QUERY_SUBSCRIBER_NUMBERS, */
            /*                                                        NULL, */
            /*                                                        ipc_data, */
            /*                                                        (ErrorFunction)modem->sim_ipc->get_subscriber_numbers_error, */
            /*                                                        (gpointer)modem->sim_ipc, */
            /*                                                        INTERFACE_SIM, */
            /*                                                        NULL); */
            break;
        case AT_HANDLER_DONT_UNDERSTAND:
        case AT_HANDLER_NEED_MORE:
        case AT_HANDLER_RETRY:
            return status;
        }
        break;
    }
    return status;
}

void gsmd_sim_command_get_sim_info(SIMInterface *sim,
                                   gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GValue *imsi = NULL;
    GValue *subs_num = NULL;
    GValue *dialcode = NULL;
    if (!modem->no_cache )
    {
        imsi = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_IMSI);
        subs_num = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_SUBSCRIBER_NUMBER);
        dialcode = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_DIAL_CODE);
    }

    if (imsi && subs_num && dialcode)
    {
        g_debug("%s: sim info is cached",__func__);
        GHashTable *info = gsmd_utils_create_hash_table();
        gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_IMSI,imsi);
        gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_SUBSCRIBER_NUMBER,subs_num);
        gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_DIAL_CODE, dialcode);
        modem->sim_ipc->get_sim_info_reply(modem->sim_ipc,
                                           ipc_data,
                                           info);
        g_hash_table_destroy(info);
    }
    else
    {
        AtCommandContext *cmd = NULL;
        cmd = gsmd_at_command_context_new_from_id( modem,
                                                   IMSI_QUERY,
                                                   NULL,
                                                   ipc_data,
                                                   (ErrorFunction)modem->sim_ipc->get_sim_info_error,
                                                   (gpointer)modem->sim_ipc,
                                                   NULL);
        cmd->handler = &gsmd_sim_handler_get_sim_info;
        gsmd_modem_post_at_command(modem,cmd,INTERFACE_SIM);
    }

}

static
gboolean gsmd_sim_command_get_phonebook_info_reply(ModemInterface *modem,
                                                   gpointer ipc_data,
                                                   GHashTable *info)
{
    g_debug("%s", __func__);
    if ( !modem->no_cache )
    {
        GValue *slots = g_hash_table_lookup(info,GSMD_SIM_KEY_PHONEBOOK_SLOTS);
        GValue *used = g_hash_table_lookup(info,GSMD_SIM_KEY_PHONEBOOK_SLOTS_USED);
        GValue *number_len = g_hash_table_lookup(info,GSMD_SIM_KEY_PHONEBOOK_NUMBER_LEN);
        GValue *name_len = g_hash_table_lookup(info,GSMD_SIM_KEY_PHONEBOOK_NAME_LEN);
        if ( slots )
        {
            gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_PHONEBOOK_SLOTS,slots);
        }
        if ( used )
        {
            gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_PHONEBOOK_SLOTS_USED, used);
        }
        if ( number_len )
        {
            gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_PHONEBOOK_NUMBER_LEN,number_len);
        }
        if ( name_len )
        {
            gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_PHONEBOOK_NAME_LEN,name_len);
        }
    }
    modem->sim_ipc->get_sim_info_reply( modem->sim_ipc,
                                        ipc_data,
                                        info);
    return TRUE;
}

static
AtCommandHandlerStatus gsmd_sim_handler_get_phonebook_info( ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response)
{
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    AtCommandContext *cmd = NULL;
    switch (at->command->cmd_id)
    {
    case PHONEBOOK_GET_INDEX_BOUNDARIES:
        g_debug("%s : PHONEBOOK_GET_INDEX_BOUNDARIES", __func__);
        status = at->command->handler(modem,at,response);
        switch (status)
        {
        case AT_HANDLER_DONE_ERROR:
        case AT_HANDLER_ERROR:
            return AT_HANDLER_DONE_ERROR;
            break;
        case AT_HANDLER_DONE:
            cmd = gsmd_at_command_context_new_from_id( modem,
                                                       PHONEBOOK_STATUS_QUERY,
                                                       NULL,
                                                       at->ipc_data,
                                                       (ErrorFunction)modem->sim_ipc->get_phonebook_info_error,
                                                       (gpointer)modem->sim_ipc,
                                                       at->handler_data);
            at->handler_data = NULL;
            cmd->handler = &gsmd_sim_handler_get_phonebook_info;
            gsmd_modem_post_at_command(modem,cmd,INTERFACE_SIM);
            break;
        case AT_HANDLER_DONT_UNDERSTAND:
        case AT_HANDLER_NEED_MORE:
        case AT_HANDLER_RETRY:
            return status;
        }
        break;
    case PHONEBOOK_STATUS_QUERY:
        g_debug("%s : PHONEBOOK_STATUS_QUERY", __func__);
        status = at->command->handler(modem,at,response);
        switch (status)
        {
        case AT_HANDLER_DONE_ERROR:
        case AT_HANDLER_ERROR:
            return AT_HANDLER_DONE_ERROR;
            break;
        case AT_HANDLER_DONE:
            gsmd_sim_command_get_phonebook_info_reply(modem, at->ipc_data, at->handler_data);
            break;
        case AT_HANDLER_DONT_UNDERSTAND:
        case AT_HANDLER_NEED_MORE:
        case AT_HANDLER_RETRY:
            return status;
        }
        break;
    }
    return status;
}

/**
 * @brief Retrieves the whole phonebook
 *
 * @param sim SIM interface
 * @param ipc_data IPC method context data
 */
void gsmd_sim_command_retrieve_phonebook(SIMInterface *sim,
                                         gpointer ipc_data)
{
    //TODO implement
}

/**
 * @brief Requests phonebook info
 *
 * @param sim SIM interface
 * @param ipc_data IPC method context data
 */
void gsmd_sim_command_get_phonebook_info(SIMInterface *sim,
                                         gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GValue *slots = NULL;
    GValue *used = NULL;
    GValue *number_len = NULL;
    GValue *name_len = NULL;
    if (!modem->no_cache )
    {
        slots = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_PHONEBOOK_SLOTS);
        used = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_PHONEBOOK_SLOTS_USED);
        number_len = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_PHONEBOOK_NUMBER_LEN);
        name_len = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_PHONEBOOK_NAME_LEN);
    }

    if (slots && used && number_len && name_len)
    {
        g_debug("%s: info is cached",__func__);
        GHashTable *info = gsmd_utils_create_hash_table();
        gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_PHONEBOOK_SLOTS,slots);
        gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_PHONEBOOK_SLOTS_USED, used);
        gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_PHONEBOOK_NUMBER_LEN,number_len);
        gsmd_utils_table_insert_copy(info,GSMD_SIM_KEY_PHONEBOOK_NAME_LEN,name_len);
        modem->sim_ipc->get_phonebook_info_reply(modem->sim_ipc,
                                                 ipc_data,
                                                 info);
        g_hash_table_destroy(info);
    }
    else
    {
        AtCommandContext *cmd = NULL;
        cmd = gsmd_at_command_context_new_from_id( modem,
                                                   PHONEBOOK_GET_INDEX_BOUNDARIES,
                                                   NULL,
                                                   ipc_data,
                                                   (ErrorFunction)modem->sim_ipc->get_phonebook_info_error,
                                                   (gpointer)modem->sim_ipc,
                                                   NULL);
        cmd->handler = &gsmd_sim_handler_get_phonebook_info;
        gsmd_modem_post_at_command(modem,cmd,INTERFACE_SIM);
    }

}

/**
 * @brief Creates at command to set service center
 *
 * @param modem modem to send command to
 * @param centerNo center number
 */
void gsmd_sim_command_set_service_center (SIMInterface *sim,
                                          gpointer ipc_data,
                                          const char* centerNo)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GString* command= g_string_new ("");
    gint format=0;
    if (g_strrstr (centerNo, "+"))
    {
        //Internation format
        format = 145;
    }
    else
    {
        //national format
        format = 129;
    }
    g_string_printf (command, "%s,%d", centerNo, format);
    gsmd_modem_post_at_command_id( modem,
                                   SMS_SET_CENTER,
                                   command->str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->set_service_center_number_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
    g_string_free(command,TRUE);
}

/**
 * @brief Handler for replies sent by the gsm modem when a service center query
 * command is sent
 *
 * @param modem modem whose replies to interpret
 * @return true if replies were correct
 */
AtCommandHandlerStatus gsmd_sim_handler_query_service_center (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response)
{
    //input is +CSCA: "+393359609600", 145
    GScanner* scanner = modem->scanner;
    if (scanner->token == SYMBOL_OK)
    {
        if (modem->sim_ipc->get_service_center_number_reply)
        {
            GValue *val = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_SERVICE_CENTER);
            if (!val)
            {
                g_warning("%s : Invalid handler data. service center: %p",
                          __func__,
                          val);

                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "OK WITHOUT RESPONSE");
            }
            modem->sim_ipc->get_service_center_number_reply(modem->sim_ipc,
                                                            at->ipc_data,
                                                            g_value_get_string(val));

        }
        return AT_HANDLER_DONE;
    }
    if (scanner->token == '+')
    {
        g_scanner_get_next_token (scanner);
        if (scanner->token == SYMBOL_CSCA)
        {
            g_scanner_get_next_token (scanner);//get :
            if (scanner->token == ':')
            {
                g_scanner_get_next_token (scanner);
                gsmd_utils_table_insert_string(at->handler_data,GSMD_SIM_KEY_SERVICE_CENTER,scanner->value.v_string);


                return AT_HANDLER_NEED_MORE;
            }
        }
    }
    return AT_HANDLER_DONT_UNDERSTAND;
}


/**
 * @brief Creates at command to query service center
 *
 * @param modem modem to send command to
 */
void gsmd_sim_command_query_service_center (SIMInterface *sim,
                                            gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GValue *val = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_SERVICE_CENTER);
    if (val && modem->no_cache)
    {
        modem->sim_ipc->get_service_center_number_reply(modem->sim_ipc,
                                                        ipc_data,
                                                        g_value_get_string(val));
    }
    else
    {
        gsmd_modem_post_at_command_id( modem,
                                       SMS_CENTER_QUERY,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->sim_ipc->get_service_center_number_error,
                                       (gpointer)modem->sim_ipc,
                                       INTERFACE_SIM,
                                       NULL);
    }
}



/**
 * @brief Handler for replies sent by the gsm modem when a service center query
 * command is sent
 *
 * @param modem modem whose replies to interpret
 * @return true if replies were correct
 */
static
AtCommandHandlerStatus gsmd_sim_handler_get_subscriber_numbers (ModemInterface *modem,
                                                                AtCommandContext *at,
                                                                GString *response)
{
    /*
    at+cnum

    +CNUM: "","",129

    OK
    */

    return AT_HANDLER_DONE;
}


/**
 * @brief Handler for replies sent by the gsm modem when a set service centre
 * command is sent
 *
 * @param modem modem whose replies to interpret
 * @return true if replies were correct
 */
AtCommandHandlerStatus gsmd_sim_handler_set_service_center (ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response)
{
    GScanner* scanner = modem->scanner;
    g_assert( at );
    switch (scanner->token)
    {
    case SYMBOL_OK:
        modem->sim_ipc->set_service_center_number_reply(modem->sim_ipc,
                                                        at->ipc_data);
        return AT_HANDLER_DONE;
        break;
    case SYMBOL_ERROR:
        gsmd_utils_send_error(at,
                              GSMD_ERROR_UNKNOWN,
                              "FAILED TO SET SERVICE CENTER");
        return AT_HANDLER_DONE_ERROR;
        break;
    default:
        break;
    }
    return AT_HANDLER_DONT_UNDERSTAND;
}

static void gsmd_sim_command_store_message (SIMInterface *sim,
                                            gpointer ipc_data,
                                            const gchar *message,
                                            const gchar *number)
{
    /*
    at+cmgw=48

    > 0011000C915348500496870000AA0AE8329BFD4697D9EC37
    OK
    */
    g_debug("%s : Storing message %s to %s",__func__,message,number);
    ModemInterface *modem = (ModemInterface*)sim->priv;
    gsmd_sms_send_pdu (modem,
                       ipc_data,
                       message,
                       number,
                       SMS_STORE,
                       INTERFACE_SIM,
                       (ErrorFunction)modem->sim_ipc->store_message_error,
                       modem->sim_ipc,
                       TRUE);
}



/**
 * @brief 7 bit decode
 *
 * @param src source message
 * @param dst destination message
 * @param src_length length of the source string
 * @return resulting destination's string
 */
static int gsmd_sim_decode_7bits(const guchar*src, guchar* dst, gint src_length)
{
//divide bytes into groups, each group has 7 bytes, decode it
//to 8 bytes
    gint src_p=0;//processed bytes in source gourp
    gint dst_p=0;//processed bytes in dest group
    gint cur_index=0;// char index of current processing group
    gint cur_left=0;// the left bits from last byte
    while (src_p < src_length)
    {
        *dst = ((*src << cur_index) | cur_left) & 0x7f;
        cur_left = *src >> (7 - cur_index);
        dst++;
        dst_p++;
        cur_index++;
        if (cur_index == 7)
        {
            *dst = cur_left;
            dst++;
            dst_p++;
            cur_left = 0;
            cur_index = 0;
        }
        src++;
        src_p++;
        g_assert(dst_p < 512);
    }
    *dst=0;
    return dst_p;
}

/**
 * @brief Normalizes received telephonenumber.
 *
 * Adds + to the beginning of
 * phone number if the phonenumber is in international format.
 *
 * @param pdu sms parameters
 */
static void gsmd_sim_normalize_received_telnumber (SmsParam* pdu)
{
    gint i=0;
    gint len = strlen ((const char*)pdu->TPA);
    if (pdu->TOA == 0x91)
    {
        //international format
        //add +before the number
        for (i=len; i>0; i--)
        {
            pdu->TPA[i] = pdu->TPA[i-1];
        }
        pdu->TPA[0] = '+';
        pdu->TPA[len+1] = '\0';
    }
}







//method call handler
/**
 * @brief Handler for delete sms command's replies.
 *
 * @param modem whose replies to handle
 * @return true if response was recognized
 */
AtCommandHandlerStatus gsmd_sim_handler_delete(ModemInterface *modem,
                                               AtCommandContext *at,
                                               GString *response)
{
    GScanner* scanner = modem->scanner;
    guint symbol;
    symbol = scanner->token;
    if (scanner->token == SYMBOL_OK)
    {
        if (modem->sim_ipc->delete_message_reply)
        {
            modem->sim_ipc->delete_message_reply(modem->sim_ipc,at->ipc_data);
        }
        return AT_HANDLER_DONE;
    }

    if (gsmd_utils_send_error_from_response(at,
                                            response,
                                            "UNKNOWN ERROR OCCURRED WHEN DELETING A MESSAGE",
                                            "TIMEOUT OCCURRED WHEN DELETING A MESSAGE"))
    {
        return AT_HANDLER_DONE_ERROR;
    }


    return AT_HANDLER_DONT_UNDERSTAND;
}




static gboolean gsmd_sim_convert(guchar** src,
                                 guchar* dst,
                                 int src_length,
                                 int *dst_length,
                                 int move)
{
    if (strlen((char*)*src) < src_length + move)
        return FALSE;
    if (!gsmd_sms_string_to_bytes(*src,dst,src_length,dst_length))
        return FALSE;

    *src+=move;
    return TRUE;
}

static gboolean gsmd_sim_convert_number(guchar** src,
                                        guchar* dst,
                                        int src_length,
                                        gboolean is_encode,
                                        int move)
{
    if (!gsmd_sms_invert_number(*src,dst,src_length,is_encode))
        return FALSE;
    if (strlen((char*)*src) < move)
        return FALSE;
    *src+=move;
    return TRUE;
}

static gboolean sim_decoder_pdu_header( guchar **src, SmsParam *pdu)
{
    guchar tmp;

    //the SMSC Number length
    if (!gsmd_sim_convert(src,&tmp,2,NULL,4))
        return FALSE;

    tmp = (tmp-1)*2;

    //SMSC number
    if (!gsmd_sim_convert_number(src,pdu->SCA,tmp,FALSE,tmp))
        return FALSE;

    return TRUE;
}


static gboolean sim_decoder_pdu_type( guchar **src, SmsParam *pdu)
{
    guchar tmp;

    if (!gsmd_sim_convert(src,&tmp,2,NULL,2))
        return FALSE;

    //check if it is send or receive
    pdu->storing = (tmp & 1);
    //check if it's multipart
    pdu->multipart = (tmp&64);

    if (pdu->storing) //if encoding submitted message, ignore reference number
        *src+=2;


    return TRUE;
}

static gboolean sim_decoder_pdu_sender_num( guchar **src, SmsParam *pdu)
{
    guchar tmp;



    //the real status does not match the spec, in spec the 7bits should be 1 to
    //include the reply path. but the real value is 04H
    //include reply info

    //length of sender number
    if (!gsmd_sim_convert(src,&tmp,2,NULL,2))
        return FALSE;

    if (tmp & 1)
        tmp += 1;

    //length of sender number
    if (!gsmd_sim_convert(src,&pdu->TOA,2,NULL,2))
        return FALSE;

    if (!gsmd_sim_convert_number(src,pdu->TPA,tmp,FALSE,tmp))
        return FALSE;

    gsmd_sim_normalize_received_telnumber (pdu);

    return TRUE;
}

static gboolean sim_decoder_pdu_protocol( guchar **src, SmsParam *pdu)
{
    return gsmd_sim_convert(src,&pdu->TP_PID,2,NULL,2);
}

static gboolean sim_decoder_pdu_coding_scheme( guchar **src, SmsParam *pdu)
{
    return gsmd_sim_convert(src,&pdu->TP_DCS,2,NULL,2);
}

static gboolean sim_decoder_pdu_validity_period( guchar **src, SmsParam *pdu)
{
    if (pdu->storing)
    {
        *src+=2; //if submit format, skip validity period
    }

    return TRUE;
}


static gboolean sim_decoder_pdu_timestamp( guchar **src, SmsParam *pdu)
{
    if (!pdu->storing)
    {
        if (!gsmd_sim_convert_number(src,pdu->TP_SCTS,14,FALSE,14))
            return FALSE;
    }
    else
    {
        memset(pdu->TP_SCTS,0,14);
    }

    return TRUE;
}


static gboolean sim_decoder_pdu_userdata( guchar **src, SmsParam *pdu)
{
    int dst_length;
    guchar tmp;
    gint len;
    guchar buf[256];
    guchar div = 0;

    if (!gsmd_sim_convert(src,&tmp,2,NULL,2))
        return FALSE;


    if (pdu->multipart)
    {
        if (!gsmd_sim_convert(src,&div,2,NULL,6))
            return FALSE;
        if (!gsmd_sim_convert(src,&pdu->multipart_ref,2,NULL,2))
            return FALSE;
        if (!gsmd_sim_convert(src,&pdu->multipart_count,2,NULL,2))
            return FALSE;
        if (!gsmd_sim_convert(src,&pdu->multipart_index,2,NULL,2))
            return FALSE;


        div+=1;


        //if header doesn't stop at the edge of septet, skip fill bytes
        if (pdu->TP_DCS == GSM_7BIT && (div % 7) != 0)
        {
            div += (7-((div)%7));
        }
        //skip back userdata header
        *src-=12;

    }
    else
    {
        pdu->multipart_ref = 0;
        pdu->multipart_count = 0;
        pdu->multipart_index = 0;
    }

    if (pdu->TP_DCS == GSM_7BIT)
    {
        // 7-bit
        len = (tmp - tmp/8)*2;
        if (!gsmd_sim_convert(src,buf,len,&dst_length,0))
            return FALSE;

        gsmd_sim_decode_7bits (buf, pdu->TP_UD, dst_length);    //TP-DU

        //If it's multipart, remove its header
        if (div > 0)
        {
            memmove(pdu->TP_UD,&pdu->TP_UD[div],tmp-div);
            memset(&pdu->TP_UD[tmp-div],0,div);
        }

        dst_length = tmp;
        pdu->TP_UD_LEN = dst_length;
        return TRUE;

    }
    else if (pdu->TP_DCS == GSM_UCS2)
    {
        // UCS2
        len = tmp*2;
        if (!gsmd_sim_convert(src,buf,len,&dst_length,0))
            return FALSE;
        memset (pdu->TP_UD, 0, sizeof(pdu->TP_UD));
        memcpy (pdu->TP_UD, buf, dst_length);
        pdu->TP_UD_LEN = dst_length;
        return TRUE;

    }
    else
    {
        // 8-bit
        g_warning("Not supported format");
        return FALSE;
    }

    return FALSE;
}

static gboolean sim_decoder_pdu_userdata_normalize( guchar **src, SmsParam *pdu)
{
    gchar code_buffer[161];
    guchar* q;
    gint i;
    gchar temp;
    glong write_bytes;
    GError* error;
    glong read_words;

    pdu->number = g_strdup((gchar*)pdu->TPA);

    if (!pdu->storing)
    {
        pdu->timestamp = gsmd_utils_convert_to_timestamp((gchar*)pdu->TP_SCTS);
    }
    else
    {
        pdu->timestamp = 0;
    }


    if (pdu->TP_DCS == GSM_7BIT)
    {
        pdu->message = g_strdup((gchar*)pdu->TP_UD);
    }
    else if (pdu->TP_DCS == GSM_UCS2)
    {
        //covert it to UTF8
        memset (code_buffer, 0, sizeof(code_buffer));
        /*
         * The intel CPU is little-endian, but the ucs2 in sms
         * is big-endian, so the usc_string byte order need to
         * be converted
         * For example chinese hello in utf16 is 4F60 597D
         * but usc_string is 604F 7D59
         * Following while() is to convert the byte order
         */
        q = pdu->TP_UD;
        for (i=0; i < pdu->TP_UD_LEN; i += 2)
        {
            temp = *q;
            *q = *(q+1);
            *(q+1) = temp;
            q += 2;
        }

        pdu->message = g_strdup(g_utf16_to_utf8 ((gunichar2*)pdu->TP_UD,
                                                 -1
                                                 ,&read_words,
                                                 &write_bytes,
                                                 &error));
    }
    else
    {
        return FALSE;
    }

    return TRUE;
}

/**
 * @brief Pdu decoder
 *
 * @param src string to decode
 * @param pdu sms parameters
 * @return true if pdu was succesfully decoded
 */
static gboolean gsmd_sim_decoder_pdu (guchar* src,SmsParam* pdu)
{
    g_debug("%s : %s",__func__,src);


    if (!sim_decoder_pdu_header(&src,pdu))
        return FALSE;
    if (!sim_decoder_pdu_type(&src,pdu))
        return FALSE;
    if (!sim_decoder_pdu_sender_num(&src,pdu))
        return FALSE;
    if (!sim_decoder_pdu_protocol(&src,pdu))
        return FALSE;
    if (!sim_decoder_pdu_coding_scheme(&src,pdu))
        return FALSE;
    if (!sim_decoder_pdu_validity_period(&src,pdu))
        return FALSE;
    if (!sim_decoder_pdu_timestamp(&src,pdu))
        return FALSE;
    if (!sim_decoder_pdu_userdata(&src,pdu))
        return FALSE;
    return sim_decoder_pdu_userdata_normalize(&src,pdu);
}

/**
 * @brief Removes the trailing \r\n from input stream
 *
 * @param input input to modify
 * @return modified string
 */
static guchar* gsmd_sim_get_sms_body (GString* input)
{
    gchar *s = g_strdup(input->str);
    g_strstrip(s);
    g_string_truncate(input, 0);
    g_string_append(input,s);
    g_free( s );
    return (guchar*)input->str;
}



static void gsmd_sim_command_retrieve_messagebook (SIMInterface *sim,
                                                   gpointer ipc_data,
                                                   const gchar *status)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    g_debug("%s: Listing messages with status %s",__func__,status);

    gchar *valid[] = {"unread","read","unsent","sent","all"};
    gint i=0;
    GHashTable *data = gsmd_utils_create_hash_table();

    GArray *list = g_array_new(FALSE,TRUE,sizeof(int));
    gsmd_utils_table_insert_pointer(data,GSMD_SIM_KEY_MESSAGE_BOOK,list);

    gsmd_utils_table_insert_string(data,GSMD_SIM_KEY_MESSAGE_BOOK_TYPE,status);

    GString *param = g_string_new("");


    for (i=0;i<5;i++)
    {
        if (g_str_equal(status,valid[i]))
        {
            g_string_printf(param,"%d",i);
            gsmd_modem_post_at_command_id( modem,
                                           SMS_LIST,
                                           param->str,
                                           ipc_data,
                                           (ErrorFunction)modem->sim_ipc->retrieve_messagebook_error,
                                           (gpointer)modem->sim_ipc,
                                           INTERFACE_SIM,
                                           data);
            g_string_free(param,TRUE);
            return;
        }
    }

    g_string_free(param,TRUE);
    g_array_free(list,TRUE);
    g_hash_table_destroy(data);
    gsmd_utils_send_error_full((ErrorFunction)modem->sim_ipc->retrieve_messagebook_error,
                               modem->sim_ipc,
                               ipc_data,
                               GSMD_ERROR_UNKNOWN,
                               "INVALID SMS STATUS");
}

/**
 * @brief Creates the at command to delete a sms message.
 *
 * @param modem modem whose sms to delete
 * @param pos sms message's index
 */
static void gsmd_sim_command_delete (SIMInterface *sim,
                                     gpointer ipc_data,
                                     gint pos)
{
    ModemInterface *modem = (ModemInterface*)sim->priv;
    gchar *pos_str = g_strdup_printf ("%d", pos);
    gsmd_modem_post_at_command_id( modem,
                                   SMS_DELETE,
                                   pos_str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->delete_message_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
    g_free (pos_str);
}

/**
 * @brief Sends a sms from database
 *
 * Fetches specified sms from the database, prepares it for sending and
 * calls gsmd_sms_command_send to actually send the message.
 *
 * @param modem modem whose message to send
 * @param msgid id of the mssage to send
 */
void gsmd_sim_command_send_stored_message (SIMInterface *sim,
                                           gpointer ipc_data,
                                           gint index)
{
    //TODO want_report is currently ignored
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GString* message = g_string_new ("");
    g_string_printf(message, "%d",index);

    gsmd_modem_post_at_command_id( modem,
                                   SIM_SEND,
                                   message->str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->send_stored_message_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
    g_string_free(message,TRUE);
}

/**
* @brief Free function for retrieve_message method
*
* @param at at command context which contents should be freed
*/
static void gsmd_sms_handler_read_free(AtCommandContext *at)
{
    g_debug("%s",__func__);
    GValue *val = g_hash_table_lookup(at->handler_data,
                                      GSMD_SIM_KEY_PDU_MULTIPART_LIST);

    if (!val)
        return;

    GList *list = g_value_get_pointer(val);
    if (!list)
        return;

    GList *iter = list;
    while (iter)
    {
        multipart_sms *sms = iter->data;
        if (sms)
        {
            g_free(sms->message);
            g_free(sms);
        }
        iter = g_list_next(iter);
    }

    g_hash_table_remove(at->handler_data,
                        GSMD_SIM_KEY_PDU_MULTIPART_LIST);

    g_list_free(list);

}


/**
* @brief Gets message part's contents from given index
*
* Finds given index in glist containing multipart_sms and
* returns it's message, NULL if not found.
*
* @param list list containing multipart_sms
* @param index index to find
* @return message contents or null if not found
*/
gchar *gsmd_sim_handler_read_assemble_get_index(GList *list,gint index)
{
    while (list)
    {
        multipart_sms *sms = list->data;
        if (sms && sms->multipart_index == index)
            return sms->message;
        list = g_list_next(list);
    }

    return NULL;
}

/**
* @brief Gets the count of messages to assemble
*
* Gets the first multipart_sms from glist and returns it's
* multipart_count
* @param  list GList containing multipart_sms
* @return messages in to assemble
*/
gint gsmd_sim_handler_read_assemble_get_count(GList *list)
{
    if (!list)
        return -1;

    multipart_sms *sms = list->data;
    if (!sms)
        return -1;

    return sms->multipart_count;
}


/**
* @brief Assemble multipart messages
*
* Once all multipart messages are retrieved with AT+CMGL=4
* this function assembles those messages together and sends
* ipc reply
* @param modem pointer to modem interface
* @param at at command context
* @return handler status
*/
AtCommandHandlerStatus gsmd_sim_handler_read_assemble(ModemInterface *modem,
                                                      AtCommandContext *at)
{
    GList *list = gsmd_utils_table_get_pointer(at->handler_data,
                                               GSMD_SIM_KEY_PDU_MULTIPART_LIST,
                                               NULL);

    if (!list)
    {
        gsmd_utils_send_error(at,
                              GSMD_ERROR_UNKNOWN,
                              "INVALID HANDLER DATA");
        return AT_HANDLER_DONE_ERROR;
    }

    gint count = gsmd_sim_handler_read_assemble_get_count(list);

    if (count < 2)
    {
        gsmd_utils_send_error(at,
                              GSMD_ERROR_UNKNOWN,
                              "INVALID HANDLER DATA");
        return AT_HANDLER_DONE_ERROR;
    }

    GString *result = g_string_new("");
    gint i=1;

    for (i=1;i<=count;i++)
    {
        gchar *part = gsmd_sim_handler_read_assemble_get_index(list,i);
        if (part)
        {
            g_string_append(result,part);
        }
        else
        {
            g_string_append_printf(result,"<Message %d/%d missing>",i,count);
        }
    }

    g_debug("%s : Assembled message is '%s'",__func__,result->str);

    modem->sim_ipc->retrieve_message_reply(modem->sim_ipc,
                                           at->ipc_data,
                                           gsmd_utils_table_get_string(at->handler_data,
                                                                       GSMD_SIM_KEY_NUMBER,
                                                                       ""),
                                           result->str);

    g_string_free(result,TRUE);

    return AT_HANDLER_DONE;
}


/**
* @brief Checks if given pdu is multipart and part of message we are assembling
* and adds it to at->handler_data if it is.
*
* @param modem pointer to modem interface
* @param at at command context
* @param pdu of the message to check
* @return handler status
*/
AtCommandHandlerStatus gsmd_sim_handler_read_check_pdu(ModemInterface *modem,
                                                       AtCommandContext *at,
                                                       gchar *pdu)
{
    g_debug("%s",__func__);
    SmsParam *sms = g_try_new0(SmsParam,1);

    if (!gsmd_sim_decoder_pdu((guchar*)pdu,sms))
    {
        gsmd_sms_sms_param_free(sms);
        return AT_HANDLER_DONT_UNDERSTAND;
    }

    if (!sms->multipart)
    {
        gsmd_sms_sms_param_free(sms);
        return AT_HANDLER_NEED_MORE;
    }


    gint ref = gsmd_utils_table_get_int(at->handler_data,
                                        GSMD_SIM_KEY_PDU_MULTIPART_REF,
                                        0);

    if (sms->multipart_ref != ref)
    {
        g_debug("%s : got multipart ref %d, wanted %d",
                __func__,
                sms->multipart_ref,
                ref);

        gsmd_sms_sms_param_free(sms);
        return AT_HANDLER_NEED_MORE;
    }



    GList *list = gsmd_utils_table_get_pointer(at->handler_data,
                                               GSMD_SIM_KEY_PDU_MULTIPART_LIST,
                                               NULL);

    multipart_sms *part = g_try_new0(multipart_sms,1);
    part->multipart_count = sms->multipart_count;
    part->multipart_index = sms->multipart_index;
    part->multipart_ref = sms->multipart_ref;
    part->message = g_strdup(sms->message);

    g_debug("%s : Got part %d/%d of multipart message : '%s'",
            __func__,
            part->multipart_index,
            part->multipart_count,
            part->message);

    list = g_list_append(list,part);

    gsmd_utils_table_insert_pointer(at->handler_data,
                                    GSMD_SIM_KEY_PDU_MULTIPART_LIST,
                                    list);
    gsmd_sms_sms_param_free(sms);
    return AT_HANDLER_NEED_MORE;
}

/**
* @brief Handler to find multipart message's each part
*
* When retrieving a multipart message, all parts must be retrieved
* so at+cmgl=4 is sent to modem to list all messages. This function
* Finds all of the messages parts, assembles then and sends the
* ipc response.
*
* @param modem pointer to modem interface
* @param at command context
* @param response from modem
* @return handler status
*/
AtCommandHandlerStatus gsmd_sim_handler_read_list (ModemInterface *modem,
                                                   AtCommandContext *at,
                                                   GString *response)
{
    g_debug("%s",__func__);
    /*
    at+cmgl=1
    +CMGL: 1,1,"",145
    <pdu>
    +CMGL: 2,1,"",148
    <pdu>
    +CMGL: 3,1,"",158

    Regular expression:
    \+CMGL:\s(\d+),.*
    */

    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CMGL:\\s(?<id>\\d+),.*|(?<error>ERROR)|(?<ok>OK)|(?<pdu>[0-9A-F]+)", 0, 0, error);

    GMatchInfo *match_info;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        //Ignore +CMGL line
        if (gsmd_utils_match_exists(match_info,"id"))
        {
            status = AT_HANDLER_NEED_MORE;
        }
        else //If error occurs, then return error
            if (gsmd_utils_match_exists(match_info,"error"))
            {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "UNABLE TO READ MESSAGE");
                status = AT_HANDLER_DONE_ERROR;
                //If we get ok, then we'll assemble the pieces
                //and send reply
            }
            else if (gsmd_utils_match_exists(match_info,"ok"))
            {
                status = gsmd_sim_handler_read_assemble(modem,at);
            }
            else  //when we have a pdu, then check it and add to
                //list
                if (gsmd_utils_match_exists(match_info,"pdu"))
                {
                    status = gsmd_sim_handler_read_check_pdu(modem,
                                                             at,
                                                             response->str);
                }
    }

    g_free(error);
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;
}

/**
 * @brief Handler for replies sent by the gsm modem when a read command is sent.
 *
 * First message's pdu is returned and decoded. If it isn't a multipart message
 * then message's contents are sent through ipc. Otherwise the message is
 * assembled from it's parts.
 *
 * To assemble a message all it's parts have to be found. Message has a
 * reference to identify it from other multipart messages. This is stored
 * to at->handler_data. Then AT+CMGL=4
 * is sent to gsm modem to list all messages. Replies to them are handled
 * by gsmd_sim_handler_read_list which uses gsmd_sim_handler_read_check_pdu
 * to check if listed message has proper reference code. If it has proper
 * reference code then it is added to a list in at->handler_data.
 * once AT+CMGL=4 is done (OK is returned) then gsmd_sim_handler_read_assemble
 * is called to assemble the message from list within at->handler_data.
 * Once message is assembled, it is sent to ipc.
 *
 *
 * @param modem pointer to modem interface
 * @param at at command context
 * @param response modem's response
 * @return handler status
 */
AtCommandHandlerStatus gsmd_sim_handler_read (ModemInterface *modem,
                                              AtCommandContext *at,
                                              GString *response)
{
    /*         g_debug("%s",__func__); */
    //Do we have pdu to parse?
    gboolean pdu_ready = gsmd_utils_table_get_boolean(at->handler_data,
                                                      GSMD_SIM_KEY_PDU_READY,
                                                      FALSE);

    //Is it a multipart message? Then let gsmd_sim_handler_read_list
    //handle AT+CMGL=4's responses
    if (gsmd_utils_table_get_boolean(at->handler_data,
                                     GSMD_SIM_KEY_PDU_MULTIPART_LISTING,
                                     FALSE))
        return gsmd_sim_handler_read_list(modem,at,response);


    GScanner* scanner = modem->scanner;
    guchar* src;
    GString* input_data;

    guint symbol;
    g_assert( at );
    symbol = scanner->token;

    //pdu_ready will be set when there is +CMGR
    if (pdu_ready)
    {


        SmsParam *pdu = g_try_new0(SmsParam,1);
        //decode the pdu;
        input_data = g_string_new (response->str);
        src = gsmd_sim_get_sms_body (input_data);

        if (!gsmd_sim_decoder_pdu (src, pdu))
        {
            gsmd_sms_sms_param_free(pdu);
            return AT_HANDLER_DONT_UNDERSTAND;
        }

        if (pdu->multipart)   //if it's multipart then only the first part
        {
            //is a valid one from ipc's perspective
            //and others are invalid
            g_debug("%s : Got first sms of multipart message"
                    ,__func__);


            if (pdu->multipart_index != 1)
            {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN, //TODO change better error code
                                      "INVALID MESSAGE INDEX");
                gsmd_sms_sms_param_free(pdu);
                return AT_HANDLER_DONE_ERROR;
            }
            else
            {
                //mark handler_data that we are dealing with
                //multipart message and store messages reference
                //code
                gsmd_utils_table_insert_boolean(at->handler_data,
                                                GSMD_SIM_KEY_PDU_MULTIPART,
                                                TRUE);
                gsmd_utils_table_insert_int(at->handler_data,
                                            GSMD_SIM_KEY_PDU_MULTIPART_REF,
                                            pdu->multipart_ref);
            }
        }
        else
        {

            //If it's normal (single) message, store the message
            gsmd_utils_table_insert_string(at->handler_data,
                                           GSMD_SIM_KEY_MESSAGE,
                                           pdu->message);
        }

        //and store phone number and timestamp
        gsmd_utils_table_insert_string(at->handler_data,
                                       GSMD_SIM_KEY_NUMBER,
                                       pdu->number);

        /*
        gsmd_utils_table_insert_int(at->handler_data,
                                    GSMD_SIM_KEY_TIMESTAMP,
                                    pdu->timestamp);
        */


        g_string_free (input_data, TRUE);

        gsmd_utils_table_insert_boolean(at->handler_data,
                                        GSMD_SIM_KEY_PDU_READY,
                                        FALSE);
        gsmd_sms_sms_param_free(pdu);
        return AT_HANDLER_NEED_MORE;
    }

    switch (symbol)
    {
    case SYMBOL_OK:
    {
        //once we've got ok, check if the message is a multipart one
        //then we'll need to find the rest
        if (gsmd_utils_table_get_boolean(at->handler_data,
                                         GSMD_SIM_KEY_PDU_MULTIPART,
                                         FALSE))
        {

            gsmd_utils_table_insert_boolean(at->handler_data,
                                            GSMD_SIM_KEY_PDU_MULTIPART_LISTING,
                                            TRUE);

            gsmd_modem_serial_write_simple( modem,
                                            "AT+CMGL=4\r\n",
                                            INTERFACE_SMS);
            return AT_HANDLER_NEED_MORE;
        }

        GValue *msg = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_MESSAGE);
        GValue *num = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_NUMBER);
        //GValue *time = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_TIMESTAMP);
        if (!msg || !num)
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "OK WITHOUT REPSPONSE");
            g_warning("%s : Invalid handler data, number or message missing",__func__);
            return AT_HANDLER_ERROR;
        }

        if (modem->sim_ipc->retrieve_message_reply)
        {
            modem->sim_ipc->retrieve_message_reply(modem->sim_ipc,
                                                   at->ipc_data,
                                                   g_value_get_string(num),
                                                   g_value_get_string(msg)
                                                   /*,g_value_get_int(time)*/);
        }
    }
    return AT_HANDLER_DONE;
    case '+':
        g_scanner_get_next_token (scanner);
        if (scanner->token == SYMBOL_CMS)
        {
            g_warning ("read sms from simcard error\n");
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "FAILED TO READ FROM SIM CARD");
            return AT_HANDLER_DONE_ERROR;
        }
        else if (scanner->token == SYMBOL_CMGR)
        {

            g_scanner_get_next_token (scanner);//get :
            g_scanner_get_next_token (scanner);//get status
            //sms_status = scanner->value.v_int;
            gsmd_utils_table_insert_boolean(at->handler_data,
                                            GSMD_SIM_KEY_PDU_READY,
                                            TRUE);


            return AT_HANDLER_NEED_MORE;
        }
    }
    return AT_HANDLER_DONT_UNDERSTAND;

}


/**
 * @brief Creates the at command to read a sms message
 *
 * @param modem modem where to read from
 * @param pos index of the message to read
 */
void gsmd_sim_command_read (SIMInterface *sim,
                            gpointer ipc_data,
                            gint pos)
{
    gchar *pos_str = g_strdup_printf ("%d", pos);
    ModemInterface *modem = (ModemInterface*)sim->priv;
    GHashTable *data = gsmd_utils_create_hash_table();

    gsmd_utils_table_insert_boolean(data,
                                    GSMD_SIM_KEY_PDU_READY,
                                    FALSE);

    gsmd_modem_post_at_command_id( modem,
                                   SMS_READ,
                                   pos_str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->retrieve_message_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   data);
    g_free (pos_str);
}

/* void gsmd_sim_free_storage_spaces_array(GHashTable *table) */
/* { */
/*         int i=0; */
/*         GValue *val = g_hash_table_lookup(table,GSMD_SIM_KEY_STORAGE_SPACES); */
/*         if (val) { */
/*                 GArray *array = g_value_get_pointer(val); */

/*                 for (i=0;i<array->len;i++) { */
/*                         g_string_free(g_array_index(array, */
/*                                                     GString*, */
/*                                                     i), */
/*                                       TRUE); */
/*                 } */
/*                 g_array_free(array,TRUE); */


/*         } */
/* } */

/* static void gsmd_sim_storage_spaces_free(AtCommandContext *at) */
/* { */
/*         gsmd_sim_free_storage_spaces_array(at->handler_data); */
/* } */

/* AtCommandHandlerStatus */
/* gsmd_sim_handler_get_storage_spaces(ModemInterface *modem, */
/*                                           AtCommandContext *at, */
/*                                           GString *response) */
/* { */
/*         /\* */
/*         AT+CPBS=? */

/*         +CPBS: ("SM","FD","LD","MC","RC") */

/*         OK */
/*         *\/ */


/*         AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND; */
/*         GError *error = NULL; */
/*         GRegex *regex = g_regex_new ("(?<cpbs>\\+CPBS:\\s\\((\"\\w+\",?)+\\))|(?<error>ERROR)|(?<ok>OK)", 0, 0, NULL); */
/*         GMatchInfo *match_info; */

/*         if (g_regex_match (regex, response->str, 0, &match_info)) { */
/*                 if (gsmd_utils_match_exists(match_info,"cpbs")) { */
/*                         g_free(error); */
/*                         g_match_info_free (match_info); */
/*                         g_regex_unref (regex); */
/*                         error = NULL; */
/*                         match_info = NULL; */


/*                         regex = g_regex_new ("\"(?<id>\\w+)\"", 0, 0, NULL); */

/*                         if (g_regex_match (regex, response->str, 0, &match_info)) { */
/*                                 GString *line = NULL; */


/*                                 GArray *array = g_array_new(FALSE, */
/*                                                             TRUE, */
/*                                                             sizeof(GString*)); */
/*                                 gchar *match = NULL; */

/*                                 while (g_match_info_matches (match_info)) { */
/*                                         match = g_match_info_fetch_named(match_info,"id"); */
/*                                         line = g_string_new(match); */
/*                                         g_free(match); */
/*                                         g_array_append_val(array,line); */
/*                                         g_match_info_next (match_info, NULL); */
/*                                 } */

/*                                 gsmd_utils_table_insert_pointer(at->handler_data, */
/*                                                                 GSMD_SIM_KEY_STORAGE_SPACES, */
/*                                                                 array); */

/*                         } */

/*                         status = AT_HANDLER_NEED_MORE; */

/*                 } else if (gsmd_utils_match_exists(match_info,"error")) { */
/*                         gsmd_utils_send_error(at, */
/*                                               GSMD_ERROR_UNKNOWN, */
/*                                               "FAILED TO GET STORAGE SPACES"); */

/*                         status = AT_HANDLER_DONE_ERROR; */
/*                 } else if (gsmd_utils_match_exists(match_info,"ok")) { */
/*                         GValue *val = g_hash_table_lookup(at->handler_data, */
/*                                                           GSMD_SIM_KEY_STORAGE_SPACES); */

/*                         if (!val) { */
/*                                 g_warning("%s : Invalid handler data. storage spaces: %p", */
/*                                           __func__, */
/*                                           val); */

/*                                 gsmd_utils_send_error(at, */
/*                                                       GSMD_ERROR_UNKNOWN, */
/*                                                       "INVALID HANDLER DATA"); */
/*                                 status = AT_HANDLER_DONE_ERROR; */
/*                         } else { */
/*                                 gsmd_sim_free_storage_spaces_array(modem->caches[INTERFACE_SIM]); */
/*                                 g_hash_table_remove(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_STORAGE_SPACES); */

/* /\*                                 modem->sim_ipc->get_storage_spaces_reply(modem->sim_ipc, *\/ */
/* /\*                                                                                at->ipc_data, *\/ */
/* /\*                                                                                g_value_get_pointer(val)); *\/ */
/*                                 gsmd_utils_table_insert_pointer(modem->caches[INTERFACE_SIM], */
/*                                                                 GSMD_SIM_KEY_STORAGE_SPACES, */
/*                                                                 g_value_get_pointer(val)); */
/*                                 g_hash_table_remove(at->handler_data,GSMD_SIM_KEY_STORAGE_SPACES); */
/*                                 status = AT_HANDLER_DONE; */
/*                         } */
/*                 } */


/*         } */

/*         g_free(error); */
/*         g_match_info_free (match_info); */
/*         g_regex_unref (regex); */


/*         return status; */
/* } */


/* void gsmd_sim_command_get_storage_spaces(SIMInterface *sim, */
/*                                                gpointer ipc_data) */
/* { */
/*         ModemInterface *modem = (ModemInterface*)sim->priv; */
/*         GValue *val = g_hash_table_lookup(modem->caches[INTERFACE_SIM],GSMD_SIM_KEY_STORAGE_SPACES); */

/*         if (!modem->no_cache && val) { */
/* /\*                 modem->sim_ipc->get_storage_spaces_reply(modem->sim_ipc, *\/ */
/* /\*                                                                ipc_data, *\/ */
/* /\*                                                                g_value_get_pointer(val)); *\/ */
/*         } else { */

/*                 gsmd_modem_post_at_command_id( modem, */
/*                                                PHONEBOOK_SPACES_QUERY, */
/*                                                NULL, */
/*                                                ipc_data, */
/*                                                (ErrorFunction)modem->sim_ipc->get_storage_spaces_error, */
/*                                                (gpointer)modem->sim_ipc, */
/*                                                INTERFACE_SIM, */
/*                                                NULL); */
/*         } */
/* } */

AtCommandHandlerStatus gsmd_sim_handler_get_phonebook_status(ModemInterface *modem,
                                                             AtCommandContext *at,
                                                             GString *response)
{
    /*
    AT+CPBS?

    +CPBS: "SM",2,250

    OK

    \+CPBS:\s"(\w+)",\d+,\d+
    */
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GError *error = NULL;
    GRegex *regex = g_regex_new ("\\+CPBS:\\s\"(?<space>\\w+)\",(?<used>\\d+),(?<total>\\d+)|"
                                 "(?<error>ERROR)|(?<ok>OK)", 0, 0, &error);
    GMatchInfo *match_info;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"used"))
        {
            gchar *match = g_match_info_fetch_named(match_info,"used");
            gint used = atoi(match);
            gsmd_utils_table_insert_int(at->handler_data,GSMD_SIM_KEY_PHONEBOOK_SLOTS_USED,used);
            g_free(match);

            status = AT_HANDLER_NEED_MORE;
        }

        if (gsmd_utils_match_exists(match_info,"error"))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "FAILED TO GET DEFAULT PHONEBOOK STORAGE SPACE");
            status = AT_HANDLER_DONE_ERROR;
        }

        if (gsmd_utils_match_exists(match_info,"ok"))
        {
            GValue *val = g_hash_table_lookup(at->handler_data,
                                              GSMD_SIM_KEY_PHONEBOOK_SLOTS_USED);
            if (!val)
            {
                g_warning("%s : Invalid handler data.", __func__);

                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "OK WITHOUT RESPONSE");
            }
            status = AT_HANDLER_DONE;
        }

    }

    if ( error )
    {
        g_error_free(error);
    }
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;
}


/* AtCommandHandlerStatus gsmd_sim_handler_set_default_storage_space(ModemInterface *modem, */
/*                                                                         AtCommandContext *at, */
/*                                                                         GString *response) */
/* { */
/*         AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND; */
/*         GError **error = NULL; */
/*         GRegex *regex = g_regex_new ("(?<error>ERROR)|(?<ok>OK)", 0, 0, error); */
/*         GMatchInfo *match_info; */


/*         if (g_regex_match (regex, response->str, 0, &match_info)) { */
/*                 if (gsmd_utils_match_exists(match_info,"error")) { */
/*                         gsmd_utils_send_error(at, */
/*                                               GSMD_ERROR_UNKNOWN, */
/*                                               "FAILED TO SET DEFAULT PHONEBOOK STORAGE SPACE"); */
/*                         status = AT_HANDLER_DONE_ERROR; */
/*                 } */

/*                 if (gsmd_utils_match_exists(match_info,"ok")) { */
/*                         //Since storage space changed, it's index boundaries */
/*                         //might have changed aswell */
/*                         gsmd_utils_table_insert_int(modem->caches[INTERFACE_SIM], */
/*                                                     GSMD_SIM_KEY_LOWEST_INDEX, */
/*                                                     9999); */

/*                         gsmd_utils_table_insert_int(modem->caches[INTERFACE_SIM], */
/*                                                     GSMD_SIM_KEY_HIGHEST_INDEX, */
/*                                                     -9999); */

/*                         GValue *val = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_DEFAULT_STORAGE_SPACE); */

/*                         if (!val) { */
/*                                 g_warning("%s : Invalid handler data. default storage space: %p", */
/*                                           __func__, */
/*                                           val); */

/*                                 gsmd_utils_send_error(at, */
/*                                                       GSMD_ERROR_UNKNOWN, */
/*                                                       "INVALID HANDLER DATA"); */
/*                         } */

/*                         gsmd_utils_table_insert_copy(modem->caches[INTERFACE_SIM], */
/*                                                      GSMD_SIM_KEY_DEFAULT_STORAGE_SPACE, */
/*                                                      val); */

/*                         modem->sim_ipc->set_default_storage_space_reply(modem->sim_ipc, */
/*                                                                               at->ipc_data); */

/*                         status = AT_HANDLER_DONE; */
/*                 } */

/*         } */

/*         g_free(error); */
/*         g_match_info_free (match_info); */
/*         g_regex_unref (regex); */


/*         return status; */
/* } */


/* void gsmd_sim_command_set_default_storage_space(SIMInterface *sim, */
/*                                                       gpointer ipc_data, */
/*                                                       const char *space) */
/* { */
/*         ModemInterface *modem = (ModemInterface*)sim->priv; */

/*         GHashTable *data = gsmd_utils_create_hash_table(); */
/*         gsmd_utils_table_insert_string(data, */
/*                                        GSMD_SIM_KEY_DEFAULT_STORAGE_SPACE, */
/*                                        space); */

/*         gsmd_modem_post_at_command_id( modem, */
/*                                        PHONEBOOK_SPACE_SET, */
/*                                        space, */
/*                                        ipc_data, */
/*                                        (ErrorFunction)modem->sim_ipc->set_default_storage_space_error, */
/*                                        (gpointer)modem->sim_ipc, */
/*                                        INTERFACE_SIM, */
/*                                        data); */
/* } */

static
AtCommandHandlerStatus gsmd_sim_handler_get_index_boundaries(
    ModemInterface *modem,
    AtCommandContext *at,
    GString *response)
{
    /*
    AT+CPBR=?

    +CPBR: (1-250),20,20

    OK

    Regular expression: \+CPBR:\s\((\d+)-(\d+)\)
    */
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GError *error = NULL;
    GRegex *regex = g_regex_new ("\\+CPBR:\\s\\((?<min>\\d+)-(?<max>\\d+)\\),(?<number_len>\\d+),(?<name_len>\\d+)|"
                                 "(?<error>ERROR)|(?<ok>OK)", 0, 0, &error);
    GMatchInfo *match_info;
    gchar *match = NULL;



    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"min"))
        {
            match = g_match_info_fetch_named(match_info,"min");
            gint min = atoi(match);
            g_free(match);

            match = g_match_info_fetch_named(match_info,"max");
            gint max = atoi(match);
            gsmd_utils_table_insert_int(at->handler_data,GSMD_SIM_KEY_PHONEBOOK_SLOTS, max-min+1);
            g_free(match);

            match = g_match_info_fetch_named(match_info,"number_len");
            gint number_len = atoi(match);
            gsmd_utils_table_insert_int(at->handler_data,GSMD_SIM_KEY_PHONEBOOK_NUMBER_LEN, number_len);
            g_free(match);

            match = g_match_info_fetch_named(match_info,"name_len");
            gint name_len = atoi(match);
            gsmd_utils_table_insert_int(at->handler_data,GSMD_SIM_KEY_PHONEBOOK_NAME_LEN, name_len);
            g_free(match);

            status = AT_HANDLER_NEED_MORE;
        }
        else if (gsmd_utils_match_exists(match_info,"error"))
        {
            //TODO should empty string be sent as a reply instead?
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "UNABLE TO READ PHONEBOOK INFO");
            status = AT_HANDLER_DONE_ERROR;
        }
        else if (gsmd_utils_match_exists(match_info,"ok"))
        {
            GValue *slots = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_PHONEBOOK_SLOTS);
            GValue *number_len = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_PHONEBOOK_NUMBER_LEN);
            GValue *name_len = g_hash_table_lookup(at->handler_data,GSMD_SIM_KEY_PHONEBOOK_NAME_LEN);

            if (!slots || !number_len || !name_len)
            {
                g_warning("%s : Invalid handler data. slots: %p, number_len: %p, name_len: %p",
                          __func__, slots, number_len, name_len);

                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "INVALID HANDLER DATA");
                status = AT_HANDLER_DONE_ERROR;
            }
            else
            {
                status = AT_HANDLER_DONE;
            }
        }

    }

    if ( error )
    {
        g_error_free(error);
    }
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;
}


AtCommandHandlerStatus gsmd_sim_handler_delete_entry(ModemInterface *modem,
                                                     AtCommandContext *at,
                                                     GString *response)
{
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GError **error = NULL;
    GRegex *regex = g_regex_new ("(?<error>ERROR)|(?<ok>OK)", 0, 0, error);
    GMatchInfo *match_info;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"error"))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "FAILED TO DELETE PHONEBOOK ENTRY");
            status = AT_HANDLER_DONE_ERROR;
        }

        if (gsmd_utils_match_exists(match_info,"ok"))
        {
            modem->sim_ipc->delete_entry_reply(modem->sim_ipc,
                                               at->ipc_data);
            status = AT_HANDLER_DONE;
        }

    }

    g_free(error);
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;
}

void gsmd_sim_command_delete_entry(SIMInterface *sim,
                                   gpointer ipc_data,
                                   const int index)
{
    GString* message = g_string_new ("");
    g_string_printf(message, "%d",index);

    ModemInterface *modem = (ModemInterface*)sim->priv;
    gsmd_modem_post_at_command_id( modem,
                                   PHONEBOOK_ENTRY_DELETE,
                                   message->str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->delete_entry_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
    g_string_free(message,TRUE);
}



AtCommandHandlerStatus gsmd_sim_handler_store_entry(
    ModemInterface *modem,
    AtCommandContext *at,
    GString *response)
{
    /*
    AT+CPBW=2,+35844123456789,145,Name

    OK
    */

    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GError **error = NULL;
    GRegex *regex = g_regex_new ("(?<error>ERROR)|(?<ok>OK)", 0, 0, error);
    GMatchInfo *match_info;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"error"))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "FAILED TO STORE PHONEBOOK ENTRY");
            status = AT_HANDLER_DONE_ERROR;
        }

        if (gsmd_utils_match_exists(match_info,"ok"))
        {
            modem->sim_ipc->store_entry_reply(modem->sim_ipc,
                                              at->ipc_data);
            status = AT_HANDLER_DONE;
        }

    }

    g_free(error);
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;

}

void gsmd_sim_command_get_home_zones(SIMInterface *sim,
                                     gpointer ipc_data)
{
    //TODO implement
    ModemInterface *modem = (ModemInterface*)sim->priv;
    modem->sim_ipc->get_home_zones_reply(modem->sim_ipc,
                                         ipc_data,
                                         NULL);
}

void gsmd_sim_command_send_generic_sim_command(SIMInterface *sim,
                                               gpointer ipc_data,
                                               const gchar *command)
{
    //TODO implement
    ModemInterface *modem = (ModemInterface*)sim->priv;
    modem->sim_ipc->send_generic_sim_command_reply(modem->sim_ipc,
                                                   ipc_data,
                                                   NULL);
}

void gsmd_sim_command_send_restricted_sim_command(SIMInterface *sim,
                                                  gpointer ipc_data,
                                                  gint command,
                                                  gint fileid,
                                                  gint p1,
                                                  gint p2,
                                                  gint p3,
                                                  const gchar *data)
{
    //TODO implement
    ModemInterface *modem = (ModemInterface*)sim->priv;
    modem->sim_ipc->send_generic_sim_command_reply(modem->sim_ipc,
                                                   ipc_data,
                                                   NULL);
}


void gsmd_sim_command_store_entry(SIMInterface *sim,
                                  gpointer ipc_data,
                                  const int index,
                                  const char *name,
                                  const char *number)
{
    GString* message = g_string_new ("");
    // TODO Better number checks
    if (*number == '+')
    {
        g_string_printf(message, "%d,%s,145,%s",index,number,name);
    }
    else
    {
        g_string_printf(message, "%d,%s,129,%s",index,number,name);
    }

    ModemInterface *modem = (ModemInterface*)sim->priv;
    gsmd_modem_post_at_command_id( modem,
                                   PHONEBOOK_ENTRY_SET,
                                   message->str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->store_entry_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
    g_string_free(message,TRUE);
}


AtCommandHandlerStatus gsmd_sim_handler_retrieve_entry(ModemInterface *modem,
                                                       AtCommandContext *at,
                                                       GString *response)
{
    /*
    AT+CPBR=1

    +CPBR: 1,"040123456789",129,"Name"

    OK

    Regular expression: \+CPBR:\s(\d+),"(\w+)",(\d+),"(\w+)"
    */
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CPBR:\\s(?<id>\\d)+,\"(?<number>\\+?\\w+)\",(?<type>\\d+),\"(?<text>\\w+)\"|(?<error>ERROR)|(?<ok>OK)", 0, 0, error);

    GMatchInfo *match_info;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"id"))
        {

            gchar *type = g_match_info_fetch_named(match_info,"type");
            gchar *name = g_match_info_fetch_named(match_info,"text");
            gchar *number = g_match_info_fetch_named(match_info,"number");

            //TODO only send this once we have ok
            modem->sim_ipc->retrieve_entry_reply(modem->sim_ipc,
                                                 at->ipc_data,
                                                 name,
                                                 number);
            g_free(type);
            g_free(name);
            g_free(number);

            status = AT_HANDLER_NEED_MORE;
        }

        if (gsmd_utils_match_exists(match_info,"error"))
        {
            //TODO should empty string be sent as a reply instead?
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "UNABLE TO READ PHONEBOOK INDEX BOUNDARIES");
            status = AT_HANDLER_DONE_ERROR;
        }

        if (gsmd_utils_match_exists(match_info,"ok"))
        {
            status = AT_HANDLER_DONE;
        }

    }

    g_free(error);
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;
}



void gsmd_sim_command_retrieve_entry(SIMInterface *sim,
                                     gpointer ipc_data,
                                     const int index)
{
    GString* message = g_string_new ("");
    g_string_printf(message, "%d",index);

    ModemInterface *modem = (ModemInterface*)sim->priv;
    gsmd_modem_post_at_command_id( modem,
                                   PHONEBOOK_ENTRY_GET,
                                   message->str,
                                   ipc_data,
                                   (ErrorFunction)modem->sim_ipc->retrieve_entry_error,
                                   (gpointer)modem->sim_ipc,
                                   INTERFACE_SIM,
                                   NULL);
    g_string_free(message,TRUE);
}


void gsmd_sim_init(ModemInterface *modem)
{
    modem->sim->priv = (gpointer)modem;
    modem->sim->send_auth_code = &gsmd_sim_command_setup_pin;
    modem->sim->get_auth_status = &gsmd_sim_command_query_pin_status;
    modem->sim->get_sim_info = &gsmd_sim_command_get_sim_info;
    modem->sim->change_auth_code = &gsmd_sim_command_change_auth_code;
    modem->sim->unlock = &gsmd_sim_command_unlock;
    modem->sim->retrieve_messagebook = &gsmd_sim_command_retrieve_messagebook;
    modem->sim->retrieve_phonebook = &gsmd_sim_command_retrieve_phonebook;
    modem->sim->send_stored_message = &gsmd_sim_command_send_stored_message;
    modem->sim->delete_message = &gsmd_sim_command_delete;
    modem->sim->set_service_center_number = &gsmd_sim_command_set_service_center;
    modem->sim->get_service_center_number = &gsmd_sim_command_query_service_center;
    modem->sim->store_message = &gsmd_sim_command_store_message;
    modem->sim->retrieve_message = &gsmd_sim_command_read;
    modem->sim->retrieve_entry = &gsmd_sim_command_retrieve_entry;
    modem->sim->store_entry = &gsmd_sim_command_store_entry;
    modem->sim->delete_entry = &gsmd_sim_command_delete_entry;
    modem->sim->get_phonebook_info = &gsmd_sim_command_get_phonebook_info;
    modem->sim->send_generic_sim_command = &gsmd_sim_command_send_generic_sim_command;
    modem->sim->send_restricted_sim_command = &gsmd_sim_command_send_restricted_sim_command;
    modem->sim->get_home_zones = &gsmd_sim_command_get_home_zones;
    gsmd_sim_init_at_handler(modem);

}

void gsmd_sim_deinitialize(ModemInterface *modem)
{
}
