/*
 *  dbus_pdp_object.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef DBUS_PDP_OBJECT_H
#define DBUS_PDP_OBJECT_H


#include <glib-object.h>
#include "dbus_objects.h"
#include "pdp_interface.h"

typedef struct DBusPDPObject DBusPDPObject;
typedef struct DBusPDPObjectClass DBusPDPObjectClass;

GType dbus_pdp_object_get_type (void);


struct DBusPDPObject {
        GObject parent;
        PDPInterface *pdp;
};

struct DBusPDPObjectClass {
        GObjectClass parent;
        guint context_activated;
        guint context_deactivated;
        guint context_changed;
};

/* Macros that are needed for making the proper GObject */
#define DBUS_PDP_TYPE_OBJECT              (dbus_pdp_object_get_type ())
#define DBUS_PDP_OBJECT(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), DBUS_PDP_TYPE_OBJECT, DBusPDPObject))
#define DBUS_PDP_OBJECT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), DBUS_PDP_TYPE_OBJECT, DBusPDPObjectClass))
#define DBUS_IS_PDP_OBJECT(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), DBUS_PDP_TYPE_OBJECT))
#define DBUS_IS_PDP_OBJECT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), DBUS_PDP_TYPE_OBJECT))
#define DBUS_PDP_OBJECT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), DBUS_PDP_TYPE_OBJECT, DBusPDPObjectClass))

DBusPDPObject *gsmd_dbus_pdp_initialize (PDPIPCInterface *pdp_ipc, PDPInterface *pdp);
void gsmd_dbus_pdp_uninitialize(DBusPDPObject *obj);

//Methods
gboolean smartphone_pdp_list_gprs_classes(DBusPDPObject *obj, DBusGMethodInvocation *method_context);
gboolean smartphone_pdp_select_gprs_class(DBusPDPObject *obj, const gchar *klass, DBusGMethodInvocation *method_context);
gboolean smartphone_pdp_activate(DBusPDPObject *obj, const gint index, DBusGMethodInvocation *method_context);
gboolean smartphone_pdp_deactivate(DBusPDPObject *obj, const gint index, DBusGMethodInvocation *method_context);
gboolean smartphone_pdp_select_context(DBusPDPObject *obj, const gchar *context, DBusGMethodInvocation *method_context);
gboolean smartphone_pdp_add_context(DBusPDPObject *obj, const gchar *context, DBusGMethodInvocation *method_context);
gboolean smartphone_pdp_delete_context(DBusPDPObject *obj, const gint index, DBusGMethodInvocation *method_context);
gboolean smartphone_pdp_list_contexts(DBusPDPObject *obj, DBusGMethodInvocation *method_context);



//Signals




#endif
