/*
 *  pdp_interface.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef PDP_INTERFACE
#define PDP_INTERFACE

typedef struct _PDPInterface PDPInterface;

/**
* @brief PDP interface
*/
struct _PDPInterface {
        gpointer priv;

        /**
        * @brief Lists gprs classes
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*list_gprs_classes) (PDPInterface *pdp,
                                   gpointer ipc_data);

        /**
        * @brief Selects gprs class
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*select_gprs_class) (PDPInterface *pdp,
                                   gpointer ipc_data,
                                   const char *class);

        /**
        * @brief Activates ??
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*activate) (PDPInterface *pdp,
                          gpointer ipc_data,
                          int index);

        /**
        * @brief Deactivates ??
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*deactivate) (PDPInterface *pdp,
                            gpointer ipc_data,
                            int index);

        /**
        * @brief Selects context ??
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*select_context) (PDPInterface *pdp,
                                gpointer ipc_data,
                                const char *context);

        /**
        * @brief Adds context ??
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*add_context) (PDPInterface *pdp,
                             gpointer ipc_data,
                             const char *context);

        /**
        * @brief Deletes context ??
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*delete_context) (PDPInterface *pdp,
                                gpointer ipc_data,
                                int index);

        /**
        * @brief Lists contexts ??
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*list_contexts) (PDPInterface *pdp,
                               gpointer ipc_data);

};

typedef struct _PDPIPCInterface PDPIPCInterface;

struct _PDPIPCInterface {

        gpointer priv;

        /*********************Replies to methods*****************/

        /**
        * @brief Reply to "list_gprs_classes" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*list_gprs_classes_reply) (PDPIPCInterface *pdp_ipc,
                                         gpointer ipc_data,
                                         const char **classes);

        /**
        * @brief Reply to "select_gprs_class" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*select_gprs_class_reply) (PDPIPCInterface *pdp_ipc,
                                         gpointer ipc_data);

        /**
        * @brief Reply to "activate" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*activate_reply) (PDPIPCInterface *pdp_ipc,
                                gpointer ipc_data);

        /**
        * @brief Reply to "deactivate" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*deactivate_reply) (PDPIPCInterface *pdp_ipc,
                                  gpointer ipc_data);

        /**
        * @brief Reply to "select_context" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*select_context_reply) (PDPIPCInterface *pdp_ipc,
                                      gpointer ipc_data);

        /**
        * @brief Reply to "add_context" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*add_context_reply) (PDPIPCInterface *pdp_ipc,
                                   gpointer ipc_data,
                                   int index);

        /**
        * @brief Reply to "delete_context" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*delete_context_reply) (PDPIPCInterface *pdp_ipc,
                                      gpointer ipc_data);

        /**
        * @brief Reply to "list_contexts" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*list_contexts_reply) (PDPIPCInterface *pdp_ipc,
                                     gpointer ipc_data,
                                     const char **contexts);


        /*********************Method errors**********************************/
        /**
        * @brief Error reply to "list_gprs_classes" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*list_gprs_classes_error) (PDPIPCInterface *pdp_ipc,
                                         gpointer ipc_data,
                                         GError *error);

        /**
        * @brief Error reply to "select_gprs_class" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*select_gprs_class_error) (PDPIPCInterface *pdp_ipc,
                                         gpointer ipc_data,
                                         GError *error);

        /**
        * @brief Error reply to "activate" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*activate_error) (PDPIPCInterface *pdp_ipc,
                                gpointer ipc_data,
                                GError *error);

        /**
        * @brief Error reply to "deactivate" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*deactivate_error) (PDPIPCInterface *pdp_ipc,
                                  gpointer ipc_data,
                                  GError *error);

        /**
        * @brief Error reply to "select_context" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*select_context_error) (PDPIPCInterface *pdp_ipc,
                                      gpointer ipc_data,
                                      GError *error);

        /**
        * @brief Error reply to "add_context" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*add_context_error) (PDPIPCInterface *pdp_ipc,
                                   gpointer ipc_data,
                                   GError *error);

        /**
        * @brief Error reply to "delete_context" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*delete_context_error) (PDPIPCInterface *pdp_ipc,
                                      gpointer ipc_data,
                                      GError *error);

        /**
        * @brief Error reply to "list_contexts" method
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*list_contexts_error) (PDPIPCInterface *pdp_ipc,
                                     gpointer ipc_data,
                                     GError *error);





        /*********************Signals**********************************/
        /**
        * @brief Context activated ??
        *
        * @param modem pointer to modem struct
        * @param id ??
        */
        void (*context_activated) (PDPIPCInterface *pdp_ipc, int id);

        /**
        * @brief Context deactivated ??
        *
        * @param modem pointer to modem struct
        * @param id ??
        */
        void (*context_deactivated) (PDPIPCInterface *pdp_ipc, int id);

        /**
        * @brief Context changed ??
        *
        * @param modem pointer to modem struct
        * @param id ??
        */
        void (*context_changed) (PDPIPCInterface *pdp_ipc, int id);


};

#endif



