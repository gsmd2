/*
 *  pdp.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include "pdp.h"

void gsmd_pdp_command_list_gprs_classes(PDPInterface *pdp,
                                        gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)pdp->priv;
        const char *classes[] = {"First","Second","Third"};
        modem->pdp_ipc->list_gprs_classes_reply(modem->pdp_ipc,ipc_data,classes);
}


void gsmd_pdp_command_select_gprs_class(PDPInterface *pdp,
                                        gpointer ipc_data,
                                        const char *class)
{
        ModemInterface *modem = (ModemInterface*)pdp->priv;
        modem->pdp_ipc->select_gprs_class_reply(modem->pdp_ipc,ipc_data);
}


void gsmd_pdp_command_activate(PDPInterface *pdp,
                               gpointer ipc_data,
                               int index)
{
        ModemInterface *modem = (ModemInterface*)pdp->priv;
        modem->pdp_ipc->activate_reply(modem->pdp_ipc,ipc_data);
}


void gsmd_pdp_command_deactivate(PDPInterface *pdp,
                                 gpointer ipc_data,
                                 int index)
{
        ModemInterface *modem = (ModemInterface*)pdp->priv;
        modem->pdp_ipc->deactivate_reply(modem->pdp_ipc,ipc_data);
}


void gsmd_pdp_command_select_context(PDPInterface *pdp,
                                     gpointer ipc_data,
                                     const char *context)
{
        ModemInterface *modem = (ModemInterface*)pdp->priv;
        modem->pdp_ipc->select_context_reply(modem->pdp_ipc,ipc_data);
}


void gsmd_pdp_command_add_context(PDPInterface *pdp,
                                  gpointer ipc_data,
                                  const char *context)
{
        ModemInterface *modem = (ModemInterface*)pdp->priv;
        modem->pdp_ipc->add_context_reply(modem->pdp_ipc,ipc_data,0);
}


void gsmd_pdp_command_delete_context(PDPInterface *pdp,
                                     gpointer ipc_data,
                                     int index)
{
        ModemInterface *modem = (ModemInterface*)pdp->priv;
        modem->pdp_ipc->delete_context_reply(modem->pdp_ipc,ipc_data);
}



void gsmd_pdp_command_list_contexts(PDPInterface *pdp,
                                    gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)pdp->priv;
        const char *contexts[] = {"First","Second","Third"};
        modem->pdp_ipc->list_contexts_reply(modem->pdp_ipc,ipc_data,contexts);
}




void gsmd_pdp_init(ModemInterface *modem)
{
        modem->pdp->priv = (gpointer)modem;
        modem->pdp->list_contexts = &gsmd_pdp_command_list_contexts;
        modem->pdp->delete_context = &gsmd_pdp_command_delete_context;
        modem->pdp->add_context = &gsmd_pdp_command_add_context;
        modem->pdp->select_context = &gsmd_pdp_command_select_context;
        modem->pdp->deactivate = &gsmd_pdp_command_deactivate;
        modem->pdp->activate = &gsmd_pdp_command_activate;
        modem->pdp->select_gprs_class = &gsmd_pdp_command_select_gprs_class;
        modem->pdp->list_gprs_classes = &gsmd_pdp_command_list_gprs_classes;
}

