/*
 *  dbus_sms_object.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef DBUS_SMS_OBJECT_H
#define DBUS_SMS_OBJECT_H



#include <glib-object.h>
#include "sms_interface.h"
#include "dbus_objects.h"


typedef struct DBusSmsObject DBusSmsObject;
typedef struct DBusSmsObjectClass DBusSmsObjectClass;


struct DBusSmsObject {
        GObject parent;
        SMSInterface *sms;
};

struct DBusSmsObjectClass {
        GObjectClass parent;
        guint incoming_message;
        guint message_sent;
};

/* Macros that are needed for making the proper GObject */
#define DBUS_SMS_TYPE_OBJECT              (dbus_sms_object_get_type ())
#define DBUS_SMS_OBJECT(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), DBUS_SMS_TYPE_OBJECT, DBusSmsObject))
#define DBUS_SMS_OBJECT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), DBUS_SMS_TYPE_OBJECT, DBusSmsObjectClass))
#define DBUS_IS_SMS_OBJECT(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), DBUS_SMS_TYPE_OBJECT))
#define DBUS_IS_SMS_OBJECT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), DBUS_SMS_TYPE_OBJECT))
#define DBUS_SMS_OBJECT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), DBUS_SMS_TYPE_OBJECT, DBusSmsObjectClass))

DBusSmsObject *gsmd_dbus_sms_initialize (SMSIPCInterface *sms_ipc, SMSInterface *sms);
void gsmd_dbus_sms_uninitialize(DBusSmsObject *obj);


gboolean smartphone_sms_get_service_bearer(DBusSmsObject *obj,
                                           DBusGMethodInvocation *method_context);

gboolean smartphone_sms_set_service_bearer(DBusSmsObject *obj,
                                           const gchar *mode,
                                           DBusGMethodInvocation *method_context);

gboolean smartphone_sms_send_message(DBusSmsObject *obj,
                                     const gchar *message,
                                     const gchar *number,
                                     const gboolean want_report,
                                     DBusGMethodInvocation *method_context);
#endif
