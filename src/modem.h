/*
 *  modem.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef GSMD_MODEM_H
#define GSMD_MODEM_H

#include <glib.h>
#include "serial.h"
#include "call_interface.h"
#include "sim_interface.h"
#include "sms_interface.h"
#include "pdp_interface.h"
#include "network_interface.h"
#include "device_interface.h"



#define GSMD_MODEM_KEY_PHONE_NUMBER "number"
#define GSMD_MODEM_KEY_NETWORK_STATUS "network_status"
#define GSMD_MODEM_KEY_CALL_STATUS_ARRAY "call_status_array"

/**
* @brief Modem's status
*/
typedef enum {

        /**
        * @brief Modem is uninitialized
        * Might also have once worked but timed out on AT command
        */
        MODEM_UNINITIALIZED=0,

        /**
        * @brief Modem's serial ports are initialized
        * Waiting for initializing at commands to finish.
        */
        MODEM_SERIAL_INIT,

        /**
        * @brief Modem is currently being initialized
        */
        MODEM_INITIALIZING,

        /**
        * @brief Modem is ready to receive commands
        */
        MODEM_READY,

        /**
        * @brief Modem should be resetted
        */
        MODEM_RESET
} ModemStatus;

/**
* @brief Call status
*/
typedef enum {
        CALL_IDLE=0,     /**< Idle*/
        CALL_CALLING,    /**< Calling. After send ATD command, before call is connected */
        CALL_CONNECTED,  /**< Call established */
        CALL_WAITING,    /**< Wainting for remote to accept our call*/
        CALL_INCOMING,   /**< Incoming call */
        CALL_HOLD,       /**< Call is on hold */
} CallStatus;


//TODO these aren't defined in freesmartphone's api
/**
* @brief Call status
*/
typedef enum {
        SIM_UNKNOWN=0,      /**< PIN status hasn't been checked yet */
        SIM_ERROR,          /**< Error reading sim card             */
        SIM_MISSING,        /**< SIM card is missing                */
        SIM_NEED_PIN,       /**< PIN code needs to be entered       */
        SIM_NEED_PUK,       /**< PUK code needs to be entered       */
        SIM_READY,          /**< Get RING with CLIP                 */
} SIMStatus;

/**
* @brief Network status
*/
typedef enum {

        /**
        * Not registered, not searching a new operator to register to.
        */
        NETWORK_UNREGISTERED = 0,

        /**
        * Registered, home network
        */
        NETWORK_REGISTERED,

        /**
        * Not registered, but currently searching a new operator to
        * register to
        */
        NETWORK_BUSY,

        /**
        * Registration denied
        */
        NETWORK_DENIED,

        /**
        * Unknown
        */
        NETWORK_UNKNOWN,

        /**
        * Registered, roaming
        */
        NETWORK_ROAMING,
} NetworkStatus;

/**
* @brief AT command symbol table struct
*/
enum {
        SYMBOL_OK = G_TOKEN_LAST + 1,
        SYMBOL_ERROR ,
        SYMBOL_RING ,
        SYMBOL_NO ,
        SYMBOL_CARRIER ,
        SYMBOL_ANSWER ,
        SYMBOL_CME ,
        SYMBOL_CMS ,
        SYMBOL_COPS ,
        SYMBOL_CPIN ,
        SYMBOL_READY ,
        SYMBOL_CPAS ,
        SYMBOL_CSQ ,
        SYMBOL_CREG,
        SYMBOL_BUSY,
        SYMBOL_CMUT,
        SYMBOL_CFUN,
        SYMBOL_CLIP,
        SYMBOL_CRING,
        SYMBOL_GREATER,
        SYMBOL_CMTI,
        SYMBOL_CMGR,
        SYMBOL_SIM,
        SYMBOL_CCLK,
        SYMBOL_CSCA,
        SYMBOL_CRSL,
        SYMBOL_CMGW,
        SYMBOL_CCWA,
        SYMBOL_CGSN,
        SYMBOL_CGMR,
        SYMBOL_CGMM,
        SYMBOL_CGMI,
        SYMBOL_PIN,
        SYMBOL_PUK,
        SYMBOL_TIME_OUT,

        VENDOR_SYMBOL
};


typedef struct _VendorInterface VendorInterface;

typedef struct _SerialInterface SerialInterface;

typedef struct _ModemInterface ModemInterface;

typedef enum {
        AT_HANDLER_DONE      = 0,   /**< Completed successfully */
        AT_HANDLER_DONE_ERROR,      /**< Completed with an error from modem*/
        AT_HANDLER_NEED_MORE,       /**< Need more input(response) from modem */
        AT_HANDLER_DONT_UNDERSTAND, /**< Handler doesn't understand the input */
        AT_HANDLER_RETRY,           /**< Retry needed */
        AT_HANDLER_ERROR            /**< Unexpected error */
} AtCommandHandlerStatus;

/**
* @brief AtCommand handler function definition
**/
typedef gboolean (*AtCommandPrepare)(ModemInterface *modem);


/**
* @brief AtCommand handler function definition
**/
typedef void (*ErrorFunction)(gpointer error_data, gpointer ipc_data, GError *error);

typedef struct _AtCommand AtCommand;
/**
 * @brief Symbol table struct, it is used by Scanner for token analysis
 */
typedef struct _SymbolTable {
        gchar *symbol_name;
        guint  symbol_token;
} SymbolTable;


/**
* @brief Call struct, stores information on active calls
*/
typedef struct _Call {
        /**
        * @brief Call's current status
        */
        CallStatus status;

        /**
        * @brief Remote ends phonenumber
        */
        GString *number;

        /**
        * @brief Call's type
        */
        GString *type;

        /**
        * @brief Calls id in the gsmd
        * This id is the one published through interfaces
        */
        guint id;
        /**
        * @brief Calls index on modem
        * This is the index in for example at+clcc
        */
        gint index;
        /**
        * @brief Calls vendor id
        * This id is used internally by vendors
        */
        gint vendor_id;
        /**
         * @brief Calls vendor properties
         * This id is used internally by vendors
         * and value in it are sent with call status
         */
        GHashTable *vendor_properties;
}Call;

/**
* @brief AT command ids
*/
typedef enum {
        PIN_QUERY = G_TOKEN_LAST,
        QUERY_SUBSCRIBER_NUMBERS,
        REPLY_COMMAND,
        SOFT_RESET,
        AT,
        MODEM_DTMF_QUERY,
        MODEM_DTMF_SET,
        SET_OP_QUERY_REPLY,
        PIN_SETUP,
        PIN_CHANGE,
        PUK_SETUP,
        SET_PDU_MODE,
        SET_OPERATOR,
        SET_OPERATOR_LOCKED,
        CUR_OP_QUERY,
        OPS_QUERY,
        POWER_ON,
        MODEM_STATUS_QUERY,
        NETWORK_REG_QUERY,
        NETWORK_REG_ENABLE,
        NETWORK_REGISTER,
        SIGNAL_QUERY,
        ACTIVATE_CALL,
        QUERY_SEND_IDENTIFICATION,
        SET_SEND_IDENTIFICATION,
        CUR_CALLS_QUERY,
        ACCEPT_CALL,
        CONFERENCE_ACTIVATE_CALL,
        CONFERENCE_ACCEPT_CALL,
        CONFERENCE_JOIN,
        HANGUP_CALLS,
        HANGUP_CURRENT,
        RELEASE_CALL,
        RELEASE_HELD,
        INITIATE_CALL,
        MUTE_QUERY,
        ENABLE_CLIP,
        ENABLE_CNMI,
        ENABLE_CMEE,
        ENABLE_CCWA,
        DISABLE_ECHO,
        SET_DATE_TIME,
        DATE_TIME_QUERY,
        SMS_READ,
        SMS_SEND,
        SMS_STORE,
        SIM_SEND,
        SMS_DELETE,
        SMS_SET_CENTER,
        SMS_CENTER_QUERY,
        SMS_LIST,
        SMS_INIT,
        RING_VOLUME_QUERY,
        SEND_DTMF,
        ESCAPE_COMMAND,
        SERVICE_BEARER_QUERY,
        SET_SERVICE_BEARER,
        CAPABILITIES_GET,
        MANUFACTURER_QUERY,
        MODEL_QUERY,
        SW_REVISION_QUERY,
        IMEI_QUERY,
        IMSI_QUERY,
        ANTENNA_POWER_GET,
        ANTENNA_POWER_SET,
        PHONEBOOK_SPACES_QUERY,
        PHONEBOOK_STATUS_QUERY,
        PHONEBOOK_SPACE_SET,
        PHONEBOOK_GET_INDEX_BOUNDARIES,
        PHONEBOOK_ENTRY_GET,
        PHONEBOOK_ENTRY_SET,
        PHONEBOOK_ENTRY_DELETE,

        VENDOR_COMMAND
} AtCommandID;

typedef enum {
        INTERFACE_GENERAL=0,
        INTERFACE_CALL,
        INTERFACE_DEVICE,
        INTERFACE_NETWORK,
        INTERFACE_PDP,
        INTERFACE_SIM,
        INTERFACE_SMS,
        INTERFACE_LAST
} InterfaceType;

typedef struct _AtCommandContext AtCommandContext;

/**
* @brief AtCommand handler function definition
**/
typedef AtCommandHandlerStatus (*AtCommandHandler)(ModemInterface *modem,
                                                   AtCommandContext *at,
                                                   GString *response);

typedef gpointer (*IPCInit)(ModemInterface *modem);

/**
* @brief Modem initilize function definition
**/
typedef void (*InitATCommand) (ModemInterface* modem);

struct _ModemInterface {

        gpointer priv;

        /**
        * If true, all responses are requested from modem
        */
        gboolean no_cache;

        /**
         * Loaded configuration file so that vendor can access it too.
         */
        GKeyFile *conf_file;

        /**
        * Scanner of input buffer
        */
        GScanner *scanner;

        /**
        * voice call interface
        */
        CallInterface *call;

        /**
        * voice call IPC interface
        */
        CallIPCInterface *call_ipc;

        /**
        * sms interface
        */
        SMSInterface *sms;

        /**
        * sms ipc interface
        */
        SMSIPCInterface *sms_ipc;

        /**
        * Device interface
        */
        DeviceInterface *device;

        /**
        * Device IPC interface
        */
        DeviceIPCInterface *device_ipc;

        /**
        * Network interface
        */
        NetworkInterface *network;

        /**
        * Network IPC interface
        */
        NetworkIPCInterface *network_ipc;

        /**
        * PDP interface
        */
        PDPInterface *pdp;

        /**
        * PDP IPC interface
        */
        PDPIPCInterface *pdp_ipc;

        /**
        * SIM interface
        */
        SIMInterface *sim;

        /**
        * SIM ipc interface
        */
        SIMIPCInterface *sim_ipc;

        /**
         * Pointer to inter-process communication data
         */
        void *ipc_data;

        /**
        * Does sim card need pin code before being able to send commands
        */
        SIMStatus sim_status;

        /**
        * modem status
        */
        GList *calls;

        /**
        * Devices used by different interfaces
        */
        gint interface_devices[INTERFACE_LAST];

        /**
        * Devices used by different interfaces
        */
        GList *devices;

        /**
        * Handle to sms database
        */
        gpointer sms_handle;

        /**
         * Caches to hold data read from modem by interfaces
         */
        GHashTable *caches[INTERFACE_LAST];

        /**
        * Is modem alive (responses to AT command)
        */
        ModemStatus status;

        gboolean no_call_monitoring_needed;
        /**
         * @brief Timer used to monitor call status
         *
         */
        guint call_monitor_timer;
};


/**
* @brief A function to free at command context's data
* Used to free e.g. GArray within handler_data
**/
typedef void (*ATCommandContextFree) (AtCommandContext *at);


/**
 * @brief Struct representing an AT command.
 *
 * See also AtCommandContext
 */
struct _AtCommand {
        /**
         * AtCommand id
         */
        const guint cmd_id;
        /**
         * AtCommand buffer
         */
        gchar *command;
        /**
         * AtCommand timeout in milliseconds
         */
        guint timeout;
        /**
         * Can this command be cancelled
         */
        gboolean cancellable;
        /**
        * default retry count for the command
        */
        gint retry;
        /**
         * AT command response handle function
         */
        AtCommandHandler handler;
        /**
         * AT command prepare function
         */
        AtCommandPrepare prepare;

        /**
        * Minimum sim state required for sending this command
        */
        SIMStatus required_state;

        /**
        * Function to call before freeing handler_data
        */
        ATCommandContextFree free_function;
};


/**
* @brief Vendor Init
**/
typedef gboolean (*VendorInit) ( VendorInterface *vendor, ModemInterface* modem );

/**
* @brief Vendor AT Init
**/
typedef gboolean (*VendorATInit) ( VendorInterface *vendor);

/**
* @brief Vendor AT Init
**/
typedef gboolean (*VendorModemWakeup) ( VendorInterface *vendor);

/**
* @brief Function to call when a modem is not responding
* @return TRUE if modem is alive and AT command should be tried again
* FALSE if vendor was unable to bring modem alive again
**/
typedef gboolean (*VendorModemNotResponding) ( VendorInterface *vendor);

/**
 * @brief Function to call when a modem is not responding
 * @param vendor
 * @param modem
 * @param command AT command that is to be executed
 * @return AT command that is executed before command
**/
typedef AtCommandContext* (*VendorCommandPrepare) ( VendorInterface *vendor,
                                                    ModemInterface *modem,
                                                    AtCommandContext *command);

/**
 * @brief Function to call when modem receives data
 * This is called before data is handled.
 **/
typedef void (*VendorDataIn)(ModemInterface *modem,
                             AtCommandContext *at,
                             GString *data);
struct _VendorInterface {

        gpointer priv;

        VendorATInit init_at;
        VendorATInit init_at_sim_ready;
        VendorModemWakeup wakeup_modem;
        VendorModemNotResponding not_responding;
        VendorCommandPrepare command_prepare;
        /**
        * vendor unsolicite message handler
        */
        AtCommandHandler unsolicite_handler;
        /**
         * Data in hook for vendor.
         */
        VendorDataIn data_in;

};

/**
 * @brief Serial plugin initialize
 **/
typedef gboolean (*SerialPluginInit) ( SerialInterface *serial_if, ModemInterface *modem );

/**
 * @brief Serial plugin open serial port
 **/
typedef gint (*SerialOpen) ( SerialInterface *serial_if, const gchar *device );

/**
 * @brief Serial plugin close serial port
 **/
typedef void (*SerialClose) ( SerialInterface *serial_if, const gint fd );

/**
 * @brief Request list of serial ports to be used
 **/
typedef GList* (*SerialGetPorts) ( SerialInterface *serial_if);

struct _SerialInterface {

        gpointer priv;

        SerialOpen open_serial;
        SerialClose close_serial;
        SerialGetPorts get_ports;
};
/**
 * @brief At command context struct. It is represents
 * an AtCommand that is executed
 */
struct _AtCommandContext {

        /**
        * AT command buffer
        */
        AtCommand *command;

        /**
        * AT command param
        */
        gchar *command_param;

        /**
        * retry number for the command
        * If it is 0, stop retry
        */
        gint retry_counter;

        /**
        * This command's timeout's event id
        */
        gint timeout_event_id;

        /**
        * IPC data
        */
        gpointer ipc_data;

        /**
        * Error callback function
        */
        ErrorFunction error_function;

        /**
        * Error callback data
        */
        gpointer error_data;

        /**
         * AT command response handle function
         */
        AtCommandHandler handler;

        /**
        * Attched data for handle function
        */
        GHashTable *handler_data;

        /**
         * Marked true when write is completed
         */
        gboolean write_complete;

};

//send the intialize at command to modem
void gsmd_modem_general_at_init (ModemInterface* modem);

void gsmd_modem_general_at_init_sim_ready (ModemInterface* modem);

void gsmd_modem_register_command (ModemInterface *modem,
                                  const AtCommand *command);

AtCommand* gsmd_modem_get_command (ModemInterface *modem,
                                   guint cmd_id);

void gsmd_modem_change_sim_status(ModemInterface *modem, SIMStatus status);

Trigger* gsmd_modem_serial_register_trigger(ModemInterface *modem,
                                            const gchar *trigger_str,
                                            SerialDataIn handler,
                                            gpointer data,
                                            gboolean onetime,
                                            InterfaceType interface_index);

gboolean gsmd_modem_serial_queue_write ( ModemInterface *modem,
                                         WriteData *data,
                                         guint device_index);

gboolean gsmd_modem_serial_write ( ModemInterface *modem,
                                   WriteData *data,
                                   guint device_index);

gboolean gsmd_modem_serial_write_simple ( ModemInterface *modem,
                                          const gchar *command,
                                          InterfaceType interface_index);

gboolean gsmd_modem_post_at_command ( ModemInterface *modem,
                                      AtCommandContext *at,
                                      InterfaceType interface);

gboolean gsmd_modem_post_at_command_id ( ModemInterface* modem,
                                         AtCommandID command_id,
                                         const gchar *param,
                                         gpointer *ipc_data,
                                         ErrorFunction error_function,
                                         gpointer error_data,
                                         InterfaceType interface,
                                         GHashTable *handler_data);

gboolean gsmd_modem_post_at_command_id_and_cancel ( ModemInterface* modem,
                                                    AtCommandID command_id,
                                                    const gchar *param,
                                                    gpointer *ipc_data,
                                                    ErrorFunction error_function,
                                                    gpointer error_data,
                                                    InterfaceType interface,
                                                    GHashTable *handler_data);


gboolean gsmd_modem_cancel_command_id(ModemInterface *modem,
                                      AtCommandID cmd_id,
                                      InterfaceType interface);

void gsmd_modem_post_alive_test (ModemInterface *modem, gboolean reset);

void gsmd_modem_reset(ModemInterface *modem);

AtCommandContext* gsmd_at_command_context_new (AtCommand *atcmd,
                                               const gchar *param,
                                               gpointer ipc_data,
                                               GHashTable *handler_data,
                                               ErrorFunction error_function,
                                               gpointer error_data);



AtCommandContext* gsmd_at_command_context_new_from_id (ModemInterface *modem,
                                                       AtCommandID cmd_id,
                                                       const gchar *param,
                                                       gpointer ipc_data,
                                                       ErrorFunction error_function,
                                                       gpointer error_data,
                                                       GHashTable *handler_data);

void gsmd_modem_change_network_status(  ModemInterface *modem,
                                        NetworkStatus status);

void gsmd_modem_change_network_status_data(  ModemInterface *modem,
                                             NetworkStatus status,
                                             gpointer *ipc_data,
                                             ErrorFunction error_function,
                                             gpointer error_data);

#endif
