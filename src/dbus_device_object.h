/*
 *  dbus_device_object.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef DBUS_DEVICE_OBJECT_H
#define DBUS_DEVICE_OBJECT_H


#include <glib-object.h>
#include "dbus_objects.h"

#include "device_interface.h"

typedef struct DBusDeviceObject DBusDeviceObject;
typedef struct DBusDeviceObjectClass DBusDeviceObjectClass;

GType dbus_device_object_get_type (void);


struct DBusDeviceObject {
        GObject parent;
        DeviceInterface *device;
};

struct DBusDeviceObjectClass {
        GObjectClass parent;
        guint status;
        guint subscriber_numbers;
};

/* Macros that are needed for making the proper GObject */
#define DBUS_DEVICE_TYPE_OBJECT              (dbus_device_object_get_type ())
#define DBUS_DEVICE_OBJECT(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), DBUS_DEVICE_TYPE_OBJECT, DBusDeviceObject))
#define DBUS_DEVICE_OBJECT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), DBUS_DEVICE_TYPE_OBJECT, DBusDeviceObjectClass))
#define DBUS_IS_DEVICE_OBJECT(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), DBUS_DEVICE_TYPE_OBJECT))
#define DBUS_IS_DEVICE_OBJECT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), DBUS_DEVICE_TYPE_OBJECT))
#define DBUS_DEVICE_OBJECT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), DBUS_DEVICE_TYPE_OBJECT, DBusDeviceObjectClass))

DBusDeviceObject *gsmd_dbus_device_initialize (DeviceIPCInterface *device_ipc,
                                               DeviceInterface* device);
void gsmd_dbus_device_uninitialize(DBusDeviceObject *obj);

//Methods
gboolean smartphone_device_get_info(DBusDeviceObject *obj,
                                    DBusGMethodInvocation *method_context);
gboolean smartphone_device_set_antenna_power(DBusDeviceObject *obj,
                                             gboolean antenna_power,
                                             DBusGMethodInvocation *method_context);
gboolean smartphone_device_get_features(DBusDeviceObject *obj,
                                        DBusGMethodInvocation *method_context);
gboolean smartphone_device_get_antenna_power(DBusDeviceObject *obj,
                                             DBusGMethodInvocation *method_context);
gboolean smartphone_device_recover_from_suspend(DBusDeviceObject *obj,
                                                       DBusGMethodInvocation *method_context);
gboolean smartphone_device_prepare_to_suspend(DBusDeviceObject *obj,
                                              DBusGMethodInvocation *method_context);

//Signals



#endif
