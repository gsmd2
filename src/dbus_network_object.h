/*
 *  dbus_network_object.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef DBUS_NETWORK_OBJECT_H
#define DBUS_NETWORK_OBJECT_H


#include <glib-object.h>
#include "dbus_objects.h"


typedef struct DBusNetworkObject DBusNetworkObject;
typedef struct DBusNetworkObjectClass DBusNetworkObjectClass;

GType dbus_network_object_get_type (void);


struct DBusNetworkObject {
        GObject parent;
        NetworkInterface *network;
};

struct DBusNetworkObjectClass {
        GObjectClass parent;
        guint status;
        guint subscriber_numbers;
};

/* Macros that are needed for making the proper GObject */
#define DBUS_NETWORK_TYPE_OBJECT              (dbus_network_object_get_type ())
#define DBUS_NETWORK_OBJECT(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), DBUS_NETWORK_TYPE_OBJECT, DBusNetworkObject))
#define DBUS_NETWORK_OBJECT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), DBUS_NETWORK_TYPE_OBJECT, DBusNetworkObjectClass))
#define DBUS_IS_NETWORK_OBJECT(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), DBUS_NETWORK_TYPE_OBJECT))
#define DBUS_IS_NETWORK_OBJECT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), DBUS_NETWORK_TYPE_OBJECT))
#define DBUS_NETWORK_OBJECT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), DBUS_NETWORK_TYPE_OBJECT, DBusNetworkObjectClass))

DBusNetworkObject *gsmd_dbus_network_initialize (   NetworkIPCInterface *network_ipc,
                                                    NetworkInterface *network);
void gsmd_dbus_network_uninitialize(DBusNetworkObject *obj);

//Methods
gboolean smartphone_network_get_country_code(DBusNetworkObject *obj,
                                             DBusGMethodInvocation *method_context);

gboolean smartphone_network_get_home_country_code(DBusNetworkObject *obj,
                                                  DBusGMethodInvocation *method_context);

gboolean smartphone_network_register(   DBusNetworkObject *obj,
                                        DBusGMethodInvocation *method_context);

gboolean smartphone_network_get_status(DBusNetworkObject *obj,
                                       DBusGMethodInvocation *method_context);

gboolean smartphone_network_list_providers(DBusNetworkObject *obj,
                                           DBusGMethodInvocation *method_context);

gboolean smartphone_network_register_with_provider(DBusNetworkObject *obj,
                                                   const gint index,
                                                   DBusGMethodInvocation *method_context);

gboolean smartphone_network_get_subscriber_numbers(DBusNetworkObject *obj,
                                                   DBusGMethodInvocation *method_context);



//Signals
void smartphone_network_status (NetworkIPCInterface *network_ipc,
                                GHashTable *status);

void smartphone_network_subscriber_numbers (NetworkIPCInterface *network_ipc,
                                            const gchar** number);


gboolean smartphone_network_unregister(DBusNetworkObject *obj,
                                       DBusGMethodInvocation *method_context);
#endif
