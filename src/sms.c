/*
 *  sms.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include <glib.h>
#include <glib/gprintf.h>
#include <string.h>


#include "gsmd-error.h"
#include "serial.h"
#include "sms.h"
#include "utils.h"
#include <stdlib.h>



/* utility function*/





static const AtCommand
sms_commands[]
= {
        //Store
        { SMS_STORE,                     "AT+CMGW=%s\r\n",           180000,
                FALSE,  3,
                gsmd_sms_handler_send_pdu, gsmd_sms_command_send_prepare,
                SIM_READY, gsmd_sms_sms_data_free},

        //Send sms
        { SMS_SEND,                     "AT+CMGS=%s\r\n",           180000,
          FALSE,  3,
          gsmd_sms_handler_send_pdu, gsmd_sms_command_send_prepare,
          SIM_READY, gsmd_sms_sms_data_free},


        //Init Select Message Service
        { SMS_INIT,                     "AT+CSMS=0\r\n",           100,
          FALSE,  0,
          gsmd_utils_handler_ok, NULL, SIM_UNKNOWN, NULL},

        { 0,                    NULL,                100,    TRUE,  1,
          NULL, NULL, SIM_UNKNOWN, NULL },

}, *sms_command_p = sms_commands;

static const SymbolTable
sms_symbols[] = {
        { "CSCA",       SYMBOL_CSCA,    },
        { "CMGR",       SYMBOL_CMGR,    },
        { NULL, 0,                      },
}, *sms_symbol_p = sms_symbols;

void gsmd_sms_init_at_handler(ModemInterface* modem)
{
        while (sms_symbol_p->symbol_name) {
                g_scanner_add_symbol (modem->scanner, sms_symbol_p->symbol_name,
                                      GINT_TO_POINTER(sms_symbol_p->symbol_token));
                sms_symbol_p++;
        }

        while (sms_command_p->command) {
                gsmd_modem_register_command (modem,sms_command_p);
                sms_command_p++;
        }
}






/*************************End of Utility Function*******************************/

/*************************Commands**********************************************/











/**
 * @brief Directly send a sms message
 *
 *
 * @param sms sms interface pointer
 * @param ipc_data ipc data
 * @param message message's content
 * @param number recipient's phone number
 * @param want_report should report be sent
 */
void gsmd_sms_command_send_message (SMSInterface *sms,
                                    gpointer ipc_data,
                                    const gchar *message,
                                    const gchar *number,
                                    gboolean want_report)
{
        //TODO want_report is ignored
        ModemInterface *modem = (ModemInterface*)sms->priv;
        g_debug("%s : message: '%s', number: '%s'",__func__,message,number);
        gsmd_sms_send_pdu (modem,
                           ipc_data,
                           message,
                           number,
                           SMS_SEND,
                           INTERFACE_SMS,
                           (ErrorFunction)modem->sms_ipc->send_message_error,
                           modem->sms_ipc,
                           FALSE);

}



/****************Unimplemented commands and handlers***************************/
AtCommandHandlerStatus gsmd_sms_handler_get_service_bearer (ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response)
{
        gsmd_utils_send_error(at,
                              GSMD_ERROR_UNKNOWN,
                              "NOT IMPLEMENTED YET");

        return AT_HANDLER_DONE_ERROR;
}

AtCommandHandlerStatus gsmd_sms_handler_set_service_bearer (ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response)
{
        gsmd_utils_send_error(at,
                              GSMD_ERROR_UNKNOWN,
                              "NOT IMPLEMENTED YET");
        return AT_HANDLER_DONE_ERROR;
}















/**
* @brief Initialize sms structure and bind function pointers to real funtions.
* @param modem modem to initialize
*/
void gsmd_sms_init(ModemInterface *modem)
{
        gsmd_sms_init_at_handler(modem);
        modem->sms->priv = (gpointer)modem;
        modem->sms->send_message = &gsmd_sms_command_send_message;
}


