/*
 *  gsmd-error.c
 *
 *  Copyright(C) 2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Heikki Paajanen
 */

#ifndef GSMD_ERROR_H
#define GSMD_ERROR_H

#include <glib-object.h>

#define GSMD_ERROR gsmd_error_quark ()

GQuark gsmd_error_quark();

typedef enum {
        GSMD_ERROR_DEVICE_TIMEOUT = 1,
        GSMD_ERROR_UNKNOWN,
        GSMD_ERROR_INTERNAL,
        GSMD_ERROR_RESET,
        GSMD_ERROR_NOTSUPPORTED,
        GSMD_ERROR_NETWORK_NOT_PRESENT,
        GSMD_ERROR_SIM_UNAUTHORIZED,
        GSMD_ERROR_CALL_NOTFOUND,
        GSMD_ERROR_CALL_INVALIDSTATE
} GsmdErrorCode;
#endif /* GSMD_ERROR_H */

