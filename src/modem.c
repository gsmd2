/*
 *  modem.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include <glib.h>
#include <gmodule.h>
#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#include "serial.h"
#include "modem_internal.h"
#include "call.h"
#include "call_interface.h"
#include "sms.h"
#include "sms_impl.h"
#include "device.h"
#include "pdp.h"
#include "network.h"
#include "sim.h"
#include "sms_interface.h"
#include "pdp_interface.h"
#include "network_interface.h"
#include "device_interface.h"
#include "sim_interface.h"
#include "gsmd-error.h"
#include "utils.h"


#define TIME_OUT "TIME_OUT\n"

/** Default time out value for AT command */
#define DEFAULT_TIME_OUT 100

/** Time out value for AT command */
#define TIME_OUT_PADDING 5000


/* static Modem *modem_priv; */

/**
* @brief Serial device
* Modem can have multiple serial devices, each of them has their own Serial
* and command queue
*
*/
typedef struct _SerialDevice
{
    /**
     * AT command queue
     */
    GQueue *commandQueue;
    /**
     * IOChannel of serial device
     */
    Serial *serial;

    /**
     * Pointer to modem interface used
     */
    Modem *modem;

    /**
     * Index of the serial device to use
     */
    guint device_index;

} SerialDevice;

/**
 * @brief Modem Struct
 **/
struct _Modem
{
    /**
     * time out flag
     */
    gboolean is_timeout;

    /**
     * Serial devices
     */
    GArray *serial_devices;

    /**
     * AT command table
     */
    GHashTable *commands_table;

    /**
     * modem interface
     */
    ModemInterface *modem_if;

    /**
     * vendor lib handle
     */
    GModule* vendor_handle;

    /**
     * Vendor interface
     */
    VendorInterface *vendor;

    /**
     * Serial plugin handle
     */
    GModule* serial_plugin_handle;

    /**
     * SerialPlugin interface
     */
    SerialInterface *serial_if;

};


/*********************service function**************************/
static void gsmd_modem_init ( ModemInterface* modem);
static AtCommandHandlerStatus gsmd_modem_handler_unsolicite (ModemInterface *modem,
                                                             AtCommandContext *at,
                                                             GString *response);

static void gsmd_modem_process_next_command (Modem* modem, InterfaceType interface);
static gboolean gsmd_modem_timeout_handle (gpointer data);

static
SerialDevice *gsmd_modem_get_serial_device(Modem *modem, InterfaceType interface);

static
AtCommandHandlerStatus gsmd_modem_handler_alive(ModemInterface *modem,
                                                AtCommandContext *at,
                                                GString *response);

static
AtCommandHandlerStatus gsmd_modem_handler_current_calls(ModemInterface *modem,
                                                        AtCommandContext *at,
                                                        GString *response);

static void gsmd_modem_current_calls_free(AtCommandContext *at);
/********************End service function***********************/


static const AtCommand
commands[]
=
{
    //Disable command echos
    { DISABLE_ECHO,                 "ATE0\r\n",                 100,
        TRUE,  0,
        gsmd_utils_handler_ok, NULL, SIM_UNKNOWN, NULL},

    //Enable everything
    { POWER_ON,                     "AT+CFUN=1\r\n",             15000,
      TRUE,  0,
      gsmd_utils_handler_ok, NULL, SIM_UNKNOWN, NULL},

    //Enable Report Mobile Equipment Error
    { ENABLE_CMEE,                 "AT+CMEE=1\r\n",             100,
      TRUE,  0,
      gsmd_utils_handler_ok, NULL, SIM_UNKNOWN, NULL},

    //Set response of AT+COPS? to respond with operator name
    { SET_OP_QUERY_REPLY,                 "AT+COPS=3,0\r\n",    100,
      TRUE,  0,
      gsmd_utils_handler_ok, NULL, SIM_READY, NULL},

    //Enable Report Mobile Equipment Error
    { ENABLE_CMEE,                 "AT+CMEE=1\r\n",             100,
      TRUE,  3,
      gsmd_utils_handler_ok, NULL, SIM_UNKNOWN, NULL},

    //Enable call waiting
    { ENABLE_CCWA,                 "AT+CCWA=1,1\r\n",             100,
      TRUE,  0,
      gsmd_utils_handler_ok, NULL, SIM_READY, NULL},

    //Enable call waiting
    { CUR_CALLS_QUERY,                 "AT+CLCC\r\n",             100,
      TRUE,  0,
      gsmd_modem_handler_current_calls, NULL, SIM_READY,
      gsmd_modem_current_calls_free},

    //Query if we are muted
    { MUTE_QUERY,                   "AT+CMUTE=?\r\n",           5000,
      TRUE,  0,
      gsmd_utils_handler_ok, NULL, SIM_READY, NULL},

    //Enable caller identification
    { ENABLE_CLIP,                  "AT+CLIP=1\r\n",            180000,
      TRUE,  3,
      gsmd_utils_handler_ok, NULL, SIM_READY, NULL},

    //Enable new message identification
    { ENABLE_CNMI,                  "AT+CNMI=2,1,0,0,0\r\n",    5000,
      TRUE,  3,
      gsmd_utils_handler_ok, NULL, SIM_READY, NULL},

    //Query ring volume
    { RING_VOLUME_QUERY,            "AT+CRSL?\r\n",             5000,
      TRUE,   0,
      gsmd_utils_handler_ok, NULL, SIM_READY, NULL},

    //Escape command
    { ESCAPE_COMMAND,               "+++\r\n",                  100,
      FALSE,  0,
      gsmd_utils_handler_ok, NULL, SIM_UNKNOWN, NULL},

    //Just send at command
    { AT,               "AT\r\n",                    5000,
      FALSE,  0,
      gsmd_utils_handler_ok, NULL, SIM_UNKNOWN, NULL},

    //Test modem is alive
    { REPLY_COMMAND,               "AT\r\n",                    5000,
      FALSE,  0,
      gsmd_modem_handler_alive, NULL, SIM_UNKNOWN, NULL},

    //Set PDU mode
    { SET_PDU_MODE,               "AT+CMGF=0\r\n",                  100,
      FALSE,  0,
      gsmd_utils_handler_ok, NULL, SIM_READY, NULL},

    //Test modem is alive
    { SOFT_RESET,               "ATZ\r\n",                    5000,
      FALSE,  3,
      gsmd_modem_handler_alive, NULL, SIM_UNKNOWN, NULL},

    { 0,                    NULL,                100,    TRUE,  1,
      NULL, NULL, SIM_UNKNOWN, NULL },

}, *command_p = commands;


static const SymbolTable
symbols[] =
{
    { "TIME_OUT",   SYMBOL_TIME_OUT,},
    { "OK",         SYMBOL_OK,      },
    { "ERROR",      SYMBOL_ERROR,   },
    { "RING",       SYMBOL_RING,    },
    { "NO",         SYMBOL_NO,      },
    { "ANSWER",     SYMBOL_ANSWER,  },
    { "CARRIER",    SYMBOL_CARRIER, },
    { "BUSY",       SYMBOL_BUSY,    },
    { "READY",      SYMBOL_READY,   },
    { "CME",        SYMBOL_CME,     },
    { "CMS",        SYMBOL_CMS,     },
    { "CPIN",       SYMBOL_CPIN,    },
    { "CPAS",       SYMBOL_CPAS,    },
    { "CSQ",        SYMBOL_CSQ,     },
    { "CMUT",       SYMBOL_CMUT,    },
    { "CLIP",       SYMBOL_CLIP,    },
    { "CCWA",       SYMBOL_CCWA,    },
    { "CRING",      SYMBOL_CRING,   },
    { "CMTI",       SYMBOL_CMTI,    },
    { "CRSL",       SYMBOL_CRSL,    },
    { "CMGW",       SYMBOL_CMGW,    },
    { "PIN",        SYMBOL_PIN,     },
    { "PUK",        SYMBOL_PUK,     },
    { NULL, 0,                      },
}, *symbol_p = symbols;


/**
* @brief Creates modem interface and sets pointers
*
* @param modem modem whose interface to create
* @return
*/
void gsmd_modem_set_up_interface(Modem *modem)
{
    modem->modem_if = g_new0(ModemInterface, 1);
    modem->modem_if->priv = (gpointer)modem;
}

/**
* @brief Function used to free gvalues stored in modem's cache
*
* @param data pointer to gvalue to be freed
*/
static void gsmd_modem_cache_destroy(gpointer data)
{
    GValue *value = data;
    g_value_unset(value);
    g_free(value);
}

/**
* @brief Allocates a new modem struct
*
* @return newly created modem struct
*/
Modem* gsmd_modem_allocate()
{
    ModemInterface *modem = NULL;
    Modem *modem_priv = g_try_new0 (Modem, 1);

    if (!modem_priv)
    {
        g_warning("%s : Failed to allocate a modem structure.", __func__);
        return NULL;
    }

    gsmd_modem_set_up_interface(modem_priv);

    modem = modem_priv->modem_if;

    modem_priv->vendor_handle = NULL;
    modem_priv->is_timeout = FALSE;

    modem->status = MODEM_UNINITIALIZED;
    //modem->sim_status = SIM_UNKNOWN;
    modem->devices = NULL;



    modem->calls = NULL;

    modem->scanner = g_scanner_new (NULL);
    if (!modem->scanner)
    {
        g_warning("%s : Failed to set a scanner for the modem.", __func__);
        goto error;
    }

    modem->scanner->config->symbol_2_token = TRUE;
    modem->scanner->config->numbers_2_int = TRUE;
    modem->scanner->config->skip_comment_single = FALSE;
    modem->scanner->config->cpair_comment_single = "";
    /* the character to skip*/

    //Add commands list into hash table
    modem_priv->commands_table = g_hash_table_new(NULL,NULL);
    if (!modem_priv->commands_table)
    {
        g_warning("%s : Failed to set command table for the modem.", __func__);
        goto error;
    }
    gint i=0;
    for (i=0; i < INTERFACE_LAST; i++)
    {
        modem->caches[i] = g_hash_table_new_full(NULL,
                                                 g_str_equal,
                                                 NULL,
                                                 gsmd_modem_cache_destroy);
    }
    gsmd_utils_table_insert_int(modem->caches[INTERFACE_NETWORK],
                                GSMD_MODEM_KEY_NETWORK_STATUS,
                                NETWORK_UNKNOWN);

    modem->call = g_try_new0 (CallInterface, 1);
    if (!modem->call)
        goto error;
    modem->call_ipc = g_try_new0 (CallIPCInterface, 1);
    if (!modem->call_ipc)
        goto error;
    modem->sms = g_try_new0 (SMSInterface, 1);
    if (!modem->sms)
        goto error;
    modem->sms_ipc = g_try_new0 (SMSIPCInterface, 1);
    if (!modem->sms_ipc)
        goto error;
    modem->device= g_try_new0 (DeviceInterface, 1);
    if (!modem->device)
        goto error;
    modem->device_ipc= g_try_new0 (DeviceIPCInterface, 1);
    if (!modem->device_ipc)
        goto error;

    modem->network = g_try_new0 (NetworkInterface, 1);
    if (!modem->network)
        goto error;
    modem->network_ipc = g_try_new0 (NetworkIPCInterface, 1);
    if (!modem->network_ipc)
        goto error;
    modem->pdp = g_try_new0 (PDPInterface, 1);
    if (!modem->pdp)
        goto error;
    modem->pdp_ipc = g_try_new0 (PDPIPCInterface, 1);
    if (!modem->pdp_ipc)
        goto error;
    modem->sim = g_try_new0 (SIMInterface, 1);
    if (!modem->sim)
        goto error;
    modem->sim_ipc = g_try_new0 (SIMIPCInterface, 1);
    if (!modem->sim_ipc)
        goto error;


    return modem_priv;

error:
    g_warning("%s: error", __func__);
    if ( modem )
    {
        g_free (modem->device);
        g_free (modem->device_ipc);
        g_free (modem->sms);
        g_free (modem->sms_ipc);
        g_free (modem->call);
        g_free (modem->call_ipc);
        g_free (modem->network);
        g_free (modem->network_ipc);
        g_free (modem->pdp);
        g_free (modem->pdp_ipc);
        g_free (modem->sim);
        gint i=0;
        for (i=0; i < INTERFACE_LAST; i++)
        {
            g_hash_table_destroy (modem->caches[i]);
        }
        g_free (modem);
    }
    if ( modem_priv )
    {
        if (modem_priv->commands_table)
            g_hash_table_destroy (modem_priv->commands_table);
        if (modem_priv->modem_if->scanner)
            g_scanner_destroy (modem->scanner);
        g_free (modem_priv);
    }
    return NULL;
}


/**
 * @brief Loads and initializes vendor
 * @param modem modem to attach the handler
 * @param vendor the vendor's name. E.g. "telit"
 * @return TRUE if load was successful, otherwise FALSE
 */
gboolean gsmd_modem_load_vendor_handler (Modem* modem, const gchar* vendor)
{
    VendorInit vendor_init;
    gpointer vendor_init_symbol = NULL;;
    GString*  vendor_name = NULL;
    const gchar *plugindir = g_getenv("GSMD2VENDORPLUGINDIR");
    //perform vendor init
    //vendor_telit_unsolicite_handler_init (modem);
    if (!vendor)
        return FALSE;

    g_debug("%s : Vendor: %s", __func__, vendor);
    vendor_name = g_string_new ("");

    if ( !plugindir )
    {
        plugindir = GSMD2PLUGINDIR;
    }
    g_string_printf (vendor_name, "vendor%s", vendor);
    gchar *plugin_path = g_module_build_path(plugindir, vendor_name->str);
    modem->vendor_handle = g_module_open (plugin_path ,G_MODULE_BIND_MASK);
    g_string_free (vendor_name, TRUE);
    vendor_name = NULL;
    g_free( plugin_path );
    plugin_path = NULL;
    if (!modem->vendor_handle)
    {
        g_critical ("Vendor %s not found from %s",vendor, plugindir);
        return FALSE;
    }

    if (!g_module_symbol (modem->vendor_handle,"vendor_handle_init", &vendor_init_symbol) )
    {
        g_critical ("'vendor_handle_init' symbol %s not found from %s",vendor, plugindir);
        g_module_close(modem->vendor_handle);
        modem->vendor_handle = NULL;
        return FALSE;
    }
    vendor_init = (VendorInit)vendor_init_symbol;
    return vendor_init (modem->vendor, modem->modem_if);
}

/**
 * Finds the correct vendor handler
 * @param modem modem to attach the handler
 * @param vendor the vendor's name. E.g. "telit"
 * @return TRUE if load was successful, otherwise FALSE
 */
gboolean gsmd_modem_load_serial_plugin (Modem* modem, const gchar* plugin)
{
    g_debug("%s : Serial plugin: %s", __func__, plugin);
    SerialPluginInit serial_init = NULL;
    gpointer serial_init_symbol = NULL;
    GString*  plugin_name = NULL;
    //perform vendor init
    //plugin_telit_unsolicite_handler_init (modem);
    if (!plugin)
    {
        return FALSE;
    }
    plugin_name = g_string_new ("");

    const gchar *plugindir = g_getenv("GSMD2SERIALPLUGINDIR");
    if ( !plugindir )
    {
        plugindir = GSMD2PLUGINDIR;
    }
    g_string_printf (plugin_name, "serial%s", plugin);
    gchar *plugin_path = g_module_build_path(plugindir, plugin_name->str);
    modem->serial_plugin_handle = g_module_open (plugin_path ,G_MODULE_BIND_MASK);
    g_string_free (plugin_name, TRUE);
    plugin_name = NULL;
    g_free(plugin_path);
    plugin_path = NULL;
    if (!modem->serial_plugin_handle)
    {
        g_critical ("%s : Serial plugin %s not found from %s",__func__, plugin, plugindir);
        return FALSE;
    }
    if (!g_module_symbol(modem->serial_plugin_handle,"serial_plugin_init", &serial_init_symbol) )
    {
        g_critical ("%s : Serial plugin %s init function not found",__func__, plugin);
        g_module_close(modem->serial_plugin_handle);
        modem->serial_plugin_handle = NULL;
        return FALSE;
    }
    g_module_make_resident(modem->serial_plugin_handle);
    serial_init = (SerialPluginInit)serial_init_symbol;

    modem->serial_if = g_new0(SerialInterface, 1);

    return serial_init (modem->serial_if, modem->modem_if);
}

/**
* @brief Adds default scanner symbols and registers at commands to modem struct
*
* @param modem modem to initialize
*/
void gsmd_modem_init_standard_at_handler(ModemInterface* modem)
{
    g_debug("%s", __func__);

    while (symbol_p->symbol_name)
    {
        g_scanner_add_symbol (modem->scanner, symbol_p->symbol_name,
                              GINT_TO_POINTER(symbol_p->symbol_token));
        symbol_p++;
    }

    while (command_p->command)
    {
        gsmd_modem_register_command (modem,command_p);
        command_p++;
    }
}

/**
* @brief Function to call when command needs to be cancelled prematurely
*
* Removes timeout and calls error_function
*
* @param data pointer to atcommand context to be cancelled
* @param user_data not used
*/
void gsmd_modem_clear_queue_func(gpointer data,
                                 gpointer user_data)
{
    AtCommandContext *at = (AtCommandContext*)data;
    gsmd_utils_send_error(at, GSMD_ERROR_RESET, "Reseting modem");
    if (at->timeout_event_id != 0)
    {
        g_debug("%s : Removing timeout id: %d",
                __func__,
                at->timeout_event_id);

        g_source_remove(at->timeout_event_id);
    }
    gsmd_at_command_context_free(at);
}


/**
* @brief Closes all serial ports
*
* @param modem pointer to modem whose serial ports to close
*/
void gsmd_modem_close_serial_ports(Modem *modem)
{
    g_debug("%s",__func__);
    gint i=0;

    if (modem->serial_devices)
    {
        for (i=0;i<modem->serial_devices->len;i++)
        {
            SerialDevice *serial = g_array_index(modem->serial_devices,SerialDevice*,i);
            if (modem->serial_if->close_serial)
            {
                modem->serial_if->close_serial(modem->serial_if,serial->serial->fd);
            }
            gsmd_serial_free(serial->serial);
            g_queue_foreach(serial->commandQueue,gsmd_modem_clear_queue_func,NULL);
            g_queue_free(serial->commandQueue);
        }
        g_array_free(modem->serial_devices,TRUE);
        modem->serial_devices = NULL;
    }

    modem->modem_if->status = MODEM_UNINITIALIZED;
}

/**
* @brief Initializes modem's serial ports
*
* @param modem modem whose serial ports to initialize
* @return TRUE if they were intialized
*/
gboolean gsmd_modem_init_serial_ports(Modem *modem)
{
    g_debug("%s",__func__);

    //Make sure we aren't initializing the modem twice
    if (modem->modem_if->status != MODEM_UNINITIALIZED)
        return FALSE;

    modem->modem_if->status = MODEM_SERIAL_INIT;

    gint i=0;
    g_assert(modem);
    g_assert(modem->modem_if);
    g_assert(modem->modem_if->devices);

    modem->serial_devices = g_array_new(TRUE,TRUE,sizeof(SerialDevice*));


    GList *device_it = modem->modem_if->devices;
    while (device_it)
    {
        gchar *device = device_it->data;
        g_debug("Adding device %s",device);
        gint device_fd = modem->serial_if->open_serial( modem->serial_if, device);

        if ( !device_fd )
        {
            g_warning ("%s : init_serial failed\n",
                       __func__);
            gsmd_modem_close_serial_ports (modem);
            return FALSE;
        }

        Serial *serial = gsmd_serial_open_port (device_fd);
        if (!serial)
        {
            g_warning("%s : Failed to open device %s",
                      __func__,
                      device);
            //open failed, release the resource
            gsmd_modem_close_serial_ports (modem);
            return FALSE;
        }
        SerialDevice *serial_device = g_new0(SerialDevice, 1);
        serial_device->serial = serial;
        serial_device->commandQueue = g_queue_new();
        serial_device->modem = modem;
        serial_device->device_index = i;

        gsmd_serial_register_trigger( serial, "\r\n",
                                      &gsmd_modem_data_in,
                                      (gpointer)serial_device,
                                      FALSE);
        gsmd_serial_register_trigger( serial, "\r",
                                      &gsmd_modem_data_in,
                                      (gpointer)serial_device,
                                      FALSE);
        gsmd_serial_register_trigger( serial, "\n",
                                      &gsmd_modem_data_in,
                                      (gpointer)serial_device,
                                      FALSE);

        g_array_append_val(modem->serial_devices,serial_device);
        device_it = g_list_next(device_it);
        i++;
    }

    if ( modem->vendor->wakeup_modem )
    {
        if (modem->vendor->wakeup_modem(modem->vendor))
        {
            gsmd_modem_post_alive_test(modem->modem_if,TRUE);
        }
    }
    else
    {
        gsmd_modem_post_alive_test(modem->modem_if,TRUE);
    }

    return TRUE;
}

/**
 * @brief Opens a modem device.
 *
 * Before using modem, this function must be called to open modem and
 * get modem device struct pointer
 *
 * @param devices list of device file names
 * @param vendor The libname name of the vendor
 * for example "telit"
 * @param serial_plugin The libname name of the serial plugin
 * for example "fsomuxer"
 * @param no_cache should caching be used(FALSE) or not (TRUE)
 * @param conf configuration file
 * @param ipc_init pointer to ipc initialization function
 * @param interface_devices a list that maps interfaces to devices
 * @return pointer to newly created modem interface struct
 */
ModemInterface* gsmd_modem_open (gchar **devices,
                                 const gchar* vendor,
                                 const gchar* serial_plugin,
                                 gboolean no_cache,
                                 GKeyFile *conf,
                                 IPCInit ipc_init,
                                 int *interface_devices)
{
    // g_type_init ();
    Modem* modem = gsmd_modem_allocate();
    modem->modem_if->no_cache = no_cache;
    modem->modem_if->conf_file = conf;
    gchar *device = NULL;


    gint i = 0;


    if (!modem)
        return NULL;

    gsmd_modem_init (modem->modem_if);

    modem->modem_if->ipc_data = ipc_init(modem->modem_if);

    if (!modem->modem_if->ipc_data)
    {
        g_warning("Cannot initialize ipc!\n");
        return NULL;
    }


    if (!gsmd_modem_load_vendor_handler (modem, vendor))
    {
        g_warning ("Can't find the vendor or vendor init failed: %s\n", vendor);
        return NULL;
    }

    if (!gsmd_modem_load_serial_plugin (modem, serial_plugin))
    {
        g_warning ("Can't find the serial plugin or plugin init failed: %s\n", serial_plugin);
        return NULL;
    }

    gsmd_modem_init_standard_at_handler(modem->modem_if);

    if ( modem->serial_if->get_ports )
    {
        modem->modem_if->devices = modem->serial_if->get_ports(modem->serial_if);
        if ( modem->modem_if->devices )
        {
            g_debug("%s : Using ports given by serial plugin", __func__);
        }
    }
    if ( !modem->modem_if->devices )
    {
        g_debug("%s : Using ports given from command line or configuration file", __func__);
        i=0;
        device = devices[i];
        while (device)
        {
            g_debug("%s : '%s'", __func__, device);
            modem->modem_if->devices = g_list_append(modem->modem_if->devices,device);
            i++;
            device = devices[i];
        }
    }

    if (!gsmd_modem_init_serial_ports(modem))
        return NULL;


    if ( !gsmd_modem_assign_device_indices(modem->modem_if,interface_devices) )
    {
        gsmd_modem_free(modem->modem_if);
        return NULL;
    }

    return modem->modem_if;
}

/**
* @brief Changes sim's status
*
* Calls gsmd_modem_general_at_init_sim_ready when appropriate
*
* @param modem modem whose sim status to change
* @param status new status to set
*/
void gsmd_modem_change_sim_status(ModemInterface *modem, SIMStatus status)
{
    g_assert( modem );
    Modem *modem_priv = (Modem*)modem->priv;
    if ( modem->sim_status < SIM_READY
            && status >= SIM_READY)
    {
        if ( modem_priv->vendor->init_at_sim_ready )
        {
            modem_priv->vendor->init_at_sim_ready (modem_priv->vendor);
        }
        else
        {
            gsmd_modem_general_at_init_sim_ready(modem);
        }
    }
    modem->sim_status = status;
}

/**
* @brief Frees all serial devices within the modem
*
* @param modem modem whose serial devices to free
*/
void gsmd_modem_serial_devices_free(Modem *modem)
{
    gint i=0;
    if (modem->serial_devices)
    {
        for (i=0;i<modem->serial_devices->len;i++)
        {
            SerialDevice *device = g_array_index(modem->serial_devices,SerialDevice*,i);
            /*                         //ask vendor to close it */
            /*                         if (modem->vendor->close_serial) */
            /*                                 modem->vendor->close_serial(modem->vendor, */
            /*                                                             serial->fd); */
            /*                         else //or close it ourselves */
            /*                                 close(serial->fd); */
            gsmd_serial_free(device->serial);
            g_queue_foreach(device->commandQueue,gsmd_modem_clear_queue_func,NULL);
            g_queue_free(device->commandQueue);

        }

        g_array_free(modem->serial_devices,FALSE);
        modem->serial_devices = NULL;
    }
}

/**
 * @brief Frees modem struct
 *
 * @param modem The pointer to the modem device struct which to be close
 */
void gsmd_modem_free (ModemInterface* modem)
{
    Modem *modem_priv = (Modem*)modem->priv;
    g_assert (modem_priv);
    g_debug("%s", __func__);
    g_assert (modem);
    if (modem_priv->vendor_handle)
        dlclose (modem_priv->vendor_handle);
    g_free (modem->call);
    g_free (modem->call_ipc);

    g_free (modem->sms);
    g_free (modem->sms_ipc);

    gsmd_network_deinitialize(modem);
    g_free (modem->network);
    g_free (modem->network_ipc);

    g_free (modem->device);
    g_free (modem->device_ipc);

    g_free (modem->pdp);
    g_free (modem->pdp_ipc);


    gsmd_sim_deinitialize(modem);
    g_free (modem->sim);
    g_free (modem->sim_ipc);

    gint i=0;
    for (i=0; i < INTERFACE_LAST; i++)
    {
        g_hash_table_destroy (modem->caches[i]);
    }

    g_scanner_destroy (modem->scanner);
    g_hash_table_destroy (modem_priv->commands_table);

    gsmd_modem_serial_devices_free(modem_priv);


    GList *list = modem->calls;
    while ( list )
    {
        Call *call = (Call*)list->data;
        g_free(call);
        list = g_list_next( list );
    }
    g_list_free(modem->calls);
    modem->calls = NULL;

    g_key_file_free(modem->conf_file);

    g_free (modem);
}


/************************service function**************************************/

/**
* @brief Get serial device with specified index
*
* @param modem pointer to modem whose serial device to get
* @param device_index index of the device to get
* @return serial device or NULL if none found
*/
SerialDevice* gsmd_modem_get_serial_device_index(Modem *modem,
                                                 guint device_index)
{
    g_assert(modem);
    g_assert(modem->serial_devices);
    if ( modem->serial_devices->len > device_index )
    {
        return g_array_index(modem->serial_devices, SerialDevice*, device_index);
    }
    else
    {
        g_warning("%s : Invalid index: %d. We have %d device(s)",
                  __func__, device_index, modem->serial_devices->len);
        g_assert(FALSE);
    }
    return NULL;
}

/**
* @brief Gets specific interface's serial device
*
* @param modem modem whose serial devices to get
* @param interface interface whose serial to get
* @return serialdevice mapped to specified interface
*/
SerialDevice* gsmd_modem_get_serial_device(Modem *modem,InterfaceType interface)
{
    g_assert(modem);
    g_return_val_if_fail(modem->serial_devices, NULL);
    guint device_index = modem->modem_if->interface_devices[interface];
    return gsmd_modem_get_serial_device_index(modem, device_index);
}

/**
 * @brief Sends initializing at commands to the modem
 *
 * @param modem modem to initialize
 */
void gsmd_modem_general_at_init (ModemInterface* modem)
{
    g_assert(modem);
    gsmd_modem_post_at_command_id (modem,
                                   DISABLE_ECHO,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);

    gsmd_modem_post_at_command_id (modem,
                                   ENABLE_CMEE,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);

    gsmd_modem_post_at_command_id( modem,
                                   IMEI_QUERY,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_DEVICE,
                                   NULL);

    gsmd_modem_post_at_command_id( modem,
                                   MANUFACTURER_QUERY,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_DEVICE,
                                   NULL);

    gsmd_modem_post_at_command_id( modem,
                                   SW_REVISION_QUERY,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_DEVICE,
                                   NULL);

    gsmd_modem_post_at_command_id( modem,
                                   MODEL_QUERY,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_DEVICE,
                                   NULL);

    gsmd_modem_post_at_command_id (modem,
                                   PIN_QUERY,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);
}
/**
 * @brief Sends initializing at commands that need sim_status >=
 * SIM_READY to the modem
 *
 * @param modem modem to initialize
 */
void gsmd_modem_general_at_init_sim_ready (ModemInterface* modem)
{
    g_assert(modem);
    gsmd_modem_post_at_command_id (modem,
                                   ENABLE_CLIP,
                                   NULL,NULL,NULL,NULL,
                                   INTERFACE_GENERAL,
                                   NULL);

    gsmd_modem_post_at_command_id (modem,
                                   ENABLE_CNMI,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);

    gsmd_modem_post_at_command_id (modem,
                                   SET_PDU_MODE,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);
    gsmd_modem_post_at_command_id (modem,
                                   SET_OP_QUERY_REPLY,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);

    gsmd_modem_post_at_command_id (modem,
                                   ENABLE_CCWA,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);

    gsmd_modem_post_at_command_id (modem,
                                   CUR_CALLS_QUERY,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);
}


/**
 * @brief Get AT command processed currently
 *
 * @param modem modem device struct pointer
 * @param device_index index of the device whose command to get
 * @return atcommand that is currently processed
 */
AtCommandContext* gsmd_modem_get_current_at_command_index (ModemInterface *modem,
                                                           guint device_index)
{
    g_assert( modem );
    Modem *modem_priv = (Modem*)modem->priv;
    g_assert (modem_priv);
    g_assert (modem_priv->serial_devices);
    SerialDevice *device = gsmd_modem_get_serial_device_index( modem_priv, device_index);
    if (device)
        return g_queue_peek_head(device->commandQueue);
    return NULL;
}

/**
 * @brief Get the AT command that is processed at the moment
 *
 * @param modem modem device struct pointer
 * @param interface which interface's command to get
 * @return at command that is currently processed
 */
AtCommandContext* gsmd_modem_get_current_at_command (ModemInterface *modem,
                                                     InterfaceType interface)
{
    return gsmd_modem_get_current_at_command_index(modem,
                                                   gsmd_modem_get_interface_device_index(modem,
                                                                                         interface));
}

/**
 * @brief Send AT command to front of the queue
 *
 * Diffrent from modem_add_command, forces command to be the first in
 * command queue.
 *
 * @param modem modem device struct pointer
 * @param at AT command to send
 * @param interface which interface's queue is this command forced to
 *
 */
void gsmd_modem_send_command_force_next (Modem *modem,
                                         AtCommandContext *at,
                                         InterfaceType interface)
{

    g_assert( modem );
    g_assert( at );
    SerialDevice *device = gsmd_modem_get_serial_device( modem, interface);

    if (device)
    {
        AtCommandContext *current_at = g_queue_pop_head ( device->commandQueue );
        g_queue_push_tail ( device->commandQueue, at );
        if ( current_at )
        {
            g_queue_push_head (device->commandQueue, current_at );
        }
    }
}


/**
 * @brief Adds an modem alive test command (AT) or soft reset (ATZ) to the front
 * of the queue ignoring uncancellable commands and processes it
 *
 * @param modem_if pointer to modem device struct
 * @param reset if TRUE, send ATZ instead of AT
 */
void gsmd_modem_post_alive_test (ModemInterface *modem_if, gboolean reset)
{
    g_debug("%s",__func__);
    g_assert(modem_if);
    Modem *modem = (Modem*)modem_if->priv;
    g_assert(modem);
    g_assert(modem->serial_devices);
    SerialDevice *device = gsmd_modem_get_serial_device( modem, INTERFACE_GENERAL);
    g_assert(device);

    AtCommandContext *at = NULL;
    if (reset)
    {
        g_assert(modem->modem_if->status < MODEM_READY);
        at = gsmd_at_command_context_new_from_id(modem->modem_if,
                                                 SOFT_RESET,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL);
    }
    else
    {
        at = gsmd_at_command_context_new_from_id(modem->modem_if,
                                                 REPLY_COMMAND,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL);
    }
    g_queue_push_head (device->commandQueue, at);
    gsmd_modem_process_next_command(modem,INTERFACE_GENERAL);
}




/**
 * @brief Add an AT command to command queue
 *
 * @param modem pointer to modem device struct
 * @param at custom at command to add
 * @param interface interface to post the command to
 * @param cancel should we try to cancel the current command and
 * place this command first (or second if current command can't
 * be cancelled) in queue.
 * @return TRUE if command was posted properly
 */
static
gboolean gsmd_modem_post_at_command_internal ( ModemInterface *modem,
                                               AtCommandContext *at,
                                               InterfaceType interface,
                                               gboolean cancel)
{
    g_assert(modem);
    g_assert (at);
    gsmd_utils_print_data(__func__,"command: ",at->command->command);
    Modem *modem_priv = (Modem*)modem->priv;
    if ( !modem_priv->serial_devices )
    {
        //If serial ports aren't initialized then initialize them
        gsmd_modem_init_serial_ports(modem_priv);

        // modem is not ready so fail this request

    }
    if (modem->status < MODEM_INITIALIZING)
    {
        gsmd_utils_print_data(__func__,
                              "Modem is uninitialized, unable to add command: ",
                              at->command->command);
        /*
            g_debug("%s : Unable to add command %s because modem is uninitialized",
                    __func__,at->command->command);
                    */
        gsmd_utils_send_error(at, GSMD_ERROR_RESET, "Modem not ready yet");
        gsmd_at_command_context_free(at);
        return TRUE;
    }

    SerialDevice *device = gsmd_modem_get_serial_device( modem_priv,
                                                         interface);

    if (!device)
    {
        g_warning("%s : improper device index",__func__);
        return FALSE;
    }
    AtCommandContext *current_at = gsmd_modem_get_current_at_command ( modem,
                                                                       interface);

    //If no commands in queue, insert this one and process the queue
    if (!current_at)
    {
        if ( cancel )
        {
            g_debug("%s : No commands in queue, no need to cancel",
                    __func__);
        }
        g_queue_push_tail (device->commandQueue, at);
        gsmd_modem_process_next_command(modem_priv,interface);
        return TRUE;
    }

    if ( cancel )
    {

        //if there are commands in the queue and we can cancel the first
        // one, break it (+++) and push ours first
        if (current_at->command->cancellable)
        {
            g_debug("%s : Cancelling current command",__func__);
            //Remove timeout for the previous command
            if (current_at->timeout_event_id != 0)
            {
                g_debug("%s : Removing timeout id: %d",
                        __func__,
                        current_at->timeout_event_id);

                g_source_remove(current_at->timeout_event_id);
            }

            g_queue_push_head (device->commandQueue, at);
            gsmd_modem_serial_write_simple (modem,
                                            "+++\r\n",
                                            interface);

            //Process the new command
            gsmd_modem_process_next_command(modem_priv,interface);
        }
        else   //if the current command isn't cancellable,
        {
            //push our new command second in line
            g_debug("%s : Current command can't be overridden, moving next in queue",
                    __func__);
            g_queue_pop_head(device->commandQueue);
            g_queue_push_head(device->commandQueue, at);
            g_queue_push_head(device->commandQueue,current_at);
        }


    }
    else
    {
        g_queue_push_tail (device->commandQueue, at);
    }
    return TRUE;
}

/**
 * @brief Add a custom AT command to command queue. Cancels the first
 * command if possible and tries to run this command as soon as possible.
 *
 * @param modem pointer to modem device struct
 * @param at custom at command to add
 * @param interface interface to post the command to
 * @return result of gsmd_modem_post_at_command_internal
 */
gboolean gsmd_modem_post_at_command_and_cancel (ModemInterface *modem,
                                                AtCommandContext *at,
                                                InterfaceType interface)
{
    return gsmd_modem_post_at_command_internal(modem, at, interface, TRUE);
}

/**
 * @brief Add an AT command to command queue
 *
 * @param modem pointer to modem device struct
 * @param at custom at command to add
 * @param interface interface to post the command to
 * @return result of gsmd_modem_post_at_command_internal
 */
gboolean gsmd_modem_post_at_command ( ModemInterface *modem,
                                      AtCommandContext *at,
                                      InterfaceType interface)
{
    return gsmd_modem_post_at_command_internal(modem, at, interface, FALSE);
}


/**
 * @brief Posts at command to the queue
 *
 *
 * @param modem pointer to modem interface
 * @param command_id id of the command to post
 * @param param parameter for the command
 * @param ipc_data ipc data
 * @param error_function error function to call incase an error is occurred
 * @param error_data data to pass with error_function call
 * @param interface interface to post the command to
 * @param handler_data if command creater wants to add data to at command
 * for the handler, it can create a new GHashTable and insert it there. If
 * no handler_data is specified then a new handler_data will be created
 * automatically.
 */
gboolean gsmd_modem_post_at_command_id ( ModemInterface* modem,
                                         AtCommandID command_id,
                                         const gchar *param,
                                         gpointer *ipc_data,
                                         ErrorFunction error_function,
                                         gpointer error_data,
                                         InterfaceType interface,
                                         GHashTable *handler_data)
{
    AtCommandContext *at = gsmd_at_command_context_new_from_id( modem,
                                                                command_id,
                                                                param,
                                                                ipc_data,
                                                                error_function,
                                                                error_data,
                                                                handler_data);
    return gsmd_modem_post_at_command (modem, at,interface);
}


/**
 * @brief Posts at command to the front of the queue if possible
 *
 * This command will cancel current command if possible and place itself
 * to the front of the command queue (or second if first can't be cancelled)
 *
 * @param modem pointer to modem interface
 * @param command_id id of the command to post
 * @param param parameter for the command
 * @param ipc_data ipc data
 * @param error_function error function to call incase an error is occurred
 * @param error_data data to pass with error_function call
 * @param interface interface to post the command to
 * @param handler_data if command creater wants to add data to at command
 * for the handler, it can create a new GHashTable and insert it there. If
 * no handler_data is specified then a new handler_data will be created
 * automatically.
 */
gboolean gsmd_modem_post_at_command_id_and_cancel ( ModemInterface* modem,
                                                    AtCommandID command_id,
                                                    const gchar *param,
                                                    gpointer *ipc_data,
                                                    ErrorFunction error_function,
                                                    gpointer error_data,
                                                    InterfaceType interface,
                                                    GHashTable *handler_data)
{
    AtCommandContext *at = gsmd_at_command_context_new_from_id( modem,
                                                                command_id,
                                                                param,
                                                                ipc_data,
                                                                error_function,
                                                                error_data,
                                                                handler_data);
    return gsmd_modem_post_at_command_and_cancel (modem, at,interface);
}





/**
 * @brief Create new AT command context struct
 *
 * @param atcmd at command to use
 * @param param parameter for the command
 * @param ipc_data ipc data
 * @param error_function error function to call incase an error is occurred
 * @param error_data data to pass with error_function call
 * @param handler_data if command creater wants to add data to at command
 * for the handler, it can create a new GHashTable and insert it there. If
 * no handler_data is specified then a new handler_data will be created
 * automatically.
 * @return newly created at command context
 */
AtCommandContext* gsmd_at_command_context_new (AtCommand *atcmd,
                                               const gchar *param,
                                               gpointer ipc_data,
                                               GHashTable *handler_data,
                                               ErrorFunction error_function,
                                               gpointer error_data)
{
    AtCommandContext *at = NULL;
    at = g_slice_new0 (AtCommandContext);
    if (!at)
    {
        g_warning ("Failed to create new AtCommandContext.\n");
        return NULL;
    }
    at->command = atcmd;


    at->command_param = g_strdup(param);
    at->retry_counter = atcmd->retry;
    at->timeout_event_id = 0;
    at->ipc_data = ipc_data;
    at->error_function = error_function;
    at->error_data = error_data;
    if (!handler_data)
        at->handler_data = gsmd_utils_create_hash_table();
    else
        at->handler_data = handler_data;



    if ( at->error_function && !at->error_data)
    {
        g_warning("%s : AT command '%s' has error function but no error data!",
                  __func__, atcmd->command);
    }

    return at;
}


/**
* @brief Creates a new at command context from at command id
*
* @param modem Modem whose commands to use
* @param cmd_id command's id
* @param param parameter for the command
* @param ipc_data ipc data
* @param error_function error function to call incase an error is occurred
* @param error_data data to pass with error_function call
* @param handler_data if command creater wants to add data to at command
* for the handler, it can create a new GHashTable and insert it there. If
* no handler_data is specified then a new handler_data will be created
* automatically.
* @return newly created at command context
*/
AtCommandContext* gsmd_at_command_context_new_from_id (ModemInterface *modem,
                                                       AtCommandID cmd_id,
                                                       const gchar *param,
                                                       gpointer ipc_data,
                                                       ErrorFunction error_function,
                                                       gpointer error_data,
                                                       GHashTable *handler_data)
{
    Modem *modem_priv = (Modem*)modem->priv;
    AtCommandContext *at = NULL;
    AtCommand *command = (AtCommand *) g_hash_table_lookup(modem_priv->commands_table,
                                                           GINT_TO_POINTER(cmd_id));

    g_assert(command);
    at = gsmd_at_command_context_new(command,
                                     param,
                                     ipc_data,
                                     handler_data,
                                     error_function,
                                     error_data);
    return at;
}

/**
 * @brief Free the AT command context struct
 *
 * @param at The AT command context pointer
 */
void gsmd_at_command_context_free (AtCommandContext* at)
{
    if (at== NULL) return;
    if (at->command_param)
        g_free(at->command_param);

    if (at->command->free_function)
    {
        at->command->free_function(at);
    }
    if ( at->handler_data )
    {
        g_hash_table_destroy(at->handler_data);
    }
    g_slice_free (AtCommandContext,at);
}

/**
 * @brief Function called after a command has been finished.
 *
 * AtCommand will be removed from queue and freed.
 * If gsm modem failed to respond to command, AT command can be
 * sent to gsm modem to test weither it is still alive. This is
 * done when test_alive is TRUE.
 *
 * @param modem pointer to the modem
 * @param interface device whose command to finish
 * @param test_alive should test alive command be sent
 */
static void gsmd_modem_finish_command (Modem* modem,
                                       InterfaceType interface,
                                       gboolean test_alive)
{
    /*         g_debug("%s\n",__func__); */
    SerialDevice *device = gsmd_modem_get_serial_device( modem, interface);
    if (!device)
    {
        return;
    }

    GQueue *queue = device->commandQueue;

    AtCommandContext *at = g_queue_pop_head (queue);

    if (at)
    {
        /*                 gsmd_utils_print_data(__func__, */
        /*                                       "Removing command ", */
        /*                                       at->command->command); */

        //g_debug("%s : %s",__func__,at->command->command);
        if (at->timeout_event_id != 0)
        {
            /*                         g_debug("%s : Removing timeout id: %d", __func__, at->timeout_event_id); */
            g_source_remove(at->timeout_event_id);
        }
        gsmd_at_command_context_free (at);

    }
    else
        g_debug("Got no next command to finish\n");

    // if command handler (e.g. some vendor command)
    // marked the modem not alive
    if ( modem->modem_if->status == MODEM_RESET)
    {
        gsmd_modem_close_serial_ports(modem);
        gsmd_modem_init_serial_ports(modem);
    }
    else if (test_alive)
    {
        g_debug("%s : testing that modem is alive",__func__);
        gsmd_modem_post_alive_test(modem->modem_if, FALSE);
    }
    else
    {
        /*                 g_debug("%s : processing next command",__func__); */
        gsmd_modem_process_next_command(modem,interface);
    }
}

/**
* @brief Function to be called when serial has finished writing current command.
*
* This indicates that the command has been written and to stop handler from
* reading previous command's data.
*
* For example on some modem's AT+CPIN? responds with CPIN: READY\\r\\nOK\\r\\n,
* for others it returns with just CPIN: READY\\r\\n
* If AT+CPIN command's handler were to assume that it never replies with OK,
* and there were ATD command in the queue after AT+CPIN?. This would
* cause ATD's handler to handle AT+CPIN's OK as an indicator that a call
* has been initiated. gsmd_modem_data_in checks at->write_complete before
* allowing at's handler to handle the data to prevent these errors.
*
* @param write write data associated witht the command
* @param data pointer to at command context
*/
static void gsmd_modem_write_complete(WriteData *write, gpointer data)
{
    g_debug("%s", __func__);
    AtCommandContext *at = (AtCommandContext*)data;
    at->write_complete = TRUE;
}

/**
 * @brief Takes the next command from command queue and processes it
 *
 * @param modem whose next command to process
 * @param interface which interface's next command to process
 */
static void gsmd_modem_process_next_command (Modem* modem, InterfaceType interface)
{
    /*         g_debug("%s: device: %d",__func__,interface); */
    AtCommandContext *at = gsmd_modem_get_current_at_command ( modem->modem_if,
                                                               interface );

    if (modem->vendor->command_prepare)
    {
        at = modem->vendor->command_prepare(modem->vendor,modem->modem_if,at);
        if ( at )
        {
            gsmd_utils_print_data(__func__,
                                  "Running command_prepare AT command: ",
                                  at->command->command);

            /*
                                    g_debug("%s : Running command_prepare AT command: '%s'",
                                            __func__,
                                            at->command->command);
                                            */
            SerialDevice *device = gsmd_modem_get_serial_device(modem, interface);
            g_assert(device);
            GQueue *queue = device->commandQueue;
            g_queue_push_head (queue, at);

        }
    }

    //Get current command again to allow vendor to insert their own command
    at = gsmd_modem_get_current_at_command ( modem->modem_if,
                                             interface );

    if (!at)
    {
        g_debug("%s : No commands in queue", __func__);
        return;
    }
    //Most commands require PIN code to be set
    if (at->command->required_state > modem->modem_if->sim_status)
    {
        g_debug("%s : sim status: %d, command '%s' requires: %d",
                __func__, modem->modem_if->sim_status,
                at->command->command, at->command->required_state);
        if (at->error_function)
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_SIM_UNAUTHORIZED,
                                  "SIM AUTHENTICATION ERROR");
        }

        gsmd_modem_finish_command(modem, interface, FALSE);
        return;
    }

    AtCommandPrepare prepare = at->command->prepare;
    gboolean prepare_ok = TRUE;

    gsmd_utils_print_data(__func__,
                          "Processing command ",
                          at->command->command);


    modem->is_timeout = TRUE;//set the timeout flag

    GString *command = g_string_new (at->command->command);


    if (at->command_param)
    {
        g_string_printf(command,at->command->command,
                        at->command_param);
    }



    if ( prepare )
    {
        prepare_ok = (*prepare)(modem->modem_if);
    }
    // TODO handle failed prepare
    if ( !prepare_ok )
    {
        g_warning("%s: Prepare failed!", __func__);
        gsmd_utils_send_error(at,
                              GSMD_ERROR_INTERNAL,
                              "COMMAND PREPARE FAILED");
        gsmd_modem_finish_command(modem, interface, FALSE);
        return;
    }

    WriteData *data = g_try_new0(WriteData,1);

    data->command = g_strdup(command->str);
    g_string_free(command,TRUE);
    command = NULL;



    data->timeout = at->command->timeout + TIME_OUT_PADDING;



    data->timeoutid = &at->timeout_event_id;
    data->timeout_data = gsmd_modem_get_serial_device(modem,
                                                      interface);
    data->timeout_function = &gsmd_modem_timeout_handle;
    data->write_complete_data = at;
    data->write_complete_function = &gsmd_modem_write_complete;
    if (!gsmd_modem_serial_queue_write (modem->modem_if, data,interface))
    {
        g_warning("%s: Queue write failed!", __func__);
    }

}

/**
 * @brief Initializes scanner for modem's input
 *
 * @param modem whose scanner to initialize
 * @param str string to initialize scanner with
 */
static
void gsmd_modem_scanner_init (ModemInterface *modem, GString *str)
{
    GScanner* scanner = modem->scanner;
    g_scanner_input_text (scanner, str->str, str->len);
    g_scanner_get_next_token (scanner);
    g_scanner_peek_next_token (scanner);
}

static
gboolean gsmd_modem_call_activity(gpointer data)
{
    ModemInterface *modem = (ModemInterface*)data;
    g_debug("%s", __func__);
    gsmd_modem_post_at_command_id (modem,
                                   CUR_CALLS_QUERY,
                                   NULL, NULL, NULL, NULL,
                                   INTERFACE_GENERAL,
                                   NULL);
    /*         AtCommandContext *at = gsmd_at_command_context_new_from_id (modem, */
    /*                                                                     CUR_CALLS_QUERY, */
    /*                                                                     NULL, */
    /*                                                                     NULL, */
    /*                                                                     NULL, */
    /*                                                                     NULL, */
    /*                                                                     NULL); */
    return FALSE;
}

static
void gsmd_modem_monitor_call_activity(ModemInterface *modem)
{
    if ( modem->no_call_monitoring_needed )
    {
        return;
    }
    if (modem->call_monitor_timer != 0)
    {
        g_source_remove(modem->call_monitor_timer);
    }
    modem->call_monitor_timer = g_timeout_add(1000,&gsmd_modem_call_activity,modem);
}

/**
 * @brief Function to handle data read from serial port
 *
 * Checks SerialData on which device received the data, then
 * checks which was the last command sent to that interface
 * and runs that commands handler. If the handler doesn't understand
 * data, then unsolicite handler is called. This function also retries
 * the command if necessary.
 *
 * @param buffer data that has been read
 * @param data pointer to SerialData containing information on
 * what was read (buffer)
 */
void gsmd_modem_data_in (GString *buffer, gpointer data )
{
    SerialDevice *serial = (SerialDevice*)data;
    Modem *modem = serial->modem;
    ModemInterface *modem_if = modem->modem_if;
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    AtCommandHandler handler = NULL;
    AtCommandContext *at = NULL;
    g_assert(modem);

    at = gsmd_modem_get_current_at_command_index ( modem_if, serial->device_index);
    if ( at && !at->write_complete)
    {
        g_debug("%s : Got input, but current command has not been written.", __func__);
        at = NULL;
    }
    if (at && at->command)
    {
        if ( at->handler )
        {
            handler = at->handler;
        }
        else if ( at->command->handler )
        {
            handler = at->command->handler;
        }
        else
        {
            handler = &gsmd_utils_handler_ok;
        }
    }
    GString *debug_str = g_string_new("");
    g_string_printf(debug_str,"Received from port %d: ",serial->device_index);

    gsmd_utils_print_data(__func__,
                          debug_str->str,
                          buffer->str);
    g_string_free(debug_str,TRUE);

    if ( modem->vendor->data_in )
    {
        modem->vendor->data_in(modem_if, at, buffer);
    }
    gsmd_modem_scanner_init (modem_if, buffer);

    if ( handler )
    {

        status = (*handler)(modem_if, at, buffer);
        switch ( status )
        {
        case AT_HANDLER_DONE:
        case AT_HANDLER_DONE_ERROR:
            //if handled then process next command if there is
            modem->is_timeout = FALSE;
            gsmd_modem_finish_command(modem,
                                      serial->device_index,
                                      FALSE);
            break;
        case AT_HANDLER_ERROR:
            modem->is_timeout = FALSE;
            gsmd_modem_finish_command(modem,
                                      serial->device_index,
                                      FALSE);
            // run unsolicite because we don't know if the message
            // belonged to handler or not
            gsmd_modem_handler_unsolicite( modem_if, NULL, buffer );
            break;
        case AT_HANDLER_NEED_MORE:
            break;
        case AT_HANDLER_DONT_UNDERSTAND:
            gsmd_modem_handler_unsolicite( modem_if, NULL, buffer );
            break;
        case AT_HANDLER_RETRY:
            gsmd_modem_retry_current_command( modem,
                                              serial->device_index );
            break;

        }
    }
    else
    {
        if (gsmd_modem_handler_unsolicite( modem_if, NULL, buffer ) ==
                AT_HANDLER_DONE)
            modem->is_timeout = FALSE;
    }
}








/**
 * @brief Sms indicator parser, parses new sms's index
 *
 * @param scanner whose data to parse
 * @return sms index
 */
gint gsmd_modem_sms_indicator_parse (GScanner* scanner)
{
    //TODO reimplement without assuming that we'll always get proper values
    g_debug("%s",__func__);
    //input: CMTI: "SM", 1
    gint pos = -1;
    if (scanner->token == SYMBOL_CMTI)
    {
        g_scanner_get_next_token (scanner);// get ,
        g_scanner_get_next_token (scanner);//get s
        g_scanner_get_next_token (scanner);//get m
        g_scanner_get_next_token (scanner);//get ,
        pos = scanner->value.v_int;
        //g_message("sms pos is %d\n",pos);

    }
    return pos;
}

/**
* @brief Registers a new command for the modem
*
* @param modem modem to register the new command to
* @param command command to register
*/
void gsmd_modem_register_command (ModemInterface *modem,
                                  const AtCommand *command)
{
    Modem *modem_priv = (Modem*)modem->priv;
    g_hash_table_insert(modem_priv->commands_table,
                        GINT_TO_POINTER(command->cmd_id),
                        (gpointer)command);
}

/**
* @brief Helper function to register serial trigger
*
* @param modem pointer to modem interface
* @param trigger_str trigger string
* @param handler handler for received data that has the trigger string
* @param handler_data user data for the handler
* @param onetime is this a onetime trigger (discarded when trigged)
* or permanent.
* @param interface interface to apply the trigger to
* @return newly created trigger. It is returned only so that it can
* be deregistered with gsmd_serial_deregister_trigger
*/
Trigger* gsmd_modem_serial_register_trigger(ModemInterface *modem,
                                            const gchar *trigger_str,
                                            SerialDataIn handler,
                                            gpointer handler_data,
                                            gboolean onetime,
                                            InterfaceType interface)
{
    Modem *modem_priv = (Modem*)modem->priv;
    SerialDevice *device = gsmd_modem_get_serial_device(modem_priv, interface);
    if (!device)
    {
        return NULL;
    }

    if ( !handler )
    {
        g_debug("%s : Using default handler/data", __func__);
        handler = &gsmd_modem_data_in;
        handler_data = device;
    }

    return gsmd_serial_register_trigger(device->serial,
                                        trigger_str,
                                        handler,
                                        handler_data,
                                        onetime);
}

/**
* @brief Queue a new write to serial port
*
* @param modem pointer to modem interface
* @param data writedata that contains timeout information
* and the data to write to serial port
* @param interface which interface to be used to write command
* @return FALSE if write failed
*/
gboolean gsmd_modem_serial_queue_write ( ModemInterface *modem,
                                         WriteData *data,
                                         InterfaceType interface)
{
    Modem *modem_priv = (Modem*)modem->priv;
    SerialDevice *device = gsmd_modem_get_serial_device(modem_priv, interface);
    if (device)
    {
        if ( !gsmd_serial_queue_write( device->serial, data) )
        {
            g_warning("%s : Error in serial.", __func__);
            gsmd_modem_close_serial_ports(modem_priv);
            return FALSE;
        }
    }
    else
    {
        g_warning("%s : Writing data to improper serial device %d",
                  __func__,
                  device->device_index);
        return FALSE;

    }
    return TRUE;
}

/**
* @brief Writes data directly to serial port
*
* @param modem pointer to modem interface
* @param data writedata that contains timeout information
* and the data to write to serial port
* @param interface which interface to be used to write command
* @return FALSE if write failed
*/
gboolean gsmd_modem_serial_write ( ModemInterface *modem,
                                   WriteData *data,
                                   InterfaceType interface)
{
    Modem *modem_priv = (Modem*)modem->priv;
    SerialDevice *device = gsmd_modem_get_serial_device(modem_priv,
                                                        interface);

    if (device)
    {
        if ( !gsmd_serial_write( device->serial, data) )
        {
            g_warning("%s : Error in serial.", __func__);
            gsmd_modem_close_serial_ports(modem_priv);
            return FALSE;
        }
    }
    else
    {
        g_warning("%s : Writing data to improper serial device %d",
                  __func__,
                  device->device_index);
        return FALSE;
    }
    return TRUE;
}


/**
 * @brief Write command (or data) directly to serial port
 *
 * @param modem modem device struct pointer
 * @param command AT command (or data) to write
 * @param interface which interface to be used to write command
 */
gboolean gsmd_modem_serial_write_simple ( ModemInterface *modem,
                                          const gchar *command,
                                          InterfaceType interface )
{
    WriteData *data = g_try_new0(WriteData,1);
    data->command = g_strdup(command);
    data->timeoutid = NULL;
    data->timeout = 0;
    data->timeout_data = NULL;
    data->timeout_function = NULL;
    return gsmd_modem_serial_write(modem,data,interface);
}

/**
 * @brief Initializes modem
 *
 * Calls all interface initializers aswell.
 *
 * @param modem pointer to modem to initialize
 */
static void gsmd_modem_init (ModemInterface* modem)
{
    Modem *modem_priv = (Modem*)modem->priv;

    modem_priv->vendor = g_new0(VendorInterface, 1);
    gsmd_device_init (modem);
    gsmd_pdp_init (modem);
    gsmd_sim_init (modem);
    gsmd_network_init (modem);
    gsmd_call_init (modem);
    gsmd_sms_init (modem);
}


/**
 * @brief Timeout handler
 *
 * Fakes that modem has received TIME_OUT message and runs the
 * (timeouted) command's handler to handle that input. This allows
 * command handler's creator to perform actions before command is being
 * deleted. If handler doesn't return HANDLER_DONE or HANDLER_DONE_ERROR
 * then command's error function is called by this function.
 * Finally the command is removed.
 *
 * @param data pointer to serialdata
 * @return always returns false
 */
static gboolean gsmd_modem_timeout_handle (gpointer data)
{
    SerialDevice *serial = (SerialDevice*) data;
    guint device_index = serial->device_index;
    AtCommandHandlerStatus handled = AT_HANDLER_DONT_UNDERSTAND;
    Modem* modem = serial->modem;
    AtCommandHandler handler;
    AtCommandContext *at = gsmd_modem_get_current_at_command_index ( modem->modem_if,
                                                                     device_index );

    if ( !at )
    {
        g_warning("%s : No current command!",__func__);
        return FALSE;
    }
    g_debug("%s: timeout id: %d, userdata: %p",__func__,at->timeout_event_id, data);
    // Setting timeout_event_id to 0 as it is removed when we return FALSE here.
    at->timeout_event_id = 0;
    g_assert( at->command);
    handler = at->command->handler;

    g_debug("Timeout on command %s\n",at->command->command);

    if (modem->is_timeout)
    {
        modem->is_timeout = FALSE;//clear the timeout flag

        //If it has a handler, test if it can handle TIME_OUT
        if (handler)
        {
            //copy TIME_OUT string to input buffer
            GString *timeout_buffer = g_string_new (TIME_OUT) ;//
            gsmd_modem_scanner_init (modem->modem_if, timeout_buffer);
            g_debug("Running command's handler for TIME_OUT");
            handled = (*handler)(modem->modem_if, at, timeout_buffer);
            g_string_free (timeout_buffer, TRUE);
        }

        //if it couldn't handle it or didn't have a handler, let
        //unsolicite handler take care of it

        if (!handler || (handled != AT_HANDLER_DONE && handled != AT_HANDLER_DONE_ERROR))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_DEVICE_TIMEOUT,
                                  "TIMEOUT");
        }


        gsmd_modem_finish_command(modem,device_index, TRUE);
    }

    return FALSE;
}
/***********************end of service function********************************/


/**
 * @brief Change network status and use ipc data if new commands are issued
 *
 * This function will post current operator query command if network's new
 * status is registered. This is used by getstatus fso method.
 *
 * @param modem pointer to modem struct
 * @param status new network status
 * @param ipc_data ipc data used to post new commands if necessary
 * @param error_function error function to call if error occurs
 * @param error_data data to be supplied to error function call
 */
void gsmd_modem_change_network_status_data(ModemInterface *modem,
                                           NetworkStatus status,
                                           gpointer *ipc_data,
                                           ErrorFunction error_function,
                                           gpointer error_data)
{
    //TODO check that these assumptions are sane
    /* Simplified assumptions for actions for new statuses
       NETWORK_UNREGISTERED -> clear provider name, send empty status signal
       NETWORK_REGISTERED -> ask provider name and signal strength
       NETWORK_BUSY -> send empty status signal
       NETWORK_DENIED -> send empty status signal
       NETWORK_UNKNOWN -> send empty status signal
       NETWORK_ROAMING -> ask provider name and signal strength
    */

    GString* message = g_string_new ("");
    g_string_printf(message, "%d",status);



    gsmd_utils_table_insert_int(modem->caches[INTERFACE_NETWORK],
                                GSMD_MODEM_KEY_NETWORK_STATUS,
                                status);

    switch (status)
    {
    case NETWORK_UNREGISTERED:
    case NETWORK_BUSY:
    case NETWORK_DENIED:
    case NETWORK_UNKNOWN:
    {
        if (modem->network->provider_name)
        {
            g_string_free(modem->network->provider_name,TRUE);
        }
        GHashTable *status = gsmd_utils_create_hash_table();
        gsmd_utils_table_insert_string(status,"provider","");
        gsmd_utils_table_insert_string(status,"registration",message->str);
        gsmd_utils_table_insert_int(status,"strength",0);
        modem->network->provider_name = g_string_new("");
        modem->network_ipc->status(modem->network_ipc,
                                   status);
        g_hash_table_destroy(status);
    }
    break;
    case NETWORK_ROAMING:
    case NETWORK_REGISTERED:
        gsmd_modem_post_at_command_id(  modem,
                                        CUR_OP_QUERY,
                                        NULL,
                                        ipc_data,
                                        error_function,
                                        error_data,
                                        INTERFACE_NETWORK,
                                        NULL);
        break;

    }



    g_string_free(message,TRUE);
}

/**
 * @brief Change network status
 *
 * @param modem modem interface pointer
 * @param new status
 */
void gsmd_modem_change_network_status(ModemInterface *modem,
                                      NetworkStatus status)
{
    gsmd_modem_change_network_status_data(modem,status,
                                          NULL,
                                          NULL,
                                          NULL);
}


/**
* @brief Handles network status (+CREG) messages
*
* @param modem modem to apply network status
* @param response string to handle
*/
static
void gsmd_modem_handle_network_status(ModemInterface *modem,
                                      GString *response)
{
    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CREG:\\s(?<status>[0-5]),?(?<lac>\\d*),?(?<id>\\d*)",
                                 0,
                                 0,
                                 error);
    GMatchInfo *match_info;
    NetworkStatus status = NETWORK_UNKNOWN;
    gchar *match = NULL;

    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"status"))
        {
            match = g_match_info_fetch_named(match_info,
                                             "status");
            status = g_ascii_digit_value(match[0]);
            gsmd_modem_change_network_status(modem,status);
            g_free(match);
        }
    }

    g_match_info_free (match_info);
    g_regex_unref (regex);
}


/**
* @brief Handles clip messages
* Creates a new call with unknown call id
*
* @param modem modem whose clip to handle
* @param response string with CLIP
*/
static
void gsmd_modem_handle_incoming_call(ModemInterface *modem,
                                     GString *response)
{
    g_debug("%s",__func__);
    /*
    +CLIP: "040123456789",128,"",128,"",0

    \+CLIP:\s"(\+?\w+)".*
    */

    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CLIP:\\s(?<phone>\"\\+?\\w+\")",
                                 0,
                                 0,
                                 error);
    GMatchInfo *match_info;
    gchar *match = NULL;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"phone"))
        {
            match = g_match_info_fetch_named(match_info,
                                             "phone");

            gchar *number = NULL;
            if (g_str_equal(match,"\"\""))
                number = g_strdup("Unknown number");
            else
            {
                GString *temp = g_string_new(&match[1]);
                g_string_erase(temp,temp->len-1,1);
                number = g_strdup(temp->str);
                g_string_free(temp,TRUE);
            }

            g_debug("%s : incoming call from : %s",__func__,number);

            Call *call = gsmd_utils_find_call_number(modem,number);
            if ( !call)
            {
                call = gsmd_utils_new_call(modem,
                                           number,
                                           "", //TODO type ignored
                                           CALL_INCOMING);
            }
            gsmd_modem_monitor_call_activity(modem);

            gsmd_utils_call_send_status(modem,
                                        call,
                                        "incoming");

            g_free(match);
            g_free(number);
        }
    }
    else
    {
        g_debug("Didn't understand +clip");
    }

    g_match_info_free (match_info);
    g_regex_unref (regex);
}


/**
* @brief Finds first call with incoming status and sends call status signal
* Should be called when ring message is received from gsm modem.
*
* @param modem modem who sent ring message
*/
static
void gsmd_modem_handle_ring(ModemInterface *modem)
{
    Call *call = gsmd_utils_find_first_call_status(modem,CALL_INCOMING);
    if (call)
    {
        gsmd_utils_call_send_status(modem,
                                    call,
                                    "incoming");
    }
}

/**
* @brief Handles CCWA (call waiting) messages from modem
*
* @param modem modem whose call waiting message to handle
* @param response string with CCWA
*/
static
void gsmd_modem_handle_call_waiting(ModemInterface *modem,
                                    GString *response)
{
    /*
    +CCWA: "0401234567",129,1,"",0

    \+CCWA:\s"(\+?\d+)"
    */

    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CCWA:\\s\"(?<phone>\\+?\\d+)\"",
                                 0,
                                 0,
                                 error);
    GMatchInfo *match_info;
    gchar *match = NULL;

    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"phone"))
        {
            match = g_match_info_fetch_named(match_info,
                                             "phone");

            Call *call = gsmd_utils_find_call_number(modem,match);
            if ( !call)
            {
                call = gsmd_utils_new_call(modem,
                                           match,
                                           "", //TODO type ignored
                                           CALL_INCOMING);
            }


            gsmd_utils_call_send_status(modem,
                                        call,
                                        "incoming");

            g_free(match);
        }
    }
    else
    {
        g_debug("Didn't understand +ccwa");
    }

    g_match_info_free (match_info);
    g_regex_unref (regex);
}

/**
* @brief Parses clcc line and applies information to modem's calls
* Can also create new calls if necessary.
*
* @param modem pointer to modem whose calls to modify
* @param clcc line returned by clcc
* @return handled call or NULL
*/
Call* gsmd_modem_apply_current_call(ModemInterface *modem, const gchar *clcc)
{
    /*
    Test data with clip and ecam:
    +CLIP: "044123456",128,"",128,"",0

    #ECAM: 0,6,1,,,

    RING

    +CLIP: "044123456",128,"",128,"",0
    at+clcc

    +CLCC: 1,1,4,0,0,"044123456",128,""

    Test data with two calls
    +CLCC: 1,0,0,0,0,"+35840123456",145,""
    +CLCC: 2,1,5,0,0,"044654321",128,""

    OK

    In the first example #ECAM has call id 0
    but CLCC says call id is 1
    Call id can't be trusted to be call->vendor_id,
    but vendor_id _could_ be clcc's id -1

    \+CLCC:\s\d+,(\d),(\d),(\d),\d,"(\+?\d+)"
    */

    static GRegex *regex = NULL;
    if ( !regex )
    {
        regex = g_regex_new ("\\+CLCC:\\s(?<index>\\d+),(?<direction>\\d),"
                             "(?<state>\\d),(?<mode>\\d),\\d,\"(?<number>\\+?\\d+)\"",
                             0,
                             0,
                             NULL);
    }
    GMatchInfo *match_info;
    Call *call = NULL;

    if (g_regex_match (regex, clcc, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"index"))
        {
            gint index = gsmd_utils_fetch_match_int(match_info,
                                                    "index",0);

            gint state = gsmd_utils_fetch_match_int(match_info,
                                                    "state",0);

            gchar *number = g_match_info_fetch_named(match_info,
                                                     "number");

            //phone number types might cause a problem if modem
            //automatically converts them to national/international
            //format. This would cause the call to not be found
            call = gsmd_utils_find_call_number(modem,number);
            if (!call)
            {
                gsmd_utils_print_calls(modem);
                call = gsmd_utils_new_call(modem,number,"",CALL_IDLE);
            }
            if (call->index < 0)
            {
                call->index = index;
            }
            else if (call->index != index)
            {
                // TODO handle this?
                g_warning("%s : Number matches but index does not (%d != %d)",__func__, call->index, index);
                gsmd_utils_print_calls(modem);
            }

            switch (state)
            {
            case 0:
                call->status = CALL_CONNECTED;
                break;
            case 1:
                call->status = CALL_HOLD;
                break;
            case 2:
            case 3:
                call->status = CALL_CALLING;
                break;
            case 4:
            case 5:
                call->status = CALL_INCOMING;
                break;
            }

            //Mode is currently ignored
            g_debug("%s: Got a call to/from %s with status %s",
                    __func__,
                    number,
                    gsmd_utils_call_status_to_string(call->status));

            g_free(number);

        }
    }

    g_match_info_free (match_info);

    return call;
}

/**
* @brief Iterates all calls in current calls list in given table
* and calls gsmd_modem_apply_current_call on each of them
*
* @param modem modem who own's the calls to parse
* @param table GHashtable containing pointer to current calls list under
* GSMD_MODEM_KEY_CALL_STATUS_ARRAY key
*/
void gsmd_modem_current_calls_parse(ModemInterface *modem,GHashTable *table)
{
    g_assert(table);
    g_assert(modem);

    GValue *val = g_hash_table_lookup(table,
                                      GSMD_MODEM_KEY_CALL_STATUS_ARRAY);

    GList *calls = NULL;
    if (val)
    {

        GList *list = g_value_get_pointer(val);

        while (list)
        {
            gchar *line = list->data;
            Call *call = gsmd_modem_apply_current_call(modem,line);
            if ( call )
            {
                calls = g_list_append (calls, call);
            }
            list = g_list_next( list );
        }
    }
    if (modem->call_monitor_timer != 0)
    {
        g_debug("%s : Checking calls", __func__);
        gboolean need_activity_monitor = FALSE;
        GList *list = modem->calls;
        GList *calls_to_remove = NULL;
        while (list)
        {
            Call *call = (Call*)list->data;
            GList *tmp = calls;
            gboolean found = FALSE;
            while (tmp)
            {
                if (tmp->data == (gpointer)call )
                {
                    found = TRUE;
                    break;
                }
                tmp = g_list_next(tmp);
            }
            if ( !found )
            {
                g_debug("%s : Call %d not anymore", __func__, call->id);
                calls_to_remove = g_list_append(calls_to_remove, call);
            }
            else if ( call->status == CALL_INCOMING )
            {
                need_activity_monitor = TRUE;
            }
            list = g_list_next(list);
        }
        list = calls_to_remove;
        while ( list )
        {
            gsmd_utils_remove_call_direct(modem,(Call*)list->data,TRUE);
            list = g_list_next(list);
        }
        g_list_free(calls_to_remove);
        if ( need_activity_monitor )
        {
            modem->call_monitor_timer = 0;
            gsmd_modem_monitor_call_activity (modem);
        }
    }
}

/**
* @brief Free function for list current calls method
* Frees current calls array from the at command
*
* @param at at command context to free current calls array
*/
static
void gsmd_modem_current_calls_free(AtCommandContext *at)
{
    g_assert(at);

    GList *list = NULL;
    GValue *val = g_hash_table_lookup(at->handler_data,
                                      GSMD_MODEM_KEY_CALL_STATUS_ARRAY);

    if (!val)
        return;

    list = g_value_get_pointer(val);

    while (list)
    {
        g_free(list->data);
        list = g_list_next( list );
    }

    g_list_free(g_value_get_pointer(val));
}

/**
* @brief Handler for a command to list current calls
* Adds calls to a list and then updates call structs
* within modem to match.
*
* @param modem pointer to modem interface
* @param at current at command's context
* @param response message to handle
* @return handler status
*/
static
AtCommandHandlerStatus gsmd_modem_handler_current_calls(ModemInterface *modem,
                                                        AtCommandContext *at,
                                                        GString *response)
{
    g_debug("%s",__func__);
    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CLCC:\\s(?<index>\\d+),(?<direction>\\d),(?<state>\\d),(?<mode>\\d),\\d,\"(?<number>\\+?\\d+)\"|(?<error>ERROR)|(?<timeout>TIME_OUT)|(?<ok>OK)",
                                 0,
                                 0,
                                 error);
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GMatchInfo *match_info;
    GValue *val = NULL;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"index"))
        {

            GList *list = NULL;
            val = g_hash_table_lookup(at->handler_data,
                                      GSMD_MODEM_KEY_CALL_STATUS_ARRAY);
            if (val)
                list = g_value_get_pointer(val);

            list = g_list_append(list,g_strdup(response->str));

            gsmd_utils_table_insert_pointer(at->handler_data,
                                            GSMD_MODEM_KEY_CALL_STATUS_ARRAY,
                                            list);


            status = AT_HANDLER_NEED_MORE;
        }
        else if (gsmd_utils_match_exists(match_info,"error"))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "FAILED TO READ CURRENT CALL LIST");
            status = AT_HANDLER_DONE_ERROR;
        }
        else if (gsmd_utils_match_exists(match_info,"timeout"))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_DEVICE_TIMEOUT,
                                  "TIMEOUT WHEN READING CURRENT CALL LIST");
            status = AT_HANDLER_DONE_ERROR;
        }
        else if (gsmd_utils_match_exists(match_info,"ok"))
        {

            gsmd_modem_current_calls_parse(modem,at->handler_data);


            status = AT_HANDLER_DONE;
        }

    }

    g_free(error);
    g_match_info_free (match_info);
    g_regex_unref (regex);


    return status;

}

/**
* @brief Handler for command to test if modem is alive
*
* @param modem pointer to modem interface
* @param at current at command's context
* @param response message to handle
* @return handler status
*/
static
AtCommandHandlerStatus gsmd_modem_handler_alive(ModemInterface *modem,
                                                AtCommandContext *at,
                                                GString *response)
{
    g_debug("%s",__func__);
    Modem *modem_priv = (Modem*)modem->priv;
    GScanner* scanner = modem->scanner;

    //we've got a response, modem is alive. Start initializations
    if (scanner->token == SYMBOL_OK)
    {
        if (modem->status == MODEM_SERIAL_INIT)
        {
            modem->status = MODEM_INITIALIZING;

            if ( modem_priv->vendor->init_at )
            {
                modem_priv->vendor->init_at (modem_priv->vendor);
            }
            else
            {
                gsmd_modem_general_at_init(modem);
            }
        }
        return AT_HANDLER_DONE;
    }

    //Modem didn't reply or replied with an error
    if (scanner->token == SYMBOL_TIME_OUT ||
            scanner->token == SYMBOL_ERROR)
    {
        modem->status = MODEM_UNINITIALIZED;
        modem->sim_status = SIM_UNKNOWN;

        //Ask vendor if it can deal with an unresponsive modem
        if (modem_priv->vendor->not_responding &&
                modem_priv->vendor->not_responding(modem_priv->vendor))
            return AT_HANDLER_RETRY;
        else
        {
            gsmd_modem_close_serial_ports(modem_priv);
            return AT_HANDLER_DONE;
        }
    }

    return AT_HANDLER_DONT_UNDERSTAND;
}

/**
 * @brief Handles unsolicited messages such as +CREG.
 *
 * Unsolicite mesaage is an unexpected message or reply or a reply the
 * command's handler didn't understand.
 *
* @param modem pointer to modem interface
* @param at current at command's context
* @param response message to handle
* @return handler status
 */
static
AtCommandHandlerStatus gsmd_modem_handler_unsolicite (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response)
{
    Modem *modem_priv = (Modem*)modem->priv;
    GScanner* scanner = modem->scanner;
    gint sms_pos;
    g_debug("%s", __func__);
    Call *call = NULL;
    gint call_id = 0;

    if (g_scanner_eof(scanner))
        return AT_HANDLER_DONE;

    //Let vendor override unsolicite messages first
    if (modem_priv->vendor->unsolicite_handler)   //if there is vendor handler
    {

        AtCommandHandlerStatus status;
        /*                 g_debug("%s: Running vendor_unsolicite_handler", __func__); */
        //if vendor handled it, then we dont need to handle it ourselves
        status = modem_priv->vendor->unsolicite_handler (modem, NULL, response);
        if ( status != AT_HANDLER_DONT_UNDERSTAND )
        {
            /*                         g_debug("%s: vendor_unsolicite_handler understood", __func__); */
            return status;
        }
        //reset the scanner
        gsmd_modem_scanner_init (modem, response);
    }


    g_debug("%s: Running our default handler", __func__);

    switch (scanner->token)
    {
    case '+':
        g_scanner_get_next_token (scanner);
        switch (scanner->token)
        {
        case SYMBOL_CCWA:
            gsmd_modem_handle_call_waiting(modem,response);
        case SYMBOL_CLIP://display the incoming call phoneNo
            gsmd_modem_handle_incoming_call(modem,response);
            break;
        case SYMBOL_CRING://display +CRING: VOICE
            // don't handle RING, we use CLIP
            gsmd_modem_handle_ring(modem);

            break;
        case SYMBOL_CREG:
            gsmd_modem_handle_network_status(modem,response);
            break;
        case SYMBOL_CMTI://sms indicator
            g_debug("%s: CMTI", __func__);
            sms_pos = gsmd_modem_sms_indicator_parse (scanner);
            if (sms_pos > 0 && modem->sim_ipc->incoming_message)
            {
                modem->sim_ipc->incoming_message(modem->sim_ipc,
                                                 sms_pos);

            }

            break;
        default:
            g_warning ("Got unrecognized unsolicited symbol: %s\n",
                       response->str);
        }
        break;

        /**
        * NOTE! Handling NO CARRIER and BUSY have to be overridden in
        * vendor unsolicite handler if multiple concurrent calls are to be
        * implemented. By default we assume that only concurrent call
        * is possible and we remove the first call in both previously
        * mentioned cases.
        */
    case SYMBOL_BUSY://remote hangup the call
        g_debug("%s: BUSY", __func__);
        call = gsmd_utils_call_get_current(modem);
        if (call)
        {
            call_id = call->id;
            gsmd_utils_remove_call(modem,call_id, TRUE);
        }

        break;
    case SYMBOL_RING://handled in CLIP
        break;


    case SYMBOL_NO:
        g_scanner_get_next_token(scanner);
        if (scanner->token == SYMBOL_CARRIER)
        {
            call = gsmd_utils_call_get_current(modem);
            if (call)
            {
                call_id = call->id;


                gsmd_utils_remove_call(modem,call_id, TRUE);
            }
        }
        else
        {
            g_scanner_unexp_token (scanner,
                                   SYMBOL_CARRIER,
                                   NULL,
                                   "symbol",
                                   NULL,
                                   NULL,
                                   TRUE);

        }
        break;
    default:
        break;

    }

    return AT_HANDLER_DONE;
}
/**********************End of Default handler**********************************/


/**
 * @brief Re-excute current AT command at the tail of command queue
 *
 * @param modem Modem device struct pointer
 * @param interface interface whose current command to retry
 */
void gsmd_modem_retry_current_command (Modem* modem, InterfaceType interface)
{
    g_debug("%s: device: %d", __func__,interface);
    g_assert ( modem );
    g_assert ( modem->serial_devices );
    AtCommandContext *at = gsmd_modem_get_current_at_command ( modem->modem_if,
                                                               interface );
    modem->is_timeout = FALSE;


    //Remove old timeout
    if (at->timeout_event_id != 0)
    {
        g_debug("%s : Removing timeout id: %d", __func__, at->timeout_event_id);
        g_source_remove(at->timeout_event_id);
        at->timeout_event_id = 0;
    }


    if (at->retry_counter > 0)
    {
        g_debug("Retrying command %s",at->command->command);
        at->retry_counter--;
        gsmd_modem_process_next_command (modem,interface);
    }
    else
    {
        g_debug("Finished retrying command %s",at->command->command);
        gsmd_modem_finish_command(modem,interface,FALSE);
    }


}


/**
* @brief Finds all commands with specified command id in the command queue
* and removes them. Doesn't properly cancel the command if it's currently
* being processed (e.g. if it has already been sent to gsm modem)
*
* @param modem modem whose commands to cancel
* @param cmd_id id of the commands to cancel
* @param interface which interface's commands to cancel
* @return TRUE if atleast one command was cancelled
*/
gboolean gsmd_modem_cancel_command_id(ModemInterface *modem,
                                      AtCommandID cmd_id,
                                      InterfaceType interface)
{
    Modem * modem_priv = (Modem*)modem->priv;
    SerialDevice *device = gsmd_modem_get_serial_device(modem_priv, interface);
    g_assert(device);
    GQueue* queue = device->commandQueue;
    g_assert(queue);
    GList* list = NULL;
    gboolean res = FALSE;

    gint len = g_queue_get_length (queue);
    gint i=0;
    AtCommandContext* at = NULL;
    for (i=0; i < len; i++)   //the tail should not be remove which is handling
    {
        at = (AtCommandContext*) g_queue_peek_nth (queue, i);
        if ( at->command->cmd_id == cmd_id )
        {
            //g_queue_remove (queue, (gpointer) at);
            list = g_list_append (list, at);
        }
    }
    while (list)
    {
        res = TRUE;
        g_queue_remove (queue, list->data);
        gsmd_at_command_context_free(list->data);
        list = g_list_next (list);
    }
    return res;
}

/**
* @brief Assigns device indices to each of gsmd2's interfaces
*
* @param modem modem whose interface devices to set
* @param indices a list of indices to set
* @return TRUE if indices were properly assigned
*/
gboolean gsmd_modem_assign_device_indices(ModemInterface *modem, int *indices)
{
    int i=0;
    int last_device = -1;
    for (i=0;i<INTERFACE_LAST;i++)
    {
        int dev = indices[i];
        SerialDevice *device = gsmd_modem_get_serial_device_index((Modem*)modem->priv,dev);
        if (!device)
        {
            if ( last_device < 0)
            {
                return FALSE;
            }
            g_warning("%s : Assigning device %d because device %d is not available",
                      __func__, last_device, dev);
            dev = last_device;
        }
        last_device = dev;
        modem->interface_devices[i]=dev;
    }
    return TRUE;
}


/**
* @brief Gets given interface's device index
* Modem might have several serial devices, each interface
* (CALL,SMS,SIM,NETWORK,...) is assigned to one of those devices.
*
*
* @param modem modem whose interface to lookup
* @param interface interface whose device to lookup
* @return interface's device index
*/
guint gsmd_modem_get_interface_device_index(ModemInterface *modem,
                                            InterfaceType interface)
{
    g_assert(modem);
    return modem->interface_devices[interface];
}

/**
* @brief Finds command with specified command id from commands_table
*
* @param modem modem whose command to find
* @param cmd_id command id to find
* @return command's pointer or NULL if none found
*/
AtCommand* gsmd_modem_get_command (ModemInterface *modem,
                                   guint cmd_id)
{
    Modem *modem_priv = (Modem*)modem->priv;
    return (AtCommand*)g_hash_table_lookup(modem_priv->commands_table,
                                           GINT_TO_POINTER(cmd_id));
}

/**
* @brief Sets modem's state to RESET
*
* @param modem modem to set
*/
void gsmd_modem_reset(ModemInterface *modem)
{
    g_debug("%s",__func__);
    g_assert( modem );

    modem->status = MODEM_RESET;
}

