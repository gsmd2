/*
 *  serial.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include <string.h>

#include <glib.h>
#include <unistd.h>
#include "serial.h"
#include "modem.h"
#include "utils.h"

struct _Trigger {
        gchar *name;
        gchar *str;
        SerialDataIn handler;
        gpointer handler_data;
        gboolean onetime;
};


static
void gsmd_write_data_free(WriteData *data)
{
        g_free(data->command);
        g_free(data);
}


Trigger* gsmd_serial_register_trigger(Serial *serial,
                                      const gchar *trigger_str,
                                      SerialDataIn handler,
                                      gpointer handler_data,
                                      gboolean onetime)
{
        g_assert(serial);
        Trigger *trig = NULL;
        trig = g_new0(Trigger,1);
        trig->str = g_strdup( trigger_str );
        trig->name = g_strescape(trigger_str,NULL);
        g_debug("%s: '%s', %p, %p, %p",__func__, trig->name, trig, handler, handler_data);
        trig->handler = handler;
        trig->handler_data = handler_data;
        trig->onetime = onetime;
        serial->triggers = g_list_append( serial->triggers, trig );
        return trig;
}

void gsmd_serial_trigger_free( Trigger *trig )
{
        g_debug("%s", __func__);
        g_assert( trig );
        g_free( trig->str );
        trig->str = NULL;
        g_free( trig->name );
        trig->name = NULL;
        g_free( trig );
}

void gsmd_serial_deregister_trigger( Serial *serial, Trigger *trigger )
{
        g_debug("%s", __func__);
        serial->triggers = g_list_remove(serial->triggers, trigger);
        gsmd_serial_trigger_free( trigger );
}

static
gboolean gsmd_serial_check_triggers(Serial *serial)
{
        Trigger *match = NULL;
        gchar *match_str = NULL;
        GList *list = NULL;
        g_assert( serial );
        list = serial->triggers;
        while ( list ) {
                gchar *str;
                Trigger *trig = (Trigger*)list->data;
                str = g_strstr_len( serial->buffer->str,
                                    serial->buffer->len,
                                    trig->str);
                if ( str ) {
                        if ( !(match && str >= match_str) ) {
                                match = trig;
                                match_str = str;
                        }
                }
                list = g_list_next( list );
        }
        if ( match ) {
                gssize len = match_str - serial->buffer->str + strlen(match->str);
                gchar *tmp = g_strndup( serial->buffer->str, len);
                GString *buffer = g_string_new_len(tmp, len);
                g_string_erase(serial->buffer, 0, len);
                g_free( tmp );
                /*                 g_debug("%s: Running handler (%s)", __func__, match->name); */
                (*match->handler)( buffer, match->handler_data );
                g_string_free( buffer, TRUE );
                buffer = NULL;
                if ( match->onetime ) {
                        /*                         g_debug("%s: Removing onetime handler", __func__); */
                        gsmd_serial_deregister_trigger( serial, match );
                }
                return TRUE;
        }
        return FALSE;
}

#define READ_BUF_LEN 100
static
void gsmd_serial_read (Serial *serial)
{
        GIOStatus r = G_IO_STATUS_NORMAL;
        gsize length = 1;
        GError *error = NULL;
        gchar buf[READ_BUF_LEN];
        GIOChannel *io = serial->io;
        while (length) {
                r = g_io_channel_read_chars(io, buf, READ_BUF_LEN, &length, &error);
                if ( error != NULL ) {
                        g_warning("%s : Failed to read data from serial port: %s",__func__, error->message);
                        g_error_free (error);
                        error = NULL;
                        length=0;
                } else if ( r != G_IO_STATUS_NORMAL ) {
                        if (r != G_IO_STATUS_EOF) {
                                g_warning("%s : GIOStatus != normal: %d", __func__, r);
                        }
                        length = 0;
                } else {
                        gint i=0;
                        buf[length] = 0;
                        // Deal with zeros. Any better ideas?
                        for ( i=0; i < length;i++ ) {
                                if ( buf[i] == 0) {
                                        buf[i] = '\r';
                                }
                        }


                        serial->buffer = g_string_append_len(serial->buffer, buf, length );

                        //g_debug("%s : '%s'",__func__,serial->buffer->str);
                        while ( gsmd_serial_check_triggers(serial) );
                }
        }

/*         gsmd_utils_print_data(__func__,"Read: ",serial->buffer->str); */
}
static
gboolean gsmd_serial_ready_to_read (GIOChannel *io, GIOCondition condition, gpointer data)
{
        Serial *serial = (Serial*)data;
        g_assert( serial );
        g_assert( serial->io == io );
        if ( (condition & G_IO_ERR) || (condition & G_IO_HUP)) {
                g_debug("%s : G_IO_ERR || G_IO_HUP", __func__);
                serial->error = TRUE;
                if (serial->watch) {
                        g_source_remove(serial->watch);
                        serial->watch = 0;
                }
                return TRUE;
        }
/*         if ( (condition & G_IO_IN) ) { */
/*                 g_debug("%s : G_IO_IN", __func__); */
/*         } */
/*         if ( (condition & G_IO_NVAL) ) { */
/*                 g_debug("%s : G_IO_NVAL", __func__); */
/*         } */
/*         if ( (condition & G_IO_PRI) ) { */
/*                 g_debug("%s : G_IO_PRI", __func__); */
/*         } */
        gsmd_serial_read(serial);
        return TRUE;
}


void gsmd_serial_free(Serial *serial)
{
        if ( !serial ) {
                return;
        }
        GList *list;
        g_assert( serial );
        GError *error = NULL;


        g_io_channel_shutdown(serial->io,TRUE,&error);

        if (error) {
                g_warning("%s : Error occurred when shutting io channel down: %s",
                          __func__,
                          error->message);
        }
        g_io_channel_unref (serial->io);
        close(serial->fd);

        if (serial->watch) {
                g_source_remove(serial->watch);
                serial->watch = 0;
        }

        list = serial->triggers;
        while ( list ) {
                gsmd_serial_trigger_free( (Trigger*)list->data);
                list->data = NULL;
                list = g_list_next(list);
        }
        g_list_free( serial->triggers );
        serial->triggers = NULL;
        if ( serial->buffer ) {
                g_string_free(serial->buffer, TRUE);
                serial->buffer = NULL;
        }

        g_free( serial );
}

/**
 * @brief Opens a serial port.
 *
 * @param device: the device string
 * @param modem: Modem struct
 * @return io channel or NULL if error occurred
 */
Serial *gsmd_serial_open_port (const gint fd)
{
        GError* err = NULL;
        Serial *serial = g_new0(Serial, 1);
        serial->fd = fd;
        serial->triggers = NULL;
        serial->buffer = g_string_new("");
        serial->error = FALSE;


        serial->io = g_io_channel_unix_new (fd);
        if (!serial->io || err) {
                g_warning("%s : open failed: %s", __func__, err->message);
                goto error;
        }
        g_io_channel_set_encoding(serial->io, NULL, &err);
        if (err) {
                g_warning("%s : set encoding failed: %s", __func__, err->message);
                goto error;
        }
        g_io_channel_set_flags( serial->io,
                                G_IO_FLAG_NONBLOCK |G_IO_FLAG_APPEND ,
                                &err);
        if (err) {
                g_warning("%s : set flags failed: %s", __func__, err->message);
                goto error;
        }

        serial->watch = g_io_add_watch (serial->io,
                                        G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP | G_IO_NVAL ,
                                        &gsmd_serial_ready_to_read,
                                        serial);

        return serial;

error:
        if (serial ) {
                if ( serial->io ) {
                        g_io_channel_unref (serial->io);
                }
                g_free( serial );
        }
        return NULL;
}



/**
 * @brief Writes date to serial port.
 *
 * This is the function that does the actual writing to serial port.
 *
 * @param io io where to write
 * @param write WriteData to write
 */
void gsmd_serial_do_write (GIOChannel* io,
                           WriteData *write)
{
        g_return_if_fail(write);
        gsize written;
        gsize cmd_len;

        GError *error = NULL;
        gsmd_utils_print_data(__func__,"write to buffer: ",write->command);

        cmd_len = strlen(write->command);
        if ( g_io_channel_write_chars (io,
                                       write->command,
                                       cmd_len,
                                       &written,
                                       &error) != G_IO_STATUS_NORMAL) {
                g_debug("%s : Failing write?", __func__);
        }

        if (error) {
                g_debug ("write: %s", error->message);
                g_error_free (error);
                error = NULL;
        }
        if ( g_io_channel_flush (io, &error) != G_IO_STATUS_NORMAL) {
                g_debug("%s : Failing flush?", __func__);
        }
/*         g_debug ("written: %d", written); */
        if( written < cmd_len ) {
                g_warning("%s : %d < %d. TODO deal with partial writes if you see this", __func__, (gint)written, (gint)cmd_len );
        }
        if ( write->write_complete_function ) {
                write->write_complete_function(write, write->write_complete_data);
        }
        if (error) {
                g_debug ("flush: %s", error->message);
                g_error_free (error);
                error = NULL;
        }
        //Add a timeout event
        if (write->timeoutid && write->timeout && write->timeout_function) {
                gint id = g_timeout_add_full(G_PRIORITY_DEFAULT,
                                             write->timeout,
                                             write->timeout_function,
                                             write->timeout_data,
                                             NULL);
/*                 g_debug("%s : timeout id: %d (userdata %p), timeout %dms", */
/*                         __func__, */
/*                         id, */
/*                         write->timeout_data, */
/*                         write->timeout); */

                *write->timeoutid = id;
                /*
                    *write->timeoutid = g_timeout_add_full(G_PRIORITY_DEFAULT,
                                                           write->timeout,
                                                           write->timeout_function,
                                                           write->user_data,
                                                           NULL);
                                                           */
        }

        gsmd_write_data_free(write);
}

/**
 * @brief Function that is called when serial port is ready to
 * accept data.
 *
 * @param io io where to write
 * @param condition ignored
 * @param data data to write
 * @return always returns false
 */
static gboolean gsmd_serial_ready_to_write (GIOChannel* io,
                                            GIOCondition condition,
                                            gpointer data)
{
        gsmd_serial_do_write(io,(WriteData*)data);
        return FALSE;
}

/**
 * @brief Writes data directly to serial port.
 *
 * @param serial serial to write to
 * @param write WriteData to write
 */
gboolean gsmd_serial_write (Serial *serial,
                            WriteData *write)
{
        if( serial->error ) {
                gsmd_write_data_free(write);
                return FALSE;
        }
        gsmd_serial_do_write(serial->io, write);
        return TRUE;
}

/**
 * @brief Function that queues writing of data
 *
 * Doesn't actually write anything to serial port but adds an
 * io watch for the actual writing function. Data will be written
 * once serial is ready to write.
 *
 * @param io io channel to write to
 * @param command data (in this case the at command) to write
 */
gboolean gsmd_serial_queue_write ( Serial *serial, WriteData *data)
{
        if( serial->error ) {
                gsmd_write_data_free(data);
                return FALSE;
        }
        g_io_add_watch (serial->io, G_IO_OUT, gsmd_serial_ready_to_write,data);
        return TRUE;
}
