/*
 *  utils.h1
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#ifndef GSMD_UTILS_H
#define GSMD_UTILS_H

#include <glib.h>
#include <glib/gstdio.h>
#include "modem.h"
#include "gsmd-error.h"
#include <termios.h>

#define CMS_ERROR_INVALID_MESSAGE_INDEX		321


gboolean gsmd_utils_check_end_token(GScanner* scanner);
gboolean gsmd_utils_match_exists(GMatchInfo *match_info,const char *name);
void gsmd_utils_send_error(AtCommandContext *at,
                           GsmdErrorCode code,
                           const char *message);
void gsmd_utils_send_error_full(ErrorFunction error_function,
                                gpointer error_data,
                                gpointer ipc_data,
                                GsmdErrorCode code,
                                const char *message);

Call* gsmd_utils_new_call(ModemInterface *modem,
                          const gchar *number,
                          const gchar *type,
                          CallStatus status);

void gsmd_utils_remove_call(ModemInterface *modem, guint id, gboolean send_released_call);

void gsmd_utils_remove_call_vendor_id(ModemInterface *modem,
                                      gint vendor_id,
                                      gboolean send_released_call);

void gsmd_utils_set_unknown_vendor_id(ModemInterface *modem, gint vendor_id);

Call* gsmd_utils_find_call_vendor_id(ModemInterface *modem, gint vendor_id);

void gsmd_utils_remove_call_direct(ModemInterface *modem,
                                   Call *call,
                                   gboolean send_released_call);

Call* gsmd_utils_find_first_call_status(ModemInterface *modem, CallStatus status);

Call* gsmd_utils_find_call_number(ModemInterface *modem, const gchar *number);

Call* gsmd_utils_find_call_id(ModemInterface *modem, guint id);

CallStatus gsmd_utils_get_call_status(ModemInterface *modem, gint id);

Call* gsmd_utils_call_get_current(ModemInterface *modem);

void gsmd_utils_print_calls(ModemInterface *modem);

gint gsmd_utils_get_call_status_count(ModemInterface *modem,CallStatus status);

gboolean gsmd_utils_send_error_from_response(AtCommandContext *at,
                                             GString* response,
                                             const gchar *errormsg,
                                             const gchar *timeoutmsg);

const gchar *gsmd_utils_table_get_string(GHashTable *table,
                                         gchar *key,
                                         const gchar* value);

gpointer gsmd_utils_table_get_pointer(GHashTable *table,
                                      gchar *key,
                                      gpointer value);

gint gsmd_utils_table_get_int(GHashTable *table,
                              gchar *key,
                              gint value);

gboolean gsmd_utils_table_get_boolean(GHashTable *table,
                                      gchar *key,
                                      const gboolean value);

void gsmd_utils_table_insert_boolean(GHashTable *table,
                                     gchar *key,
                                     const gboolean value);

void gsmd_utils_table_insert_string(GHashTable *table,
                                    gchar *key,
                                    const gchar *value);

void gsmd_utils_table_insert_int(GHashTable *table,
                                 gchar *key,
                                 const gint value);

void gsmd_utils_table_insert_pointer(GHashTable *table,
                                     gchar *key,
                                     const gpointer value);

void gsmd_utils_table_insert_copy(GHashTable *table,
                                  gchar *key,
                                  GValue *value);
void gsmd_utils_remove_calls_with_status(ModemInterface *modem,
                                         CallStatus status,
                                         gboolean send_released_call);

const gchar *gsmd_utils_call_status_to_string(CallStatus status);

void gsmd_utils_remove_calls(ModemInterface *modem,
                             gboolean send_released_call);

GHashTable* gsmd_utils_call_get_properties(Call *call);

void gsmd_utils_call_send_status(ModemInterface *modem,
                                 Call *call,
                                 const gchar *status);

gint gsmd_utils_fetch_match_int(GMatchInfo *match_info,
                                gchar *match,
                                gint value);

GString *gsmd_utils_fetch_match_string(GMatchInfo *match_info,
                                       gchar *match,
                                       GString *value);

gint gsmd_utils_default_serial_init (const gchar *device,
                                     tcflag_t cflag,
                                     speed_t speed);

Call* gsmd_utils_get_first_call(ModemInterface *modem);

GHashTable *gsmd_utils_create_hash_table();

const gchar *gsmd_utils_call_status_to_fso(CallStatus status);

AtCommandHandlerStatus gsmd_utils_handler_ok (ModemInterface *modem,
                                              AtCommandContext *at,
                                              GString *response);

void gsmd_utils_print_data(const gchar *func,
                           const gchar *prepend,
                           const gchar *data);

time_t gsmd_utils_convert_to_timestamp(const gchar *str);

gboolean gsmd_utils_parse_cms_error(GString *response, gint *code);

gboolean gsmd_utils_parse_cme_error(GString *response, gint *code);

#endif /* GSMD_UTILS_H */
