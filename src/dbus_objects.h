/*
 *  dbus_objects.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#ifndef DBUS_OBJECTS
#define DBUS_OBJECTS

#include <dbus/dbus-glib-bindings.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "modem.h"

#define GSM_SERVICE_NAME "org.freesmartphone.GSM"
#define GSM_SERVICE_PATH "/"


typedef struct _DBusObjects{
        /**
        * Call interface
        */
        struct DBusCallObject *call_object;

        /**
        * SMS interface
        */
        struct DBusSmsObject *sms_object;

        /**
        * SIM interface
        */
        struct DBusSimObject *sim_object;

        /**
        * SIM interface
        */
        struct DBusNetworkObject *network_object;

        /**
        * Pdp interface
        */
        struct DBusPDPObject *pdp_object;

        /**
        * Device interface
        */
        struct DBusDeviceObject *device_object;

        /**
        * Dbus connection
        */
        DBusGConnection *connection;

        /**
        * Dbus proxy
        */
        DBusGProxy *proxy;
} DBusObjects;

/**
* @brief Initializes dbus, creates gobjects to be used with dbus and registers
* them to dbus.
*
* @param modem pointer to modem interface struct
* @return pointer to struct containing dbus connection and gobjects
*/
gpointer gsmd_dbus_initialize(ModemInterface *modem);

/**
* Uninitializes dbus, free's gobjects and deregisters them from the dbus
*
* @param objects pointer to dbus_objects struct to be freed
*/
void gsmd_dbus_uninitialize(DBusObjects *dbus);

#endif
