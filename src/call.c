/*
 *  call.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include <glib.h>

#include "gsmd-error.h"
#include "modem.h"
#include "utils.h"
#include "call_interface.h"
#include "call.h"
#include <stdlib.h>




static
AtCommandHandlerStatus gsmd_call_handler_accept_call (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response);
static
AtCommandHandlerStatus gsmd_call_handler_activate_call (ModemInterface *modem,
                                                        AtCommandContext *at,
                                                        GString *response);
static
AtCommandHandlerStatus gsmd_call_handler_activate_call (ModemInterface *modem,
                                                        AtCommandContext *at,
                                                        GString *response);

static
AtCommandHandlerStatus gsmd_call_handler_join (ModemInterface *modem,
                                               AtCommandContext *at,
                                               GString *response);

static
AtCommandHandlerStatus gsmd_call_handler_release_call (ModemInterface *modem,
                                                       AtCommandContext *at,
                                                       GString *response);
static
AtCommandHandlerStatus gsmd_call_handler_initiate (ModemInterface *modem,
                                                   AtCommandContext *at,
                                                   GString *response);
static
AtCommandHandlerStatus gsmd_call_handler_send_dtmf (ModemInterface *modem,
                                                    AtCommandContext *at,
                                                    GString *response);

static
AtCommandHandlerStatus gsmd_call_handler_query_dtmf_duration (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response);
static
AtCommandHandlerStatus gsmd_call_handler_set_dtmf_duration (ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response);

static
AtCommandHandlerStatus gsmd_call_handler_release_held (ModemInterface *modem,
                                                       AtCommandContext *at,
                                                       GString *response);

static
AtCommandHandlerStatus gsmd_call_handler_release_all (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response);
static
AtCommandHandlerStatus gsmd_call_handler_set_send_identification (ModemInterface *modem,
                                                                  AtCommandContext *at,
                                                                  GString *response);

static
AtCommandHandlerStatus gsmd_call_handler_get_send_identification (ModemInterface *modem,
                                                                  AtCommandContext *at,
                                                                  GString *response);
static const AtCommand
call_commands[]
=
{
//        //Query modem status
//        { MODEM_STATUS_QUERY,           "AT+CPAS\r\n",              5000,
//                TRUE,  0,
//        gsmd_call_handler_query_modem_status, NULL, SIM_READY},

    //Query dtmf duration
    { MODEM_DTMF_QUERY,           "+VTD?\r\n",              100,
        TRUE,  0,
        gsmd_call_handler_query_dtmf_duration, NULL, SIM_READY, NULL},

    //Set dtmf duration
    { MODEM_DTMF_SET,           "+VTD=%s\r\n",              100,
      TRUE,  0,
      gsmd_call_handler_set_dtmf_duration, NULL, SIM_READY, NULL},

    //activates incoming call
    { ACCEPT_CALL,                  "ATA\r\n",                  100,
      FALSE, 0,
      gsmd_call_handler_accept_call, NULL, SIM_READY, NULL},

    //puts all calls on hold and activates the other
    { ACTIVATE_CALL,                  "AT+CHLD=2\r\n",          100,
      FALSE, 0,
      gsmd_call_handler_activate_call, NULL, SIM_READY, NULL},

    //Accepts an incoming call (when ActivateConference was called with
    //no current calls)
    { CONFERENCE_ACCEPT_CALL,                  "ATA\r\n",                  100,
      FALSE, 0,
      gsmd_call_handler_accept_call, NULL, SIM_READY, NULL},

    //Adds call to conference
    { CONFERENCE_ACTIVATE_CALL,                  "AT+CHLD=3\r\n",          100,
      FALSE, 0,
      gsmd_call_handler_activate_call, NULL, SIM_READY, NULL},

    //Join all active and held calls
    { CONFERENCE_JOIN,                  "AT+CHLD=4\r\n",          100,
      FALSE, 0,
      gsmd_call_handler_join, NULL, SIM_READY, NULL},


    //Hang up all calls
    { HANGUP_CALLS,                  "AT+CHUP\r\n",              1000,
      FALSE, 1,
      gsmd_call_handler_release_all, NULL, SIM_READY, NULL},

    //Hang up current active call
    { HANGUP_CURRENT,                  "ATH\r\n",              1000,
      FALSE, 1,
      gsmd_call_handler_release_call, NULL, SIM_READY, NULL},

    //Release call
    { RELEASE_CALL,                  "AT+CHLD=1%s\r\n",              100,
      FALSE, 1,
      gsmd_call_handler_release_call, NULL, SIM_READY, NULL},

    //Release held calls
    { RELEASE_HELD,                  "AT+CHLD=0\r\n",              100,
      FALSE, 1,
      gsmd_call_handler_release_held, NULL, SIM_READY, NULL},


    //Initiate a call
    { INITIATE_CALL,                "ATD%s;\r\n",               15000,
      TRUE, 0,
      gsmd_call_handler_initiate, NULL, SIM_READY, NULL},

    //Send dtmf
    { SEND_DTMF,                    "AT+VTS=%s\r\n",            20000,
      FALSE,  0,
      gsmd_call_handler_send_dtmf, NULL, SIM_READY, NULL},

    //Set if our number should be sent when calling
    { SET_SEND_IDENTIFICATION,    "AT+CLIR=%s\r\n",            18000,
      FALSE,  0,
      gsmd_call_handler_set_send_identification, NULL, SIM_READY, NULL},

    //Get if our number should be sent when calling
    { QUERY_SEND_IDENTIFICATION,  "AT+CLIR?\r\n",            20000,
      FALSE,  0,
      gsmd_call_handler_get_send_identification, NULL, SIM_READY, NULL},

    { 0,                    NULL,                100,    TRUE,  1,
      NULL, NULL, SIM_UNKNOWN, NULL},

}, *call_command_p = call_commands;


static const SymbolTable
call_symbols[] =
{
    { "COPS",       SYMBOL_COPS,    },
    { NULL, 0,                      },
}, *call_symbol_p = call_symbols;

void gsmd_call_init_at_handler(ModemInterface* modem)
{
    while (call_symbol_p->symbol_name)
    {
        g_scanner_add_symbol (modem->scanner, call_symbol_p->symbol_name,
                              GINT_TO_POINTER(call_symbol_p->symbol_token));
        call_symbol_p++;
    }
    while (call_command_p->command)
    {
        gsmd_modem_register_command (modem,call_command_p);
        call_command_p++;
    }
}

/**
 * Below are functios that handle responses from gsm modem
 * when an AT command has been sent.
 *
 * Each AT command has it's own handler function.
 */




/**
 * @brief Handler for dial command
 *
 * @param modem modem whose responses to handle
 * @return TRUE if handler understood the modem's reply
 */
static
AtCommandHandlerStatus gsmd_call_handler_initiate (ModemInterface *modem,
                                                   AtCommandContext *at,
                                                   GString *response)
{
    g_debug("%s", __func__);
    GScanner* scanner = modem->scanner;
    guint symbol = scanner->token;
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    Call *call = NULL;
    gint call_id = 0;

    if (gsmd_utils_send_error_from_response(at,
                                            response,
                                            "UNKNOWN ERROR OCCURRED WHEN INITIATING A CALL",
                                            "TIMEOUT OCCURRED WHEN INITIATING A CALL"))
    {
        gsmd_utils_remove_call(modem,call_id, FALSE);
        return AT_HANDLER_DONE_ERROR;
    }

    gint id = gsmd_utils_table_get_int(at->handler_data,GSMD_CALL_KEY_ID,-1);




    if (id < 0)
    {
        gsmd_utils_send_error(at,
                              GSMD_ERROR_INTERNAL,
                              "INVALID HANDLER DATA");
        g_warning("%s : Invalid handler data, id missing",__func__);
        return AT_HANDLER_ERROR;
    }


    call = gsmd_utils_find_call_id(modem,id);

    if ( !call )
    {
        if (modem->call_ipc->initiate_error)
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_INTERNAL,
                                  "UNABLE TO FIND INITIATED CALL");
        }
        return AT_HANDLER_ERROR;
    }
    call_id = call->id;

    g_assert( at );



    switch (symbol)
    {
    case SYMBOL_OK:
        //the call starts only after getting #ECAM:0,3,1
        call->status = CALL_CALLING;
        if (modem->call_ipc->initiate_reply)
            modem->call_ipc->initiate_reply(modem->call_ipc,
                                            at->ipc_data,
                                            call_id);


        status = AT_HANDLER_DONE;
        break;
    case SYMBOL_NO:
        g_scanner_get_next_token(scanner);
        if (scanner->token == SYMBOL_CARRIER)
        {
            if (modem->call_ipc->initiate_error)
            {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_NETWORK_NOT_PRESENT,
                                      "UNABLE TO INITIATE CALL, NO CARRIER");

            }
            //Remove the call
        }


        gsmd_utils_remove_call(modem,call_id, FALSE);
        status = AT_HANDLER_DONE_ERROR;
        break;
    }


    return status;
}



/**
 * @brief Handler for accept command
 *
 * @param modem modem whose responses to handle
 * @return TRUE if handler understood the modem's reply
 */
static
AtCommandHandlerStatus gsmd_call_handler_accept_call (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response)
{
    GScanner* scanner = modem->scanner;
    GValue *id_v = g_hash_table_lookup(at->handler_data,GSMD_CALL_KEY_ID);
    GValue *conference_v = g_hash_table_lookup(at->handler_data,GSMD_CALL_KEY_CONFERENCE);

    if (!id_v || !conference_v)
    {
        gsmd_utils_send_error(at,
                              GSMD_ERROR_INTERNAL,
                              "INVALID HANDLER DATA");
        g_warning("%s : Invalid handler data. id: %p, conference: %p",
                  __func__,
                  id_v,
                  conference_v);
        return AT_HANDLER_ERROR;
    }

    gint id = g_value_get_int(id_v);
    gboolean conference = g_value_get_boolean(conference_v);

    Call *call = gsmd_utils_find_call_id(modem,id);

    //gsmd_call_command_activate ensures that call exists
    if ( !call )
    {
        if (modem->call_ipc->activate_error)
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_CALL_NOTFOUND,
                                  "UNABLE TO ACCEPT CALL");
        }
        return AT_HANDLER_ERROR;
    }

    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;

    g_debug("%s", __func__);
    if (scanner->token == SYMBOL_OK)
    {
        call->status = CALL_CONNECTED;
        if (!conference && modem->call_ipc->activate_reply)
            modem->call_ipc->activate_reply(modem->call_ipc,
                                            at->ipc_data);

        if (conference && modem->call_ipc->activate_conference_reply)
            modem->call_ipc->activate_conference_reply(modem->call_ipc,
                                                       at->ipc_data);


        gsmd_utils_call_send_status(modem,
                                    call,
                                    "active");
        status = AT_HANDLER_DONE;
    }
    else if (scanner->token == SYMBOL_NO)
    {
        g_scanner_get_next_token (scanner);
        if (scanner->token == SYMBOL_CARRIER)
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_NETWORK_NOT_PRESENT,
                                  "UNABLE TO ACCEPT CALL");

            //remove call
            gsmd_utils_remove_call(modem,call->id, TRUE);
            status = AT_HANDLER_DONE;
        }
    }
    else if (gsmd_utils_send_error_from_response(at,
                                                 response,
                                                 "UNABLE TO ACCEPT CALL",
                                                 "TIMEOUT OCCURRED WHEN ACCEPTING A CALL"))
    {
        //TODO should we remove the call?
        gsmd_utils_remove_call(modem,call->id, TRUE);
        status = AT_HANDLER_DONE_ERROR;
    }

    return status;
}


/**
 * @brief Handler for activate command
 *
 * @param modem modem whose responses to handle
 * @return status of handling response
 */
static
AtCommandHandlerStatus gsmd_call_handler_activate_call (ModemInterface *modem,
                                                        AtCommandContext *at,
                                                        GString *response)
{
    GValue *id_v = g_hash_table_lookup(at->handler_data,GSMD_CALL_KEY_ID);
    GValue *conference_v = g_hash_table_lookup(at->handler_data,GSMD_CALL_KEY_CONFERENCE);

    if (!id_v || !conference_v)
    {
        gsmd_utils_send_error(at,
                              GSMD_ERROR_INTERNAL,
                              "INVALID HANDLER DATA");
        g_warning("%s : Invalid handler data. id: %p, conference: %p",
                  __func__,
                  id_v,
                  conference_v);

        return AT_HANDLER_ERROR;
    }

    gint id = g_value_get_int(id_v);
    gboolean conference = g_value_get_boolean(conference_v);

    GScanner* scanner = modem->scanner;
    Call *call = gsmd_utils_find_call_id(modem,id);

    //gsmd_call_command_activate ensures that call exists
    if ( !call )
    {
        if (modem->call_ipc->activate_error)
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_CALL_NOTFOUND,
                                  "UNABLE TO ACTIVATE CALL");
        }

        return AT_HANDLER_ERROR;
    }

    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;

    g_debug("%s", __func__);
    if (scanner->token == SYMBOL_OK)
    {
        //Phone number hasn't changed so no need to send it
        GHashTable *properties = g_hash_table_new(g_str_hash,
                                                  NULL);

        if (!conference && modem->call_ipc->activate_reply)
            modem->call_ipc->activate_reply(modem->call_ipc,
                                            at->ipc_data);
        if (conference && modem->call_ipc->activate_conference_reply)
            modem->call_ipc->activate_conference_reply(modem->call_ipc,
                                                       at->ipc_data);

        gsmd_utils_call_send_status(modem,
                                    call,
                                    "active");

        //If we are not activting a call for conference then find all
        //connected calls, put them on hold and send signal
        if (!conference)
        {
            GList *list = modem->calls;
            Call *cur_call = NULL;
            while ( list )
            {
                cur_call = (Call*)list->data;
                if (cur_call->status == CALL_CONNECTED)
                {
                    cur_call->status = CALL_HOLD;
                    gsmd_utils_call_send_status(modem,
                                                cur_call,
                                                "held");
                }
                list = g_list_next( list );
            }
        }


        //Finally set our current call's status to connected
        call->status = CALL_CONNECTED;
        status = AT_HANDLER_DONE;
        g_hash_table_destroy(properties);

    }
    else if (gsmd_utils_send_error_from_response(at,
                                                 response,
                                                 "UNABLE TO ACTIVATE CALL",
                                                 "TIMEOUT WHEN ACTIVATING A CALL"))
    {
        status = AT_HANDLER_DONE_ERROR;
    }


    return status;
}



/**
 * @brief Handler for joining active and held calls
 *
 * @param modem modem whose responses to handle
 * @return status of handling response
 */
static
AtCommandHandlerStatus gsmd_call_handler_join (ModemInterface *modem,
                                               AtCommandContext *at,
                                               GString *response)
{
    GScanner* scanner = modem->scanner;


    if (scanner->token == SYMBOL_NO)
    {
        g_scanner_get_next_token (scanner);
        if (scanner->token == SYMBOL_CARRIER)
        {
            //Connected calls are joined to held calls
            //and released
            gsmd_utils_remove_calls_with_status(modem,CALL_CONNECTED,TRUE);
            gsmd_utils_remove_calls_with_status(modem,CALL_HOLD,TRUE);
            return AT_HANDLER_DONE;
        }
    }

    return AT_HANDLER_DONT_UNDERSTAND;
}




/**
 * @brief Handler for ReleaseCall command
 *
 * @param modem modem whose responses to handle
 * @return TRUE if handler understood the modem's reply
 */
static
AtCommandHandlerStatus gsmd_call_handler_release_call (ModemInterface *modem,
                                                       AtCommandContext *at,
                                                       GString *response)
{
    GScanner* scanner = modem->scanner;
    g_debug("%s", __func__);
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;

    GValue *id_v = g_hash_table_lookup(at->handler_data,GSMD_CALL_KEY_ID);
    if (!id_v)
    {
        g_warning("%s : Invalid handler data. id: %p",
                  __func__,
                  id_v);

        gsmd_utils_send_error(at,
                              GSMD_ERROR_INTERNAL,
                              "INVALID HANDLER DATA");
    }

    guint id = g_value_get_int(id_v);
    Call *call = gsmd_utils_find_call_id(modem,id);
    gint call_id = 0;

    //gsmd_call_command_activate ensures that call exists
    if (call)
        call_id = call->id;


    if (scanner->token == SYMBOL_OK)
    {
        if (modem->call_ipc->release_reply)
            modem->call_ipc->release_reply(modem->call_ipc,
                                           at->ipc_data);

        gsmd_utils_remove_call(modem,id, TRUE);
        status = AT_HANDLER_DONE;

    }
    if (scanner->token == SYMBOL_ERROR || scanner->token == SYMBOL_TIME_OUT)
    {
        //TODO should call be removed?
        //gsmd_utils_remove_call(modem,id,TRUE);
        return AT_HANDLER_RETRY;
    }

    return status;
}


/**
 * @brief Handler for ReleaseCall command
 *
 * @param modem modem whose responses to handle
 * @return TRUE if handler understood the modem's reply
 */
static
AtCommandHandlerStatus gsmd_call_handler_release_all (ModemInterface *modem,
                                                      AtCommandContext *at,
                                                      GString *response)
{
    GScanner* scanner = modem->scanner;
    g_debug("%s", __func__);
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;

    if (scanner->token == SYMBOL_OK)
    {
        if (modem->call_ipc->release_all_reply)
            modem->call_ipc->release_all_reply(modem->call_ipc,
                                               at->ipc_data);

        gsmd_utils_remove_calls(modem,TRUE);
        status = AT_HANDLER_DONE;

    }
    if (scanner->token == SYMBOL_ERROR || scanner->token == SYMBOL_TIME_OUT)
    {
        status = AT_HANDLER_RETRY;
    }

    return status;
}

/**
 * @brief Handler for ReleaseCall command
 *
 * @param modem modem whose responses to handle
 * @return TRUE if handler understood the modem's reply
 */
static
AtCommandHandlerStatus gsmd_call_handler_release_held (ModemInterface *modem,
                                                       AtCommandContext *at,
                                                       GString *response)
{
    GScanner* scanner = modem->scanner;
    g_debug("%s", __func__);



    if (scanner->token == SYMBOL_OK)
    {
        if (modem->call_ipc->release_reply)
            modem->call_ipc->release_held_reply(modem->call_ipc,
                                                at->ipc_data);

        gsmd_utils_remove_calls_with_status(modem,CALL_HOLD, TRUE);
        gsmd_utils_remove_calls_with_status(modem,CALL_INCOMING, TRUE);
        return AT_HANDLER_DONE;

    }
    else if (gsmd_utils_send_error_from_response(at,
                                                 response,
                                                 "TIMEOUT WHEN RELEASING HELD CALLS",
                                                 "UNABLE TO RELEASE HELD CALLS"))
    {
        //TODO should calls be removed?
        return AT_HANDLER_DONE_ERROR;
    }

    return AT_HANDLER_DONT_UNDERSTAND;
}




/**
 * @brief Handler for sendDtmf command
 *
 * @param modem modem whose responses to handle
 * @return TRUE if handler understood the modem's reply
 */
static
AtCommandHandlerStatus gsmd_call_handler_send_dtmf (ModemInterface *modem,
                                                    AtCommandContext *at,
                                                    GString *response)
{
    g_debug("%s", __func__);
    if (gsmd_utils_send_error_from_response(at,
                                            response,
                                            "UNABLE TO SEND DTMF",
                                            "TIMEOUT OCCURRED WHEN SENDING DTMF"))
    {

        return AT_HANDLER_DONE_ERROR;
    }

    switch (modem->scanner->token)
    {
    case SYMBOL_OK:
        modem->call_ipc->send_dtmf_reply(modem->call_ipc,
                                         at->ipc_data,
                                         "");
        return AT_HANDLER_DONE;
        break;
    default:
        return AT_HANDLER_DONT_UNDERSTAND;
    }


}



/*****************end of handlers*************************************/


/**
 * Below are functions that create proper AT commands for the gsm modem.
 * Functions add the commands to the modem's command queue along with
 * appropriate response handler (functions listed above).
 *
 */

/**
 * @brief Initiates a voice call
 * @param modem pointer of modem device struct
 * @param ipc_data pointer to ipc data
 * @param number phone number
 * @param type phone number type
 */
static void gsmd_call_command_initiate (CallInterface *call,
                                        gpointer ipc_data,
                                        const char* number,
                                        const char* type)
{
    ModemInterface *modem = (ModemInterface*)call->priv;

    //Call's id is passed as handler data
    //Create a new call to call array
    Call *c = gsmd_utils_new_call(modem,number,type,CALL_IDLE);

    GHashTable *data = gsmd_utils_create_hash_table();
    gsmd_utils_table_insert_int(data,GSMD_CALL_KEY_ID,c->id);
    g_debug("%s : New call created with id %d",__func__,c->id);


    gsmd_modem_post_at_command_id( modem,
                                   INITIATE_CALL,
                                   number,
                                   ipc_data,
                                   (ErrorFunction)modem->call_ipc->initiate_error,
                                   modem->call_ipc,
                                   INTERFACE_CALL,
                                   data);

}


/**
 * @brief Joins all held and active calls and releases them
 * @param modem pointer of modem device struct
 * @param ipc_data pointer to ipc data
 */
static void gsmd_call_command_join (CallInterface *call,
                                    gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)call->priv;

    gsmd_modem_post_at_command_id( modem,
                                   CONFERENCE_JOIN,
                                   NULL,
                                   ipc_data,
                                   (ErrorFunction)modem->call_ipc->join_error,
                                   modem->call_ipc,
                                   INTERFACE_CALL,
                                   NULL);

}

/**
 * @brief Function that creates activate call command.
 *
 * @param modem modem to send the command to
 * @param ipc_data pointer to ipc data
 */
static void gsmd_call_command_activate (CallInterface *call,
                                        gpointer ipc_data,
                                        gint id)
{
    g_debug("%s : id %d",__func__,id);

    ModemInterface *modem = (ModemInterface*)call->priv;
    gsmd_utils_print_calls(modem);


    Call *cur = gsmd_utils_find_call_id(modem,id);
    //Call *cur = gsmd_utils_get_first_call(modem);

    if (!cur)
    {
        gsmd_utils_send_error_full((ErrorFunction)modem->call_ipc->activate_error,
                                   modem->call_ipc,
                                   ipc_data,
                                   GSMD_ERROR_CALL_NOTFOUND,
                                   "UNKNOWN CALL ID");

        g_warning("Activating a call with id %d that isn't known to gsmd2",id);
        g_debug("Current calls:");
        gsmd_utils_print_calls(modem);
        return;
    }

    //Make sure the call we are trying to activate isn't already activated
    if (cur->status == CALL_CONNECTED)
    {
        g_debug("%s: Trying to activate a call that is already active.",__func__);
        modem->call_ipc->activate_reply(modem->call_ipc, ipc_data);
        return;
    }


    int current_calls = gsmd_utils_get_call_status_count(modem,CALL_CONNECTED);



    GHashTable *data = gsmd_utils_create_hash_table();
    gsmd_utils_table_insert_int(data,GSMD_CALL_KEY_ID,cur->id);
    gsmd_utils_table_insert_boolean(data,GSMD_CALL_KEY_CONFERENCE,FALSE);

    if (current_calls == 0)
    {


        //If we have no current calls, send ATA
        gsmd_modem_post_at_command_id( modem,
                                       ACCEPT_CALL,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->call_ipc->activate_error,
                                       (gpointer)modem->call_ipc,
                                       INTERFACE_CALL,
                                       data);
    }
    else
    {
        //Oterwise put all calls on hold and active the new one
        //FIXME this assumes that there is only 1 call on hold
        gsmd_modem_post_at_command_id( modem,
                                       ACTIVATE_CALL,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->call_ipc->activate_error,
                                       (gpointer)modem->call_ipc,
                                       INTERFACE_CALL,
                                       data);
    }


}


/**
 * @brief Function that creates activate conference command.
 *
 * @param modem modem to send the command to
 * @param ipc_data pointer to ipc data
 * @param id id of the call to activate
 */
static void gsmd_call_command_activate_conference (CallInterface *call,
                                                   gpointer ipc_data,
                                                   gint id)
{
    g_debug("%s: id %d",__func__,id);
    ModemInterface *modem = (ModemInterface*)call->priv;
    Call *held_call = gsmd_utils_find_call_id(modem,id);

    //Make sure such a call exists
    if (!held_call)
    {
        gsmd_utils_send_error_full((ErrorFunction)modem->call_ipc->activate_conference_error,
                                   modem->call_ipc,
                                   ipc_data,
                                   GSMD_ERROR_CALL_NOTFOUND,
                                   "UNKNOWN CALL ID");

        g_warning("Activating a call with id %d that isn't known to gsmd2",id);
        g_debug("Current calls:");
        gsmd_utils_print_calls(modem);
        return;
    }

    //Make sure it's held
    if (held_call->status != CALL_HOLD && held_call->status != CALL_INCOMING)
    {
        g_debug("%s: Trying to activate call that has status %s",
                __func__,
                gsmd_utils_call_status_to_string(held_call->status));

        gsmd_utils_send_error_full((ErrorFunction)modem->call_ipc->activate_conference_error,
                                   modem->call_ipc,
                                   ipc_data,
                                   GSMD_ERROR_CALL_INVALIDSTATE,
                                   "SPECIFIED CALL NOT HELD");
        return;
    }

    //Check if we have an ongoing call
    int current_calls = gsmd_utils_get_call_status_count(modem,CALL_CONNECTED);

    GHashTable *data = gsmd_utils_create_hash_table();
    gsmd_utils_table_insert_int(data,GSMD_CALL_KEY_ID,id);
    gsmd_utils_table_insert_boolean(data,GSMD_CALL_KEY_CONFERENCE,TRUE);

    if (current_calls == 0)
    {
        g_debug("%s: Activating a call to conference with no other calls",
                __func__);
        //If we have no current calls, send ATA
        gsmd_modem_post_at_command_id( modem,
                                       CONFERENCE_ACCEPT_CALL,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->call_ipc->activate_conference_error,
                                       (gpointer)modem->call_ipc,
                                       INTERFACE_CALL,
                                       data);
    }
    else
    {
        //Oterwise add all (+chld's restriction) held calls to conference
        g_debug("%s: Activating a call to conference with %d other calls",
                __func__,
                current_calls);

        gsmd_modem_post_at_command_id( modem,
                                       CONFERENCE_ACTIVATE_CALL,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->call_ipc->activate_conference_error,
                                       (gpointer)modem->call_ipc,
                                       INTERFACE_CALL,
                                       data);
    }


}

/**
 * Below are functions that create proper AT commands for the gsm modem.
 * Functions add the commands to the modem's command queue along with
 * appropriate response handler (functions listed above).
 *
 */




/**
 * @brief Function removes call request command from the modem's command
 * queue.
 *
 * @param modem modem whose command to remove
 */
static void gsmd_call_cancel_call_req (ModemInterface *modem)
{
    gsmd_modem_cancel_command_id( modem,
                                  INITIATE_CALL,
                                  INTERFACE_CALL);
}



/**
 * @brief Function that creates hangup call command.
 *
 * @param modem modem to send the command to
 * @param ipc_data pointer ipc data
 * @param message TODO what is this anyway?
 * @param id phone call's id
 */
static void gsmd_call_command_release (CallInterface *call,
                                       gpointer ipc_data,
                                       const gchar *message,
                                       const gint id)
{
    g_debug("%s : id %d",__func__,id);
    //TODO message is ignored
    ModemInterface *modem = (ModemInterface*)call->priv;

    GHashTable *data = gsmd_utils_create_hash_table();

    gsmd_utils_print_calls(modem);



    Call *cur_call = gsmd_utils_find_call_id(modem,id);
    //Call *cur_call = gsmd_utils_get_first_call(modem);

    if (!cur_call)
    {
        g_debug("%s : Call not found, cancelling all initiate call commands",__func__);
        gsmd_call_cancel_call_req (modem);
        modem->call_ipc->release_reply(modem->call_ipc,ipc_data);
        return;
    }

    gsmd_utils_table_insert_int(data,GSMD_CALL_KEY_ID,cur_call->id);



    if (gsmd_utils_get_call_status_count(modem,CALL_CONNECTED) > 1)
    {
        g_debug("%s : releasing a call from conference",__func__);
        GString *param = g_string_new("");
        g_string_printf(param,"%d",id);
        gsmd_modem_post_at_command_id( modem,
                                       RELEASE_CALL,
                                       param->str,
                                       ipc_data,
                                       (ErrorFunction)modem->call_ipc->release_error,
                                       (gpointer)modem->call_ipc,
                                       INTERFACE_CALL,
                                       data);
        g_string_free(param,TRUE);

    }
    else if (cur_call->status == CALL_CONNECTED ||
             cur_call->status == CALL_INCOMING ||
             cur_call->status == CALL_CALLING ||
             cur_call->status == CALL_WAITING)
    {
        g_debug("%s: Hangup from connected state",__func__);

        gsmd_modem_post_at_command_id_and_cancel( modem,
                                                  HANGUP_CURRENT,
                                                  NULL,
                                                  ipc_data,
                                                  (ErrorFunction)modem->call_ipc->release_error,
                                                  (gpointer)modem->call_ipc,
                                                  INTERFACE_CALL,
                                                  data);
        gsmd_call_cancel_call_req (modem);


    }
    else
    {
        //FIXME check if the call initiate command has already been sent
        g_debug("Hangup from non-connected state");
        gsmd_call_cancel_call_req (modem);
    }
}


/**
 * @brief Function that creates hangup all calls command.
 *
 * @param modem modem to send the command to
 * @param ipc_data pointer ipc data
 * @param message TODO what is this anyway?
 */
static void gsmd_call_command_release_all (CallInterface *call,
                                           gpointer ipc_data,
                                           const gchar *message)
{
    ModemInterface *modem = (ModemInterface*)call->priv;
    gsmd_modem_post_at_command_id( modem,
                                   HANGUP_CALLS,
                                   NULL,
                                   ipc_data,
                                   (ErrorFunction)modem->call_ipc->release_all_error,
                                   (gpointer)modem->call_ipc,
                                   INTERFACE_CALL,
                                   NULL);
}


/**
 * @brief Function that creates hangup held calls command.
 *
 * @param modem modem to send the command to
 * @param ipc_data pointer ipc data
 * @param message TODO what is this anyway?
 */
static void gsmd_call_command_release_held (CallInterface *call,
                                            gpointer ipc_data,
                                            const gchar *message)
{
    ModemInterface *modem = (ModemInterface*)call->priv;
    gsmd_modem_post_at_command_id( modem,
                                   RELEASE_HELD,
                                   NULL,
                                   ipc_data,
                                   (ErrorFunction)modem->call_ipc->release_held_error,
                                   (gpointer)modem->call_ipc,
                                   INTERFACE_CALL,
                                   NULL);
}


static CallStatusStruct *gsmd_call_new_call_status_struct(Call *call)
{
    if (!call)
        return NULL;

    CallStatusStruct *str = 0;
    //New call status struct
    str = g_new0(CallStatusStruct,1);

    //Id
    str->id = call->id;

    //Status
    str->status = g_string_new(gsmd_utils_call_status_to_string(call->status));

    //Hash table containing properties
    str->properties = gsmd_utils_create_hash_table();
    gsmd_utils_table_insert_string(str->properties,
                                   GSMD_CALL_KEY_CALL_STATUS_NUMBER,
                                   call->number->str);
    return str;
}

/**
 * @brief Lists all calls
 *
 * @param modem modem to send the command to
 * @param ipc_data pointer ipc data
 */
static void gsmd_call_command_list_calls (CallInterface *call,
                                          gpointer ipc_data)
{

    ModemInterface *modem = (ModemInterface*)call->priv;
    GArray *calls = g_array_new(FALSE,TRUE,sizeof(CallStatusStruct));

    GList *list = modem->calls;
    Call *call_it = NULL;
    CallStatusStruct *tmp = 0;
    guint i=0;


    while ( list )
    {
        call_it = (Call*)list->data;
        tmp = gsmd_call_new_call_status_struct(call_it);
        g_array_append_val(calls,tmp);
        list = g_list_next( list );
    }



    modem->call_ipc->list_calls_reply(modem->call_ipc,
                                      ipc_data,
                                      calls);

    //Clear the list
    for (i=0;i<calls->len;i++)
    {
        tmp = g_array_index(calls,CallStatusStruct*,i);
        if (tmp)
        {
            g_string_free(tmp->status,TRUE);
            g_hash_table_destroy(tmp->properties);
        }
    }
    g_array_free(calls,TRUE);
}

/**
 * @brief Handler for get send identification
 *
 * @param modem modem whose responses to handle
 * @return TRUE if handler understood the modem's reply
 */
static
AtCommandHandlerStatus gsmd_call_handler_get_send_identification (ModemInterface *modem,
                                                                  AtCommandContext *at,
                                                                  GString *response)
{

    /*
    +CLIR: 0,4

    OK
    */
    AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
    GError **error = NULL;
    GRegex *regex = g_regex_new ("\\+CLIR:\\s(?<mobile>\\d),(?<network>\\d)|(?<error>ERROR)|(?<ok>OK)",
                                 0,
                                 0,
                                 error);

    GMatchInfo *match_info;


    if (g_regex_match (regex, response->str, 0, &match_info))
    {
        if (gsmd_utils_match_exists(match_info,"mobile"))
        {
            gchar *match = g_match_info_fetch_named(match_info,"mobile");
            gint clir = atoi(match);
            g_free(match);

            /*
            Telit's manual on AT+CLIR?
            mobile values:

            0 - CLIR facility according to CLIR service network status
            1 - CLIR facility active (CLI not sent)
            2 - CLIR facility not active (CLI sent)

            network values:
            0 - CLIR service not provisioned
            1 - CLIR service provisioned permanently
            2 - unknown (e.g. no network present, etc.)
            3 - CLI temporary mode presentation restricted
            4 - CLI temporary mode presentation allowed

            mobile 1 -> send_identification is true
            mobile 2 -> send_identification is true
            mobile 0 -> check network
                network 0/3 -> send_identification is false
                network 1/2/4 -> send_identification is true
            */

            if (clir == 1)
            {
                gsmd_utils_table_insert_boolean(at->handler_data,
                                                GSMD_CALL_KEY_SEND_IDENTIFICATION,
                                                TRUE);
            }
            else if (clir == 2)
            {
                gsmd_utils_table_insert_boolean(at->handler_data,
                                                GSMD_CALL_KEY_SEND_IDENTIFICATION,
                                                FALSE);
            }
            else if (clir == 0)
            {
                match = g_match_info_fetch_named(match_info,"network");
                gint nclir = atoi(match);
                g_free(match);

                if (nclir == 0 || nclir == 3)
                {
                    gsmd_utils_table_insert_boolean(at->handler_data,
                                                    GSMD_CALL_KEY_SEND_IDENTIFICATION,
                                                    FALSE);
                }
                else
                {

                    gsmd_utils_table_insert_boolean(at->handler_data,
                                                    GSMD_CALL_KEY_SEND_IDENTIFICATION,
                                                    FALSE);
                }
            }



            status = AT_HANDLER_NEED_MORE;
        }

        if (gsmd_utils_match_exists(match_info,"error"))
        {
            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "FAILED TO GET DEFAULT PHONEBOOK STORAGE SPACE");
            status = AT_HANDLER_DONE_ERROR;
        }

        if (gsmd_utils_match_exists(match_info,"ok"))
        {
            GValue *val = g_hash_table_lookup(at->handler_data,
                                              GSMD_CALL_KEY_SEND_IDENTIFICATION);
            if (!val)
            {
                g_warning("%s : Invalid handler data. default storage space: %p",
                          __func__,
                          val);

                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "OK WITHOUT RESPONSE");
            }
            modem->call_ipc->get_send_identification_reply(modem->call_ipc,
                                                           at->ipc_data,
                                                           g_value_get_boolean(val));

            gsmd_utils_table_insert_copy(modem->caches[INTERFACE_CALL],
                                         GSMD_CALL_KEY_SEND_IDENTIFICATION,
                                         val);

            status = AT_HANDLER_DONE;
        }

    }

    g_free(error);
    g_match_info_free (match_info);
    g_regex_unref (regex);

    return status;
}

/**
 * @brief Handler for set send identification
 *
 * @param modem modem whose responses to handle
 * @return TRUE if handler understood the modem's reply
 */
static
AtCommandHandlerStatus gsmd_call_handler_set_send_identification (ModemInterface *modem,
                                                                  AtCommandContext *at,
                                                                  GString *response)
{
    g_debug("%s", __func__);
    if (gsmd_utils_send_error_from_response(at,
                                            response,
                                            "UNKNOWN ERROR OCCURRED WHEN SETTING SEND IDENTIFICATION",
                                            "TIMEOUT OCCURRED WHEN SETTING SEND IDENTIFICATION"))
    {

        return AT_HANDLER_DONE_ERROR;
    }

    switch (modem->scanner->token)
    {
    case SYMBOL_OK:
        modem->call_ipc->set_send_identification_reply(modem->call_ipc,
                                                       at->ipc_data);
        return AT_HANDLER_DONE;
        break;
    default:
        return AT_HANDLER_DONT_UNDERSTAND;
        break;
    }
}

/**
 * @brief Get send identification
 *
 * @param modem modem to send the command to
 * @param ipc_data pointer ipc data
 */
void gsmd_call_command_get_send_identification (CallInterface *call,
                                                gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)call->priv;
    GValue *val = g_hash_table_lookup(modem->caches[INTERFACE_CALL],
                                      GSMD_CALL_KEY_SEND_IDENTIFICATION);

    if (!modem->no_cache && val)
    {
        modem->call_ipc->get_send_identification_reply(modem->call_ipc,
                                                       ipc_data,
                                                       g_value_get_boolean(val));
    }
    else
    {
        gsmd_modem_post_at_command_id( modem,
                                       QUERY_SEND_IDENTIFICATION,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->call_ipc->get_send_identification_error,
                                       (gpointer)modem->call_ipc,
                                       INTERFACE_CALL,
                                       NULL);
    }
}

/**
 * @brief set send identification
 *
 * @param modem modem to send the command to
 * @param ipc_data pointer ipc data
 * @param mode ??
 */
void gsmd_call_command_set_send_identification (CallInterface *call,
                                                gpointer ipc_data,
                                                gboolean mode)
{
    ModemInterface *modem = (ModemInterface*)call->priv;
    GString *param = NULL;

    if (mode)
        param = g_string_new("1");
    else
        param = g_string_new("2");

    gsmd_modem_post_at_command_id( modem,
                                   SET_SEND_IDENTIFICATION,
                                   param->str,
                                   ipc_data,
                                   (ErrorFunction)modem->call_ipc->set_send_identification_error,
                                   (gpointer)modem->call_ipc,
                                   INTERFACE_CALL,
                                   NULL);
    g_string_free(param,TRUE);
}


/**
 * @brief Function that creates send dtmf command.
 *
 * @param modem modem to send the command to
 * @param str dtmf value
 */
void gsmd_call_command_send_dtmf (CallInterface *call,
                                  gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)call->priv;
    gsmd_modem_post_at_command_id( modem,
                                   SEND_DTMF,
                                   NULL,
                                   ipc_data,
                                   (ErrorFunction)modem->call_ipc->send_dtmf_error,
                                   (gpointer)modem->call_ipc,
                                   INTERFACE_CALL,
                                   NULL);
}



///**
// * @brief Handler to modems response from queryModemStatus command
// *
// * @param modem modem whose response to handle
// * @return true if responses were recognized
// */
//static
//AtCommandHandlerStatus gsmd_call_handler_query_modem_status (ModemInterface *modem,
//                                                             AtCommandContext *at,
//                                                             GString *response)
//{
//        //for queryModemStatus
//        //+CPAS:1
//
//        guint symbol;
//        GValue *val = NULL;
//        AtCommandHandlerStatus at_status = AT_HANDLER_DONT_UNDERSTAND;
//
//        GScanner* scanner = modem->scanner;
//
//        //Check if we have an error
//        if (gsmd_utils_send_error_from_response(at,
//                                                scanner,
//                                                "UNABLE TO QUERY MODEM STATUS",
//                                                "TIMEOUT OCCURRED WHEN QUERYING MODEM STATUS"))
//                return AT_HANDLER_DONE_ERROR;
//
//
//
//        //Check if we have ok, then return the result
//        if (scanner->token == SYMBOL_OK) {
//                val = g_hash_table_lookup(at->handler_data,GSMD_CALL_KEY_MODEM_STATUS);
//                if (!val) {
//                        gsmd_utils_send_error(at,
//                                              GSMD_ERROR_UNKNOWN,
//                                              "COMMAND RETURNED OK WITHOUT VALUES");
//                        return AT_HANDLER_DONE_ERROR;
//                }
//
//                if (modem->call_ipc->get_status_reply)
//                        modem->call_ipc->get_status_reply(modem->call_ipc,
//                                                          at->ipc_data,
//                                                          g_value_get_string(val));
//                return AT_HANDLER_DONE;
//        }
//
//
//        //Check for the data
//        if (scanner->token != '+') {
//                return AT_HANDLER_DONT_UNDERSTAND;
//        }
//        g_scanner_get_next_token (scanner);
//        symbol = scanner->token;
//        switch (symbol) {
//        case  SYMBOL_CPAS:
//                g_scanner_peek_next_token (scanner);
//                if (scanner->next_token == ':') {
//                        g_scanner_get_next_token(scanner);
//                        g_scanner_get_next_token(scanner);
//
//                        //Cache data
//                        gsmd_utils_table_insert_string(at->handler_data,
//                                                       GSMD_CALL_KEY_MODEM_STATUS,
//                                                       scanner->value.v_string);
//                        //TODO check that the status sent here is what is expected in xml
//
//                        at_status = AT_HANDLER_NEED_MORE;
//                } else {
//                        g_scanner_unexp_token (scanner,
//                                               ':',
//                                               NULL,
//                                               "symbol",
//                                               NULL,
//                                               NULL,
//                                               TRUE);
//                }
//                break;
//        default:
//                at_status = AT_HANDLER_DONT_UNDERSTAND;
//                break;
//        }
//        return at_status;
//}
//
//
//
///**
// * @brief AtCommand to query modem status
// *
// * @param modem modem whose response to handle
// */
//__attribute__ ((unused))
//static void gsmd_call_command_query_modem_status (CallInterface *call,
//                                                  gpointer ipc_data,
//                                                  const gint index)
//{
//        ModemInterface *modem = (ModemInterface*)call->priv;
//        //TODO this is not the command freesmartphone's specifications intended
//        //also index is ignored
//        gsmd_modem_post_at_command_id( modem,
//                                       MODEM_STATUS_QUERY,
//                                       NULL,
//                                       ipc_data,
//                                       NULL, NULL,
//                                       INTERFACE_CALL,
//                                       NULL);
//}


static
AtCommandHandlerStatus gsmd_call_handler_query_dtmf_duration (ModemInterface *modem,
                                                              AtCommandContext *at,
                                                              GString *response)
{
    GScanner* scanner = modem->scanner;
    if (gsmd_utils_send_error_from_response(at,
                                            response,
                                            "UNKNOWN ERROR OCCURRED WHEN QUERYING DTMF DURATION",
                                            "TIMEOUT OCCURRED WHEN QUERYING DTMF DURATION"))
        return AT_HANDLER_DONE_ERROR;

    if (scanner->token == SYMBOL_OK)
    {
        GValue *val = g_hash_table_lookup(at->handler_data,GSMD_CALL_KEY_DTMF_DURATION);
        if (!val)
        {
            g_warning("%s : Invalid handler data. id: %p",
                      __func__,
                      val);

            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "OK WITHOUT RESPONSE");
            return AT_HANDLER_DONE_ERROR;
        }

        modem->call_ipc->get_dtmf_duration_reply(modem->call_ipc,
                                                 at->ipc_data,
                                                 g_value_get_int(val));

        return AT_HANDLER_DONE;
    }

    if (response->len > 0)
    {
        gint i=0;
        for (i=0;i<response->len;i++)
        {
            if (!g_ascii_isdigit(response->str[i]))
                return AT_HANDLER_DONT_UNDERSTAND;
        }

        gsmd_utils_table_insert_int(at->handler_data,
                                    GSMD_CALL_KEY_DTMF_DURATION,
                                    atoi(response->str));

        return AT_HANDLER_NEED_MORE;
    }

    return AT_HANDLER_DONT_UNDERSTAND;
}

void gsmd_call_command_get_dtmf_duration(CallInterface *call,
                                         gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)call->priv;
    GValue *val = g_hash_table_lookup(modem->caches[INTERFACE_CALL],GSMD_CALL_KEY_DTMF_DURATION);
    if (modem->no_cache && val)
    {
        modem->call_ipc->get_dtmf_duration_reply(modem->call_ipc,
                                                 ipc_data,
                                                 g_value_get_int(val));
    }
    else
    {
        gsmd_modem_post_at_command_id( modem,
                                       MODEM_DTMF_QUERY,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->call_ipc->get_dtmf_duration_error,
                                       modem->call_ipc,
                                       INTERFACE_CALL,
                                       NULL);


    }


}


static
AtCommandHandlerStatus gsmd_call_handler_set_dtmf_duration (ModemInterface *modem,
                                                            AtCommandContext *at,
                                                            GString *response)
{
    GScanner* scanner = modem->scanner;
    if (gsmd_utils_send_error_from_response(at,
                                            response,
                                            "UNKNOWN ERROR OCCURRED WHEN SETTING DTMF DURATION",
                                            "TIMEOUT OCCURRED WHEN SETTING DTMF DURATION"))
        return AT_HANDLER_DONE_ERROR;

    if (scanner->token == SYMBOL_OK)
    {
        GValue *val = g_hash_table_lookup(at->handler_data,GSMD_CALL_KEY_DTMF_DURATION);
        if (!val)
        {
            g_warning("%s : Invalid handler data. id: %p",
                      __func__,
                      val);

            gsmd_utils_send_error(at,
                                  GSMD_ERROR_UNKNOWN,
                                  "OK WITHOUT RESPONSE");
        }

        modem->call_ipc->set_dtmf_duration_reply(modem->call_ipc,
                                                 at->ipc_data);
        gsmd_utils_table_insert_copy(modem->caches[INTERFACE_CALL],
                                     GSMD_CALL_KEY_DTMF_DURATION,
                                     val);

        return AT_HANDLER_DONE;
    }

    return AT_HANDLER_DONT_UNDERSTAND;
}

void gsmd_call_command_set_dtmf_duration(CallInterface *call,
                                         gpointer ipc_data,
                                         const gint mode)
{
    ModemInterface *modem = (ModemInterface*)call->priv;
    GString *param = g_string_new("");
    g_string_printf(param,"%d",mode);

    GHashTable *data = gsmd_utils_create_hash_table();
    gsmd_utils_table_insert_int(data,GSMD_CALL_KEY_DTMF_DURATION,mode);
    gsmd_modem_post_at_command_id( modem,
                                   MODEM_DTMF_SET,
                                   param->str,
                                   ipc_data,
                                   (ErrorFunction)modem->call_ipc->set_dtmf_duration_error,
                                   modem->call_ipc,
                                   INTERFACE_CALL,
                                   data);
    g_string_free(param,TRUE);
}

/*******************Unimplemented method stubs**********************/
void gsmd_call_command_emergency(CallInterface *call, gpointer ipc_data)
{
    ModemInterface *modem = (ModemInterface*)call->priv;
    modem->call_ipc->emergency_reply(modem->call_ipc,ipc_data,"112");
}







/*****************end of command functions*************************************/


/**
 * @brief Initialize the voice call interface and bind the function pointer
 *
 * @param modem modem pointer
 */
void gsmd_call_init(ModemInterface *modem)
{
    gsmd_call_init_at_handler(modem);
    modem->call->priv = (gpointer) modem;
    modem->call->initiate = &gsmd_call_command_initiate;
    modem->call->activate = &gsmd_call_command_activate;
    modem->call->activate_conference = &gsmd_call_command_activate_conference;
    modem->call->release = &gsmd_call_command_release;
    modem->call->release_all = &gsmd_call_command_release_all;
    modem->call->release_held = &gsmd_call_command_release_held;
    modem->call->list_calls = &gsmd_call_command_list_calls;
    modem->call->send_dtmf = &gsmd_call_command_send_dtmf;
    modem->call->emergency = &gsmd_call_command_emergency;
    modem->call->join = &gsmd_call_command_join;

    /*         modem->call->set_dtmf_duration = &gsmd_call_command_set_dtmf_duration; */
    /*         modem->call->get_dtmf_duration = &gsmd_call_command_get_dtmf_duration; */
    /*         modem->call->get_send_identification = &gsmd_call_command_get_send_identification; */
    /*         modem->call->set_send_identification = &gsmd_call_command_set_send_identification; */

}
