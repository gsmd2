/*
 *  device_interface.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef DEVICE_INTERFACE
#define DEVICE_INTERFACE

/**
* @brief Device interface
*/
typedef struct _DeviceInterface DeviceInterface;

struct _DeviceInterface {

        gpointer priv;
        /*TODO these are not in freesmartphone's api
        They should be defined there or placed elsewhere*/
        void (*get_date_time) (DeviceInterface *device, gpointer ipc_data);
        void (*set_date_time) (DeviceInterface *device, gpointer ipc_data,const char* date_time);

        /**
        * @brief Gets information on gsm device
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*get_info) (DeviceInterface *device,
                          gpointer ipc_data);

        /**
        * @brief ??
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param antenna_power ??
        */
        void (*set_antenna_power) (DeviceInterface *device,
                                   gpointer ipc_data,
                                   gboolean antenna_power);

        /**
        * @brief ??
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param antenna_power ??
        */
        void (*get_antenna_power) (DeviceInterface *device,
                                   gpointer ipc_data);

        /**
        * @brief Gets features?
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*get_features) (DeviceInterface *device,
                              gpointer ipc_data);
        /**
        * @brief Recover from suspend mode
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*recover_from_suspend) (DeviceInterface *device,
                                      gpointer ipc_data);
        /**
        * @brief Prepare to suspend mode
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*prepare_to_suspend) (DeviceInterface *device,
                                    gpointer ipc_data);


};

typedef struct _DeviceIPCInterface DeviceIPCInterface;

struct _DeviceIPCInterface {

        gpointer priv;

        /*********************Replies to methods*****************/

        /**
        * @brief Reply to "get service bearer" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param info hash table containing device information
        */
        void (*get_info_reply) (DeviceIPCInterface *device_ipc,
                                gpointer ipc_data,
                                GHashTable *info);

        /**
        * @brief Reply to "set antenna power" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*set_antenna_power_reply) (DeviceIPCInterface *device_ipc,
                                         gpointer ipc_data);

        /**
        * @brief Reply to "get antenna power" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*get_antenna_power_reply) (DeviceIPCInterface *device_ipc,
                                         gpointer ipc_data,
                                         gboolean antenna_power);

        /**
        * @brief Reply to "get features" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param features ??
        */
        void (*get_features_reply) (DeviceIPCInterface *device_ipc,
                                    gpointer ipc_data,
                                    GHashTable *features);
        /**
        * @brief Reply to prepare_to_suspend method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*prepare_to_suspend_reply) (DeviceIPCInterface *device_ipc,
                                          gpointer ipc_data);

        /**
        * @brief Reply to recover_from_suspend method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        */
        void (*recover_from_suspend_reply) (DeviceIPCInterface *device_ipc,
                                            gpointer ipc_data);

        /*********************Method errors**********************************/

        /**
        * @brief Error reply to "get info" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param error pointer to GError describing error
        */
        void (*get_info_error) (DeviceIPCInterface *device_ipc,
                                gpointer ipc_data,
                                GError *error);

        /**
        * @brief Error reply to "set antenna power" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param error pointer to GError describing error
        */
        void (*set_antenna_power_error) (DeviceIPCInterface *device_ipc,
                                         gpointer ipc_data,
                                         GError *error);

        /**
        * @brief Error reply to "get antenna power" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param error pointer to GError describing error
        */
        void (*get_antenna_power_error) (DeviceIPCInterface *device_ipc,
                                         gpointer ipc_data,
                                         GError *error);

        /**
        * @brief Error reply to "get features" method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param error pointer to GError describing error
        */
        void (*get_features_error) (DeviceIPCInterface *device_ipc,
                                    gpointer ipc_data,
                                    GError *error);

        /**
        * @brief Error reply to recover_from_suspend method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param error pointer to GError describing error
        */
        void (*recover_from_suspend_error) (DeviceIPCInterface *device_ipc,
                                            gpointer ipc_data,
                                            GError *error);
        /**
        * @brief Error reply to prepare_to_suspend method call
        *
        * @param modem pointer to modem struct
        * @param ipc_data ipc data
        * @param error pointer to GError describing error
        */
        void (*prepare_to_suspend_error) (DeviceIPCInterface *device_ipc,
                                          gpointer ipc_data,
                                          GError *error);

};

#endif


