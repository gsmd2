/*
 *  modem_internal.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef GSMD_MODEM_INTERNAL_H
#define GSMD_MODEM_INTERNAL_H

#include <glib.h>
#include "serial.h"
#include "call_interface.h"
#include "sim_interface.h"
#include "sms_interface.h"
#include "pdp_interface.h"
#include "network_interface.h"
#include "device_interface.h"


#include "modem.h"

typedef struct _Modem Modem;

gboolean gsmd_modem_assign_device_indices(ModemInterface *modem, int *indices);

ModemInterface* gsmd_modem_open (gchar **devices,
                                 const gchar *vendor,
                                 const gchar* serial_plugin,
                                 gboolean no_cache,
                                 GKeyFile *conf,
                                 IPCInit ipc_init,
                                 int *interface_devices);

void gsmd_modem_free (ModemInterface* modem);


AtCommandContext* gsmd_modem_get_current_at_command(ModemInterface *modem,
                                                    guint device_index);

void gsmd_modem_send_command_force_next (Modem *modem, AtCommandContext *at,
                                         guint device_index);

gboolean gsmd_modem_add_command ( ModemInterface* modem, AtCommandContext *at);




/* gboolean gsmd_modem_send_command ( Modem* modem, AtCommandContext *atcmd ); */



gboolean gsmd_modem_check_end_token (GScanner* scanner);


GString* gsmd_modem_wrapper_command (const gchar* str_cmd);




void gsmd_at_command_context_free (AtCommandContext* at);

void gsmd_modem_retry_current_command (Modem* modem, guint device_index);




void gsmd_modem_data_in (GString *buffer, gpointer data );

guint gsmd_modem_get_interface_device_index(ModemInterface *modem,
                                          InterfaceType interface);

#endif /* GSMD_MODEM_INTERNAL_H*/
