/*
 *  dbus_objects.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include "gsmd-error.h"
#include "dbus_objects.h"
#include "dbus_call_object.h"
#include "dbus_sms_object.h"
#include "dbus_sim_object.h"
#include "dbus_network_object.h"
#include "dbus_pdp_object.h"
#include "dbus_device_object.h"


gpointer gsmd_dbus_initialize(ModemInterface *modem)
{
        GError *error = NULL;
        guint request_ret;
        g_assert(modem);
        DBusObjects *dbus = g_try_new0 (DBusObjects, 1);
        if (!dbus)
                return NULL;

        dbus->connection = dbus_g_bus_get (DBUS_BUS_SYSTEM, &error);

        if (!dbus->connection) {
                g_printerr ("Failed to open connection to bus: %s\n",
                            error->message);
                g_error_free (error);
                gsmd_dbus_uninitialize(dbus);
                return NULL;
        }

        dbus->call_object = gsmd_dbus_call_initialize(modem->call_ipc,modem->call);
        dbus->sms_object = gsmd_dbus_sms_initialize(modem->sms_ipc,modem->sms);
        dbus->sim_object = gsmd_dbus_sim_initialize(modem->sim_ipc, modem->sim);
        dbus->network_object = gsmd_dbus_network_initialize(modem->network_ipc, modem->network);
        dbus->pdp_object = gsmd_dbus_pdp_initialize(modem->pdp_ipc,modem->pdp);
        dbus->device_object = gsmd_dbus_device_initialize(modem->device_ipc, modem->device);

        if (!dbus->sms_object ||
            !dbus->call_object ||
            !dbus->sim_object ||
            !dbus->network_object ||
            !dbus->pdp_object ||
            !dbus->device_object) {
                g_error("Failed to initialize dbus objects!\n");
                gsmd_dbus_uninitialize(dbus);
                return NULL;
        }


        // The proxy is needed to register our object as a service
        dbus->proxy = dbus_g_proxy_new_for_name (dbus->connection,
                                                 DBUS_SERVICE_DBUS,
                                                 DBUS_PATH_DBUS,
                                                 DBUS_INTERFACE_DBUS);

        if (!dbus->proxy) {
                g_printerr ("Failed to create proxy: %s\n", error->message);
                g_error_free (error);
                gsmd_dbus_uninitialize(dbus);
                return NULL;
        }


        //Register each object to the dbus
        dbus_g_connection_register_g_object (dbus->connection,
                                             "/org/freesmartphone/GSM/SMS",
                                             G_OBJECT(dbus->sms_object));
        dbus_g_connection_register_g_object (dbus->connection,
                                             "/org/freesmartphone/GSM/SIM",
                                             G_OBJECT(dbus->sim_object));
        dbus_g_connection_register_g_object (dbus->connection,
                                             "/org/freesmartphone/GSM/Call",
                                             G_OBJECT(dbus->call_object));
        dbus_g_connection_register_g_object (dbus->connection,
                                             "/org/freesmartphone/GSM/Network",
                                             G_OBJECT(dbus->network_object));
        dbus_g_connection_register_g_object (dbus->connection,
                                             "/org/freesmartphone/GSM/PDP",
                                             G_OBJECT(dbus->pdp_object));
        dbus_g_connection_register_g_object (dbus->connection,
                                             "/org/freesmartphone/GSM/Device",
                                             G_OBJECT(dbus->device_object));


        // Make our service available to the dbus daemon
        if (!org_freedesktop_DBus_request_name (dbus->proxy,
                                                GSM_SERVICE_NAME,
                                                DBUS_NAME_FLAG_REPLACE_EXISTING
                                                | DBUS_NAME_FLAG_ALLOW_REPLACEMENT,
                                                &request_ret,
                                                &error)) {
                g_printerr("Failed to register service");
                g_error_free(error);
                gsmd_dbus_uninitialize(dbus);
                return NULL;
        }

        return dbus;
}

void gsmd_dbus_uninitialize(DBusObjects *dbus)
{
        g_assert(dbus);
        g_assert(dbus->proxy);
        GError *error = NULL;
        guint request_ret;

        //Release service name from dbus
        if (!org_freedesktop_DBus_release_name (dbus->proxy,GSM_SERVICE_NAME,
                                                &request_ret, &error)) {
                g_error("%s: release failed: %s",__func__, error->message);
        }

        gsmd_dbus_call_uninitialize(dbus->call_object);
        gsmd_dbus_sms_uninitialize(dbus->sms_object);
        gsmd_dbus_sim_uninitialize(dbus->sim_object);
        gsmd_dbus_network_uninitialize(dbus->network_object);
        gsmd_dbus_pdp_uninitialize(dbus->pdp_object);
        gsmd_dbus_device_uninitialize(dbus->device_object);


        if (dbus->proxy)
                g_object_unref (dbus->proxy);

        g_free(dbus);
}
