/*
 *  sim.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include "sim_interface.h"
#include "at_error.h"

#include "sms_impl.h"



#define GSMD_SIM_KEY_MESSAGE_BOOK "message_book"
#define GSMD_SIM_KEY_MESSAGE_BOOK_TYPE "message_book_type"
#define GSMD_SIM_KEY_PDU_MULTIPART "pdu_multipart"
#define GSMD_SIM_KEY_PDU_MULTIPART_LISTING "pdu_multipart_listing"
#define GSMD_SIM_KEY_PDU_MULTIPART_LIST "pdu_multipart_list"
#define GSMD_SIM_KEY_PDU_MULTIPART_REF "pdu_multipart_ref"
#define GSMD_SIM_KEY_PDU_READY "pdu_ready"
#define GSMD_SIM_KEY_IMSI "imsi"
#define GSMD_SIM_KEY_SUBSCRIBER_NUMBER "subscriber_number"
#define GSMD_SIM_KEY_DIAL_CODE "dial_code"
#define GSMD_SIM_KEY_AUTH_STATUS "auth_status"
#define GSMD_SIM_KEY_AUTH_MESSAGE "auth_message"
#define GSMD_SIM_KEY_SERVICE_CENTER "service_center"
#define GSMD_SIM_KEY_MESSAGE "message"
//#define GSMD_SIM_KEY_TIMESTAMP "timestamp"
#define GSMD_SIM_KEY_NUMBER "number"
#define GSMD_SIM_KEY_CMSS_ID "cmss_id"
#define GSMD_SIM_KEY_PHONEBOOK_SLOTS "slots"
#define GSMD_SIM_KEY_PHONEBOOK_SLOTS_USED "used"
#define GSMD_SIM_KEY_PHONEBOOK_NUMBER_LEN "number_length"
#define GSMD_SIM_KEY_PHONEBOOK_NAME_LEN "name_length"

/**
* Sms data
*/
typedef struct {
        /**
        * @brief Multipart index
        */
        guchar multipart_index;

        /**
        * @brief Multipart count
        */
        guchar multipart_count;

        /**
        * @brief Multipart reference id
        */
        guchar multipart_ref;

        /**
        * @brief Message
        */
        gchar *message;

} multipart_sms;

typedef struct {

        /**
        * @brief Message's status
        */
        GString *status;

        /**
        * @brief Message sender's/receiver's number
        */
        GString *number;

        /**
        * @brief Message's contents
        */
        GString *content;

        /**
        * @brief Message's index
        */
        gint index;

} messagebook_msg;

typedef struct {

        /**
        * @brief Contact's name
        */
        GString *name;

        /**
        * @brief Contact's phonenumber
        */
        GString *number;

        /**
        * @brief Contact's index
        */
        gint index;

} phonebook_entry;

typedef struct {

        /**
        * @brief Home zone's name
        */
        GString *name;

        /**
        * @brief X value of the zone center in Gauss-Krueger format
        */
        gint x;

        /**
        * @brief Y value of the zone center in Gauss-Krueger format
        */
        gint y;

        /**
        * @brief the R*R value of the zone dimension in meters
        */
        gint r;

} homezone_entry;

void gsmd_sim_init(ModemInterface *modem);
void gsmd_sim_deinitialize(ModemInterface *modem);


//TODO Remove these two functions
void gsmd_sim_change_auth_status( ModemInterface *modem,
                                  gpointer ipc_data,
                                  SIMStatus status,
                                  const gchar *message,
                                  gboolean send_reply);


