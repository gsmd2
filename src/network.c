/*
 *  network.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include <string.h>

#include "network.h"

#include "utils.h"
#include "gsmd-error.h"

static const AtCommand
network_commands[]
= {
        //Network registration
        { NETWORK_REG_QUERY,            "AT+CREG?\r\n",             5000,
                TRUE,  0,
                gsmd_network_handler_query_network_registration, NULL, SIM_UNKNOWN,
                NULL},

        //Network registration
        { NETWORK_REGISTER,            "AT+CREG=1\r\n",             5000,
          TRUE,  0,
          gsmd_utils_handler_ok, NULL, SIM_UNKNOWN, NULL},

        //Set operator
        { SET_OPERATOR,                 "AT+COPS=0,0\r\n",          180000,
          TRUE,  0,
          gsmd_network_handler_set_operator, NULL, SIM_READY, NULL},

        //Set operator locked
        { SET_OPERATOR_LOCKED,          "AT+COPS=1,0,%s\r\n",       180000,
          TRUE,  0,
          gsmd_network_handler_set_operator, NULL, SIM_READY, NULL},

        //Query current operator
        { CUR_OP_QUERY,                 "AT+COPS?\r\n",             180000,
          TRUE,  0,
          gsmd_network_handler_query_current_operator, NULL, SIM_READY, NULL},

        //Query available operators
        { OPS_QUERY,                    "AT+COPS=?\r\n",            180000,
          TRUE,  0,
          gsmd_network_handler_query_available_operators, NULL, SIM_READY, NULL},

        //Query signal strength
        { SIGNAL_QUERY,                 "AT+CSQ\r\n",               5000,
          TRUE,  0,
          gsmd_network_handler_query_signal_strength, NULL, SIM_READY, NULL},

        { 0,                    NULL,                100,    TRUE,  1,
          NULL, NULL, SIM_UNKNOWN, NULL },

}, *network_command_p = network_commands;

static const SymbolTable
network_symbols[] = {
        { "CREG",       SYMBOL_CREG,    },
        { "COPS",       SYMBOL_COPS,    },
        { NULL, 0,                      },
}, *network_symbol_p = network_symbols;
void gsmd_network_init_at_handler(ModemInterface* modem)
{
        while (network_symbol_p->symbol_name) {
                g_scanner_add_symbol (modem->scanner, network_symbol_p->symbol_name,
                                      GINT_TO_POINTER(network_symbol_p->symbol_token));
                network_symbol_p++;
        }
        while (network_command_p->command) {
                gsmd_modem_register_command (modem,network_command_p);
                network_command_p++;
        }
}
/**
 * @brief AtCommand to set operator
 *
 * @param modem modem whose response to handle
 * @param operator operator identifier
 * @param locked should operator be selected automatically (false) or manually (true)
 */

static void gsmd_network_command_set_operator (NetworkInterface *network,
                                               gpointer ipc_data,
                                               const char* operator,
                                               gboolean locked)
{
        ModemInterface *modem = (ModemInterface*)network->priv;
        if (locked) {
                gsmd_modem_post_at_command_id( modem,
                                               SET_OPERATOR_LOCKED,
                                               operator,
                                               ipc_data,
                                               (ErrorFunction)modem->network_ipc->register_network_error,
                                               (gpointer)modem->network_ipc,
                                               INTERFACE_NETWORK,
                                               NULL);
        } else {
                gsmd_modem_post_at_command_id( modem,
                                               SET_OPERATOR,
                                               operator,
                                               ipc_data,
                                               (ErrorFunction)modem->network_ipc->register_network_error,
                                               (gpointer)modem->network_ipc,
                                               INTERFACE_NETWORK,
                                               NULL);
        }
}



/**
 * @brief AtCommand to query available operators
 *
 * @param modem modem whose response to handle
 */
static void gsmd_network_command_query_available_operators (NetworkInterface *network,
                                                            gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)network->priv;
        gsmd_modem_post_at_command_id( modem,
                                       OPS_QUERY,
                                       NULL,
                                       ipc_data,
                                       (ErrorFunction)modem->network_ipc->list_providers_error,
                                       (gpointer)modem->network_ipc,
                                       INTERFACE_NETWORK,
                                       NULL);

}

/**
 * @brief AtCommand to query current operator
 *
 * @param modem modem whose response to handle
 */
static void gsmd_network_command_query_current_operator (NetworkInterface *network,
                                                         gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)network->priv;
        gsmd_modem_post_at_command_id( modem,
                                       CUR_OP_QUERY,
                                       NULL,
                                       ipc_data,
                                       NULL, NULL,
                                       INTERFACE_NETWORK,
                                       NULL);
        //This isn't in api, hence no
        //error method reply
}


/**
 * @brief Handler to modems response from set_operator command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
AtCommandHandlerStatus gsmd_network_handler_set_operator ( ModemInterface *modem,
                                                           AtCommandContext *at,
                                                           GString *response)
{
        //OK or ERROR, if OK it will following with +CREG: status
        GScanner* scanner = modem->scanner;
        if (scanner->token == SYMBOL_OK) {
                modem->network_ipc->register_network_reply(modem->network_ipc,at->ipc_data);
                return AT_HANDLER_DONE;
        }

        if (gsmd_utils_send_error_from_response(at,
                                                response,
                                                "ERROR OCCURRED WHEN REGISTERING TO NETWORK",
                                                "TIMEOUT WHEN REGISTERING TO NETWORK")) {
                return AT_HANDLER_DONE_ERROR;
        }

        return AT_HANDLER_DONT_UNDERSTAND;
}


/**
 * @brief Handler to modems response list_providers command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
AtCommandHandlerStatus  gsmd_network_handler_query_available_operators (ModemInterface *modem,
                                                                        AtCommandContext *at,
                                                                        GString *response)
{
        /*
          at#cimi
          #CIMI: 244917070350971

          OK
        */

        AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
        GError **error = NULL;
        GRegex *regex = g_regex_new ("\\((?<mode>\\d),\"(?<name>[\\w\\s]+)\",,\"(?<id>\\d+)\"\\)|(?<error>ERROR)|(?<ok>OK)",
                                     0,
                                     0,
                                     error);
        GMatchInfo *match_info;


        if (g_regex_match (regex, response->str, 0, &match_info)) {
                if (gsmd_utils_match_exists(match_info,"mode")) {
                        GArray *result = g_array_new(FALSE,TRUE,sizeof(NetworkProvider*));
                        NetworkProvider *provider = NULL;
                        while (g_match_info_matches (match_info)) {
                                provider = g_new0(NetworkProvider, 1);

                                //TODO this is not what API specified
                                provider->nickname = g_match_info_fetch_named(match_info,"id");
                                provider->index = result->len;
                                provider->status = g_match_info_fetch_named(match_info,"mode");
                                provider->name = g_match_info_fetch_named(match_info,"name");
                                g_array_append_val(result,provider);
                                g_match_info_next (match_info, NULL);
                        }


                        //TODO only send this after OK
                        if (modem->network_ipc->list_providers_reply) {
                                modem->network_ipc->list_providers_reply(modem->network_ipc,
                                                                         at->ipc_data,
                                                                         result);
                        }

                        gint i=0;
                        for (i=0;i<result->len;i++) {
                                provider = g_array_index(result,NetworkProvider*,i);
                                g_free(provider->nickname);
                                g_free(provider->status);
                                g_free(provider->name);
                                g_free(provider);
                        }

                        g_array_free (result,TRUE);
                        status = AT_HANDLER_NEED_MORE;
                } else  if (gsmd_utils_match_exists(match_info,"error")) {
                        gsmd_utils_send_error(at,
                                              GSMD_ERROR_UNKNOWN,
                                              "FAILED TO READ OPERATOR LIST");
                        status = AT_HANDLER_DONE_ERROR;
                } else  if (gsmd_utils_match_exists(match_info,"ok")) {
                        status = AT_HANDLER_DONE;
                }


        }


        g_match_info_free (match_info);
        g_regex_unref (regex);


        return status;
}

/**
 * @brief Handler to modems response from queryCurrentOperator command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
AtCommandHandlerStatus gsmd_network_handler_query_current_operator(ModemInterface *modem,
                                                                   AtCommandContext *at,
                                                                   GString *response)
{
        /*
        Output from telit:
        at+cops?

        +COPS: 0,0,"FI SONERA"


        OK
        */

        /*


        at+cops?
        +COPS: 0,2,24491

        OK
        at+wopn=0,24491
        +WOPN: 0,"FI SONERA"

        OK

        at+cops=3,0
        OK
        at+cops?
        +COPS: 0,0,"FI SONERA"

        OK
        */




        gboolean locked;
        AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
        GScanner* scanner = modem->scanner;
        g_debug("%s",__func__);

        if (scanner->token == SYMBOL_ERROR) {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "UNABLE TO QUERY CURRENT OPERATOR");
                return AT_HANDLER_DONE_ERROR;
        }



        if (scanner->token == SYMBOL_OK) {
                return AT_HANDLER_DONE;
        }

        if (scanner->token != '+') {
                return AT_HANDLER_DONT_UNDERSTAND;
        }
        g_scanner_get_next_token(scanner);
        switch (scanner->token) {
        case SYMBOL_COPS:
                g_scanner_peek_next_token (scanner);
                if (scanner->next_token == ':') {
                        g_scanner_get_next_token (scanner);//get :
                        g_scanner_get_next_token (scanner);//get mode
                        locked = scanner->value.v_int;
                        g_scanner_get_next_token (scanner);//get ,
                        g_scanner_get_next_token (scanner);//get format
                        g_scanner_peek_next_token (scanner);//peek ,

                        if (scanner->next_token == ',') {
                                if (modem->network->provider_name)
                                        g_string_free(modem->network->provider_name,
                                                      TRUE);
                                //get the operator name here;
                                g_scanner_get_next_token (scanner);//get ,
                                g_scanner_get_next_token (scanner);

                                g_debug("%s : %p",__func__,scanner->value.v_string);
                                g_debug("%s : %s",__func__,scanner->value.v_string);
                                modem->network->provider_name = g_string_new(scanner->value.v_string);
                                //We now got operator name, query for
                                //signal strength
                                gsmd_modem_post_at_command_id(modem,
                                                              SIGNAL_QUERY,
                                                              NULL,
                                                              at->ipc_data,
                                                              at->error_function,
                                                              at->error_data,
                                                              INTERFACE_NETWORK,
                                                              NULL);



                                //this command ends with OK
                                status = AT_HANDLER_NEED_MORE;

                        } else {
                                g_scanner_unexp_token (scanner,
                                                       ',',
                                                       NULL,
                                                       "symbol",
                                                       NULL,
                                                       NULL,
                                                       TRUE);
                        }
                } else {
                        g_scanner_unexp_token (scanner,
                                               ':',
                                               NULL,
                                               "symbol",
                                               NULL,
                                               NULL,
                                               TRUE);
                }

                break;
        case SYMBOL_ERROR:
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "FAILED TO READ CURRENT OPERATOR NAME");
                status = AT_HANDLER_DONE_ERROR;
                break;
        default:
                g_warning("Format error in AT+COPS? reply");
                break;
        }

        return status;
}



/**
 * @brief Handler to modems response from querySignalStrength command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
AtCommandHandlerStatus gsmd_network_handler_query_signal_strength (
        ModemInterface *modem,
        AtCommandContext *at,
        GString *response)
{
        //+CSQ: <rssi>,<ber>
        guint symbol;
        AtCommandHandlerStatus status = AT_HANDLER_DONT_UNDERSTAND;
        g_debug ("%s", __func__);
        GScanner* scanner = modem->scanner;

        if (scanner->token == SYMBOL_ERROR) {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "UNABLE TO QUERY SIGNAL STRENGTH");
                return AT_HANDLER_DONE_ERROR;
        }



        if (scanner->token == SYMBOL_OK) {
                return AT_HANDLER_DONE;
        }


        if (scanner->token != '+') {
                return AT_HANDLER_DONT_UNDERSTAND;
        }

        g_scanner_get_next_token (scanner);
        symbol = scanner->token;
        switch (symbol) {
        case SYMBOL_CSQ:
                g_scanner_peek_next_token (scanner);
                if (scanner->next_token == ':') {
                        g_scanner_get_next_token(scanner);
                        g_scanner_get_next_token(scanner);

                        GString* message = g_string_new ("");
                        GValue *val = g_hash_table_lookup(modem->caches[INTERFACE_NETWORK],
                                                          GSMD_MODEM_KEY_NETWORK_STATUS);
                        g_assert(val); //Network status should always exist

                        g_string_printf(message, "%d",g_value_get_int(val));
                        GHashTable *table = gsmd_utils_create_hash_table();
                        gsmd_utils_table_insert_string(table,
                                                       "registration",
                                                       message->str);
                        gsmd_utils_table_insert_string(table,
                                                       "provider",
                                                       modem->network->provider_name->str);
                        gsmd_utils_table_insert_int(table,
                                                    "strength",
                                                    scanner->value.v_int);

                        if (at->ipc_data && modem->network_ipc->get_status_reply) {
                                modem->network_ipc->get_status_reply(
                                        modem->network_ipc,
                                        at->ipc_data,
                                        table);
                        }

                        if (modem->network_ipc->status) {
                                modem->network_ipc->status(modem->network_ipc,
                                                           table);
                        }

                        g_hash_table_destroy(table);

                        g_string_free(message,TRUE);
                        status = AT_HANDLER_NEED_MORE;
                } else {
                        g_scanner_unexp_token (scanner, ':', NULL, "symbol", NULL, NULL, TRUE);
                }
                break;

        default:
                g_warning ("Do not get CSQ after + \n");
                break;
        }

        return status;
}

/**
 * @brief Handler to modems response from queryNetworkRegistration command
 *
 * @param modem modem whose response to handle
 * @return true if responses were recognized
 */
AtCommandHandlerStatus gsmd_network_handler_query_network_registration (
        ModemInterface *modem,
        AtCommandContext *at,
        GString *response)
{
        //for queryNetworkRegistration
        // +CREG: 0,2
        guint symbol;
        AtCommandHandlerStatus at_status = AT_HANDLER_DONT_UNDERSTAND;

        //	g_message ("gsmd_network_command_query_network_registration handler\n");
        GScanner* scanner = modem->scanner;


        if (scanner->token == SYMBOL_ERROR) {
                gsmd_utils_send_error(at,
                                      GSMD_ERROR_UNKNOWN,
                                      "UNABLE TO QUERY NETWORK REGISTRATION STATUS");
                return AT_HANDLER_DONE_ERROR;
        }

        if (scanner->token == SYMBOL_OK) {
                return AT_HANDLER_DONE;
        }

        if (scanner->token != '+') {
                return AT_HANDLER_DONT_UNDERSTAND;
        }

        g_scanner_get_next_token (scanner);
        symbol = scanner->token;
        switch (symbol) {
        case  SYMBOL_CREG:
                g_scanner_peek_next_token (scanner);
                if (scanner->next_token == ':') {
                        g_scanner_get_next_token(scanner);// get :
                        g_scanner_get_next_token(scanner);// get 0
                        g_scanner_get_next_token(scanner);// get ,

                        if (scanner->token == ',') {
                                g_scanner_get_next_token(scanner);// get ,
                                gsmd_modem_change_network_status_data(modem,
                                                                      scanner->value.v_int,
                                                                      at->ipc_data,
                                                                      at->error_function,
                                                                      at->error_data);
                        }

                        at_status = AT_HANDLER_NEED_MORE;
                } else {
                        g_scanner_unexp_token (scanner, ':', NULL, "symbol", NULL, NULL, TRUE);
                }
                break;
        default:
                g_warning ("Do not get CSQ after + \n");
                break;
        }
        return at_status;
}


/**
 * @brief AtCommand to query signal strength
 *
 * @param modem modem whose response to handle
 */
static void gsmd_network_command_query_signal_strength (NetworkInterface *network,
                                                        gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)network->priv;
        gsmd_modem_post_at_command_id( modem,
                                       SIGNAL_QUERY,
                                       NULL,
                                       ipc_data,
                                       NULL, NULL,
                                       INTERFACE_NETWORK,
                                       NULL);
}

/**
 * @brief AtCommand to query network registration
 *
 * @param modem modem whose response to handle
 */
void gsmd_network_command_query_network_registration (NetworkInterface *network,
                                                      gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)network->priv;
        gsmd_modem_post_at_command_id( modem,
                                       NETWORK_REG_QUERY,
                                       NULL,
                                       ipc_data,
                                       NULL, NULL,
                                       INTERFACE_GENERAL,
                                       NULL);
}

void gsmd_network_command_register_network( NetworkInterface *network,
                                            gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)network->priv;
        gsmd_modem_post_at_command_id(  modem,
                                        SET_OPERATOR,
                                        NULL,
                                        ipc_data,
                                        (ErrorFunction)modem->network_ipc->register_network_error,
                                        (gpointer)modem->network_ipc,
                                        INTERFACE_NETWORK,
                                        NULL);
}

void gsmd_network_command_get_status(NetworkInterface *network,
                                     gpointer ipc_data)
{
        ModemInterface *modem = (ModemInterface*)network->priv;
        GString* message = g_string_new ("");
        GValue *val = g_hash_table_lookup(modem->caches[INTERFACE_NETWORK],
                                          GSMD_MODEM_KEY_NETWORK_STATUS);

        g_string_printf(message, "%d", g_value_get_int(val));
        g_debug("%s : Get status",__func__);


        switch (g_value_get_int(val)) {
                //We aren't registered, then we have no provider or signal
                //TODO are these assumption correct?
        case NETWORK_UNREGISTERED:
        case NETWORK_BUSY:
        case NETWORK_DENIED: {
                GHashTable *status = gsmd_utils_create_hash_table();
                gsmd_utils_table_insert_string(status,"provider","");
                gsmd_utils_table_insert_string(status,"registration",message->str);
                gsmd_utils_table_insert_int(status,"strength",0);
                g_debug("%s : Not registered to network, sending status signal",__func__);
                modem->network_ipc->status(modem->network_ipc,status);
                g_hash_table_destroy(status);
        }
        break;
        case NETWORK_REGISTERED:
        case NETWORK_ROAMING:
                //if we have current operator name, then just query signal strength
                if (network->provider_name) {
                        g_debug("%s : We have provider name, query signal",__func__);
                        gsmd_modem_post_at_command_id(  modem,
                                                        SIGNAL_QUERY,
                                                        NULL,
                                                        ipc_data,
                                                        (ErrorFunction)modem->network_ipc->get_status_error,
                                                        (gpointer)modem->network_ipc,
                                                        INTERFACE_NETWORK,
                                                        NULL);
                } else {
                        g_debug("%s : We don't have provider name",__func__);
                        //If we don't have current operator name then we'll query it
                        //and that handler will also query for the signal
                        gsmd_modem_post_at_command_id( modem,
                                                       CUR_OP_QUERY,
                                                       NULL,
                                                       ipc_data,
                                                       (ErrorFunction)modem->network_ipc->get_status_error,
                                                       (gpointer)modem->network_ipc,
                                                       INTERFACE_NETWORK,
                                                       NULL);
                }
                break;

        case NETWORK_UNKNOWN:
                g_debug("%s : Unknown status, query status",__func__);
                gsmd_modem_post_at_command_id(  modem,
                                                NETWORK_REG_QUERY,
                                                NULL,
                                                ipc_data,
                                                (ErrorFunction)modem->network_ipc->get_status_error,
                                                (gpointer)modem->network_ipc,
                                                INTERFACE_NETWORK,
                                                NULL);
        }

        g_string_free(message,TRUE);

}


void gsmd_network_command_register_with_provider(NetworkInterface *network,
                                                 gpointer ipc_data,
                                                 int index)
{
        ModemInterface *modem = (ModemInterface*)network->priv;
        modem->network_ipc->register_with_provider_reply(modem->network_ipc,
                                                         ipc_data);
}






void gsmd_network_init(ModemInterface *modem)
{
        gsmd_network_init_at_handler(modem);
        modem->network->priv = (gpointer)modem;
        modem->network->set_operator = &gsmd_network_command_set_operator;
        modem->network->query_current_operator = &gsmd_network_command_query_current_operator;
        modem->network->query_signal_strength = &gsmd_network_command_query_signal_strength;
        modem->network->list_providers = &gsmd_network_command_query_available_operators;
        modem->network->register_with_provider = &gsmd_network_command_register_with_provider;
        modem->network->get_status = &gsmd_network_command_get_status;
        modem->network->register_network = &gsmd_network_command_register_network;

        modem->network->provider_name = NULL;
        modem->network->status = NULL;
}

void gsmd_network_deinitialize(ModemInterface *modem)
{
        if (modem->network->provider_name)
                g_string_free(modem->network->provider_name,TRUE);
        if (modem->network->status)
                g_string_free(modem->network->status,TRUE);
}

