/*
 *  device.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */

#include "modem.h"
#include "device_interface.h"

#define GSMD_DEVICE_KEY_POWER "antenna_power"
#define GSMD_DEVICE_KEY_IMEI "imei"
#define GSMD_DEVICE_KEY_MODEM_REVISION "revision"
#define GSMD_DEVICE_KEY_MODEM_VENDOR "vendor"
#define GSMD_DEVICE_KEY_MODEM_MODEL "model"


void gsmd_device_init(ModemInterface *modem);
