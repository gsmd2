/*
 *  dbus_device_object.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include "dbus_device_object.h"
#include "dbus_device_glue.h"
#include "dbus_marshal.h"
#include "dbus_objects.h"
#include "device_interface.h"
#include <stdlib.h>
#include <stdio.h>

G_DEFINE_TYPE(DBusDeviceObject, dbus_device_object, G_TYPE_OBJECT)



static void
dbus_device_object_class_init(DBusDeviceObjectClass *klass)
{
}


static void dbus_device_object_init(DBusDeviceObject *gsm)
{
        dbus_g_object_type_install_info (DBUS_DEVICE_TYPE_OBJECT,
                                         &dbus_glib_smartphone_device_object_info);
}


void dbus_device_object_finalize(GObject *obj)
{
        //G_OBJECT_CLASS (parent_class)->finalize (obj);
        //DBusDeviceObject *gsm = (DBusDeviceObject *) obj;
}




void gsmd_dbus_device_uninitialize(DBusDeviceObject *obj)
{
        g_object_unref(obj);
}



/*******************************Methods***************************************/
gboolean smartphone_device_get_info(DBusDeviceObject *obj,
                                    DBusGMethodInvocation *method_context)
{
        obj->device->get_info(obj->device,method_context);
        return TRUE;
}

gboolean smartphone_device_set_antenna_power(DBusDeviceObject *obj,
                                             gboolean antenna_power,
                                             DBusGMethodInvocation *method_context)
{
        obj->device->set_antenna_power(obj->device,method_context,antenna_power);
        return TRUE;
}

gboolean smartphone_device_get_antenna_power(DBusDeviceObject *obj,
                                             DBusGMethodInvocation *method_context)
{
        obj->device->get_antenna_power(obj->device,method_context);
        return TRUE;
}

gboolean smartphone_device_get_features(DBusDeviceObject *obj,
                                        DBusGMethodInvocation *method_context)
{
        obj->device->get_features(obj->device,method_context);
        return TRUE;
}

gboolean smartphone_device_recover_from_suspend(DBusDeviceObject *obj,
                                                DBusGMethodInvocation *method_context)
{
        obj->device->recover_from_suspend(obj->device,method_context);
        return TRUE;
}
gboolean smartphone_device_prepare_to_suspend(DBusDeviceObject *obj,
                                              DBusGMethodInvocation *method_context)
{
        obj->device->prepare_to_suspend(obj->device,method_context);
        return TRUE;
}

/*****************************replies to methods*******************************/
void smartphone_device_get_info_reply( DeviceIPCInterface *device_ipc,
                                       gpointer ipc_data,
                                       GHashTable *info)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     info);
}


void smartphone_device_set_antenna_power_reply( DeviceIPCInterface *device_ipc,
                                                gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_device_prepare_to_suspend_reply( DeviceIPCInterface *device_ipc,
                                                 gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_device_recover_from_suspend_reply( DeviceIPCInterface *device_ipc,
                                                   gpointer ipc_data)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_device_get_antenna_power_reply( DeviceIPCInterface *device_ipc,
                                                gpointer ipc_data,
                                                gboolean antenna_power)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     antenna_power);
}

//TODO features param doesn't comply with specifications
void smartphone_device_get_features_reply( DeviceIPCInterface *device_ipc,
                                           gpointer ipc_data,
                                           GHashTable *features)
{
        if (ipc_data)
                dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                                     features);
}


/*****************************Error replies to methods*************************/
void smartphone_device_get_info_error( DeviceIPCInterface *device_ipc,
                                       gpointer ipc_data,
                                       GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_device_set_antenna_power_error( DeviceIPCInterface *device_ipc,
                                                gpointer ipc_data,
                                                GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_device_get_antenna_power_error( DeviceIPCInterface *device_ipc,
                                                gpointer ipc_data,
                                                GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_device_get_features_error( DeviceIPCInterface *device_ipc,
                                           gpointer ipc_data,
                                           GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_device_recover_from_suspend_error( DeviceIPCInterface *device_ipc,
                                                   gpointer ipc_data,
                                                   GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

void smartphone_device_prepare_to_suspend_error( DeviceIPCInterface *device_ipc,
                                                 gpointer ipc_data,
                                                 GError *error)
{
        if (ipc_data)
                dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,error);
}

DBusDeviceObject *gsmd_dbus_device_initialize (DeviceIPCInterface *device_ipc, DeviceInterface* device)
{
        DBusDeviceObject *obj = g_object_new (DBUS_DEVICE_TYPE_OBJECT, NULL);
        obj->device = device;

        device_ipc->get_info_reply = &smartphone_device_get_info_reply;
        device_ipc->set_antenna_power_reply = &smartphone_device_set_antenna_power_reply;
        device_ipc->get_antenna_power_reply = &smartphone_device_get_antenna_power_reply;
        device_ipc->get_features_reply = &smartphone_device_get_features_reply;
        device_ipc->recover_from_suspend_reply = &smartphone_device_recover_from_suspend_reply;
        device_ipc->prepare_to_suspend_reply = &smartphone_device_prepare_to_suspend_reply;

        device_ipc->get_info_error = &smartphone_device_get_info_error;
        device_ipc->set_antenna_power_error = &smartphone_device_set_antenna_power_error;
        device_ipc->get_antenna_power_error = &smartphone_device_get_antenna_power_error;
        device_ipc->get_features_error = &smartphone_device_get_features_error;
        device_ipc->recover_from_suspend_error = &smartphone_device_recover_from_suspend_error;
        device_ipc->prepare_to_suspend_error = &smartphone_device_prepare_to_suspend_error;


        return obj;
}
