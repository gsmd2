/*
 *  call_interface.h
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#ifndef CALL_INTERFACE
#define CALL_INTERFACE


/**
 * @brief voice call interface provice voice call service such as dial accept or
 * hangup a call
 */
typedef struct CallInterface CallInterface;

struct CallInterface
{

    gpointer priv;

    gpointer vendor_priv;

    /***************************Method calls***********************************/



    /**
     * @brief makes a call
     * @param call call interface pointer
     * @param ipc_data data for ipc
     * @param number the phone number to call
     */
    void (*initiate) (CallInterface *call,
                      gpointer ipc_data,
                      const char* number,
                      const char* type);

    /**
     * @brief Activates a call (either incoming or held)
     * @param call call interface pointer
     * @param ipc_data data for ipc
     * @param id call's id
     */
    void (*activate) (CallInterface *call,
                      gpointer ipc_data,
                      gint id);


    /**
     * @brief Adds specified call to conference
     * @param call call interface pointer
     * @param ipc_data data for ipc
     * @param id call's id
     */
    void (*activate_conference) (CallInterface *call,
                                 gpointer ipc_data,
                                 gint id);

    /**
     * @brief Hangup a call. Hangup the current call or cancel the call
     * request
     * @param call call interface pointer
     * @param ipc_data data for ipc
     * @param message TODO what's this?
     * @param id index of the call to hang up
     */
    void (*release) (CallInterface *call,
                     gpointer ipc_data,
                     const gchar *message,
                     const gint id);

    /**
     * @brief Place active call to hold.
     *
     * @param call call interface pointer
     * @param ipc_data data for ipc
     */
    void (*hold_active) (CallInterface *call,
                         gpointer ipc_data);

    /**
     * @brief Join calls.
     *
     * @param call call interface pointer
     * @param ipc_data data for ipc
     */
    void (*join) (CallInterface *call,
                  gpointer ipc_data);

    /**
     * @brief Transfer calls.
     *
     * @param call call interface pointer
     * @param ipc_data data for ipc
     */
    void (*transfer) (CallInterface *call,
                      gpointer ipc_data,
                      const gchar *number);

    /**
     * @brief Hangup all calls.
     * @param call call interface pointer
     * @param ipc_data data for ipc
     * @param message TODO what's this?
     */
    void (*release_all) (CallInterface *call,
                         gpointer ipc_data,
                         const gchar *message);

    /**
     * @brief Releases all held calls.
     * @param call call interface pointer
     * @param ipc_data data for ipc
     * @param message TODO what's this?
     */
    void (*release_held) (CallInterface *call,
                          gpointer ipc_data,
                          const gchar *message);


    /**
    * @brief Lists current calls (held and active)
    * @param call call interface pointer
    * @param ipc_data data for ipc
    */
    void (*list_calls) (CallInterface *call,
                        gpointer ipc_data);

    /**
    * @brief Set Dtmf of modem
    * @param call call interface poniter
    * @param ipc_data data for ipc
    * @param str Dtmf string
    */
    void (*send_dtmf) (CallInterface *call,
                       gpointer ipc_data);

    /**
     * @brief Get emergency number
     * @param call call interface poniter
     * @param ipc_data data for ipc
     */
    void (*emergency) (CallInterface *call,
                       gpointer ipc_data);



};

typedef struct _CallIPCInterface CallIPCInterface;

struct _CallIPCInterface
{

    gpointer priv;

    /**************************Replies to method calls*************************/

    /**
     * @brief sends a reply to ipc that phone call has been made
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     * @param id newly initiated phone call's id
     */
    void (*initiate_reply) (CallIPCInterface *call_ipc,
                            gpointer ipc_data,
                            const gint id);


    /**
     * @brief sends a reply to ipc that phone call has been activated
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*activate_reply) (CallIPCInterface *call_ipc,
                            gpointer ipc_data);

    /**
     * @brief sends a reply to ipc that phone call has been added to
     * conference
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*activate_conference_reply) (CallIPCInterface *call_ipc,
                                       gpointer ipc_data);

    /**
     * @brief sends a reply to ipc that active and held calls have been joined and released
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*join_reply) (CallIPCInterface *call_ipc,
                        gpointer ipc_data);

    /**
     * @brief sends a reply to ipc that a phone call has been released (hung up)
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*release_reply) (CallIPCInterface *call_ipc,
                           gpointer ipc_data);

    /**
     * @brief sends a reply to ipc that dtmf has been sent
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     * @param mode
     */
    void (*send_dtmf_reply) (CallIPCInterface *call_ipc,
                             gpointer ipc_data,
                             const char *mode);

    /**
     * @brief sends a reply to ipc with the emergency number
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     * @param number emergency number
     */
    void (*emergency_reply) (CallIPCInterface *call_ipc,
                             gpointer ipc_data,
                             const char* number);

    /**
     * @brief send a reply to set DTMF duration function
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*set_dtmf_duration_reply) (CallIPCInterface *call_ipc,
                                     gpointer ipc_data);

    /**
     * @brief send a reply to get DTMF duration function
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     * @param duration dtmf duration
     */
    void (*get_dtmf_duration_reply) (CallIPCInterface *call_ipc,
                                     gpointer ipc_data,
                                     const gint duration);

    /**
     * @brief send a reply to release all function
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*release_all_reply) (CallIPCInterface *call_ipc,
                               gpointer ipc_data);


    /**
     * @brief send a reply to release held function
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*release_held_reply) (CallIPCInterface *call_ipc,
                                gpointer ipc_data);

    /**
    * @brief send a reply to list calls function
    * @param call_ipc call IPC interface pointer
    * @param ipc_data data for ipc
    * @param calls active calls
    */
    void (*list_calls_reply) (CallIPCInterface *call_ipc,
                              gpointer ipc_data,
                              GArray *calls);

    /**
     * @brief sends a reply to get send identification function
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     * @param mode ??
     */
    void (*get_send_identification_reply) (CallIPCInterface *call_ipc,
                                           gpointer ipc_data,
                                           gboolean mode);

    /**
     * @brief sends a reply to set send identification function
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     */
    void (*set_send_identification_reply) (CallIPCInterface *call_ipc,
                                           gpointer ipc_data);
    /*********************Error replies to method calls************************/

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*initiate_error) (CallIPCInterface *call_ipc,
                            gpointer ipc_data,
                            GError *error);



    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*activate_error) (CallIPCInterface *call_ipc,
                            gpointer ipc_data,
                            GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*activate_conference_error) (CallIPCInterface *call_ipc,
                                       gpointer ipc_data,
                                       GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*release_error) (CallIPCInterface *call_ipc,
                           gpointer ipc_data,
                           GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*join_error) (CallIPCInterface *call_ipc,
                        gpointer ipc_data,
                        GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*release_all_error) (CallIPCInterface *call_ipc,
                               gpointer ipc_data,
                               GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*release_held_error) (CallIPCInterface *call_ipc,
                                gpointer ipc_data,
                                GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*send_dtmf_error) (CallIPCInterface *call_ipc,
                             gpointer ipc_data,
                             GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*emergency_error) (CallIPCInterface *call_ipc,
                             gpointer ipc_data,
                             GError *error);


    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*set_dtmf_duration_error) (CallIPCInterface *call_ipc,
                                     gpointer ipc_data,
                                     GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc pointer to call IPC interface struct
     * @param ipc_data ipc data
     * @param error pointer to GError describing error
     */
    void (*get_dtmf_duration_error) (CallIPCInterface *call_ipc,
                                     gpointer ipc_data,
                                     GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     * @param error pointer to GError describing error
     */
    void (*list_calls_error) (CallIPCInterface *call_ipc,
                              gpointer ipc_data,
                              GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     * @param error pointer to GError describing error
     */
    void (*set_send_identification_error) (CallIPCInterface *call_ipc,
                                           gpointer ipc_data,
                                           GError *error);

    /**
     * @brief sends an error reply to ipc
     * @param call_ipc call IPC interface pointer
     * @param ipc_data data for ipc
     * @param error pointer to GError describing error
     */
    void (*get_send_identification_error) (CallIPCInterface *call_ipc,
                                           gpointer ipc_data,
                                           GError *error);

    /********************************Signals***********************************/



    /**
     * @brief call progress signal is emited to provide additional information
     * on call's progress
     */
    void (*call_status) (CallIPCInterface *call_ipc,
                         const gint id,
                         const gchar *status,
                         GHashTable *properties
                        );


};

#endif
