/*
 *  dbus_sim_object.c
 *
 *  Copyright(C) 2007,2008 Ixonos Plc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Boston, MA 02111.
 */

/*
 * Written by
 *   Jukka Honkela
 *   Yi Zheng
 *   Matti Katila
 *   Vesa Pikki
 *   Heikki Paajanen
 */


#include "dbus_sim_object.h"
#include "dbus_sim_glue.h"
#include "dbus_marshal.h"
#include "sim_interface.h"
#include <stdlib.h>
#include <stdio.h>
#include "sim.h"

#define GSMD_SIM_MESSAGEBOOK_TYPE (dbus_g_type_get_struct ("GValueArray", \
      G_TYPE_INT, \
      G_TYPE_STRING, \
      G_TYPE_STRING, \
      G_TYPE_STRING, \
      G_TYPE_INVALID))

#define GSMD_SIM_PHONEBOOK_TYPE (dbus_g_type_get_struct ("GValueArray", \
      G_TYPE_INT, \
      G_TYPE_STRING, \
      G_TYPE_STRING, \
      G_TYPE_INVALID))

#define GSMD_SIM_HOME_ZONES_TYPE (dbus_g_type_get_struct ("GValueArray", \
        G_TYPE_STRING, \
        G_TYPE_INT, \
        G_TYPE_INT, \
        G_TYPE_INT, \
        G_TYPE_INVALID))

G_DEFINE_TYPE(DBusSimObject, dbus_sim_object, G_TYPE_OBJECT)



static void
dbus_sim_object_class_init(DBusSimObjectClass *klass)
{
    /* Make the local system aware of our kind */

    /** Register our signals so that they can be emitted */

    klass->auth_status =
        g_signal_new ("auth_status",
                      G_OBJECT_CLASS_TYPE (klass),
                      G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                      0, NULL, NULL, g_cclosure_marshal_VOID__STRING,
                      G_TYPE_NONE, 1, G_TYPE_STRING);

    klass->incoming_message = g_signal_new ("incoming_message",
                                            G_OBJECT_CLASS_TYPE (klass),
                                            G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                                            0, NULL, NULL, g_cclosure_marshal_VOID__INT,
                                            G_TYPE_NONE, 1, G_TYPE_INT);
}


static void dbus_sim_object_init(DBusSimObject *gsm)
{
    dbus_g_object_type_install_info (DBUS_SIM_TYPE_OBJECT,
                                     &dbus_glib_smartphone_sim_object_info);
}


void dbus_sim_object_finalize(GObject *obj)
{
    //G_OBJECT_CLASS (parent_class)->finalize (obj);
    //DBusSimObject *gsm = (DBusSimObject *) obj;
}




void gsmd_dbus_sim_uninitialize(DBusSimObject *obj)
{
    g_object_unref(obj);
}



/*********************Signal functions***********************/
void smartphone_sim_auth_status (SIMIPCInterface *sim_ipc, const gchar *status)
{
    g_debug("%s : %s",__func__,status);
    DBusSimObject *obj = (DBusSimObject*)sim_ipc->priv;
    g_signal_emit (obj,
                   DBUS_SIM_OBJECT_GET_CLASS(obj)->auth_status,
                   0, status);
}

void smartphone_sim_incoming_message (SIMIPCInterface *sim_ipc,
                                      gint index)
{
    DBusSimObject *obj = (DBusSimObject*)sim_ipc->priv;
    g_signal_emit (obj,
                   DBUS_SIM_OBJECT_GET_CLASS(obj)->incoming_message,
                   0, index);
}

/*********************Methods***********************/

gboolean smartphone_sim_get_home_zones(DBusSimObject *obj,
                                       DBusGMethodInvocation *method_context)
{
    obj->sim->get_home_zones( obj->sim,
                              method_context);
    return TRUE;
}


gboolean smartphone_sim_get_messagebook_info(DBusSimObject *obj,
                                             DBusGMethodInvocation *method_context)
{
    obj->sim->get_messagebook_info( obj->sim,
                                    method_context);
    return TRUE;
}

gboolean smartphone_sim_store_message(DBusSimObject *obj,
                                      const gchar *recipient_number,
                                      const gchar *contents,
                                      DBusGMethodInvocation *method_context)
{
    obj->sim->store_message( obj->sim,
                             method_context,
                             recipient_number,
                             contents);
    return TRUE;
}

gboolean smartphone_sim_delete_message(DBusSimObject *obj,
                                       const gint index,
                                       DBusGMethodInvocation *method_context)
{
    obj->sim->delete_message(obj->sim,method_context,index);
    return TRUE;
}

gboolean smartphone_sim_retrieve_messagebook(DBusSimObject *obj,
                                             const gchar *category,
                                             DBusGMethodInvocation *method_context)
{
    obj->sim->retrieve_messagebook(obj->sim,method_context,category);
    return TRUE;
}

gboolean smartphone_sim_send_stored_message(DBusSimObject *obj,
                                            gint index,
                                            DBusGMethodInvocation *method_context)
{
    obj->sim->send_stored_message(obj->sim,method_context,index);
    return TRUE;
}

gboolean smartphone_sim_retrieve_message(DBusSimObject *obj,
                                         const gint index,
                                         DBusGMethodInvocation *method_context)
{
    g_debug("%s : %d",__func__,index);
    obj->sim->retrieve_message(obj->sim,method_context,index);
    return TRUE;
}

gboolean smartphone_sim_get_service_center_number(DBusSimObject *obj,
                                                  DBusGMethodInvocation *method_context)
{
    obj->sim->get_service_center_number(obj->sim,method_context);
    return TRUE;
}

gboolean smartphone_sim_set_service_center_number(DBusSimObject *obj,
                                                  const gchar *number,
                                                  DBusGMethodInvocation *method_context)
{
    obj->sim->set_service_center_number( obj->sim,
                                         method_context,
                                         number);
    return TRUE;
}


gboolean smartphone_sim_set_auth_code_required(DBusSimObject *obj,
                                               gboolean check,
                                               DBusGMethodInvocation *method_context)
{
    // TODO
    g_assert(FALSE);
    return TRUE;
}

gboolean smartphone_sim_get_auth_code_required(DBusSimObject *obj,
                                               DBusGMethodInvocation *method_context)
{
    // TODO
    g_assert(FALSE);
    return TRUE;
}

gboolean smartphone_sim_get_auth_status(DBusSimObject *obj,
                                        DBusGMethodInvocation *method_context)
{
    obj->sim->get_auth_status(obj->sim,method_context);

    return TRUE;
}


gboolean smartphone_sim_send_auth_code(DBusSimObject *obj,
                                       const gchar *pin,
                                       DBusGMethodInvocation *method_context)
{
    obj->sim->send_auth_code(obj->sim,method_context,pin);
    return TRUE;
}

gboolean smartphone_sim_unlock(DBusSimObject *obj,
                               const gchar *puk,
                               const gchar *new_pin,
                               DBusGMethodInvocation *method_context)
{
    obj->sim->unlock(obj->sim,method_context,puk,new_pin);
    return TRUE;
}

gboolean smartphone_sim_change_auth_code(DBusSimObject *obj,
                                         const gchar *old_pin,
                                         const gchar *new_pin,
                                         DBusGMethodInvocation *method_context)
{
    obj->sim->change_auth_code(  obj->sim,
                                 method_context,
                                 old_pin,
                                 new_pin);
    return TRUE;
}

gboolean smartphone_sim_get_sim_info(DBusSimObject *obj,
                                     DBusGMethodInvocation *method_context)
{
    obj->sim->get_sim_info(obj->sim,method_context);
    return TRUE;
}

gboolean smartphone_sim_retrieve_phonebook(DBusSimObject *obj,
                                           DBusGMethodInvocation *method_context)
{
    obj->sim->retrieve_phonebook(obj->sim,method_context);
    return TRUE;
}


gboolean smartphone_sim_get_phonebook_info(DBusSimObject *obj,
                                           DBusGMethodInvocation *method_context)
{
    obj->sim->get_phonebook_info(obj->sim,method_context);
    return TRUE;
}

gboolean smartphone_sim_delete_entry(DBusSimObject *obj,
                                     gint index,
                                     DBusGMethodInvocation *method_context)
{
    obj->sim->delete_entry(obj->sim,method_context,index);
    return TRUE;
}


gboolean smartphone_sim_store_entry(DBusSimObject *obj,
                                    gint index,
                                    const gchar *name,
                                    const gchar *number,
                                    DBusGMethodInvocation *method_context)
{
    obj->sim->store_entry(obj->sim,
                          method_context,
                          index,
                          name,
                          number);
    return TRUE;
}

gboolean smartphone_sim_retrieve_entry(DBusSimObject *obj,
                                       gint index,
                                       DBusGMethodInvocation *method_context)
{
    obj->sim->retrieve_entry(obj->sim,
                             method_context,
                             index);
    return TRUE;
}


gboolean smartphone_sim_send_generic_sim_command(DBusSimObject *obj,
                                                 const gchar *command,
                                                 DBusGMethodInvocation *method_context)
{
    obj->sim->send_generic_sim_command(obj->sim,
                                       method_context,
                                       command);

    return TRUE;
}

gboolean smartphone_sim_send_restricted_sim_command(DBusSimObject *obj,
                                                    gint command,
                                                    gint fileid,
                                                    gint p1,
                                                    gint p2,
                                                    gint p3,
                                                    const gchar *data,
                                                    DBusGMethodInvocation *method_context)
{
    obj->sim->send_restricted_sim_command(obj->sim,
                                          method_context,
                                          command,
                                          fileid,
                                          p1,
                                          p2,
                                          p3,
                                          data);
    return TRUE;
}


/****************************Method replies************************************/
void smartphone_sim_get_phonebook_info_reply(SIMIPCInterface *sim_ipc,
                                             gpointer ipc_data,
                                             GHashTable *info)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,info);
}

void smartphone_sim_get_home_zones_reply(SIMIPCInterface *sim_ipc,
                                         gpointer ipc_data,
                                         GArray *zones)
{
    if (!zones)
    {
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,zones);
        return;
    }

    GPtrArray *result = NULL;
    guint i;

    result = g_ptr_array_sized_new (zones->len);

    for (i = 0; i < zones->len; i++)
    {
        homezone_entry *zone = g_array_index(zones,homezone_entry*,i);
        if (!zone)
            break;

        GValue entry = { 0, };


        g_value_init (&entry, GSMD_SIM_HOME_ZONES_TYPE);
        g_value_take_boxed (&entry,
                            dbus_g_type_specialized_construct (GSMD_SIM_HOME_ZONES_TYPE));

        dbus_g_type_struct_set (&entry,
                                0, zone->name->str,
                                1, zone->x,
                                2, zone->y,
                                3, zone->r,
                                G_MAXUINT);

        g_ptr_array_add (result, g_value_get_boxed (&entry));
    }

    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,result);

    g_ptr_array_free (result, TRUE);
}

void smartphone_sim_retrieve_phonebook_reply(SIMIPCInterface *sim_ipc,
                                             gpointer ipc_data,
                                             GArray *phonebook)
{
    if (!phonebook)
    {
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,phonebook);
        return;
    }

    GPtrArray *result = NULL;
    guint i;

    result = g_ptr_array_sized_new (phonebook->len);

    for (i = 0; i < phonebook->len; i++)
    {
        phonebook_entry *phone = g_array_index(phonebook,phonebook_entry*,i);
        if (!phone)
            break;

        GValue entry = { 0, };


        g_value_init (&entry, GSMD_SIM_PHONEBOOK_TYPE);
        g_value_take_boxed (&entry,
                            dbus_g_type_specialized_construct (GSMD_SIM_PHONEBOOK_TYPE));

        dbus_g_type_struct_set (&entry,
                                0, phone->index,
                                1, phone->name->str,
                                2, phone->number->str,
                                G_MAXUINT);

        g_ptr_array_add (result, g_value_get_boxed (&entry));
    }

    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,result);

    g_ptr_array_free (result, TRUE);
}


void smartphone_sim_get_messagebook_info_reply(SIMIPCInterface *sim_ipc,
                                               gpointer ipc_data,
                                               GHashTable *info)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,info);
}

void smartphone_sim_store_message_reply(SIMIPCInterface *sim_ipc,
                                        gpointer ipc_data,
                                        const int index)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,index);
}

void smartphone_sim_retrieve_message_reply(SIMIPCInterface *sim_ipc,
                                           gpointer ipc_data,
                                           const char *sender_number,
                                           const char *content)
{
    g_debug("%s: %s to %s",__func__,content,sender_number);
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                             sender_number,
                             content);
}


void smartphone_sim_get_auth_status_reply(SIMIPCInterface *sim_ipc,
                                          gpointer ipc_data,
                                          const char *status)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,status);
}

void smartphone_sim_send_auth_code_reply(SIMIPCInterface *sim_ipc,
                                         gpointer ipc_data)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_sim_delete_message_reply(SIMIPCInterface *sim_ipc,
                                         gpointer ipc_data)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_sim_send_stored_message_reply(SIMIPCInterface *sim_ipc,
                                              gpointer ipc_data, gint transaction_index)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,transaction_index);
}

void smartphone_sim_retrieve_messagebook_reply(SIMIPCInterface *sim_ipc,
                                               gpointer ipc_data,
                                               GArray *messages)
{
    if (!messages)
    {
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,messages);
        return;
    }

    GPtrArray *result = NULL;
    guint i;

    result = g_ptr_array_sized_new (messages->len);

    for (i = 0; i < messages->len; i++)
    {
        messagebook_msg *msg = g_array_index(messages,messagebook_msg*,i);
        if (!msg)
            break;

        GValue entry = { 0, };


        g_value_init (&entry, GSMD_SIM_MESSAGEBOOK_TYPE);
        g_value_take_boxed (&entry,
                            dbus_g_type_specialized_construct (GSMD_SIM_MESSAGEBOOK_TYPE));

        dbus_g_type_struct_set (&entry,
                                0, msg->index,
                                1, msg->status->str,
                                2, msg->number->str,
                                3, msg->content->str,
                                G_MAXUINT);



        g_ptr_array_add (result, g_value_get_boxed (&entry));
    }

    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,result);

    g_ptr_array_free (result, TRUE);

}

void smartphone_sim_unlock_reply(SIMIPCInterface *sim_ipc,
                                 gpointer ipc_data)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_sim_change_auth_code_reply(SIMIPCInterface *sim_ipc,
                                           gpointer ipc_data)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_sim_get_sim_info_reply(SIMIPCInterface *sim_ipc,
                                       gpointer ipc_data,
                                       GHashTable *info)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,info);
}

void smartphone_sim_get_service_center_number_reply(SIMIPCInterface *sim_ipc,
                                                    gpointer ipc_data,
                                                    const char *number)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,number);
}

void smartphone_sim_set_service_center_number_reply(SIMIPCInterface *sim_ipc,
                                                    gpointer ipc_data)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_sim_delete_entry_reply(SIMIPCInterface *sim_ipc,
                                       gpointer ipc_data)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_sim_store_entry_reply(SIMIPCInterface *sim_ipc,
                                      gpointer ipc_data)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data);
}

void smartphone_sim_retrieve_entry_reply(SIMIPCInterface *sim_ipc,
                                         gpointer ipc_data,
                                         const gchar *name,
                                         const gchar *number)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                             name,
                             number);
}

void smartphone_sim_send_restricted_sim_command_reply(SIMIPCInterface *sim_ipc,
                                                      gpointer ipc_data,
                                                      const gchar *result)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                             result);
}

void smartphone_sim_send_generic_sim_command_reply(SIMIPCInterface *sim_ipc,
                                                   gpointer ipc_data,
                                                   const gchar *result)
{
    if (ipc_data)
        dbus_g_method_return((DBusGMethodInvocation*) ipc_data,
                             result);
}

/****************************Method error replies******************************/


void smartphone_sim_get_home_zones_error(SIMIPCInterface *sim_ipc,
                                         gpointer ipc_data,
                                         GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}
void smartphone_sim_get_phonebook_info_error(SIMIPCInterface *sim_ipc,
                                             gpointer ipc_data,
                                             GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_retrieve_phonebook_error(SIMIPCInterface *sim_ipc,
                                             gpointer ipc_data,
                                             GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_delete_message_error(SIMIPCInterface *sim_ipc,
                                         gpointer ipc_data,
                                         GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_send_message_error(SIMIPCInterface *sim_ipc,
                                       gpointer ipc_data,
                                       GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_retrieve_messagebook_error(SIMIPCInterface *sim_ipc,
                                               gpointer ipc_data,
                                               GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_get_messagebook_info_error(SIMIPCInterface *sim_ipc,
                                               gpointer ipc_data,
                                               GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_get_sim_info_error(SIMIPCInterface *sim_ipc,
                                       gpointer ipc_data,
                                       GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}


void smartphone_sim_get_auth_status_error(SIMIPCInterface *sim_ipc,
                                          gpointer ipc_data,
                                          GError *error)
{
    g_debug("%s", __func__);
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_send_auth_code_error(SIMIPCInterface *sim_ipc,
                                         gpointer ipc_data,
                                         GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_unlock_error(SIMIPCInterface *sim_ipc,
                                 gpointer ipc_data,
                                 GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_change_auth_code_error(SIMIPCInterface *sim_ipc,
                                           gpointer ipc_data,
                                           GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_get_imsi_error(SIMIPCInterface *sim_ipc,
                                   gpointer ipc_data,
                                   GError *error)
{
    if (ipc_data)
    {
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
    }
}

void smartphone_sim_get_service_center_number_error(SIMIPCInterface *sim_ipc,
                                                    gpointer ipc_data,
                                                    GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_set_service_center_number_error(SIMIPCInterface *sim_ipc,
                                                    gpointer ipc_data,
                                                    GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_store_message_error(SIMIPCInterface *sim_ipc,
                                        gpointer ipc_data,
                                        GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_retrieve_message_error(SIMIPCInterface *sim_ipc,
                                           gpointer ipc_data,
                                           GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_delete_entry_error(SIMIPCInterface *sim_ipc,
                                       gpointer ipc_data,
                                       GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_store_entry_error(SIMIPCInterface *sim_ipc,
                                      gpointer ipc_data,
                                      GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_retrieve_entry_error(SIMIPCInterface *sim_ipc,
                                         gpointer ipc_data,
                                         GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_send_restricted_sim_command_error(SIMIPCInterface *sim_ipc,
                                                      gpointer ipc_data,
                                                      GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}

void smartphone_sim_send_generic_sim_command_error(SIMIPCInterface *sim_ipc,
                                                   gpointer ipc_data,
                                                   GError *error)
{
    if (ipc_data)
        dbus_g_method_return_error((DBusGMethodInvocation*) ipc_data,
                                   error);
}




DBusSimObject *gsmd_dbus_sim_initialize (SIMIPCInterface *sim_ipc,
                                         SIMInterface *sim)
{
    DBusSimObject *obj = g_object_new (DBUS_SIM_TYPE_OBJECT, NULL);
    sim_ipc->priv = obj;
    obj->sim = sim;

    //Signal
    sim_ipc->auth_status = &smartphone_sim_auth_status;
    sim_ipc->incoming_message = &smartphone_sim_incoming_message;


    //Method replies
    sim_ipc->get_sim_info_reply = &smartphone_sim_get_sim_info_reply;
    sim_ipc->change_auth_code_reply = &smartphone_sim_change_auth_code_reply;
    sim_ipc->unlock_reply = &smartphone_sim_unlock_reply;
    sim_ipc->send_auth_code_reply = &smartphone_sim_send_auth_code_reply;
    sim_ipc->get_auth_status_reply = &smartphone_sim_get_auth_status_reply;
    sim_ipc->get_service_center_number_reply = &smartphone_sim_get_service_center_number_reply;
    sim_ipc->set_service_center_number_reply = &smartphone_sim_set_service_center_number_reply;
    sim_ipc->store_message_reply = &smartphone_sim_store_message_reply;
    sim_ipc->retrieve_message_reply = &smartphone_sim_retrieve_message_reply;
    sim_ipc->get_phonebook_info_reply = &smartphone_sim_get_phonebook_info_reply;
    sim_ipc->get_messagebook_info_reply = &smartphone_sim_get_phonebook_info_reply;
    sim_ipc->delete_entry_reply = &smartphone_sim_delete_entry_reply;
    sim_ipc->store_entry_reply = &smartphone_sim_store_entry_reply;
    sim_ipc->retrieve_entry_reply = &smartphone_sim_retrieve_entry_reply;
    sim_ipc->delete_message_reply = &smartphone_sim_delete_message_reply;
    sim_ipc->send_stored_message_reply = &smartphone_sim_send_stored_message_reply;
    sim_ipc->retrieve_messagebook_reply = &smartphone_sim_retrieve_messagebook_reply;
    sim_ipc->retrieve_phonebook_reply = &smartphone_sim_retrieve_messagebook_reply;
    sim_ipc->send_restricted_sim_command_reply = &smartphone_sim_send_restricted_sim_command_reply;
    sim_ipc->send_generic_sim_command_reply = &smartphone_sim_send_generic_sim_command_reply;
    sim_ipc->get_home_zones_reply = &smartphone_sim_get_home_zones_reply;


    //Method errors
    sim_ipc->get_sim_info_error = &smartphone_sim_get_sim_info_error;
    sim_ipc->change_auth_code_error = &smartphone_sim_change_auth_code_error;
    sim_ipc->unlock_error = &smartphone_sim_unlock_error;
    sim_ipc->send_auth_code_error = &smartphone_sim_send_auth_code_error;
    sim_ipc->get_auth_status_error = &smartphone_sim_get_auth_status_error;
    sim_ipc->get_service_center_number_error = &smartphone_sim_get_service_center_number_error;
    sim_ipc->set_service_center_number_error = &smartphone_sim_set_service_center_number_error;
    sim_ipc->store_message_error = &smartphone_sim_store_message_error;
    sim_ipc->retrieve_message_error = &smartphone_sim_retrieve_message_error;
    sim_ipc->get_phonebook_info_error = &smartphone_sim_get_phonebook_info_error;
    sim_ipc->get_messagebook_info_error = &smartphone_sim_get_phonebook_info_error;
    sim_ipc->delete_entry_error = &smartphone_sim_delete_entry_error;
    sim_ipc->store_entry_error = &smartphone_sim_store_entry_error;
    sim_ipc->retrieve_entry_error = &smartphone_sim_retrieve_entry_error;
    sim_ipc->delete_message_error = &smartphone_sim_delete_message_error;
    sim_ipc->send_stored_message_error = &smartphone_sim_send_message_error;
    sim_ipc->retrieve_messagebook_error = &smartphone_sim_retrieve_messagebook_error;
    sim_ipc->retrieve_phonebook_error = &smartphone_sim_retrieve_messagebook_error;
    sim_ipc->send_restricted_sim_command_error = &smartphone_sim_send_restricted_sim_command_error;
    sim_ipc->send_generic_sim_command_error = &smartphone_sim_send_generic_sim_command_error;
    sim_ipc->send_generic_sim_command_error = &smartphone_sim_send_generic_sim_command_error;
    sim_ipc->get_home_zones_error = &smartphone_sim_get_home_zones_error;


    return obj;
}
