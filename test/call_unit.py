# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import unittest
from gsmd_common import GsmdCommon
import unittest, inspect,time

class CallTests(GsmdCommon):
    '''
    def test_Emergency(self):
        self.fail("Test not implemented yet!")
    def test_Emergency_Fail(self):
        self.fail("Test not implemented yet!")

    def test_SendDtmf(self):
        self.fail("Test not implemented yet!")
    def test_SendDtmf_Fail(self):
        self.fail("Test not implemented yet!")

    def test_SetDtmfDuration(self):
        self.fail("Test not implemented yet!")
    def test_SetDtmfDuration_Fail(self):
        self.fail("Test not implemented yet!")

    def test_GetDtmfDuration(self):
        self.fail("Test not implemented yet!")
    def test_GetDtmfDuration_Fail(self):
        self.fail("Test not implemented yet!")
    '''
    
    def test_GetStatus(self):
        self.incoming_call("+358123456789",0)
        self.ipc.Call_GetStatus(0)
        data = [   ("function", (self.check_call_status_reply, 0, 'incoming', '+358123456789')),
                   ("ensure_error", None), #Error wasn't returned by initiate method
                   ]
        self.perform(data)

    def test_GetStatus_Fail(self):
        self.incoming_call("+358123456789",0)
        self.ipc.Call_GetStatus(1)
        data = [  ("ensure_error_set" ),
                  ]
        self.perform(data)

    def incoming_call(self,number,index):
        data = [   ("write",'RING'),
                   ("write",'+CLIP: "'+str(number)+'",145,"",128,"",0'),
                   ]
        if self.vendor == 'ticalypsoneo1973':
            data.append(("write", "%CPI: "+str(index)+",4,0,1,1,0," + str(number) + ",145,,0xc506,0"))
        elif self.vendor == 'telit862-gps':
            data.append(("write",'#ECAM: '+str(index)+',6,1,,,'))
        else:
            self.fail("Unsupported vendor : '%s'" % self.vendor)

        data.extend([ ("function",(self.check_call_status,index,"incoming",number)),
                      ("reset"),
                      ("write",'RING'),
                      ("write",'+CLIP: "'+str(number)+'",145,"",128,"",0'),
                      ("function",(self.check_call_status,index,"incoming",number)),
                      ] )

        self.perform(data)

    def test_IncomingCall(self):
#        """Test that a incoming call signal is sent when gsmd2 receives a CLIP message."""
        self.incoming_call("+358123456789",0)

    def test_IncomingCallRemoteHangup(self):
#        """Test that a incoming call signal is sent when gsmd2 receives a CLIP message."""
        number = "+358123456789"
        index = 0
        self.incoming_call(number,index)
        if self.vendor == "ticalypsoneo1973":
            data = [ ("write", "%CPI: "+str(index)+",7,1,1,0,0,"+str(number)+",129,,0xc5ff,0"), #(release)
                     ]
        else:
            data = [ ("read", "AT+CLCC"),
                     ("write", '+CLCC: '+str(index+1)+',0,4,0,0,"'+number+'",145,""'), # incoming
                     ("write", "OK"),
                     ("read", "AT+CLCC"),
                     ("write", "OK"),
                     ]

        data.append(("function",(self.check_call_status,index,"released",number) ))
        self.perform(data)


    def test_Initiate(self):
#        """Test Dialing a call and making sure we get a 'connected' signal."""
        number = "12345"
        self.ipc.Call_Initiate(number,"")
        self.perform(self.get_initiate_data(number,0))


    def test_Initiate_Fail(self):
#        """Test the fail cases of dialing."""
        messages = [('ERROR','Unknown'),
                    #('BUSY','Reject'), # not sure if this is correct
                    ('NO CARRIER','Disconnect')]
        #messages = [('ERROR','Unknown')]

        for err in messages:
            self.ipc.Reset()
            self.ipc.Call_Initiate("12345","")
            data = [   ("read","ATD12345;"),
                       ("write",err[0]),
                       ("ensure_error_set",True), #Error wasn't returned by initiate method
                       ]
            self.perform(data)
        print "Error messages done"
        # mine for timeout
        self.ipc.Reset()
        time.sleep(1)
        print "initiating a call"
        self.ipc.Call_Initiate("12345","")
        time.sleep(1)
        data = [   ("read","ATD12345;"),
                   ("write",'TIME_OUT'),
                   ("ensure_reply_set",False), #Dial method didn't return
                   ("ensure_error_set",True), #Error is set
                   ]
        self.perform(data)



    def test_Release_Fail(self):
#        """Test when hanging up an existing call fails."""
        self.test_Initiate()
        self.reset()
        self.ipc.Call_Release("",self.ipc.call_id)

        data = [    ("read","ATH"),
                    ("write","ERROR"),
                    ("reset"),
                    ("read","ATH"),
                    ("write","OK"),
                    ("ensure_reply_set",True),  #Reply is set
                    ("ensure_error_set",False),#error is not set
                    ("function",(self.check_call_status,0,"released")),
                    ]
        self.perform(data)
        time.sleep(2)



    def test_Release(self):
#        """Test hanging up an existing call."""
        self.test_Initiate()
        self.reset()
        self.ipc.Call_Release("",self.ipc.call_id)
        data = [   ("read","ATH"),
                   ("write","OK"),
                   ("ensure_error_set",False),       #error is not set
                   ("ensure_reply_set",True),   #call is set
                   ("function",(self.check_call_status,0,"released"))
                   ]
        self.perform(data)

    def test_RemoteHangup(self):
#        """Test how gsmd2 reacts when the remote end hangs up an existing call."""
        self.test_Accept()
        data = [   ("write", "%CPI: 0,1,1,1,0,0,12345,129,,0x 510,0"),# (disconnect)
                   ("write", "%CPI: 0,1,1,0,0,0,12345,129,,0x 510,0"),# (disconnect)
                   ("write", "NO CARRIER"),
                   ("write", "%CPI: 0,7,1,0,0,0,12345,129,,0x 510,0"),# (release)
                   ("ensure_error_set",False),       #error is not set
                   ("function",(self.check_call_status,0,"released"))
                   ]
        self.perform(data)


    def test_Accept(self):
#        """Test accepting an incoming call."""
        self.test_IncomingCall()
        self.reset()
        self.ipc.Call_Activate(self.ipc.call_id);
        data = [("read","ATA"),
                   ("write","OK"),
                   ("ensure_error_set",False), #error is not set
                   ("ensure_reply_set",True), #dial is set
                   ("function",(self.check_call_status,0,"active"))
                   ]
        self.perform(data)

    def test_Accept_Fail(self):
#        """Test when accepting a call fails."""
        self.test_IncomingCall()
        self.ipc.Call_Activate(0);
        data = [   ("read","ATA"),
                        ("write",'ERROR'),
                        ("ensure_error_set",True), #error not set
                        ("ensure_reply_set",False)] #dial is not set
        self.perform(data)
    
    def test_ActivateWhileCalling(self):
        self.test_Accept()
        self.incoming_call("+358415406978",1)
        self.ipc.Call_Activate(1);
        data = [   ("reset"),
                        ("read","AT+CHLD=2"),
                        ("write",'OK'),
                        ("ensure_error_set",False), #error not set
                        ("ensure_reply_set",True),
                        ("function",(self.check_call_status,0,"held")),
                        ("function",(self.check_call_status,1,"active"))]
        self.perform(data)
        self.ipc.Call_Activate(0);
        data = [   ("reset"),
                        ("read","AT+CHLD=2"),
                        ("write",'OK'),
                        ("ensure_error_set",False), #error not set
                        ("ensure_reply_set",True),
                        ("function",(self.check_call_status,0,"active")),
                        ("function",(self.check_call_status,1,"held"))]
        self.perform(data)
    
    def test_ActivateConference(self):
        self.test_Accept()
        self.incoming_call("+358415406978",1)
        self.ipc.Call_ActivateConference(1);
        data = [   ("reset"),
                        ("read","AT+CHLD=3"),
                        ("write",'OK'),
                        ("ensure_error_set",False), #error not set
                        ("ensure_reply_set",True),
                        ("function",(self.check_call_status,1,"active"))]
        self.perform(data)
        self.incoming_call("+358425406978",2)
        self.ipc.Call_ActivateConference(2);
        data = [   ("reset"),
                        ("read","AT+CHLD=3"),
                        ("write",'OK'),
                        ("ensure_error_set",False), #error not set
                        ("ensure_reply_set",True),
                        ("function",(self.check_call_status,2,"active")),
                        ]
        self.perform(data)
        
    def test_ReleaseHeld(self):
        self.test_Accept()
        self.incoming_call("+358415406978",1)
        self.ipc.Call_ReleaseHeld("");
        data = [   ("reset"),
                        ("read","AT+CHLD=0"),
                        ("write",'OK'),
                        ("ensure_error_set",False), #error not set
                        ("ensure_reply_set",True),
                        ("function",(self.check_call_status,1,"released"))]
        self.perform(data)
        
    def test_ReleaseAll(self):
        self.test_Accept()
        self.incoming_call("+358415406978",1)
        self.ipc.Call_ActivateConference(1);
        data = [   ("reset"),
                        ("read","AT+CHLD=3"),
                        ("write",'OK'),
                        ("ensure_error_set",False), #error not set
                        ("ensure_reply_set",True),
                        ("function",(self.check_call_status,1,"active"))]
        self.perform(data)
        self.incoming_call("+358425406978",2)
        self.ipc.Call_ReleaseAll("");
        data = [   ("reset"),
                        ("read","AT+CHUP"),
                        ("write",'OK'),
                        ("ensure_error_set",False), #error not set
                        ("ensure_reply_set",True),
                        ("function",(self.check_call_status,0,"released")),
                        ("function",(self.check_call_status,1,"released")),
                        ("function",(self.check_call_status,2,"released")),
                        ]
        self.perform(data)
        
    def test_ActivateTwice(self):
        self.test_Accept()
        self.incoming_call("+358415406978",1)
        self.ipc.Call_Activate(1);
        data = [   ("reset"),
                        ("read","AT+CHLD=2"),
                        ("write",'OK'),
                        ("ensure_error_set",False), #error not set
                        ("ensure_reply_set",True),
                        ("function",(self.check_call_status,0,"held")),
                        ("function",(self.check_call_status,1,"active"))]
        self.perform(data)
        self.ipc.Call_Activate(1);
        data = [   ("ensure_error_set",False),("ensure_reply_set",True)]
        self.perform(data)
    

if __name__ == "__main__":
#    import os
#    os.spawnvpe(os.P_WAIT, "ls",("ls", "-l"), os.environ)
    try:
        unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
    except SystemExit:
        pass
    except:
        import traceback, sys
        traceback.print_exc(file=sys.stdout)
        import dbus_thread
        dbus_thread.stop_thread()
        sys.exit(1)
