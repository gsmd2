
#
# Device cdefs
#


cdef extern from "../libfreesmartphone/freesmartphone-device.h":
    ctypedef void (*FSGSMDeviceReplyFunc) (GError *error,
                                      gpointer userdata )
    ctypedef void (*FSGSMDeviceGetFeaturesReplyFunc) (GHashTable *features,
                                                      GError *error,
                                                      gpointer userdata )
    ctypedef void (*FSGSMDeviceGetInfoReplyFunc) (GHashTable *info,
                                                  GError *error,
                                                  gpointer userdata )
    gboolean fso_gsm_device_get_info(FreeSmartphone *fs,
                                     FSGSMDeviceGetInfoReplyFunc cb,
                                     gpointer userdata)

    ctypedef void (*FSGSMDeviceGetAntennaPowerReplyFunc) (gboolean antenna_power,
                                                          GError *error,
                                                          gpointer userdata )

    gboolean fso_gsm_device_get_antenna_power(FreeSmartphone *fs,
                                              FSGSMDeviceGetAntennaPowerReplyFunc cb,
                                              gpointer userdata)

    gboolean fso_gsm_device_set_antenna_power(FreeSmartphone *fs,
                                          gboolean antenna_power,
                                          FSGSMDeviceReplyFunc cb,
                                          gpointer userdata)

    gboolean fso_gsm_device_get_features(FreeSmartphone *fs,
                                     FSGSMDeviceGetFeaturesReplyFunc cb,
                                     gpointer userdata)

    gboolean fso_gsm_device_prepare_to_suspend(FreeSmartphone *fs,
                                               FSGSMDeviceReplyFunc cb,
                                               gpointer userdata)

    gboolean fso_gsm_device_recover_from_suspend(FreeSmartphone *fs,
                                                 FSGSMDeviceReplyFunc cb,
                                                 gpointer userdata)
#
# Device Handlers
#

cdef void device_get_antenna_power_reply(gboolean antenna_power, GError *error, gpointer userdata ) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        reply_cb(reply_data,False,antenna_power)
    Py_DECREF(reply)

cdef void device_reply(GError *error, gpointer userdata ) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str)
    else:
        reply_cb(reply_data,False)
    Py_DECREF(reply)

cdef void device_get_info_reply(GHashTable *info, GError *error, gpointer userdata ) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        reply_cb(reply_data,False,get_hash_table(info))
    Py_DECREF(reply)

#
# PyFreeSmartPhoneDevice class
#

cdef class PyFreeSmartphoneDevice:
    cdef FreeSmartphone *fs

    def __new__(self):
        self.fs = fso_init()
    def deinitialize(self):
        fso_free(self.fs)

#
# Device Functions
#

    def get_antenna_power(self,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_device_get_antenna_power(self.fs,&device_get_antenna_power_reply, <void*>reply_data)

    def get_info(self,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_device_get_info(self.fs,&device_get_info_reply, <void*>reply_data)

    def prepare_to_suspend(self,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_device_prepare_to_suspend(self.fs,&device_reply, <void*>reply_data)

    def recover_from_suspend(self,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_device_recover_from_suspend(self.fs,&device_reply, <void*>reply_data)

