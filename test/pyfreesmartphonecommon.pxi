#
# Common cdefs
#

cdef extern from "string.h":
    int strlen(char*)
    int sizeof(void*)
    void* memset( void* buffer, int ch, int count )

cdef extern from "Python.h":
    void Py_INCREF(object o)
    void Py_DECREF(object o)
    object PyString_FromStringAndSize(char *, int)

cdef extern from "../libfreesmartphone/freesmartphone.h":
    ctypedef struct FreeSmartphone

    FreeSmartphone* fso_init()
    void fso_free(FreeSmartphone*fs)

cdef extern from "glib.h":
    ctypedef (void*) gconstpointer
    ctypedef (void*) gpointer
    ctypedef int gboolean
    ctypedef int gint
    ctypedef int guint
    ctypedef char gchar
    ctypedef int GQuark
    ctypedef int GType
    ctypedef gboolean(*GEqualFunc)(gconstpointer a,gconstpointer b)
    ctypedef guint (*GHashFunc) (gconstpointer key)
    ctypedef void (*GDestroyNotify) (gpointer data)
    ctypedef struct GPtrArray:
        gpointer data
        guint len
    ctypedef struct GHashNode:
        gpointer key
        gpointer value
        GHashNode next
        guint key_hash
    ctypedef struct GHashTable:
        gint size
        gint nnodes
        GHashNode **nodes
        GHashFunc hash_func
        GEqualFunc key_equal_func
        gint ref_count
        GDestroyNotify key_destroy_func
        GDestroyNotify   value_destroy_func
    ctypedef struct GValue:
            pass
    ctypedef struct GArray:
            gchar *data
            guint len
    ctypedef struct GError:
            GQuark       domain
            gint         code
            gchar       *message
    ctypedef struct GList:
            gpointer data
            GList *next
            GList *prev
    (void*) g_array_index(GArray *a,void *t,int i)
    void g_error_free(GError *error)
    GList* g_hash_table_get_keys(GHashTable *hash_table)
    GList* g_hash_table_get_values(GHashTable *hash_table)
    GList* g_list_next(GList *list)
    void g_list_free(GList *list)
    GType G_VALUE_TYPE(GValue *value)
    gboolean g_value_get_boolean(GValue *value)
    gint g_value_get_int(GValue *value)
    gchar* g_value_get_string(GValue *value)
    GValue* g_value_init (GValue *value, GType g_type)
    void g_value_set_boxed (GValue *value, gpointer v_boxed)
    GType G_TYPE_INT
    GType G_TYPE_STRING
    GType G_TYPE_INVALID
    GQuark g_quark_from_static_string(gchar *str)
    guint G_MAXUINT
    gpointer g_ptr_array_index(GPtrArray *array, gint index)
    void g_free(gpointer p)

cdef extern from "dbus/dbus-glib.h":
    char* dbus_g_error_get_name(GError *error)
    gboolean dbus_g_type_struct_get (GValue *value, guint member, ...)
    GType dbus_g_type_get_struct (char *container, GType first_type, ...)
    gint DBUS_GERROR_REMOTE_EXCEPTION


cdef class ReplyData:
    cdef void *cb
    cdef void *data
    def __new__(self, cb, data):
        self.cb = <void*>cb
        self.data = <void*>data
#    def __dealloc__(self):
#        print "Dealloc"
    def get_cb(self):
        return <object>self.cb
    def get_data(self):
        return <object>self.data

cdef object get_error_string(GError *error):
        cdef char *name
        if error.code == DBUS_GERROR_REMOTE_EXCEPTION:
            name = <char*>dbus_g_error_get_name (error)
            error_name = PyString_FromStringAndSize(name, strlen(name))
            error_message = PyString_FromStringAndSize(error.message, strlen(error.message))
            g_error_free( error )
            return error_name + ": " + error_message
        else:
            return "DBUS_GERROR: code = " + error.code

cdef object get_hash_table(GHashTable *table):
    result = {}

    cdef GList* keys
    cdef GList* values
    cdef GList* keys_it
    cdef GList* values_it
    cdef GType* type
    cdef gboolean b
    cdef gint i
    cdef GType type
    cdef GValue* value
    cdef gchar* str
    keys = g_hash_table_get_keys(table)
    values = g_hash_table_get_values(table)
    keys_it = keys
    values_it = values

    while keys_it and values_it:
        key = <char*>keys_it.data
        key_str = PyString_FromStringAndSize(key, strlen(key))
        value = <GValue*>values_it.data
        type = G_VALUE_TYPE(value)
        if type == 24:
            result[key_str] = g_value_get_int(value)
        elif type == 20:
            result[key_str] = g_value_get_boolean(value)
        else:
            str =<char*>g_value_get_string(value)
            value_str = PyString_FromStringAndSize(str, strlen(str))
            result[key_str] = value_str

        keys_it = g_list_next(keys_it)
        values_it = g_list_next(values_it)

    g_list_free(keys)
    g_list_free(values)

    return result

cdef object get_int_array(GArray *array):
    result = []
    len = array.len
    print "array len: %d" % len
    for i in range(0,len):
        integer = (<int*>array.data)[i]
        print "array int: %d" % integer
        result.append(integer)

    return result

cdef object get_string(gchar *str):
    return unicode(PyString_FromStringAndSize(str, strlen(str)), 'utf-8')
