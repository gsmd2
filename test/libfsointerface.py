# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen
from ipc_interface import IPCInterface
import threading, time
import pyfreesmartphone
import gobject

def fso_callback(data,error,*arg):
    if error:
        data.ErrorHandler(error)
    else:
        data.ReplyHandler(arg)

def call_status_signal(data, id, status, properties):
    data.AddSignal(("CallStatus",id, status, properties))
    data.call_id = id

def network_status_signal(data, status):
    data.AddSignal(("NetworkStatus",status))

def sms_incoming_message_signal(data,index):
    data.message_index = index
    data.AddSignal(("IncomingMessage",index))
def sms_message_sent_signal(data,success,reason):
    data.AddSignal(("MessageSent",success,reason))

def sim_auth_status_signal(data, id, status):
    data.AddSignal(("AuthStatus",status))


class FSOGMainLoopThread(threading.Thread):
    def __init__ (self):
        threading.Thread.__init__(self)
        self.loop = None

    def stop(self):
        if self.loop:
            self.loop.quit()

    def run(self):
        self.loop = gobject.MainLoop()
        self.loop.run()
        self.loop = None


'''
def dial_callback(id, error, data):
    print str(data)
    if error:
        print "Got error"
        data.error = True
        data.ErrorHandler(error)
    else:
        data.call_id = int(id)
        print "Got call id " + str(id)
        data.ReplyHandler(id)
    data.reply_event.set()
'''
class FSOInterface(IPCInterface):
    def Initialize(self):
        IPCInterface.Initialize(self)
        self.mainloop_thread = None
        self.start_thread()

        self.fs_call = pyfreesmartphone.PyFreeSmartphoneCall()
        self.fs_sms = pyfreesmartphone.PyFreeSmartphoneSMS()
        self.fs_sim = pyfreesmartphone.PyFreeSmartphoneSIM()
        self.fs_device = pyfreesmartphone.PyFreeSmartphoneDevice()
        self.fs_network = pyfreesmartphone.PyFreeSmartphoneNetwork()

        self.fs_sim.auth_status_signal(sim_auth_status_signal, self)
        self.fs_call.status_signal(call_status_signal, self)
        self.fs_sms.incoming_message_signal(sms_incoming_message_signal, self)
        self.fs_sms.message_sent_signal(sms_message_sent_signal, self)
        self.fs_network.status_signal(network_status_signal, self)
        
    def DeInitialize(self):
        IPCInterface.DeInitialize(self)
        self.stop_thread()
        self.fs_call.status_signal_remove()
        self.fs_sim.auth_status_signal_remove()
        self.fs_sms.incoming_message_signal_remove()
        self.fs_sms.message_sent_signal_remove()
        self.fs_network.status_signal_remove()
        #self.fs.deinitialize()
        
        
    
    #Call interface
    def Call_Activate(self, id):
        self.fs_call.activate(id, fso_callback, self)
    def Call_ActivateConference(self, id):
        self.fs_call.activate_conference(id, fso_callback, self)
    def Call_Release(self,message,id):
        self.fs_call.release(message, id, fso_callback, self)
    def Call_ReleaseHeld(self,message):
        self.fs_call.release_held(message, fso_callback, self)
    def Call_ReleaseAll(self,message):
        self.fs_call.release_all(message, fso_callback, self)
    def Call_Initiate(self,number,type):
        self.fs_call.initiate(number,fso_callback, self)
    def Call_ListCalls(self):
        self.fs_call.list_calls(fso_callback, self)
    def Call_GetStatus(self,id):
        self.fs_call.get_call_status(id,fso_callback, self)

    #Device interface
    def Device_GetInfo(self):
        self.fs_device.get_info(fso_callback, self)
    def Device_GetAntennaPower(self):
        self.fs_device.get_antenna_power(fso_callback, self)
    def Device_PrepareToSuspend(self):
        self.fs_device.prepare_to_suspend(fso_callback, self)
    def Device_RecoverFromSuspend(self):
        self.fs_device.recover_from_suspend(fso_callback, self)

    #Network interface
    def Network_Register(self):
        self.fs_network.register(fso_callback, self)
    def Network_GetStatus(self):
        self.fs_network.get_status(fso_callback, self)
    def Network_ListProviders(self):
        self.fs_network.list_providers(fso_callback, self)

    #SMS interface
    def SMS_SendMessage(self,message,number,want_report):
        self.fs_sms.send_message(message, number,want_report,fso_callback, self)

    #SIM interface
    def SIM_GetAuthStatus(self):
        self.fs_sim.get_auth_status(fso_callback, self)
    def SIM_SendAuthCode(self,pin):
        self.fs_sim.send_auth_code(pin, fso_callback, self)
    def SIM_Unlock(self,puk,new_pin):
        self.fs_sim.unlock(puk,new_pin, fso_callback, self)
    def SIM_ChangeAuthCode(self,old_pin,new_pin):
        self.fs_sim.change_auth_code(old_pin,new_pin, fso_callback, self)
    def SIM_GetSimInfo(self):
        self.fs_sim.get_sim_info(fso_callback, self)
    def SIM_ListMessages(self,status):
        self.fs_sim.list_messages(status, fso_callback, self)
    def SIM_RetrieveMessage(self,index):
        self.fs_sim.retrieve_message(index, fso_callback, self)
    def SIM_GetPhonebookInfo(self):
        self.fs_sim.get_phonebook_info(fso_callback, self)
    def SIM_DeleteEntry(self,index):
        self.fs_sim.delete_entry(index,fso_callback, self)
    def SIM_StoreEntry(self,index,name,number):
        self.fs_sim.store_entry(index,name, number,fso_callback, self)
    def SIM_RetrieveEntry(self,index):
        self.fs_sim.retrieve_entry(index,fso_callback, self)

    #Mainloop
    def stop_thread(self):
        if not self.mainloop_thread:
            return
        self.mainloop_thread.stop()
        while self.mainloop_thread.loop != None:
            time.sleep(0.1)
        self.mainloop_thread = None


    def start_thread(self):
        self.mainloop_thread = FSOGMainLoopThread()
        self.mainloop_thread.start()




