# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import sys
import sys, string
from math import log
import os, time, subprocess, signal, termios, utils, thread
import utils
from dbus_interface import DBUSInterface
from ui import gsmd2ui

class modemui(gsmd2ui):
    #Testing that we generate a proper pdu using one that we know to be valid
    def test_sms_send(self):
        original = "07917283010010F5040BC87238880900F10000993092516195800AE8329BFD4697D9EC37"
        generated = self.generate_sms_pdu("27838890001","hellohello")

        if (original != generated):
            print "SMS pdu's don't match"
            print "Original:  "+original
            print "Generated: "+generated
            quit()
        else:
            print "They match"
            quit()

    #Initialize
    def init(self):
        print "modemui"
        
    def preloop(self):
        master, slave = os.openpty()
        new = termios.tcgetattr(master)
        new[0] = termios.IGNCR
        new[1] = termios.ONLRET
        new[3] = new[3] & ~termios.ICANON & ~termios.ECHO
        new[6][termios.VMIN] = 1
        new[6][termios.VTIME] = 0
        termios.tcsetattr(master, termios.TCSANOW, new)
        
        self.port = utils.DummyFile(master)
        cmd = "gsmd2 --no-conf -n -d "+os.ttyname(slave)#+"> /dev/null 2>&1 "
        self.process = subprocess.Popen(cmd, shell=True);
        os.close(slave)
        
        #wait for gsmd2 to start
        time.sleep(2)
        
        self.ipc = DBUSInterface()
        self.ipc.Initialize()
        self.do_init_defaults("")
        
        thread.start_new_thread(self.background,(self.port,self.printat))

    #Cleanup
    def postloop(self):
        print "Shutting down"
        self.port.close()
        os.kill(self.process.pid, signal.SIGTERM)
        #process.wait()
        try:
            os.kill(self.process.pid+1, signal.SIGTERM)
        except:
            pass

    #Helper function to reply OK
    def writeok(self):
        self.port.writeOK()

    #Function that writes data to virtual serial port
    def write(self,line):
        if self.printat and (not line.startswith("+CSQ") or self.print_signal_strength):
            print '>>'+line
        self.port.write_reply(line)

    #Function that writes a reply to
    def write_dial_reply(self,type):
        if type > 7:
            self.write('NO CARRIER')
        else:
            self.write('ok')
            self.write('#ECAM: 0,'+str(type)+',1,,,')

    #Function to be called when replying to gsm modem's atd command
    def dial_reply(self):
        self.writeok()
        self.send_ecam(self.dial)
            
    def send_ecam(self,value):
        if value == 3:
            self.write_dial_reply(1)
            self.write_dial_reply(2)
            self.write_dial_reply(3)
        else:
            self.write_dial_reply(value)
            

    #Function to be called when replying to gsm modem's at+csq command
    def signal_reply(self):
        self.write("+CSQ: "+str(self.signal).replace(".",","))
        self.writeok()

    def network_registration_reply(self):
        self.write("+CREG: 0,"+str(self.creg))
        self.writeok() #this was in the 0705 document's examples, not sure if it's needed

    def pin_query_reply(self):
        self.write("+CPIN: "+str(self.pin))
        self.write("OK")

    '''
    AT+COPS?
    +COPS: 0,2,"35804"
    '''
    def current_operator_query_reply(self):
        if self.operators.has_key(self.operator):
            self.write('+COPS: 0,'+self.operators[self.operator][2]+',"'+self.operators[self.operator][1]+'"')
        else:
            self.write('+COPS: 0,2,"DnaFinland"')
        self.writeok()

        '''
        AT+COPS=?
        +COPS: (1,"vodafone NL",,"20404"),(3,"O2 - NL",,"35812"),
        (3,"NL KPN",,"20408"),(3,"Orange NL",,"35820"),
        (3,"T-Mobile NL",,"20416"),,(0,1),(2)

        OK
        '''



    def get_operators(self):
        result = ""
        for operator in self.operators:
            result += '('+self.operators[operator][0]+',"'+self.operators[operator][1]+'",,"'+self.operators[operator][2]+'"),'
        return result[0:len(result)-1] #remove last ","

    def available_operator_query_reply(self):
        self.write('+COPS: '+self.get_operators())
        self.writeok()

    def pduencode(self,message):
        bits = ""        
        i = len(message) - 1
        while i >= 0:
            bits = bits + self.to7bitbinary(ord(message[i]))
            i -= 1
        
        result = ""
        length = len(bits)
        (num, x) = divmod(length, 8)
        
        for i in range(0, num + 1):
            value = 0
            for j in range(0, 8):
                offset = length - (8*i) - j - 1
                if (offset >= 0):
                    value+=int(bits[offset])*pow(2,j)
            result += "%02x" % (value)
        return(result.upper())

    
    def to7bitbinary(self,number):
        result = ['0','0','0','0','0','0','0']
        i=6
        while i >= 0:
            if number & pow(2,i):
                result[6-i] = "1"
                number -= pow(2,i)
            i-=1
            
        return "".join(result)
    
    
    def hexlen(self,message,octets):
        length = len(message)
        if octets:
            length = length /2*8/7 #convert it to length of octets
        return ("%02x" % (length)).upper()

    def swap_pairs(self,message):
        if len(message)%2:
            message+="F"
        result = ""
        for i in range(0,len(message)/2):
            result += message[i*2+1]
            result += message[i*2]
        return result

    def generate_sms_pdu(self,number,message):
        message = self.pduencode(message)
        return "07917283010010F504"+self.hexlen(number,0)+"C8"+self.swap_pairs(number)+"000099309251619580"+self.hexlen(message,1)+message
        #return "07917283010010F5040BC87238880900F10000993092516195800AE8329BFD4697D9EC37"

    def send_sms_reply(self):
        self.write(">")
        
    def read_sms_reply(self):
        index = int(self.command_param)
        number = "044421421421"
        message = "hello sailor"
        status = "0"

        if self.sms.has_key(index):
            number=self.sms[index][0]
            status = self.sms[index][2]
            message = self.sms[index][1]
        else:
            print "No message found, with index "+str(index)+" sending the default"
            print "indexes are: "
            for index in self.sms:
                print index

        print "Sending message "+message+" from "+number
        pdu = self.generate_sms_pdu(number,message)
        #self.write('+CMGR:'+status+',,'+str(len(pdu)/2)+'\r\n'+pdu)
        self.write('+CMGR:'+status+',,'+str(len(pdu)/2)+'\r\n'+pdu)
        self.writeok()



    #Removes extra parameters (like phone number) from at commands
    def convert_command(self,line):
        self.command_param = ""
        line = line.rstrip('\r\n')
        if (line.startswith("ATD")):
            return "DIAL"

        if line.startswith("AT+CMGR="):
            self.command_param = line.partition("AT+CMGR=")[2]
            return "READ_SMS"
        if line.startswith("AT+CMGS="):
            self.command_param = line.partition("AT+CMGS=")[2]
            return "SEND_SMS"
        return line

    def imei_reply(self):
        self.write("123456789012345")
        self.writeok()
    def manufacturer_reply(self):
        self.write("telit")
        self.writeok()
        
    #replies to commands sent by gsmd2
    def reply(self,line):
        try:
            {"AT+CSQ": self.signal_reply,
                "DIAL": self.dial_reply,
                "AT+CREG?" :self.network_registration_reply,
                "AT+CPIN?" :self.pin_query_reply,
                "AT+COPS=?" :self.available_operator_query_reply,
                "AT+COPS?" :self.current_operator_query_reply,
                "SEND_SMS" :self.send_sms_reply,
                "AT+CGSN" :self.imei_reply,
                "AT+CGMI" :self.manufacturer_reply,
                "READ_SMS" :self.read_sms_reply}[line]()


        except KeyError:
            print 'No reply found for "'+line+'", replying with OK'
            self.writeok()


    #Thread function
    def background(self,port,printat):
        while 1:
            try:
                line = self.port.readline()                               #Read
                if self.printat and (not line.startswith("AT+CSQ") or self.print_signal_strength):
                    print '<<' + line
                self.reply(self.convert_command(line))  #Reply
            except:
                time.sleep(0.1)                                     #Or wait


    #Commands
    def do_write(self,message):
        self.write(message)
    def help_write(self):
        print "Writes custom data to gsmd2."
    def do_ok(self,message):
        self.writeok()
    def help_ok(self):
        print "Sends ok to gsmd2"
    def do_print_signal_strength(self,message):
        try:
            self.print_signal_strength = int(message)
        except ValueError:
            print "Wrong syntax, refer to help"
    def help_print_signal_strength(self):
        print "Set if gsm signal strength signals should be printed"
    def do_print_at_commands(self,message):
        try:
            self.printat = int(message)
        except ValueError:
            print "Wrong syntax:"
            self.help_print_at_commands()
    def help_print_at_commands(self):
        print "Sets if at commands (both received and sent) should be printed. Usage: print_at_commands 1/0."
    def do_print_signals(self,message):
        try:
            self.ipc.debug = int(message)
        except ValueError:
            print "Wrong syntax:"
            self.help_print_signals()
    def help_print_signals(self):
        print "Sets if received dbus signals should be printed. Usage: print_signals 1/0."
    def help_no_carrier(self):
        print 'Sends "NO CARRIER" message to gsmd2. This can be used to stop a call.'
    def do_no_carrier(self,message):
        self.write('NO CARRIER')

    def do_set_operator(self,message):
        words = message.split()
        try:
            if len(words) > 3:
                self.operators[int(words[0])] = (words[1],words[2],words[3])
            else:
                print "Not enough parameters specified, refer to help."
        except:
            print "Invalid parameters specified, refer to help."
    def help_set_operator(self):
        print "Sets available operator. Usage: set_operator <index> <availability> <name> <operator code>"
        print "Index is just the operator's index in memory."
        print "Availability is one of these: 0 (unknown), 1 (available), 2 (current), 3 (forbidden)"
        print "Name is just a name"
        print "Operator code: numeric 5 digits [country code (3) + network code (2)]"
    def do_list_operators(self,message):
        print "Available operators."
        print "Index - Availability, Name, operator code"
        for operator in self.operators:
            print str(operator)+" - "+self.operators[operator][0]+", "+self.operators[operator][1]+", "+self.operators[operator][2]
    def help_list_operators(self):
        print "Prints a list of operators. Usage list_operators."

    def do_list_sms(self,message):
        print 'Sms messages in "memory".'
        print "index - number, status, message"
        for index in self.sms:
            print str(index)+" - "+self.sms[index][0]+", "+self.sms[index][2]+", "+self.sms[index][1]
    def help_list_sms(self):
        print 'Prints a list of sms messages in "memory". Usage list_operators.'

    def help_set_current_operator(self):
        print "Sets current operator used by this virtual modem. Usage: set_operator <index>. Refer to list_operators command for available operator indexes"
    def do_set_current_operator(self,message):
        try:
            index = int(message)
            if self.operators.has_key(index):
                self.operator = index
            else:
                print "No operator with given index found"
        except:
            print "Specify a number"


    def do_set_sms(self,message):
        words = message.split()
        if len(words) >= 4:
            self.sms[int(words[0])] = (words[1],message.rpartition(words[2]+' ')[2],words[2])
        else:
            print "Not enough parameters specified. Refer to help"
    def help_set_sms(self):
        print "Adds or modifies an existing sms in sms array (actually it's a dictionary). Usage: set_sms <index> <number> <status> <message>."
        print 'Example: set_sms 0 089424242 0 hello sailor'
        print "Status codes: 0 (new message), 1 (read message), 2 (stored message not yet sent), 3 (stored message already sent)"

    def do_incoming_sms(self,message):
        words = message.split()
        if len(words) >= 2:
            self.write('+CMTI: "'+words[0]+'", '+words[1])
        else:
            self.write('+CMTI: "SM", 0')
    def help_incoming_sms(self):
        print "Tells gsmd2 that a new sms message has arrived. Syntax: incoming_sms [SM/ME] [index]"
        print "SM and ME are memory storages, index is the index of the message in specified storage. If no parameters are specified, SM 1 are sent as default."


    def do_accept(self,message):
        self.send_ecam(3)
        
    def help_accept(self):
        print 'Accepts an incoming phonecall'
    def do_hangup(self,message):
        self.write_dial_reply(8)
    def help_hangup(self):
        print 'Hangs up an incoming phonecall'
    def do_busy(self,message):
        self.write('BUSY')
    def help_busy(self):
        print 'Sends "BUSY" message to gsmd2. This can be used to signal gsmd2 that we don\'t want to accept a call.'
    def do_creg_reply_set(self,message):
        replies = {'not_registered_not_searching': 0, 'home': 1, 'not_registered_searching' : 2,'registration_denied' :3,'unknown':4,'roaming':5}
        if replies.has_key(message):
            self.creg = replies[message]
            print "Setting reply "+str(replies[message])
        else:
            print 'Reply "'+message+'" not found. Refer to help.'
    def help_creg_reply_set(self):
        print 'Sets the default reply to creg command. Available reply types are: "not_registered_not_searching" (0), "home" (1) "not_registered_searching" (2),"registration_denied" (3),"unknown" (4), "roaming" (5)'
        print "Usage: creg_reply_set not_registered_not_searching/home/not_registered_searching/registration_denied/unknown/roaming"

    def help_pin_reply_set(self):
        print "Sets pin reply message. Reply is one of the following: READY,SIM PIN,SIM PUK,PH-SIM PIN,PH-FSIM PIN,PH-FSIM PUK,SIM PIN2,SIM PUK2,PH-NET PIN,PH-NET PUK,PH-NETSUB PIN,PH-NETSUB PUK,PH-SP PIN,PH-SP PUK,PH-CORP PIN,PH-CORP PUK"
        print "Usage: pin_reply_set READY"
    def do_pin_reply_set(self,message):
        replies = ['READY','SIM PIN','SIM PUK','PH-SIM PIN','PH-FSIM PIN','PH-FSIM PUK','SIM PIN2','SIM PUK2','PH-NET PIN','PH-NET PUK','PH-NETSUB PIN','PH-NETSUB PUK','PH-SP PIN','PH-SP PUK','PH-CORP PIN','PH-CORP PUK']
        try:
            replies.index(message)
            self.pin = message
        except ValueError:
            print "Specified pin reply not found."
            self.help_pin_reply_set()
            
    def help_creg_reply_set(self):
        print 'Sets the default reply to creg command. Available reply types are: "not_registered_not_searching" (0), "home" (1) "not_registered_searching" (2),"registration_denied" (3),"unknown" (4), "roaming" (5)'
        print "Usage: creg_reply_set not_registered_not_searching/home/not_registered_searching/registration_denied/unknown/roaming"

    def do_idle(self,message):
        self.write_dial_reply(0)
    def help_idle(self):
        print "Sends idle ecam message to gsm modem"
        
    def do_calling(self,message):
        self.write_dial_reply(1)
    def help_calling(self):
        print "Sends calling ecam message to gsm modem"
        
    def do_connecting(self,message):
        self.write_dial_reply(2)
    def help_connecting(self):
        print "Sends connecting ecam message to gsm modem"
        
    def do_active(self,message):
        self.write_dial_reply(3)
    def help_active(self):
        print "Sends active ecam message to gsm modem"
        
    def do_hold(self,message):
        self.write_dial_reply(4)
    def help_hold(self):
        print "Sends hold ecam message to gsm modem"
        
    def do_waiting(self,message):
        self.write_dial_reply(5)
    def help_waiting(self):
        print "Sends waiting ecam message to gsm modem"
        
    def do_alerting(self,message):
        self.write_dial_reply(6)
    def help_alerting(self):
        print "Sends alerting ecam message to gsm modem"
        
    def do_busy(self,message):
        self.write_dial_reply(7)
    def help_busy(self):
        print "Sends busy ecam message to gsm modem"
        
    def do_dial_reply_set(self,message):
        replies = {'idle': 0, 'calling': 1, 'connecting' : 2,'active' :3,'hold':4,'waiting':5,'alerting':6,'busy':7}
        if replies.has_key(message.lower()):
            self.dial = replies[message.lower()]
        else:
            self.dial = 3 #by default we start the call
    def help_dial_reply_set(self):
        print "Sets the default reply to dial command. Available reply types are: idle,calling, connecting,active,hold,waiting,alerting,busy,carrier."
        print "Usage: dial_reply_set connecting/active/hold/waiting/alerting/busy/carrier"

    def do_dial_reply_write(self,message):
        if (len(message) > 0):
            self.do_dial_reply_set(message)
        self.write_dial_reply(self.dial)
    def help_dial_reply_write(self):
        print "Writes a dial reply. If reply type is specified, uses it. Otherwise uses default reply. Usage: dial_reply_write [idle/calling,connecting,active,hold,waiting,alerting,busy]"
    def do_incoming_call(self,message):
        if (len(message) <= 0):
            self.write('+CLIP: "+35840123456789",145,"",128,"",0')
        else:
            self.write('+CLIP: "'+message+'",145,"",128,"",0')
    def help_incoming_call(self):
        print "Initiates an incoming call to the gsm. Calling number can also be specified. Usage: incoming_call [040123456789]"

    def do_quit(self,message):
        self.postloop()
        quit()

    def help_quit(self):
        print "Quits this program"
    def help_help(self):
        print "To get help on commands type help <command>"

    def do_set_signal(self,message):
        try:
            value = float(message);
            if ((value >= 0 and value <= 31) or value == 99):
                self.signal = value
            else:
                print 'Signal value must be between 0-31 or 99.'
        except ValueError:
            print 'Invalid signal value, it must be between 0-31 or 99. Decimal separator must be "."'
    def help_set_signal(self):
        print "Sets signal level. Usage: set_signal 25.0"



    def help_init_defaults(self):
        print "Initializes default values. Usage: init_defaults"
    def do_init_defaults(self,message):
        self.print_signal_strength = 1
        self.ipc.debug = 1
        self.signal = 25.0
        self.dial = 3
        self.printat = 1
        self.creg = 1
        self.pin = "READY"
        self.sms = {0:('044123456','Modem ui test message','0'), 1:('27838890001','hellohello','0')}
        self.operator = 0
        self.operators = { 0 : ('0','Sonera','35800'),
                                1 : ('1','DNA','35801'),
                                2 : ('2','Elisa','35802'),
                                3 : ('3','Saunalahti','35803')}

    

if __name__ == "__main__":
    ui = modemui()
    ui.cmdloop()
    #ui.test_sms_send()

