# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import unittest
from gsmd_common import GsmdCommon
import unittest

class PhonebookTests(GsmdCommon):
    '''
    def test_GetStorageInfo(self):
        self.fail("Test not implemented")
    def test_GetStorageInfo_Fail(self):
        self.fail("Test not implemented")
    '''    
    def setUp(self):
        GsmdCommon.setUp(self)
    
#     def test_GetDefaultStorageSpace(self):
#         '''
#         Test getting default storage space
        
#         Example communication:
#         AT+CPBS?
        
#         +CPBS: "SM",2,250
        
#         OK
#         '''
#         self.ipc.SIM_GetDefaultStorageSpace()
#         data = [    ("read",'AT+CPBS?'),
#                         ("write",'+CPBS: "SM",2,250'),
#                         ("write",'OK'),
#                         ("ensure_error_set",False), #Ensure no reply
#                         ("ensure_reply",'SM')]
#         self.perform(data)
        
#     def test_GetDefaultStorageSpace_Cache(self):
#         '''Test that default storage space is cached when queried'''
#         self.test_GetDefaultStorageSpace()
#         self.reset()
#         self.ipc.SIM_GetDefaultStorageSpace()
#         self.perform([("ensure_reply",'SM')]) #Make sure we get a reply without asking twice
        
#     def test_GetDefaultStorageSpace_SetCache(self):
#         '''Test that default storage space is cached when it is set'''
#         self.test_GetDefaultStorageSpace_Cache()
#         self.reset()
#         self.SetDefaultStorageSpace("TEST")
#         self.perform([("ensure_reply_set",True)]) #Make sure we get a reply without asking
#         self.reset()
#         self.ipc.SIM_GetDefaultStorageSpace()
#         self.perform([("ensure_reply",'TEST')]) #Make sure we get a reply without asking

#     def test_GetDefaultStorageSpace_Fail(self):
#         '''
#         Test failing to get default storage space
        
#         Example communication:
#         AT+CPBS?
        
#         +CPBS: "SM",2,250
        
#         OK
#         '''
#         self.ipc.SIM_GetDefaultStorageSpace()
#         data = [    ("read",'AT+CPBS?'),
#                         ("write",'ERROR'),
#                         ("ensure_error_set",True), #Ensure error was returned
#                         ("ensure_reply_set",False)] #Ensure that no reply was received
#         self.perform(data)
    
#     def SetDefaultStorageSpace(self,space):
#         '''Set Default storage space'''
#         self.ipc.SIM_SetDefaultStorageSpace(space)
#         data = [    ("read",'AT+CPBS='+str(space)),
#                         ("write",'OK'),
#                         ("ensure_error_set",False), #Ensure no reply
#                         ("ensure_reply_set",True)] #Ensure that we got a reply
#         self.perform(data)
        
#     def test_SetDefaultStorageSpace(self):
#         '''
#         Test setting default storage space
        
#         Example communication:
#         AT+CPBS=SM
        
#         OK
#         '''
#         self.SetDefaultStorageSpace("SM")
        
#     def test_SetDefaultStorageSpace_Fail(self):
#         '''
#         Test failing to set default storage space
        
#         Example communication:
#         AT+CPBS=SM
        
#         OK
#         '''
#         self.ipc.SIM_SetDefaultStorageSpace("SM")
#         data = [    ("read",'AT+CPBS=SM'),
#                         ("write",'ERROR'),
#                         ("ensure_error_set",True), #Ensure error was returned
#                         ("ensure_reply_set",False)] #Ensure that no reply was received
#         self.perform(data)
    

#     def GetIndexBoundaries(self,low,high):
#         ''' Get index boundaries '''
#         self.ipc.SIM_GetIndexBoundaries()
#         data = [    ("read",'AT+CPBR=?'),
#                         ("write",'+CPBR: ('+str(low)+'-'+str(high)+'),20,20'),
#                         ("write",'OK'),
#                         ("ensure_error_set",False), #Ensure no reply
#                         ("ensure_reply",(low,high))] #Ensure that we got a reply
#         self.perform(data)
        
#     def test_GetIndexBoundaries(self):
#         '''
#         Test getting index boundaries
        
#         Example communication:
#         AT+CPBR=?
        
#         +CPBR: (1-250),20,20
        
#         OK
#         '''
#         self.GetIndexBoundaries(1,250)
        
    
#     def test_GetIndexBoundaries_Cache(self):
#         '''Make sure that index boundaries are cached'''
#         self.test_GetIndexBoundaries()
#         #Make sure the values were cached
#         self.reset()
#         self.ipc.SIM_GetIndexBoundaries()
#         self.perform([("ensure_reply",(1,250))])
        
#     def test_GetIndexBoundaries_ClearCache(self):
#         '''Make sure that after storage space has changed index boundary cache is reset'''
#         self.test_GetIndexBoundaries()
#         self.reset()
#         self.SetDefaultStorageSpace("Another")
#         self.reset()
#         self.GetIndexBoundaries(1,20)
        
#     def test_GetIndexBoundaries_Fail(self):
#         '''
#         Test failing to get index boundaries
        
#         Example communication:
#         AT+CPBR=?
        
#         +CPBR: (1-250),20,20
        
#         OK
#         '''
#         self.ipc.SIM_GetIndexBoundaries()
#         data = [    ("read",'AT+CPBR=?'),
#                         ("write",'ERROR'),
#                         ("ensure_error_set",True), #Ensure error was returned
#                         ("ensure_reply_set",False)] #Ensure that no reply was received
#         self.perform(data)
    

    
    def test_GetPhonebookInfo(self):
        self.ipc.SIM_GetPhonebookInfo()
        data = [    ("read",'AT+CPBR=?'),
                    ("write", "+CPBR: (1-250),20,20"),
                    ("write",'OK'),
                    ("read", "AT+CPBS?"),
                    ("write", "+CPBS: \"SM\",2,250"),
                    ("write",'OK'),
                    ("ensure_error_set",False),
                    ("ensure_reply",{'slots' : 250, 'used' : 2, 'number_length': 20, 'name_length' : 20})
                    ]
        self.perform(data)
    def test_DeleteEntry(self):
#         Test deleting an entry from the phonebook.

#         Example communication
#         AT+CPBW=2

#         OK

        self.ipc.SIM_DeleteEntry(2)
        data = [    ("read",'AT+CPBW=2'),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure no reply
                        ("ensure_reply_set",True)] #Ensure that we got a reply
        self.perform(data)
        
    def test_DeleteEntry_Fail(self):
#         Test deleting an entry from the phonebook.
        
#         Example communication
#         AT+CPBW=2
        
#         OK
        self.ipc.SIM_DeleteEntry(2)
        data = [    ("read",'AT+CPBW=2'),
                        ("write",'ERROR'),
                        ("ensure_error_set",True), #Ensure error was returned
                        ("ensure_reply_set",False)] #Ensure that no reply was received
        self.perform(data)
    
    def test_StoreEntry(self):
        '''
        Test storing an entry to the phonebook.
        
        Example communication
        AT+CPBW=2,+35844123456789,145,Name
        
        OK
        '''
        self.ipc.SIM_StoreEntry(1,'Name','+35844123456789')
        data = [    ("read",'AT+CPBW=1,+35844123456789,145,Name'),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure no reply
                        ("ensure_reply_set",True)] #Ensure that we got a reply
        self.perform(data)
        self.reset()
        self.ipc.SIM_StoreEntry(1,'Name','044123456789')
        data = [    ("read",'AT+CPBW=1,044123456789,129,Name'),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure no reply
                        ("ensure_reply_set",True)] #Ensure that we got a reply
        self.perform(data)
        
    def test_StoreEntry_Fail(self):
        '''
        Test failing to store an entry to the phonebook.
        
        Example communication
        AT+CPBW=2,+35844123456789,145,Name
        
        OK
        '''
        self.ipc.SIM_StoreEntry(1,'Name','+35844123456789')
        data = [    ("read",'AT+CPBW=1,+35844123456789,145,Name'),
                        ("write",'ERROR'),
                        ("ensure_error_set",True), #Ensure error was returned
                        ("ensure_reply_set",False)] #Ensure that no reply was received
        self.perform(data)
        
        
    def test_RetrieveEntry(self):
        '''
        Test reading a phonebook entry that has the phonenumber in national format
        AT+CPBR=1
        
        +CPBR: 1,"040123456789",129,"Name"

        OK
        '''
        self.ipc.SIM_RetrieveEntry(1)
        data = [    ("read",'AT+CPBR=1'),
                        ("write",'+CPBR: 1,"040123456789",129,"Name"'),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure no reply
                        ("ensure_reply",("Name","040123456789"))] #Ensure that we got a reply
        self.perform(data)
        

    def test_RetrieveEntry2(self):
        '''
        Test reading a phonebook entry that has the phonenumber in international format
        
        TODO this communication example was self made, I'm not sure if it's correct. Check.
        AT+CPBR=1
        
        +CPBR: 2,"+35840123456789",145,"Name"

        OK
        '''
        self.ipc.SIM_RetrieveEntry(1)
        data = [    ("read",'AT+CPBR=1'),
                        ("write",'+CPBR: 2,"+35840123456789",145,"Name"'),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure no reply
                        ("ensure_reply",("Name","+35840123456789"))] #Ensure that we got a reply
        self.perform(data)
        
    
    def test_RetrieveEntry_Fail(self):
        '''Test failing to retrieve phonebook entry'''
        self.ipc.SIM_RetrieveEntry(1)
        data = [    ("read",'AT+CPBR=1'),
                        ("write",'ERROR'),
                        ("ensure_error_set",True), #Ensure error was returned
                        ("ensure_reply_set",False)] #Ensure that no reply was received
        self.perform(data)
        
#     def test_GetStorageSpaces(self):
#         '''
#         Test listing available storage spaces
        
#         Sample communication:
#         AT+CPBS=?
        
#         +CPBS: ("SM","FD","LD","MC","RC")
        
#         OK
#         '''
#         self.ipc.SIM_GetStorageSpaces()
#         data = [    ("read",'AT+CPBS=?'),
#                         ("write",'+CPBS: ("SM","FD","LD","MC","RC")'),
#                         ("write",'OK'),
#                         ("ensure_error_set",False), #Ensure no reply
#                         ("ensure_reply", ["SM","FD","LD","MC","RC"])] #Ensure that we got a reply
#         self.perform(data)
        
#         def test_GetStorageSpaces_Cache(self):
#             '''Test that storage spaces are cached properly'''
#             self.test_GetSTorageSpaces()
#             self.reset()
#             self.ipc.SIM_GetStorageSpaces()
#             data = [("ensure_error_set",False), #Ensure no reply
#                         ("ensure_reply", ["SM","FD","LD","MC","RC"])] #Ensure that we got a reply
#             self.perform(data)
            
#     def test_GetStorageSpaces_Fail(self):
#         self.ipc.SIM_GetStorageSpaces()
#         data = [    ("read",'AT+CPBS=?'),
#                         ("write",'ERROR'),
#                         ("ensure_error_set",True), #Ensure error was returned
#                         ("ensure_reply_set",False)] #Ensure that no reply was received
#         self.perform(data)
    
if __name__ == "__main__":
    try:
        unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
    except SystemExit:
        pass
    except:
        import traceback, sys
        traceback.print_exc(file=sys.stdout)
        import dbus_thread
        dbus_thread.stop_thread()
        sys.exit(1)
