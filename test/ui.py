# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import cmd
import time
import sys
from dbus_interface import DBUSInterface

class gsmd2ui(cmd.Cmd):
    def preloop(self):
        self.ipc = DBUSInterface()
        self.ipc.Initialize()
        self.ipc.debug = False
        self.init()
    def init(self):
        print "gsmd2ui"

    def do_quit(self,message):
        self.postloop()
        sys.exit(0)
    def help_quit(self):
        print "Quits this program"
    def help_help(self):
        print "To get help on commands type help <command>"

    #SMS
    def do_SMS_GetServiceBearer(self,message):
        self.ipc.SMS_GetServiceBearer()
        print "Service bearer: ",self.ipc.getResultAndReset()
    def help_SMS_GetServiceBearer(self):
        print "Gets service bearer. Usage: SMS_GetServiceBearer"


    def do_SIM_ListMessages(self,message):
        if len(message) >=1:
            self.ipc.SIM_ListMessages(message)
        else:
            self.ipc.SIM_ListMessages("all")
        print "Messages: ", self.ipc.getResultAndReset()
    def help_SIM_ListMessages(self):
        print "Lists sms messages. Usage: SIM_ListMessages status"
        print "Status is either new,read,sending,sent,all"

    def do_SMS_DeleteMessage(self,message):
        try:
            self.ipc.SMS_DeleteMessage(int(message))
        except ValueError:
            print "Specify an integer value"
            self.help_SMS_DeleteMessage()
    def help_SMS_DeleteMessage(self):
        print "Deletes a SMS message. Usage: SMS_DeleteMessage <message's index>"

    def do_SMS_SendMessage(self,message):
        words = message.split()
        if len(words) >= 2:
            try:
                self.ipc.SMS_SendMessage(words[0],words[1],False)
            except ValueError:
                self.help_SMS_SendMessage()
        else:
            self.help_SMS_SendMessage()
    def help_SMS_SendMessage(self):
        print "Sends sms message. Usage: SMS_SendMessage message number"
        
    def do_SIM_SendMessage(self,message):
        words = message.split()
        if len(words) >= 1:
            try:
                self.ipc.SIM_SendMessage(int(words[0]), False)
            except ValueError:
                self.help_SIM_SendMessage()
        else:
            self.help_SIM_SendMessage()
    def help_SIM_SendMessage(self):
        print "Sends sms message from storage. Usage: SIM_SendMessage <message's index>"

    def do_SIM_StoreMessage(self,message):
        words = message.split()
        
        if len(words) >= 2:
                try:
                    self.ipc.SIM_StoreMessage(words[0],words[1])
                except ValueError:
                    self.help_SIM_StoreMessage()
        else:
            self.help_SIM_StoreMessage()
    def help_SIM_StoreMessage(self):
        print "Stores a message. Usage: SIM_StoreMessage <recipient's number> <contents>"

    def do_SIM_RetrieveMessage(self,message):
        try:
            self.ipc.reply_message = "Message: "
            self.ipc.SIM_RetrieveMessage(int(message))
        except ValueError:
            self.help_SIM_RetrieveMessage()
    def help_SIM_RetrieveMessage(self):
        print "Retrieves sms message. Usage: SIM_RetrieveMessage <message's index>"

    #PDP
    def do_PDP_ListGprsClasses(self,message):
        print "GPRS classes: ",self.ipc.PDP_ListGprsClasses()
    def help_PDP_ListGprsClasses(self):
        print "Prints GPRS classes. Usage: PDP_ListGprsClasses"

    def do_PDP_SelectGprsClass(self,message):
        if len(message) >=1:
            self.ipc.PDP_SelectGprsClass(message)
        else:
            self.help_PDP_SelectGprsClass()
    def help_PDP_SelectGprsClass(self):
        print "Selects gprs class. Usage: PDP_SelectGprsClass <class>"

    def do_PDP_Activate(self,message):
        try:
            self.ipc.PDP_Activate(int(message))
        except ValueError:
            print "Specify an integer value"
            self.help_PDP_Activate()
    def help_PDP_Activate(self):
        print "Usage: PDP_Activate <index>"

    def do_PDP_Deactivate(self,message):
        try:
            self.ipc.PDP_Deactivate(int(message))
        except ValueError:
            print "Specify an integer value"
    def help_PDP_Deactivate(self):
        print "Usage: Deactivate  <index>"

    def do_PDP_SelectContext(self,message):
        self.ipc.PDP_SelectContext()
    def help_PDP_SelectContext(self):
        print "Usage: SelectContext"

    def do_PDP_AddContext(self,message):
        self.ipc.PDP_AddContext()
    def help_PDP_AddContext(self):
        print "Usage: AddContext"

    def do_PDP_DeleteContext(self,message):
        self.ipc.PDP_DeleteContext()
    def help_PDP_DeleteContext(self):
        print "Usage: DeleteContext"

    def do_PDP_ListContexts(self,message):
        self.ipc.PDP_ListContexts
    def help_PDP_ListContexts(self):
        print "Usage: ListContexts"

    #CALL
    def do_Call_Emergency(self,message):
        print "Emergency: ",self.ipc.Call_Emergency()
    def help_Call_Emergency(self):
        print "Usage: Call_Emergency"

    def do_Call_Activate(self,message):
        try:
            self.ipc.Call_Activate(int(message))
        except ValueError:
            print "Specify an integer"
            self.help_Call_Activate()
    def help_Call_Activate(self):
        print "Activates a call. Usage: Call_Activate <call's id>"
    def do_Call_Release(self,message):
        try:
            self.ipc.Call_Release("",int(message))
        except ValueError:
            print "Specify an integer"
            self.help_Call_Release()
    def help_Call_Release(self):
        print "Releases a call. Usage: Call_Release <call's id>"
    def do_Call_Initiate(self,message):
        if len(message) >= 1:
            self.ipc.Call_Initiate(message,"")
            print "Call id: ", self.ipc.getResultAndReset()
        else:
            self.help_Call_Initiate()
    def help_Call_Initiate(self):
        print "Initiates a call. Usage: Call_Initiate <number>"

    def do_Call_GetStatus(self,message):
        try:
            self.ipc.Call_GetStatus(int(message))
        except ValueError:
            print "Specify an integer"
            self.help_Call_GetStatus()
    def help_Call_GetStatus(self):
        print "Gets call's status. Usage: Call_GetStatus <index>"

    def do_Call_SendDtmf(self,message):
        print "Mode: ",self.ipc.Call_SendDtmf()
    def help_Call_SendDtmf(self):
        print "Usage: Call_SendDtmf"

    def do_Call_SetDtmfDuration(self,message):
        try:
            self.ipc.Call_SetDtmfDuration(int(message))
        except ValueError:
            print "Specify an integer"
    def help_Call_SetDtmfDuration(self):
        print "Usage: Call_SetDtmfDuration <mode>"

    def do_Call_GetDtmfDuration(self,message):
        self.ipc.Call_GetDtmfDuration()
        print "Duration: ", self.ipc.getResultAndReset()
    def help_Call_GetDtmfDuration(self):
        print "Usage: Call_GetDtmfDuration"
        
    def do_Call_ListCalls(self,message):
        self.ipc.Call_ListCalls()
        print "Current calls: ",self.ipc.getResultAndReset()
    def help_Call_GetDtmfDuration(self):
        print "Lists current calls. Usage: Call_ListCalls"

    #Device
    def do_Device_GetInfo(self,message):
        self.ipc.Device_GetInfo()
        print "Device info: ",self.ipc.getResultAndReset()
    def help_Device_GetInfo(self):
        print "Gets device info. Usage: Device_GetInfo"

    def do_Device_SetFunction(self,message):
        self.ipc.Device_SetFunction(dbus.Boolean(int(message)))
    def help_Device_SetFunction(self):
        print "Usage: Device_SetFunction <antenna_power (0/1)"

    def do_Device_GetFeatures(self,message):
        print "Features: ",self.ipc.Device_GetFeatures
    def help_Device_GetFeatures(self):
        print "Usage: Device_GetFeatures"

    #Network
    def do_Network_Register(self,message):
        self.ipc.Network_Register()
    def help_Network_Register(self):
        print "Usage: Network_Register"

    def do_Network_GetStatus(self,message):
        self.ipc.Network_GetStatus()
        print "Network status: ", self.ipc.getResultAndReset()
    def help_Network_GetStatus(self):
        print "Usage: Network_GetStatus"

    def do_Network_ListProviders(self,message):
        print "Providers: ",self.ipc.Network_ListProviders()
    def help_Network_ListProviders(self):
        print "Usage: Network_ListProviders"

    def do_Network_RegisterWithProvider(self,message):
        try:
            self.ipc.Network_RegisterWithProvider(int(message))
        except ValueError:
            print "Specify an integer value"
    def help_Network_RegisterWithProvider(self):
        print "Usage: Network_RegisterWithProvider <index>"

    def do_Network_GetSubscriberNumbers(self,message):
        print "Subscriber numbers",self.ipc.Network_GetSubscriberNumbers()
    def help_Network_GetSubscriberNumbers(self):
        print "Usage: Network_GetSubscriberNumbers"

    def do_Network_GetCountryCode(self,message):
        print "Country code: ",self.ipc.Network_GetCountryCode()
    def help_Network_GetCountryCode(self):
        print "Usage: Network_GetCountryCode"

    def do_Network_GetHomeCountryCode(self,message):
        print "Home country code: ",self.ipc.Network_GetHomeCountryCode()
    def help_Network_GetHomeCountryCode(self):
        print "Usage: Network_GetHomeCountryCode"



    #SIM
    def do_SIM_GetAuthStatus(self,message):
        self.ipc.SIM_GetAuthStatus()
        print "Auth status: ",self.ipc.getResultAndReset()
    def help_SIM_GetAuthStatus(self):
        print "Usage: SIM_GetAuthStatus"

    def do_SIM_SendAuthCode(self,message):
        if len(message) >= 1:
            self.ipc.SIM_SendAuthCode(message)
            self.ipc.getResultAndReset()
        else:
            self.help_SIM_SendAuthCode()
    def help_SIM_SendAuthCode(self):
        print "Usage: SIM_SendAuthCode <auth code>"

    def do_SIM_Unlock(self,message):
        words = message.split()
        if len(words) >= 2:
            self.ipc.SIM_Unlock(words[0], words[1])
        else:
            self.help_SIM_Unlock()
    def help_SIM_Unlock(self):
        print "Usage: SIM_Unlock <puk> <new pin>"

    def do_SIM_ChangeAuthCode(self,message):
        words = message.split()
        if len(words) >= 2:
            self.ipc.SIM_ChangeAuthCode(words[0], words[1])
        else:
            self.help_SIM_ChangeAuthCode()
    def help_SIM_ChangeAuthCode(self):
        print "Usage: SIM_ChangeAuthCode <old pin> <new pin>"

    def do_SIM_GetSimInfo(self,message):
        self.ipc.SIM_GetSimInfo()
        print "IMSI: ", self.ipc.getResultAndReset()
    def help_SIM_GetSimInfo(self):
        print "Usage: SIM_GetSimInfo"

    #Phonebook
    def do_Phonebook_GetStorageSpaces(self,message):
        print "Storage spaces: ",self.ipc.Phonebook_GetStorageSpaces()
    def help_Phonebook_GetStorageSpaces(self):
        print "Usage: Phonebook_GetStorageSpaces"

    def do_Phonebook_GetStorageInfo(self,message):
        print "Storage info: ",self.ipc.Phonebook_GetStorageInfo()
    def help_Phonebook_GetStorageInfo(self):
        print "Usage: Phonebook_GetStorageInfo"

    def do_Phonebook_GetDefaultStorageSpace(self,message):
        print "Default storage space: ",self.ipc.Phonebook_GetDefaultStorageSpace()
    def help_Phonebook_GetDefaultStorageSpace(self):
        print "Usage: Phonebook_GetDefaultStorageSpace"

    def do_Phonebook_SetDefaultStorageSpace(self,message):
        self.ipc.Phonebook_SetDefaultStorageSpace(message)
    def help_Phonebook_SetDefaultStorageSpace(self):
        print "Usage: Phonebook_SetDefaultStorageSpace <storage space>"

    def do_Phonebook_GetIndexBoundaries(self,message):
        print "Index boundaries: ",self.ipc.Phonebook_GetIndexBoundaries()
    def help_Phonebook_GetIndexBoundaries(self):
        print "Usage: Phonebook_GetIndexBoundaries"

    def do_Phonebook_DeleteEntry(self,message):
        try:
            self.ipc.Phonebook_DeleteEntry(int(message))
        except ValueError:
            print "Specify an integer"
    def help_Phonebook_DeleteEntry(self):
        print "Usage: Phonebook_DeleteEntry <index>"

    def do_Phonebook_StoreEntry(self,message):
        words = message.split()
        if len(words) >= 4:
            try:
                self.ipc.Phonebook_StoreEntry(int(words[0]), int(words[1]),words[2],words[3])
            except ValueError:
                print "Index and type arguments must be integers"
        else:
            self.help_Phonebook_StoreEntry()
    def help_Phonebook_StoreEntry(self):
        print "Usage: Phonebook_StoreEntry <index> <type> <name> <contents>"

    def do_Phonebook_RetrieveEntry(self,message):
        try:
            print "Entry: ",self.ipc.Phonebook_RetrieveEntry(int(message))
        except ValueError:
            print "Specify an integer"
    def help_Phonebook_RetrieveEntry(self):
        print "Usage: Phonebook_RetrieveEntry <index>"

if __name__ == "__main__":
    ui = gsmd2ui()
    ui.cmdloop()



