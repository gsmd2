#
# SIM cdefs
#
cdef extern from "../libfreesmartphone/freesmartphone-sim.h":
    ctypedef void (*FSGSMSIMReplyFunc) (GError *error,
                                   gpointer userdata )
    ctypedef void (*FSGSMSIMGetSIMInfoReplyFunc) (GHashTable *info,
                                                  GError *error,
                                                  gpointer userdata )

    gboolean fso_gsm_sim_get_sim_info(FreeSmartphone *fs,
                                      FSGSMSIMGetSIMInfoReplyFunc cb,
                                      gpointer userdata)

    ctypedef void (*FSGSMSIMGetPhonebookInfoReplyFunc) (GHashTable *info,
                                                        GError *error,
                                                        gpointer userdata )

    gboolean fso_gsm_sim_get_phonebook_info(FreeSmartphone *fs,
                                            FSGSMSIMGetPhonebookInfoReplyFunc cb,
                                            gpointer userdata)

    gboolean fso_gsm_sim_change_auth_code(FreeSmartphone *fs,
                                          gchar *old_pin,
                                          gchar *new_pin,
                                          FSGSMSIMReplyFunc cb,
                                          gpointer userdata)
    gboolean fso_gsm_sim_unlock(FreeSmartphone *fs,
                                gchar *puk,
                                gchar *new_pin,
                                FSGSMSIMReplyFunc cb,
                                gpointer userdata)
    gboolean fso_gsm_sim_send_auth_code(FreeSmartphone *fs,
                                        gchar *pin,
                                        FSGSMSIMReplyFunc cb,
                                        gpointer userdata)

    ctypedef void (*FSGSMSIMGetAuthStatusReplyFunc) (gchar *status,
                                                     GError *error,
                                                     gpointer userdata )

    gboolean fso_gsm_sim_get_auth_status(FreeSmartphone *fs,
                                         FSGSMSIMGetAuthStatusReplyFunc cb,
                                         gpointer userdata)

    ctypedef void (*FSGSMSIMAuthStatusSignalFunc) (gchar *status,
                                              gpointer userdata)
    gboolean fso_gsm_sim_auth_status_signal_remove(FreeSmartphone *fs,
                                               FSGSMSIMAuthStatusSignalFunc cb,
                                               gpointer userdata)
    gboolean fso_gsm_sim_auth_status_signal(FreeSmartphone *fs,
                                        FSGSMSIMAuthStatusSignalFunc cb,
                                        gpointer userdata)

    ctypedef void (*FSGSMSIMRetrieveMessageReplyFunc) (gchar *number,
                                                       gchar *message,
                                                       gint timestamp,
                                                       GError *error,
                                                       gpointer userdata )

    gboolean fso_gsm_sim_retrieve_message(FreeSmartphone *fs,
                                          gint index,
                                          FSGSMSIMRetrieveMessageReplyFunc cb,
                                          gpointer userdata)

    ctypedef void (*FSGSMSIMListMessagesReplyFunc) (GArray *indices,
                                                    GError *error,
                                                    gpointer userdata )

    gboolean fso_gsm_sim_list_messages(FreeSmartphone *fs,
                                       gchar *status,
                                       FSGSMSIMListMessagesReplyFunc cb,
                                       gpointer userdata)

    gboolean fso_gsm_sim_delete_entry(FreeSmartphone *fs,
                                      gint index,
                                      FSGSMSIMReplyFunc cb,
                                      gpointer userdata)

    gboolean fso_gsm_sim_store_entry(FreeSmartphone *fs,
                                     gint index,
                                     gchar *name,
                                     gchar *number,
                                     FSGSMSIMReplyFunc cb,
                                     gpointer userdata)

    ctypedef void (*FSGSMSIMRetrieveEntryReplyFunc) (gchar *name,
                                                    gchar *number,
                                                    GError *error,
                                                    gpointer userdata )

    gboolean fso_gsm_sim_retrieve_entry(FreeSmartphone *fs,
                                        gint index,
                                        FSGSMSIMRetrieveEntryReplyFunc cb,
                                        gpointer userdata)

#
# SIM Handlers
#

cdef void auth_status_signal(gchar *status, gpointer userdata) with gil:
    print "SIM auth status signal"
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    status_str = get_string(status)
    reply_cb(reply_data, id, status_str)

cdef void get_info_reply(GHashTable *info, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        reply_cb(reply_data, False, get_hash_table(info))
    Py_DECREF(reply)
    return

cdef void get_auth_status_reply(gchar *status, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        status_str = get_string(status)
        reply_cb(reply_data, False, status_str)
    Py_DECREF(reply)
    return

cdef void sim_reply(GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str)
    else:
        reply_cb(reply_data, False)
    Py_DECREF(reply)
    return

cdef void list_messages_reply(GArray *indices, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        reply_cb(reply_data, False, get_int_array(indices))
    Py_DECREF(reply)
    return

cdef void retrieve_message_reply(gchar *number, gchar *message, gint timestamp, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        number_str = get_string(number)
        message_str = get_string(message)
        reply_cb(reply_data, False, number_str, message_str, timestamp)
    Py_DECREF(reply)
    return

cdef void retrieve_entry_reply(gchar *name, gchar *number, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        name_str = get_string(name)
        number_str = get_string(number)
        reply_cb(reply_data, False, name_str,number_str)
    Py_DECREF(reply)
    return

#
# PyFreeSmartPhoneSIM class
#

cdef class PyFreeSmartphoneSIM:
    cdef FreeSmartphone *fs

    cdef object auth_status

    def __new__(self):
        self.fs = fso_init()
    def deinitialize(self):
        fso_free(self.fs)
#
# Methods
#

    def get_sim_info(self, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_get_sim_info(self.fs, &get_info_reply, <void*>reply_data)

    def get_phonebook_info(self, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_get_phonebook_info(self.fs, &get_info_reply, <void*>reply_data)

    def get_auth_status(self, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_get_auth_status(self.fs, &get_auth_status_reply, <void*>reply_data)

    def send_auth_code(self, pin, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_send_auth_code(self.fs, <char*>pin, &sim_reply, <void*>reply_data)

    def unlock(self, puk, new_pin, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_unlock(self.fs, <char*>puk, <char*>new_pin, &sim_reply, <void*>reply_data)

    def change_auth_code(self, old_pin, new_pin, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_change_auth_code(self.fs, <char*>old_pin, <char*>new_pin, &sim_reply, <void*>reply_data)

    def list_messages(self, status, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_list_messages(self.fs, <char*>status, &list_messages_reply, <void*>reply_data)

    def retrieve_message(self, index, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_retrieve_message(self.fs, <int>index, &retrieve_message_reply, <void*>reply_data)

    def delete_entry(self, index, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_delete_entry(self.fs, <int>index, &sim_reply, <void*>reply_data)

    def retrieve_entry(self, index, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_retrieve_entry(self.fs, <int>index, &retrieve_entry_reply, <void*>reply_data)

    def store_entry(self, index, name, number, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sim_store_entry(self.fs, <int>index, <char*>name, <char*>number, &sim_reply, <void*>reply_data)
#
# Signals
#
    def auth_status_signal(self, cb,data=None):
        self.auth_status = ReplyData(cb,data)
        Py_INCREF(self.auth_status)
        return fso_gsm_sim_auth_status_signal(self.fs, &auth_status_signal, <void*>self.auth_status)

    def auth_status_signal_remove(self):
        fso_gsm_sim_auth_status_signal_remove(self.fs, &auth_status_signal, <void*>self.auth_status)
        Py_DECREF(self.auth_status)
