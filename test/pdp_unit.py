# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import unittest
from gsmd_common import GsmdCommon
import unittest

class PDPTests(GsmdCommon):
    #Testing methods themselves
    def test_ListGprsClasses(self):
        self.fail("Test not implemented")
    def test_SelectGprsClass(self):
        self.fail("Test not implemented")
    def test_Activate(self):
        self.fail("Test not implemented")
    def test_Deactivate(self):
        self.fail("Test not implemented")
    def test_SelectContext(self):
        self.fail("Test not implemented")
    def test_AddContext(self):
        self.fail("Test not implemented")
    def test_DeleteContext(self):
        self.fail("Test not implemented")
    def test_ListContexts(self):
        self.fail("Test not implemented")
        
    #Testing method failure
    def test_ListGprsClasses_Fail(self):
        self.fail("Test not implemented")
    def test_SelectGprsClass_Fail(self):
        self.fail("Test not implemented")
    def test_Activate_Fail(self):
        self.fail("Test not implemented")
    def test_Deactivate_Fail(self):
        self.fail("Test not implemented")
    def test_SelectContext_Fail(self):
        self.fail("Test not implemented")
    def test_AddContext_Fail(self):
        self.fail("Test not implemented")
    def test_DeleteContext_Fail(self):
        self.fail("Test not implemented")
    def test_ListContexts_Fail(self):
        self.fail("Test not implemented")
     




    
if __name__ == "__main__":
    unittest.main() # run all tests
