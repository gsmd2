# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import unittest
from gsmd_common import GsmdCommon
import unittest, inspect,time



class MuxTests(GsmdCommon):
    def setUp(self):
        self.devicecount = 3
        self.device_indices[1] = 0    #Call
        self.device_indices[3] = 1    #Network
        self.device_indices[5] = 2    #SIM
        GsmdCommon.setUp(self)
        
        
    def test_ReadingPorts(self):
#        """Test that at commands are sent on the right port and their replies are read from them correctly"""
        self.ipc.Call_Initiate("12345","")
        data = [("read","ATD12345;"),
                    ("write","OK"),
                    ("ensure_reply_set",True)]
        self.perform(data)
        self.reset()
        
        self.ipc.Network_Register()
        data = [("read","AT+COPS=0,0",1),
                    ("write","OK",1),
                    ("ensure_reply_set",True)]
        self.perform(data)
        self.reset()
        
        self.ipc.SIM_GetSimInfo()
        data = [("read",'AT+CIMI',2),
                    ("write",'+CIMI: 244917070350971',2),
                    ("write",'OK',2),
                    ("ensure_reply_set",True)]
        self.perform(data)
        
    def test_Concurrency(self):
#        """Test that at commands are sent on the right port and their replies are read from them concurrently"""
        self.ipc.Call_Initiate("12345","")
        self.ipc.Network_Register()
        self.ipc.SIM_GetSimInfo()
        data = [("read","ATD12345;"),
                    ("read","AT+COPS=0,0",1),
                    ("read",'AT+CIMI',2),
                    ("write","OK"),
                    ("ensure_reply_set",True),
                    ("reset"),
                    ("write","OK",1),
                    ("ensure_reply_set",True),
                    ("reset"),
                    ("write",'+CIMI: 244917070350971',2),
                    ("write",'OK',2),
                    ("ensure_reply_set",True)]
        self.perform(data)
        

if __name__ == "__main__":
#    import os
#    os.spawnvpe(os.P_WAIT, "ls",("ls", "-l"), os.environ)
    try:
        unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
    except SystemExit:
        pass
    except:
        import traceback, sys
        traceback.print_exc(file=sys.stdout)
        import dbus_thread
        dbus_thread.stop_thread()
        sys.exit(1)
