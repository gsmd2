# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

"""
A util classes and funtions to do IO.

"""


import os, select

TIMEOUT = 2.0 #secs

class DummyFile:
    def __init__(self, fd):
        self.fd = fd
    def readline(self, want_eof=False):
        ret = []
        while len(ret) == 0 or not ((ret[-1] == '\n') or ( ord(ret[-1])== 0x1a))  :
            fds = [self.fd]
            (a,b,c) = select.select(fds,[],fds, TIMEOUT)
            if len(c) > 0:
                raise 'error in stream'
            if len(a) > 0:
                read = os.read(self.fd, 1)
#                print "read is: %s: %d"%(read, ord(read))
                if read not in '\r':
                    ret.append(read)
            else:
                raise Exception, 'nothing to read'
        if want_eof and len(ret) == 1 and ord(ret[0]) == 0x1a:
            return None
        return "".join(ret)[:-1]

    def read_eof(self):
        return self.readline(want_eof=True)
    def write(self, a):
        n = os.write(self.fd, a)
        #os.fdatasync(self.fd)
    def close(self):
        os.close(self.fd)
    

    def writeOK(self):
        self.write("OK\r\n")

    def write_reply(self, reply):
        self.write(reply+"\r\n")

    def write_reply_direct(self, reply):
        self.write(reply)
