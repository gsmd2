# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen
from ipc_interface import IPCInterface
import dbus_thread,threading
import dbus

class DBUSInterface(IPCInterface):
    def Initialize(self):
        IPCInterface.Initialize(self)
        self.DBUS = dbus_thread.start_thread()
        self.reply_event = threading.Event()
        self.DBUS.SetIpc(self)

    def DeInitialize(self):
        IPCInterface.DeInitialize(self)
        dbus_thread.stop_thread()
    
    def ErrorHandler(self,*arg):
        if len(arg) >= 1 and type(arg[0]) == tuple:
            my_arg = arg[0]
        else:
            my_arg = arg
        if type(my_arg[0]) == dbus.exceptions.DBusException:
            try:
                my_arg = (my_arg[0].get_dbus_name() + " : " + str(my_arg[0].args[0]), )
            except:
                pass
        IPCInterface.ErrorHandler(self,my_arg)

    def CheckError(self):
        #a hack to assert-fail test when a dbus timeout error is received, this check can be disabled with timeout_fails = False
        if self.timeout_fails:
            assert not str(self.getError()).startswith("(DBusException(dbus.String(u'Did not receive a reply."), "Dbus exception occurred: "+str(self.getError())
            '''
        (DBusException(dbus.String(u'Did not receive a reply. Possible causes include: the remote application did not send a reply, the message bus security policy blocked the reply, the reply timeout expired, or the network connection was broken.'),),)
            '''

    
    #Call interface
    def Call_Emergency(self):
        self.DBUS.Call.Emergency(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_Activate(self, id):
        self.DBUS.Call.Activate(id, reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_Release(self,message,id):
        self.DBUS.Call.Release(message,id,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_Initiate(self,number,type):
        self.DBUS.Call.Initiate(number,type,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_GetStatus(self,id):
        self.DBUS.Call.GetCallStatus(id,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_SendDtmf(self):
        self.DBUS.Call.SendDtmf(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_SetDtmfDuration(self,mode):
        self.DBUS.Call.SetDtmfDuration(mode,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_GetDtmfDuration(self):
        self.DBUS.Call.GetDtmfDuration(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_ListCalls(self):
        self.DBUS.Call.ListCalls(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_SetSendIdentification(self,mode):
        self.DBUS.Call.SetSendIdentification(mode,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_GetSendIdentification(self):
        self.DBUS.Call.GetSendIdentification(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_ReleaseHeld(self,message):
        self.DBUS.Call.ReleaseHeld(message,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_ReleaseAll(self,message):
        self.DBUS.Call.ReleaseAll(message,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Call_ActivateConference(self, id):
        self.DBUS.Call.ActivateConference(id, reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)

    #Device interface
    def Device_GetInfo(self):
        self.DBUS.Device.GetInfo(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Device_SetAntennaPower(self,antenna_power):
        self.DBUS.Device.SetAntennaPower(antenna_power,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Device_GetAntennaPower(self):
        self.DBUS.Device.GetAntennaPower(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Device_GetFeatures(self):
        self.DBUS.Device.GetFeatures(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Device_PrepareToSuspend(self):
        self.DBUS.Device.PrepareToSuspend(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Device_RecoverFromSuspend(self):
        self.DBUS.Device.RecoverFromSuspend(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)

    #Network interface
    def Network_Register(self):
        self.DBUS.Network.Register(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Network_GetStatus(self):
        self.DBUS.Network.GetStatus(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Network_ListProviders(self):
        self.DBUS.Network.ListProviders(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Network_RegisterWithProvider(self,index):
        self.DBUS.Network.RegisterWithProvider(index,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Network_GetSubscriberNumbers(self):
        self.DBUS.Network.GetSubscriberNumbers(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Network_GetCountryCode(self):
        self.DBUS.Network.GetCountryCode(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def Network_GetHomeCountryCode(self):
        self.DBUS.Network.GetHomeCountryCode(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)

    #SMS interface
    
    def SMS_SendMessage(self,message,number,want_report):
        self.DBUS.SMS.SendMessage(message,number,want_report,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    
    #SIM interface
    def SIM_GetAuthStatus(self):
        self.DBUS.SIM.GetAuthStatus(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_SendAuthCode(self,pin):
        self.DBUS.SIM.SendAuthCode(pin,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_Unlock(self,puk,new_pin):
        self.DBUS.SIM.Unlock(puk,new_pin,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_ChangeAuthCode(self,old_pin,new_pin):
        self.DBUS.SIM.ChangeAuthCode(old_pin,new_pin,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_GetSimInfo(self):
        self.DBUS.SIM.GetSimInfo(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_ListMessages(self,status):
        self.DBUS.SIM.ListMessages(status,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_SendMessage(self,index,want_report):
        self.DBUS.SIM.SendMessage(index,want_report,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_RetrieveMessage(self,index):
        self.DBUS.SIM.RetrieveMessage(index,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_StoreMessage(self,number,message):
        self.DBUS.SIM.StoreMessage(number,message,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_DeleteMessage(self,index):
        self.DBUS.SIM.DeleteMessage(index,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_GetPhonebookInfo(self):
        self.DBUS.SIM.GetPhonebookInfo(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_DeleteEntry(self,index):
        self.DBUS.SIM.DeleteEntry(index,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_StoreEntry(self,index,name,number):
        self.DBUS.SIM.StoreEntry(index,name,number,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def SIM_RetrieveEntry(self,index):
        self.DBUS.SIM.RetrieveEntry(index,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)

    #PDP interface
    def PDP_ListGprsClasses(self):
        self.DBUS.PDP.ListGprsClasses(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def PDP_SelectGprsClass(self,klass):
        self.DBUS.PDP.SelectGprsClass(klass,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def PDP_Activate(self,index):
        self.DBUS.PDP.Activate(index,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def PDP_Deactivate(self,index):
        self.DBUS.PDP.Deactivate(index,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def PDP_SelectContext(self,context):
        self.DBUS.PDP.SelectContext(context,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def PDP_AddContext(self,context):
        self.DBUS.PDP.AddContext(context,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def PDP_DeleteContext(self,index):
        self.DBUS.PDP.DeleteContext(index,reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)
    def PDP_ListContexts(self):
        self.DBUS.PDP.ListContexts(reply_handler=self.ReplyHandler, error_handler=self.ErrorHandler)

