# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen


import os, time, subprocess, signal, termios, utils, time,unittest

from dbus_interface import DBUSInterface
from libfsointerface import FSOInterface
from ipc_interface import IPCInterface

class GsmdCommon(unittest.TestCase):

    def __init__(self, name):
        unittest.TestCase.__init__(self,name)
        self.do_check_init = True
        self.env = {}
        self.ipc = None
        self.vendor = os.environ.get("GSMD2_TEST_VENDOR")
        if self.vendor is None:
            self.vendor = "ticalypsoneo1973"
        self.params = ("gsmd2", "--no-conf", "--no-cache", "-n", "--vendor", self.vendor)
        self.devicecount = 1
        self.interface_names = ["--general_index",
                                "--call_index",
                                "--device_index",
                                "--network_index",
                                "--pdp_index",
                                "--sim_index",
                                "--sms_index"]

        self.device_indices = [0, #General
                               0, #Call
                               0, #Device
                               0, #Network
                               0,#PDP
                               0,#SIM
                               0] #SMS
        self.sim_ready_data = {}
        self.sim_ready_data['ticalypsoneo1973'] = [   ("read", "AT+CSMS=0"),
                                                      ("write", "OK"),
                                                      ("read", "AT+CFUN=1"),
                                                      ("write", "OK"),
                                                      ("read", "AT+COPS"),
                                                      ("write", "OK"),
                                                      ("read", "AT%CPI=4"),
                                                      ("write", "OK"),
                                                      ("read", "AT+CLIP=1"),
                                                      ("write", "OK"),
                                                      ("read", "AT+CNMI=2,1,0,0,0"),
                                                      ("write", "OK"),
                                                      ("read", "AT+CMGF=0"),
                                                      ("write", "OK"),
                                                      ("read", "AT+COPS=3,0"),
                                                      ("write", "OK"),
                                                      ("read", "AT+CCWA=1,1"),
                                                      ("write", "OK"),
                                                      ("read", "AT+CLCC"),
                                                      ("write", "OK"),
#                                                      ("read", "AT#ECAM=1"),
#                                                      ("write", "OK"),
                                                      ]
        self.sim_ready_data['telit862-gps'] = [ ("read", "AT+CLIP=1"),
                                                ("write", "OK"),
                                                ("read", "AT+CNMI=2,1,0,0,0"),
                                                ("write", "OK"),
                                                ("read", "AT+CMGF=0"),
                                                ("write", "OK"),
                                                ("read", "AT+COPS=3,0"),
                                                ("write", "OK"),
                                                ("read", "AT+CCWA=1,1"),
                                                ("write", "OK"),
                                                ("read", "AT+CLCC"),
                                                ("write", "OK"),
                                                ("read", "AT#ECAM=1"),
                                                ("write", "OK"),
                                                ]
        self.init_data_first = {}
        self.init_data_first['ticalypsoneo1973'] = [ ("read_eof"),
                                                     ("read_eof"),
                                                     ("read_eof"),
                                                     ("read", ""),
                                                     ("write", "OK"),
                                                     ("read", "ATZ"),
                                                     ("write", "OK"),
                                                     ]
        self.init_data_first['telit862-gps'] = [ ("read", "ATZ"),
                                                 ("write", "OK"),
                                                 ]
        self.init_data = {}
        self.init_data['ticalypsoneo1973'] = [  ("read", "AT+CFUN=1"),
                                                ("write", "OK"),
                                                ("read", "AT S7=45 S0=0 L1 V1 X4 &c1 E0 Q0"),
                                                ("write", "OK"),
                                                ("read", "AT+CMEE=1"),
                                                ("write", "OK"),
                                                ("read", "AT+CREG=1"),
                                                ("write", "OK"),
                                                ("read", "ATE0"),
                                                ("write", "OK"),
                                                ("read", "AT+CMEE=1"),
                                                ("write", "OK"),
                                                ("read", "AT+CGSN"),
                                                ("write", "123456789012345"),
                                                ("write", "OK"),
                                                ("read", "AT+CGMI"),
                                                ("write", "Telit"),
                                                ("write", "OK"),
                                                ("read", "AT+CGMR"),
                                                ("write", "08.01.001"),
                                                ("write", "OK"),
                                                ("read", "AT+CGMM"),
                                                ("write", "UC864-E"),
                                                ("write", "OK"),
                                                ("read", "AT+CPIN?"),
                                                ("write", "+CPIN: READY"),
                                                ("write", "OK"),
                                                ("ensure_signal",("AuthStatus","READY"))]
        self.init_data['telit862-gps'] = [ ("read", "ATE0"),
                                           ("write", "OK"),
                                           ("read", "AT+CMEE=1"),
                                           ("write", "OK"),
                                           ("read", "AT+CGSN"),
                                           ("write", "123456789012345"),
                                           ("write", "OK"),
                                           ("read", "AT+CGMI"),
                                           ("write", "Telit"),
                                           ("write", "OK"),
                                           ("read", "AT+CGMR"),
                                           ("write", "08.01.001"),
                                           ("write", "OK"),
                                           ("read", "AT+CGMM"),
                                           ("write", "UC864-E"),
                                           ("write", "OK"),
                                           ("read", "AT+CPIN?"),
                                           ("write", "+CPIN: READY"),
                                           ("write", "OK"),
                                           ("ensure_signal",("AuthStatus","READY"))]

    def createPort(self):
        master, slave = os.openpty()
        new = termios.tcgetattr(master)
        new[0] = termios.IGNCR
        new[1] = termios.ONLRET
        new[3] = new[3] & ~termios.ECHO #& ~termios.ICANON
        new[6][termios.VMIN] = 1
        new[6][termios.VTIME] = 0
        termios.tcsetattr(master, termios.TCSANOW, new)
        return (utils.DummyFile(master),os.ttyname(slave),slave)

    def setUp(self):
        print ""
        print ""
        print "------START------------------------------"
        self.port = []
        slaves = []

        print "Creating %d port(s)" % (self.devicecount)
        for i in range(0,self.devicecount):
            port = self.createPort()
            self.port.append(port[0])
            self.params += ("-d",port[1])
            print "Adding param -d and",port[1]
            slaves.append(port[2])
            
        
        for i in range(0,len(self.device_indices)):
            if self.device_indices[i] != 0:
                self.params += (self.interface_names[i],str(self.device_indices[i]))
        #print "Parameters:",self.params
        
        
        environ = {}
        environ.update(os.environ)
        environ.update(self.env)
        print "Starting gsmd2 with params: ",self.params
        self.gsmd_pid = os.spawnvpe(os.P_NOWAIT, "gsmd2",self.params, environ)
        for slave in slaves:
            os.close(slave)
         # wait untill gsmd starts...
        if os.waitpid( self.gsmd_pid, os.WNOHANG ) != (0,0):
            import sys
            sys.exit("Failed to start gsmd2")
        # create dbus client which introspects the gsm-server
        # we did sleep a moment ago so that the introspection is lucky.
        
        if self.do_check_init:
            self.check_init()
        else:
            time.sleep(2)

    
    def check_init(self,sim_check = True):
        self.debug = True
        if self.ipc is None:
            print "Init first"
            self.ipc = IPCInterface()
            self.perform(self.init_data_first[self.vendor])
            if os.environ.get("GSMD2_TEST_INTERFACE") == "fso":
                print "Using libfreesmartphone"
                self.ipc = FSOInterface()
            else:
                print "Using dbus"
                self.ipc = DBUSInterface()
            self.ipc.Initialize()
            print "Init second"
            self.perform(self.init_data[self.vendor])
        else:
            self.reset()
            self.perform(self.init_data_first[self.vendor])
            self.perform(self.init_data[self.vendor])

        # at init sim ready
        self.reset()
        if sim_check:
            self.sim_ready()
        self.debug = True
        self.reset()
    
    def sim_ready(self):
        self.perform(self.sim_ready_data[self.vendor])
    def sleep(self,duration):
        print "Sleeping",duration,"seconds"
        time.sleep(duration)
        
    def perform(self,data):
        '''
        data should consist of action and data pairs. Optionally also port index can be specified.
        Action (index 0) defines action to perform. Available actions:
            - write Write data to gsmd2, data is written to gsmd2
            - read Read data from gsmd2, data is what is expected to be read
            - ensure_reply Ensures that a method reply is set and it has the same value as data.
            - ensure_error Ensures that a method error is set and it has the same value as data.
            - ensure_reply_set Ensures that reply is set (when data is true) or unset (when data is false)
            - ensure_error_set Ensures that error is set (when data is true) or unset (when data is false)
            - ensure_signal Ensures signal specified in data is received
            - sleep Sleeps given timespan
            - reset resets ipc reply statuses
        
        Usage: 
        data = [("write","ATZ"),("read","OK")]
        self.perform(data)
        
        or by specifying port
        data = [("write","ATZ",5),("read","OK",5)]
        self.perform(data)
        '''
        
        actions = {'write':self.write_reply,
                   'read':self.readline,
                   'read_eof':self.read_eof,
                   'sleep':self.sleep,
                   'reset':self.reset,
                   'ensure_reply':self.ipc.ensure_reply,
                   'ensure_reply_initiate':self.ipc.ensure_reply_initiate,
                   'ensure_reply_set':self.ipc.ensure_reply_set,
                   'ensure_error':self.ipc.ensure_error,
                   'ensure_error_set':self.ipc.ensure_error_set,
                   'ensure_signal':self.ipc.CheckSignal,
                   'function':self.call_function}

        for line in data:
            if type(line) == tuple:
                actions[line[0]](*line[1:])
            else:
                actions[line]()

    def call_function(self, args):
#        print "call_function args: %s" % str(args)
        args[0](*args[1:])

    def write_reply(self,message,index=0):
        if self.debug:
            print "Writing",message,"to port",index
        self.port[index].write_reply(message)
    def write_ok(self,index=0):
        self.port[index].write_reply("OK")
    def write_error(self,index=0):
        self.port[index].write_reply("ERROR")

    def read_eof(self, index=0):
        if self.debug:
            print "Waiting for EOF from port",index
        result = self.port[index].read_eof()
        assert result == None, "Got '%s' instead of EOF" % result
        print "Got EOF"
    def readline(self,expect=None,index=0):
        if self.debug:
            if expect != None:
                print "Waiting to read: '%s' from port %d" %(expect, index)
            else:
                print "Waiting for data to port",index
        result=self.port[index].readline()

        if expect != None:
            assert result == expect, ("Wanted '%s', got '%s'" % (expect,result))
        print "Got",result
        return result

    def reset(self):
        if self.debug:
            print "Resetting"
        self.ipc.Reset()

    def tearDown(self):
        for port in self.port:
            port.close()
        
        self.ipc.DeInitialize()
        if self.gsmd_pid != 0:
            try:
#                time.sleep(2)
                os.kill(self.gsmd_pid, signal.SIGTERM)
                os.waitpid(self.gsmd_pid)
            except:
                pass
        print ""
        print "------END------------------------------"

    def check_call_status(self, index = 0, status = None,number = None):
        sig = self.ipc.WaitSignalValues("CallStatus",index)
        if status != None:
            assert sig[2] == status,"CallStatus signal's status is incorrect. Wanted "+str(status)+" got "+str(sig[2])
        if number != None:
            assert sig[3]['number'] == number,"CallStatus signal's number is incorrect. Wanted "+str(number)+" got "+str(sig[3]['number'])
    def check_call_status_reply(self, index = 0, status = None,number = None):
        reply = self.ipc.getReply()
        if status != None:
            assert reply[1] == status,"GetCallStatus status is incorrect. Wanted "+str(status)+" got "+str(reply[1])
        if number != None:
            assert reply[2]['number'] == number,"GetCallStatus number is incorrect. Wanted "+str(number)+" got "+str(reply[2]['number'])

    def get_initiate_data(self, number, index):
        data = [   ("read","ATD"+str(number)+";") ]
        if self.vendor == 'ticalypsoneo1973':
            data.extend( [ ("write", "OK"),
                           ("ensure_error_set",False), #Error wasn't returned by initiate method
                           ("ensure_reply_initiate"),
                           ("write", "%CPI: "+str(index)+",3,0,0,0,0,"+str(number)+",129,,0xc5ff,0"),# (call proceed)
                           ("write", "%CPI: "+str(index)+",4,0,1,0,0,"+str(number)+",129,,0xc506,0"),# (sync)
                           ("write", "%CPI: "+str(index)+",2,1,1,0,0,"+str(number)+",129,,0xc5ff,0"), #(alert)
                           ("function",(self.check_call_status,0,"outgoing")),
                           ("reset"),
                           ("write", "%CPI: "+str(index)+",6,0,1,0,0,"+str(number)+",129,,0xc500,0"), #(connected)
                           ] )
        elif self.vendor == 'telit862-gps':
            data.extend( [ ("write","#ECAM: "+str(index)+",1,1,,,\""+str(number)+"\",129"),
                           ("write","#ECAM: "+str(index)+",2,1,,,"),
                           ("function",(self.check_call_status,0,"outgoing")),
                           ("reset"),
                           ("write",'OK'),
                           ("ensure_reply_initiate"),
                           ("ensure_error_set",False), #Error wasn't returned by initiate method
                           ("write","#ECAM: "+str(index)+",3,1,,,"),
                           ] )
        else:
            self.fail("Unsupported vendor : '%s'" % self.vendor)
        data.append( ("function",(self.check_call_status,0,"active")) )
        return data

if __name__ == "__main__":
    print "Run all_tests.py instead"
    quit()
