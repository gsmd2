# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajaneni


import dbus

class DBUS:
    def __init__ (self):
        self.bus = dbus.SystemBus()

        #Call
        self.Call = dbus.Interface(
            self.bus.get_object('org.freesmartphone.GSM',
                                '/org/freesmartphone/GSM/Call'),
            'org.freesmartphone.GSM.Call'
            )
        self.Call.connect_to_signal("CallStatus", self.CallStatus)
        

        #SMS
        self.SMS = dbus.Interface(
            self.bus.get_object('org.freesmartphone.GSM',
                                '/org/freesmartphone/GSM/SMS'),
            'org.freesmartphone.GSM.SMS'
            )

        self.SMS.connect_to_signal("MessageSent", self.MessageSent)
        self.SMS.connect_to_signal("IncomingMessage", self.IncomingMessage)

        #Phonebook
        self.Phonebook = dbus.Interface(
            self.bus.get_object('org.freesmartphone.GSM',
                                '/org/freesmartphone/GSM/Phonebook'),
            'org.freesmartphone.GSM.Phonebook'
            )

        #Device
        self.Device = dbus.Interface(
            self.bus.get_object('org.freesmartphone.GSM',
                                '/org/freesmartphone/GSM/Device'),
            'org.freesmartphone.GSM.Device')

        #PDP
        self.PDP = dbus.Interface(
            self.bus.get_object('org.freesmartphone.GSM',
                                '/org/freesmartphone/GSM/Device'),
            'org.freesmartphone.GSM.Device')

        self.PDP.connect_to_signal("ContextActivated", self.ContextActivated)
        self.PDP.connect_to_signal("ContextDeactivated", self.ContextDeactivated)
        self.PDP.connect_to_signal("ContextChanged", self.ContextChanged)

        #SIM
        self.SIM = dbus.Interface(
            self.bus.get_object('org.freesmartphone.GSM',
                                '/org/freesmartphone/GSM/SIM'),
            'org.freesmartphone.GSM.SIM'
            )

        self.SIM.connect_to_signal("AuthStatus", self.AuthStatus)

        #Network
        self.Network = dbus.Interface(
            self.bus.get_object('org.freesmartphone.GSM',
                                '/org/freesmartphone/GSM/Network'),
            'org.freesmartphone.GSM.Network'
            )

        self.Network.connect_to_signal("Status", self.NetworkStatus)
        self.Network.connect_to_signal("SubscriberNumbers", self.SubscriberNumbers)

    def terminate(self):
        self.bus.remove_signal_receiver(self.NetworkStatus)
        self.bus.remove_signal_receiver(self.SubscriberNumbers)
        self.bus.remove_signal_receiver(self.AuthStatus)
        self.bus.remove_signal_receiver(self.CallStatus)
        self.bus.remove_signal_receiver(self.ContextActivated)
        self.bus.remove_signal_receiver(self.ContextDeactivated)
        self.bus.remove_signal_receiver(self.ContextChanged)
        self.bus.remove_signal_receiver(self.IncomingMessage)
        self.bus.remove_signal_receiver(self.MessageSent)

    def SetIpc(self,ipc):
        self.ipc = ipc

    # signals

    #PDP
    def ContextActivated(self,id):
        self.ipc.AddSignal(("ContextActivated",id))
    def ContextDeactivated(self,id):
        self.ipc.AddSignal(("ContextDeactivated",id))
    def ContextChanged(self,id):
        self.ipc.AddSignal(("ContextChanged",id))

    #Network
    def NetworkStatus(self, properties):
        self.ipc.AddSignal(("NetworkStatus",properties))
    def SubscriberNumbers(self,number):
        self.ipc.AddSignal(("SubscriberNumbers",number))

    #SIM
    def AuthStatus(self,status):
        self.ipc.AddSignal(("AuthStatus",status))

    #SMS ipc
    def IncomingMessage(self,index):
        self.ipc.message_index = index
        self.ipc.AddSignal(("IncomingMessage",index))
    def MessageSent(self,success,reason):
        self.ipc.AddSignal(("MessageSent",success,reason))

    # Voice Call Ipc
    def CallStatus(self,id,status,properties):
        self.ipc.AddSignal(("CallStatus",id,status,properties))
        self.ipc.call_id = id

