#
# Network cdefs
#

cdef extern from "../libfreesmartphone/freesmartphone-network.h":
    ctypedef void (*FSGSMNetworkReplyFunc) (GError *error,
                                            gpointer userdata )
    ctypedef void (*FSGSMNetworkGetStatusReplyFunc) (GHashTable *status,
                                                     GError *error,
                                                     gpointer userdata)
    ctypedef void (*FSGSMNetworkGetSubscriberNumbersReplyFunc) (gchar **numbers,
                                                           GError *error,
                                                           gpointer userdata )
    ctypedef void (*FSGSMNetworkListProvidersReplyFunc) (GPtrArray *providers,
                                                    GError *error,
                                                    gpointer userdata )
    ctypedef void (*FSGSMNetworkGetCountryCodeReplyFunc) (gchar *dial_code,
                                                     GError *error,
                                                     gpointer userdata )
    ctypedef void (*FSGSMNetworkGetHomeCountryCodeReplyFunc) (gchar *dial_code,
                                                         GError *error,
                                                         gpointer userdata )
    gboolean fso_gsm_network_get_home_country_code(FreeSmartphone *fs,
                                               FSGSMNetworkGetHomeCountryCodeReplyFunc cb,
                                               gpointer userdata)
    gboolean fso_gsm_network_get_country_code(FreeSmartphone *fs,
                                          FSGSMNetworkGetCountryCodeReplyFunc cb,
                                          gpointer userdata)
    gboolean fso_gsm_network_get_subscriber_numbers(FreeSmartphone *fs,
                                                FSGSMNetworkGetSubscriberNumbersReplyFunc cb,
                                                gpointer userdata)
    gboolean fso_gsm_network_register_with_provider(FreeSmartphone *fs,
                                                gint operator_code,
                                                FSGSMNetworkReplyFunc cb,
                                                gpointer userdata)
    gboolean fso_gsm_network_list_providers(FreeSmartphone *fs,
                                        FSGSMNetworkListProvidersReplyFunc cb,
                                        gpointer userdata)
    gboolean fso_gsm_network_get_status(FreeSmartphone *fs,
                                    FSGSMNetworkGetStatusReplyFunc cb,
                                    gpointer userdata)
    gboolean fso_gsm_network_register(FreeSmartphone *fs,
                                  FSGSMNetworkReplyFunc cb,
                                  gpointer userdata)
    ctypedef void (*FSGSMNetworkStatusSignalFunc) (GHashTable *status,
                                                   gpointer userdata)

    ctypedef void (*FSGSMNetworkSubscriberNumbersSignalFunc) (gchar **numbers,
                                                         gpointer userdata)
    gboolean fso_gsm_network_subscriber_numbers_signal_remove(FreeSmartphone *fs,
                                                          FSGSMNetworkSubscriberNumbersSignalFunc cb,
                                                          gpointer userdata)
    gboolean fso_gsm_network_subscriber_numbers_signal(FreeSmartphone *fs,
                                                   FSGSMNetworkSubscriberNumbersSignalFunc cb,
                                                   gpointer userdata)
    gboolean fso_gsm_network_status_signal_remove(FreeSmartphone *fs,
                                              FSGSMNetworkStatusSignalFunc cb,
                                              gpointer userdata)
    gboolean fso_gsm_network_status_signal(FreeSmartphone *fs,
                                       FSGSMNetworkStatusSignalFunc cb,
                                       gpointer userdata)



cdef object get_providers_array(GPtrArray *array):
    result = []
    cdef GValue val
    cdef void *tmp
    cdef int index
    cdef gchar *status
    cdef gchar *name
    cdef gchar *nickname

    memset(&val, 0,sizeof(GValue))
    g_value_init(&val, dbus_g_type_get_struct ("GValueArray", \
                                                    G_TYPE_INT, \
                                                    G_TYPE_STRING, \
                                                    G_TYPE_STRING, \
                                                    G_TYPE_STRING, \
                                                    G_TYPE_INVALID))
    len = array.len
    for i in range(0,len):
        tmp = g_ptr_array_index(array,i)
        g_value_set_boxed(&val, tmp)
        if dbus_g_type_struct_get(&val,
                                  0, &index,
                                  1, &status,
                                  2, &name,
                                  3, &nickname,
                                  G_MAXUINT):
            name_str = get_string(name)
            status_str = get_string(status)
            nickname_str = get_string(nickname)
            result.append((index, status_str, name_str, nickname_str))
        else:
            print "dbus_g_type_struct_get failed!"

    return result


#
# Handlers
#
cdef void network_register_reply(GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str)
    else:
        reply_cb(reply_data, False)
    Py_DECREF(reply)
    return

cdef void network_list_providers_reply(GPtrArray *providers, GError *error, gpointer userdata ) with gil:
    print "Network list providers reply"
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        reply_cb(reply_data, False, get_providers_array(providers))
    Py_DECREF(reply)
    return

cdef void network_get_status_reply(GHashTable *status, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        reply_cb(reply_data, False, get_hash_table(status))
    Py_DECREF(reply)
    return

cdef void network_status_signal(GHashTable *status, gpointer userdata) with gil:
    print "Network status signal"
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    pyproperties = get_hash_table(status)
    reply_cb(reply_data, pyproperties)



#
# PyFreeSmartphoneNetwork class
#

cdef class PyFreeSmartphoneNetwork:
    cdef FreeSmartphone *fs

    #
    # Network signals
    #
    cdef object network_status

    def __new__(self):
        self.fs = fso_init()
    def deinitialize(self):
        fso_free(self.fs)

#
# Functions
#
    def register(self, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_network_register(self.fs, &network_register_reply, <void*>reply_data)

    def get_status(self, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_network_get_status(self.fs, &network_get_status_reply, <void*>reply_data)

    def list_providers(self, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_network_list_providers(self.fs, &network_list_providers_reply, <void*>reply_data)

#
# Signals
#

    def status_signal(self, cb,data=None):
        self.network_status = ReplyData(cb,data)
        Py_INCREF(self.network_status)
        return fso_gsm_network_status_signal(self.fs, &network_status_signal, <void*>self.network_status)

    def status_signal_remove(self):
        fso_gsm_network_status_signal_remove(self.fs, &network_status_signal, <void*>self.network_status)
        Py_DECREF(self.network_status)

