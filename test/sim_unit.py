# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import unittest,time
from gsmd_common import GsmdCommon
import unittest

class SIMTests(GsmdCommon):
    def test_ChangeAuthCode(self):
#        """ Test changing pin code"""
        self.test_ReadyPinStatus()
        self.ipc.SIM_ChangeAuthCode("1234","4321")
        data = [    ("read",'AT+CPIN="1234","4321"'),
                        ("write",'ok'),
                        ("ensure_reply_set",True),
                        ("ensure_error_set",False)]
        self.perform(data)


    def test_ChangeAuthCode_Fail(self):
#        """ Test failing to change pin code"""
        self.test_ReadyPinStatus()
        self.reset()
        self.ipc.SIM_ChangeAuthCode("1234","4321")
        data = [    ("read",'AT+CPIN="1234","4321"'),
                        ("write",'ERROR'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)]
        self.perform(data)


    def test_GetSimInfo(self):
#        """ Test reading IMSI"""
        '''
        at+cimi
        244917070350971

        OK
        '''
        self.ipc.SIM_GetSimInfo()
        data = [    ("read",'AT+CIMI'),
                        ("write",'244917070350971'),
                        ("write",'OK'),
                        ("ensure_reply",{'imsi' : '244917070350971'}),
                        ("ensure_error_set",False),
                    ]
        self.perform(data)
        self.reset()
        self.ipc.SIM_GetSimInfo()
        data = [    ("read",'AT+CIMI'),
                        ("write",'+CIMI: 244917070350971'),
                        ("write",'OK'),
                        ("ensure_reply",{'imsi' : '244917070350971'}),
                        ("ensure_error_set",False),
                    ]
        self.perform(data)

    def test_GetSimInfo_Fail(self):
#        """Test when reading IMSI fails"""
        self.ipc.SIM_GetSimInfo()
        data = [    ("read",'AT+CIMI'),
                        ("write",'ERROR'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)]
        self.perform(data)

    def test_GetSimInfo_Fail2(self):
#        """Test when reading IMSI with incorrect reply"""
        self.ipc.SIM_GetSimInfo()
        self.ipc.timeout_fails = False
        data = [    ("read",'AT+CIMI'),
                        ("write",'#CIMI: 244917070350971'),
                        ("write",'OK'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)]
        self.perform(data)

    def test_GetSimInfo_Fail3(self):
#        """Test when reading IMSI with second incorrect reply"""
        self.ipc.SIM_GetSimInfo()
        self.ipc.no_timeouts = False
        data = [    ("read",'AT+CIMI'),
                        ("write",'+CIMI: 244917070350971ERROR'),
                        ("write",'OK'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)]
        self.perform(data)

    def test_GetSimInfo_Fail4(self):
#        """Test when reading IMSI with third incorrect reply"""
        self.ipc.SIM_GetSimInfo()
        self.ipc.no_timeouts = False
        data = [    ("read",'AT+CIMI'),
                        ("write",'\r\n'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)]
        self.perform(data)

    def test_setupPin(self):
#        """ Test setting a correct pin code"""
        self.test_NeedPinStatus()
        self.ipc.SIM_SendAuthCode("1234", )
        data = [    ("read",'AT+CPIN="1234"'),
                        ("write",'OK'),
                        ("ensure_error_set",False),
                        ("ensure_reply_set",True),
                        ("ensure_signal",("AuthStatus","READY"))]
        self.perform(data)

    def test_setupWrongPin(self):
#        """ Test setting a wrong pin code"""
        self.test_NeedPinStatus()
        self.reset()
        self.ipc.SIM_SendAuthCode("1234", )
        data = [    ("read",'AT+CPIN="1234"'),
                        ("write",'ERROR'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)]
        self.perform(data)


    def test_setupPinWhenNotNeeded(self):
#        """ Test setting a pin when no pin code is needed"""
        self.ipc.SIM_SendAuthCode("1234", )
        data = [    ("read",'AT+CPIN="1234"'),
                        ("write",'ERROR'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)]
        self.perform(data)


    def test_ReadyPinStatus(self):
#        """Test ready pin status"""
        self.ipc.SIM_GetAuthStatus()
        data = [    ("read",'AT+CPIN?'),
                    ("write",'+CPIN: READY'),
                    ("write",'OK'),
                    ("ensure_error_set",False), #Ensure that no error has been set
                    ("ensure_reply","READY")]
        self.perform(data)
        #By default when using no-init parameter to start gsmd2, it is set to SIM_READY status so no signal is sent here

        #Test cache
#         self.reset()
#         self.ipc.SIM_GetAuthStatus()
#         self.perform([("ensure_reply","READY")])


    def test_NeedPinStatus(self):
#        """Test need pin status"""
        self.ipc.SIM_GetAuthStatus()
        data = [    ("read",'AT+CPIN?'),
                        ("write",'+CPIN: SIM PIN'),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure that no error has been set
                        ("ensure_reply","SIM PIN"),
                        ("ensure_signal",("AuthStatus","SIM PIN"))]
        self.perform(data)

        #Test cache
#         self.reset()
#         self.ipc.SIM_GetAuthStatus()
#         self.perform([("ensure_reply","SIM PIN")])


    def test_SimMissingPinStatus(self):
#        """Test sim missing pin status"""
        self.ipc.SIM_GetAuthStatus()
        data = [    ("read",'AT+CPIN?'),
                        ("write",'+CME ERROR: 10'),
                        ("ensure_error_set",False), #Ensure that no error has been set
                        ("ensure_reply","SIM MISSING"),
                        ("ensure_signal",("AuthStatus","SIM MISSING"))]
        self.perform(data)

        #Test cache
#         self.reset()
#         self.ipc.SIM_GetAuthStatus()
#         self.perform([("ensure_reply","SIM MISSING")])

    def test_NeedPukStatus(self):
#        """Test need puk status"""
        self.ipc.SIM_GetAuthStatus()
        data = [    ("read",'AT+CPIN?'),
                        ("write",'+CPIN: SIM PUK'),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure that no error has been set
                        ("ensure_reply","SIM PUK"),
                        ("ensure_signal",("AuthStatus","SIM PUK"))]
        self.perform(data)


        #Test cache
#        self.reset()
#        self.ipc.SIM_GetAuthStatus()
#        self.perform([("ensure_reply","SIM PUK")])

    def test_Unlock(self):
#        """ Test setting puk code and a new pin code"""
        self.test_NeedPukStatus()
        self.ipc.SIM_Unlock("1234","4321")
        data = [    ("read",'AT+CPIN="1234","4321"'),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure that no error has been set
                        ("ensure_reply_set",True),
                        ("ensure_signal",("AuthStatus","READY"))]
        self.perform(data)


    def test_UnlockFail(self):
#        """ Test setting puk code and a new pin code"""
        self.test_NeedPukStatus()
        self.reset()
        self.ipc.SIM_Unlock("1234","4321")
        data = [    ("read",'AT+CPIN="1234","4321"'),
                        ("write",'ERROR'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)] #Ensure that no error has been set
        self.perform(data)


if __name__ == "__main__":
    unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
