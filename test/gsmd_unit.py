# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import os, time, subprocess, signal, termios, utils,thread
import unittest,time,dbus
from gsmd_common import GsmdCommon
import call_unit
from sqlite3 import dbapi2 as sqlite

class GSMDTests(GsmdCommon):
    def setUp(self):
        self.do_check_init = False
        GsmdCommon.setUp(self)
        
    def check_call_status(self, index = 0, status = None,number = None):
        sig = self.ipc.WaitSignalValues("CallStatus",index)
        if status != None:
            assert sig[2] == status,"CallStatus signal's status is incorrect. Wanted "+str(status)+" got "+str(sig[2])
        if number != None:
            assert sig[3]['number'] == number,"CallStatus signal's number is incorrect. Wanted "+str(number)+" got "+str(sig[3]['number'])
    
    def pin_status(self,pin_reply,signal,simready):
        #find AT+CPIN? and then replace the following OK with given reply
        pos = self.init_data[self.vendor].index(("read", "AT+CPIN?"))
        self.init_data[self.vendor][pos+1] = ("write",pin_reply)
        
        pos = self.init_data[self.vendor].index(("ensure_signal",("AuthStatus","READY")))
        self.init_data[self.vendor][pos] = ("ensure_signal",("AuthStatus",signal))
        self.check_init(simready)
        
    def test_NotAllowCommandsAfterPin(self):
#        '''Test that you can't send commands when pin hasn't been set'''
        self.pin_status("+CPIN: SIM PIN","SIM PIN",False)
        self.error = -1
        self.ipc.Call_Initiate("12345","")
        data = [("ensure_reply_set",False),
                   ("ensure_error_set",True), #Error wasn't returned by initiate method
                   ]
        self.perform(data)
        
        pos = self.init_data[self.vendor].index(("read", "AT+CPIN?"))
        self.init_data[self.vendor][pos+1] = ("write","+CPIN: READY")
        pos = self.init_data[self.vendor].index(("ensure_signal",("AuthStatus","SIM PIN")))
        self.init_data[self.vendor][pos] = ("ensure_signal",("AuthStatus","READY"))
        
      
    def test_AllowCommandsAfterPin(self):
#        '''Test that commands are allowed when PIN is properly set'''
        self.pin_status("+CPIN: READY","READY",True)
        number = "12345"
        self.ipc.Call_Initiate(number,"")
        data = self.get_initiate_data(number, 0)
#         data = [   ("read","ATD"+str(number)+";"),
#                    ("write", "OK"),
#                    ("ensure_error_set",False), #Error wasn't returned by initiate method
#                    ("ensure_reply_initiate"),
#                    ("write", "%CPI: 1,3,0,0,0,0,"+str(number)+",129,,0xc5ff,0"),# (call proceed)
#                    ("write", "%CPI: 1,4,0,1,0,0,"+str(number)+",129,,0xc506,0"),# (sync)
#                    ("write", "%CPI: 1,2,1,1,0,0,"+str(number)+",129,,0xc5ff,0"), #(alert)
#                    ("function",(self.check_call_status,0,"outgoing")),
#                    ("reset"),
#                    ("write", "%CPI: 1,6,0,1,0,0,"+str(number)+",129,,0xc500,0"), #(connected)
#                    ("function",(self.check_call_status,0,"active")),
#                    ]
        self.perform(data)
#         self.ipc.Call_Initiate("12345","")
#         data = [   ("read","ATD12345;"),
#                    ("write",'#ECAM: 0,1,1,,,"12345",129'),
#                    ("function",(self.check_call_status,0,"outgoing")),
#                    ("reset"),
#                    ("write",'#ECAM: 0,2,1,,,'),
#                    ("function",(self.check_call_status,0,"outgoing")),
#                    ("reset"),
#                    ("write",'OK'),
#                    ("write",'#ECAM: 0,3,1,,,'),
#                    ("ensure_reply_initiate"),
#                    ("ensure_error_set",False), #Error wasn't returned by initiate method
#                    ("function",(self.check_call_status,0,"active")),
#                    ]
#         self.perform(data)
        
    def test_StartupWithCalls(self):
        #Add two calls
        pos = self.sim_ready_data[self.vendor].index(("read", "AT+CLCC"))
        self.sim_ready_data[self.vendor].insert(pos+1,("write",'+CLCC: 1,0,0,0,0,"+35840123456",145,""'))
        self.sim_ready_data[self.vendor].insert(pos+2, ("write",'+CLCC: 2,1,5,0,0,"044654321",128,""'))
        #and initialize
        self.check_init()
        
        #List calls
        self.ipc.Call_ListCalls()
        self.perform([("ensure_reply_set",True)])
        calls = self.ipc.reply[0]
        self.reset()
        
        #First call should be incoming
        self.ipc.Call_GetStatus(calls[0])
        self.perform([("ensure_reply_set",True)])
        
        assert self.ipc.reply[1] == "incoming", "Incorrect call status. Got "+str(self.ipc.reply[1])+" instead of incoming"
        assert str(self.ipc.reply[2]['number']) == '044654321', 'Incorrect phonenumber, got '+str(self.ipc.reply[2]['number'])+' instead of 044654321'
        self.reset()
        
        #Second call should be connected
        self.ipc.Call_GetStatus(calls[1])
        self.perform([("ensure_reply_set",True)])
        assert self.ipc.reply[1] == "active", "Incorrect call status. Got "+str(self.ipc.reply[1])+" instead of active"
        assert str(self.ipc.reply[2]['number']) == '+35840123456', 'Incorrect phonenumber, got '+str(self.ipc.reply[2]['number'])+' instead of +35840123456'
        
    
    
if __name__ == "__main__":
    unittest.main() # run all tests
