# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import unittest
from call_unit import CallTests
from sms_unit import SMSTests
from sim_unit import SIMTests
from device_unit import DeviceTests
from network_unit import NetworkTests
from phonebook_unit import PhonebookTests
from mux_unit import MuxTests
from gsmd_unit import GSMDTests

#from pdp_unit import PDPTests
#from storage_unit import StorageTests

if __name__ == "__main__":
#    import os
#    os.spawnvpe(os.P_WAIT, "ls",("ls", "-l"), os.environ)
    try:
        unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
    except SystemExit:
        pass
    except:
        import traceback, sys
        traceback.print_exc(file=sys.stdout)
        import dbus_thread
        dbus_thread.stop_thread()
        sys.exit(1)
