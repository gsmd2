# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import unittest
from gsmd_common import GsmdCommon
import unittest

class DeviceTests(GsmdCommon):

#     def test_SetFunction(self):
#         self.fail("Test not implemented")
#     def test_SetFunction_Fail(self):
#         self.fail("Test not implemented")

#     def test_GetFeatures(self):
#         self.fail("Test not implemented")
#     def test_GetFeatures_Fail(self):
#         self.fail("Test not implemented")


    def test_GetInfo(self):
        self.ipc.Device_GetInfo()
        data = [   ("read", "AT+CGSN"),
                   ("write", "123456789012345"),
                   ("write", "OK"),
                   ("read", "AT+CGMI"),
                   ("write", "Telit"),
                   ("write", "OK"),
                   ("read", "AT+CGMR"),
                   ("write", "08.01.001"),
                   ("write", "OK"),
                   ("read", "AT+CGMM"),
                   ("write", "UC864-E"),
                   ("write", "OK"),
                   ("ensure_error_set", False),
                   ("ensure_reply", {"vendor" : "Telit","model" : "UC864-E","revision" : "08.01.001","imei" : "123456789012345"}),
                   ]
        self.perform(data)
    def test_GetInfo_Fail(self):
        self.ipc.Device_GetInfo()
        data = [   ("read", "AT+CGSN"),
                   ("write", "12345678901234"),
                   ("write", "OK"),
                   ("ensure_error_set", True),
                   ]
        self.perform(data)
        self.reset()
        self.ipc.Device_GetInfo()
        data = [   ("read", "AT+CGSN"),
                   ("write", "123456789012345"),
                   ("write", "OK"),
                   ("read", "AT+CGMI"),
                   ("write", "ERROR"),
                   ("ensure_error_set", True),
                   ]
        self.perform(data)
        self.reset()
        self.ipc.Device_GetInfo()
        data = [   ("read", "AT+CGSN"),
                   ("write", "123456789012345"),
                   ("write", "OK"),
                   ("read", "AT+CGMI"),
                   ("write", "Telit"),
                   ("write", "OK"),
                   ("read", "AT+CGMR"),
                   ("write", "ERROR"),
                   ("ensure_error_set", True),
                   ]
        self.perform(data)
        self.reset()
        self.ipc.Device_GetInfo()
        data = [   ("read", "AT+CGSN"),
                   ("write", "123456789012345"),
                   ("write", "OK"),
                   ("read", "AT+CGMI"),
                   ("write", "Telit"),
                   ("write", "OK"),
                   ("read", "AT+CGMR"),
                   ("write", "08.01.001"),
                   ("write", "OK"),
                   ("read", "AT+CGMM"),
                   ("write", "ERROR"),
                   ("ensure_error_set", True),
                   ]
        self.perform(data)

    def test_GetAntennaPower(self):
        self.ipc.Device_GetAntennaPower()
        data = [   ("read", "AT+CFUN?"),
                   ("write", "+CFUN: 1"),
                   ("write", "OK"),
                   ("ensure_error_set", False),
                   ("ensure_reply", True),
                   ]
        self.perform(data)
    def test_GetAntennaPower_Fail(self):
        self.ipc.Device_GetAntennaPower()
        data = [   ("read", "AT+CFUN?"),
                   ("write", "ERROR"),
                   ("ensure_error_set"),
                   ("ensure_reply_set", False),
                   ]
        self.perform(data)



    def test_PrepareToSuspend(self):
        self.ipc.Device_PrepareToSuspend()
        data = [ ("ensure_error_set", False),
                 ("ensure_reply_set"),
                 ]
        self.perform(data)

    def test_RecoverFromSuspend(self):
        self.ipc.Device_RecoverFromSuspend()
        data = [ ("ensure_error_set", False),
                 ("ensure_reply_set"),
                 ]
        self.perform(data)







if __name__ == "__main__":
    try:
        unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
    except SystemExit:
        pass
    except:
        import traceback, sys
        traceback.print_exc(file=sys.stdout)
        import dbus_thread
        dbus_thread.stop_thread()
        sys.exit(1)
