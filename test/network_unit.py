# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen

import unittest,time
from gsmd_common import GsmdCommon
import unittest

class NetworkTests(GsmdCommon):
    '''
    def test_RegisterWithProvider(self):
        self.fail("Test not implemented")
    def test_RegisterWithProvider_Fail(self):
        self.fail("Test not implemented")
        
    def test_GetSubscriberNumbers(self):
        self.fail("Test not implemented")
    def test_GetSubscriberNumbers_Fail(self):
        self.fail("Test not implemented")
        
    def test_GetCountryCode(self):
        self.fail("Test not implemented")
    def test_GetCountryCode_Fail(self):
        self.fail("Test not implemented")
        
    def test_GetHomeCountryCode(self):
        self.fail("Test not implemented")
    def test_GetHomeCountryCode_Fail(self):
        self.fail("Test not implemented")
    '''
    
    def test_Register(self):
        self.ipc.Network_Register()
        data = [    ("read","AT+COPS=0,0"),
                        ("write","OK"),                        
                        ("write",'+CREG: 1,1'),
                        ("ensure_reply_set",True)] #Make sure we get some reply
        self.perform(data)
        self.queryCurrentOperator("Operator")
        self.signalStrength(20)
        data = [    ("ensure_signal",("NetworkStatus",
                                      {"provider" : "Operator",
                                       "registration" : "1",
                                       "strength" : 20})),
                    ("ensure_reply_set",True)] #Ensure that reply is set
        self.perform(data)
    
    def test_Register_Fail(self):
        self.ipc.Network_Register()
        data = [    ("read","AT+COPS=0,0"),
                        ("write","ERROR"),
                        ("ensure_error_set",True)] #Ensure that error is set
        self.perform(data)
    
    def networkRegistrationStatus(self,status):
        data = [("read","AT+CREG?") ]
        self.perform(data)
        
        if status != "ERROR":
             data = [("write","+CREG: 0,"+str(status)),
                            ("write","OK")]
        else:
            data = [("write","ERROR")]
        
        self.perform(data)
        
    def queryCurrentOperator(self,name):
        data = [("read","AT+COPS?")] #Ensure that error is set
        self.perform(data)       
        
        if name != "ERROR":
             data = [   ("write",'+COPS: 1,0,"'+name+'"'),
                            ("write","OK")]
        else:
            data = [("write","ERROR") ]
            
        self.perform(data)       
        

    
    def signalStrength(self,strength):
        data = [("read","AT+CSQ")]
        self.perform(data)
        
        if strength != "ERROR":
            data = [   ("write",'+CSQ: '+str(strength)+',0'),
                            ("write","OK")]
        else:
            data = [("write","ERROR")]
        
        self.perform(data)
        

    def status_handler(self,provider,status,strength):
        if self.debug:
            print "Provider: ",provider
            print "Status: ",status
            print "Strength: ",strength
        self.reply = (provider,status,strength)
        self.reply_event.set()

    def write_status(self,status,name):
        data = [("write","+CREG: "+str(status))]
        self.perform(data)
        self.queryCurrentOperator(name)
        self.signalStrength(20)
        data = [("ensure_signal",("NetworkStatus",
                                  {"provider" : name,
                                   "registration" : str(status),
                                   "strength" : 20}))]
        self.perform(data)
    
    
        
    def test_Status(self):
        self.write_status(1,"FI 2G")
        

    
    def test_GetStatus(self):
        self.ipc.Network_GetStatus()
        self.networkRegistrationStatus(1)
        self.queryCurrentOperator("FI 2G")
        self.signalStrength(20)
        data = [    ("ensure_error_set",False), #Ensure we don't have an error reply
                    ("ensure_reply",{"provider" : "FI 2G","registration" : "1","strength" : 20})]
        self.perform(data)
        
        #Ensure that values are cached
        self.reset()
        self.ipc.Network_GetStatus()
        self.signalStrength(20)
        data = [    ("ensure_error_set",False), #Ensure we don't have an error reply
                    ("ensure_reply",{"provider" : "FI 2G","registration" : "1","strength" : 20})]
        self.perform(data)
        
    def test_GetStatus_Fail(self):
        self.ipc.Network_GetStatus()
        self.networkRegistrationStatus("ERROR")
        data = [("ensure_error_set",True)] #Ensure that error is set
        self.perform(data)
    
    def test_GetStatus_Fail2(self):
        self.ipc.Network_GetStatus()
        self.networkRegistrationStatus(1)
        self.queryCurrentOperator("ERROR")
        data = [("ensure_error_set",True)] #Ensure that error is set        
        self.perform(data)
    
    
    def test_GetStatus_Fail3(self):
        self.ipc.Network_GetStatus()
        self.networkRegistrationStatus(1)
        self.queryCurrentOperator("FI 2G")
        self.signalStrength("ERROR")
        data = [("ensure_error_set",True)] #Ensure that error is set        
        self.perform(data)
    
    
    
    def test_ListProviders(self):
        '''Test listing providers
        
        Sample output
        AT+COPS=?
        
        +COPS: (2,"FI SONERA",,"24491"),(3,"FI 2G",,"24412"),(3,"FI RADIOLINJA",,"24405"),(3,"FI 07",,"24407"),,(0-4),(0,2)
        
        '''
        self.ipc.Network_ListProviders()
        data = [    ("read",'AT+COPS=?'),
                        ("write",'+COPS: (2,"FI SONERA",,"24491"),(3,"FI 2G",,"24412"),(3,"FI RADIOLINJA",,"24405"),(3,"FI 07",,"24407"),,(0-4),(0,2)'),
                        ("ensure_reply",[(0,"2","FI SONERA", "24491"),(1,"3","FI 2G", "24412"),(2,"3","FI RADIOLINJA", "24405"),(3,"3","FI 07", "24407")])] 
        self.perform(data)
        
    def test_ListProviders_Fail(self):
        '''Test failing to list providers'''
        self.ipc.Network_ListProviders()
        data = [    ("read",'AT+COPS=?'),
                        ("write",'ERROR'),
                        ("ensure_reply_set",False),
                        ("ensure_error_set",True)]
        self.perform(data)
        
    '''


    def test_modemStatus():
        DBUS = dbus_thread.get_instance().dbus
        DBUS.modemctrl.queryModemStatus()
        atcmd = readline(port)
        assert atcmd == "AT+CPAS", 'got '+atcmd
        write_reply(port,"+CPAS: 0")
        writeOK(port)
        assert DBUS.got_modemctrl_modemStatus(0)
        DBUS.clear()
        DBUS.modemctrl.queryModemStatus()
        write_reply(port,"+CPAS: 1")
        assert DBUS.got_modemctrl_modemStatus(1)
        
        DBUS.clear()
        DBUS.modemctrl.queryModemStatus()
        write_reply(port,"+CPAS: 3")
        writeOK(port)
        assert DBUS.got_modemctrl_modemStatus(3)
        
        DBUS.clear()
        DBUS.modemctrl.queryModemStatus()
        write_reply(port,"+CPAS: 4")
        writeOK(port)
        assert DBUS.got_modemctrl_modemStatus(4)

    




    def test_date_time ():
        DBUS = dbus_thread.get_instance().dbus
        DBUS.modemctrl.setDateTime("02/09/07,22:30:00+00")
        atcmd = readline (port)
        assert atcmd == 'AT+CCLK="02/09/07,22:30:00+00"', 'got '+atcmd 
        writeOK (port)
        assert DBUS.got_modemctrl_setDateTimeResp (1);
    #read back
        DBUS.clear()
        DBUS.modemctrl.queryDateTime()
        atcmd = readline (port)
        assert atcmd == 'AT+CCLK?', 'got '+ atcmd
        write_reply (port, '+CCLK: "02/09/07,22:30:25"') 
        assert DBUS.got_modemctrl_dateTime ("02/09/07,22:30:25");
    '''
    
if __name__ == "__main__":
    unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
