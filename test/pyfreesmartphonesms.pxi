#
# SMS cdefs
#

cdef extern from "../libfreesmartphone/freesmartphone-sms.h":
    ctypedef void (*FSGSMSMSReply) (GError *error,gpointer userdata)
    ctypedef void (*FSGSMSMSMessageSent) (gboolean success,gchar *reason,gpointer userdata)
    ctypedef void (*FSGSMSMSIncomingMessage) (gint index,gpointer userdata)
    gboolean fso_gsm_call_initiate(FreeSmartphone *fs, gchar *number, gchar *type, FSGSMCallInitiateReply cb, gpointer userdata)

    gboolean fso_gsm_sms_send_message(FreeSmartphone *fs,
                                  gchar *message,
                                  gchar *number,
                                  gboolean want_report,
                                  FSGSMSMSReply cb,
                                  gpointer userdata)




    gboolean fso_gsm_sms_incoming_message_signal_remove(FreeSmartphone *fs,
                                                    FSGSMSMSIncomingMessage cb,
                                                    gpointer userdata)

    gboolean fso_gsm_sms_incoming_message_signal(FreeSmartphone *fs,
                                             FSGSMSMSIncomingMessage cb,
                                             gpointer userdata)

    gboolean fso_gsm_sms_message_sent_signal_remove(FreeSmartphone *fs,
                                                FSGSMSMSMessageSent cb,
                                                gpointer userdata)

    gboolean fso_gsm_sms_message_sent_signal(FreeSmartphone *fs,
                                         FSGSMSMSMessageSent cb,
                                         gpointer userdata)

#
# SMS Handlers
#

cdef void sms_incoming_message_signal(gint index, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    reply_cb(reply_data, index)
    return

cdef void sms_message_sent_signal(gboolean success, gchar *reason, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    reason_str = get_string(reason)
    reply_cb(reply_data, success,reason_str)
    return


cdef void sms_send_message_reply(GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str,None)
    else:
        reply_cb(reply_data,False,None)
    Py_DECREF(reply)
    return



#
# PyFreeSmartPhoneSMS class
#

cdef class PyFreeSmartphoneSMS:
    cdef FreeSmartphone *fs

    #
    # SMS signals
    #
    cdef object incoming_message
    cdef object message_sent

    def __new__(self):
        self.fs = fso_init()
    def deinitialize(self):
        fso_free(self.fs)

#
# SMS Functions
#



    def send_message(self,message,number,want_report,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_sms_send_message(self.fs,<char*>message, <char*>number, <gboolean> want_report,&sms_send_message_reply, <void*>reply_data)


#
# SMS Signals
#

    def message_sent_signal(self, cb,data=None):
        self.message_sent = ReplyData(cb,data)
        Py_INCREF(self.message_sent)
        return fso_gsm_sms_message_sent_signal(self.fs, &sms_message_sent_signal, <void*>self.message_sent)

    def message_sent_signal_remove(self):
        fso_gsm_sms_message_sent_signal_remove(self.fs, &sms_message_sent_signal, <void*>self.message_sent)
        Py_DECREF(self.message_sent)

    def incoming_message_signal(self, cb,data=None):
        self.incoming_message = ReplyData(cb,data)
        Py_INCREF(self.incoming_message)
        return fso_gsm_sms_incoming_message_signal(self.fs, &sms_incoming_message_signal, <void*>self.incoming_message)

    def incoming_message_signal_remove(self):
        fso_gsm_sms_incoming_message_signal_remove(self.fs, &sms_incoming_message_signal, <void*>self.incoming_message)
        Py_DECREF(self.incoming_message)


