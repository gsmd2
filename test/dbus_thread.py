# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajaneni


from __future__ import with_statement
import threading, time

import dbus

# this is needed or main loop is not binded by default
import dbus.glib
import gobject

from dbus_helper import DBUS

gobject.threads_init()

from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)
dbus.mainloop.glib.threads_init()

class DBUSGMainLoopThread(threading.Thread):
    def __init__ (self):
        threading.Thread.__init__(self)
#        self.stop = False
        self.loop = None
        self.dbus = None

#     def check_stop(self):
#         if self.stop and self.loop:
#                 self.loop.quit()
#         return True

    def stop(self):
        if self.loop:
            self.loop.quit()

    def run(self):
        self.dbus = DBUS()
        self.create_dbus = 0
#        print "Starting main loop"
        self.loop = gobject.MainLoop()
        self.loop.run()
        self.loop = None
        self.dbus.terminate()
        self.dbus = None
#        print "Stopped main loop"

global __mainloop_thread
__mainloop_thread = DBUSGMainLoopThread()
def get_instance():
    global __mainloop_thread
    return __mainloop_thread

def stop_thread():
    global __mainloop_thread
#    print "Stopping event thread"
    __mainloop_thread.stop()
    while __mainloop_thread.loop != None:
        time.sleep(0.1)
    __mainloop_thread = DBUSGMainLoopThread()
def start_thread():
    global __mainloop_thread
#    print "start_thread"
    __mainloop_thread.start()

    while __mainloop_thread.dbus == None:
        time.sleep(0.1)
    return __mainloop_thread.dbus
