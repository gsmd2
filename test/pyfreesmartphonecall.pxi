#
# Call cdefs
#

cdef extern from "../libfreesmartphone/freesmartphone-call.h":
    ctypedef void (*FSGSMCallInitiateReply) (int id, GError *error, gpointer userdata)
    gboolean fso_gsm_call_initiate(FreeSmartphone *fs, gchar *number, gchar *type, FSGSMCallInitiateReply cb, gpointer userdata)

    ctypedef int FSGSMCallStatus
    ctypedef void (*FSGSMCallStatusSignalFunc) (gint id,
                                           gchar *status,
                                           GHashTable *properties,
                                           gpointer userdata)
    gboolean fso_gsm_call_status_signal(FreeSmartphone *fs,
                                      FSGSMCallStatusSignalFunc cb,
                                      gpointer userdata)

    gboolean fso_gsm_call_status_signal_remove(FreeSmartphone *fs,
                                             FSGSMCallStatusSignalFunc cb,
                                             gpointer userdata)


    ctypedef void (*FSGSMCallStatusReplyFunc) ( gint id,
                                           gchar *status,
                                           GHashTable *properties,
                                           GError *error,
                                           gpointer userdata )

    gboolean fso_gsm_call_get_call_status( FreeSmartphone *fs,
                                       gint id,
                                       FSGSMCallStatusReplyFunc cb,
                                       gpointer userdata )

    ctypedef void (*FSGSMCallListReplyFunc) ( GPtrArray *calls,
                                              GError *error,
                                              gpointer userdata )


    gboolean fso_gsm_call_list_calls( FreeSmartphone *fs,
                                      FSGSMCallListReplyFunc cb,
                                      gpointer userdata)

    ctypedef void (*FSGSMCallActivateReplyFunc) (  GError *error,
                                                 gpointer userdata )

    gboolean fso_gsm_call_activate( FreeSmartphone *fs,
                                             gint id,
                                             FSGSMCallActivateReplyFunc cb,
                                             gpointer userdata)

    ctypedef void (*FSGSMCallActivateConferenceReplyFunc) (  GError *error,
                                                             gpointer userdata )

    gboolean fso_gsm_call_activate_conference( FreeSmartphone *fs,
                                               gint id,
                                               FSGSMCallActivateConferenceReplyFunc cb,
                                               gpointer userdata)

    ctypedef void (*FSGSMCallReleaseReplyFunc) ( GError *error,
                                                 gpointer userdata )

    gboolean fso_gsm_call_release( FreeSmartphone *fs,
                                              gchar *message,
                                              gint id,
                                              FSGSMCallReleaseReplyFunc cb,
                                              gpointer userdata )

    ctypedef void (*FSGSMCallReleaseHeldReplyFunc) ( GError *error,
                                                     gpointer userdata )

    gboolean fso_gsm_call_release_held( FreeSmartphone *fs,
                                        gchar *message,
                                        FSGSMCallReleaseHeldReplyFunc cb,
                                        gpointer userdata )

    ctypedef void (*FSGSMCallReleaseAllReplyFunc) ( GError *error,
                                                    gpointer userdata )

    gboolean fso_gsm_call_release_all( FreeSmartphone *fs,
                                       gchar *message,
                                       FSGSMCallReleaseAllReplyFunc cb,
                                       gpointer userdata )

#
# Call Handlers
#
cdef void call_initiate_reply(int id, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, -1)
    else:
        reply_cb(reply_data, False,id)
    Py_DECREF(reply)
    return

cdef void call_get_call_status_reply(gint id, gchar *status,GHashTable *properties, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, None)
    else:
        status_str = get_string(status)
        reply_cb(reply_data, False, id,status_str,get_hash_table(properties))
    Py_DECREF(reply)
    return

cdef void call_activate_reply(GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str)
    else:
        reply_cb(reply_data, False)
    Py_DECREF(reply)
    return

cdef void call_release_reply(GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str)
    else:
        reply_cb(reply_data, False)
    Py_DECREF(reply)
    return

cdef void call_list_calls_reply(GPtrArray *calls, GError *error, gpointer userdata) with gil:
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    if error != NULL:
        error_str = get_error_string(error)
        reply_cb(reply_data, error_str, -1)
    else:
        pass #reply_cb(reply_data, False,get_int_array(calls)) #TODO: add support for gptrarray
    Py_DECREF(reply)
    return

cdef void call_status_signal(gint id, gchar *status, GHashTable *properties, gpointer userdata) with gil:
    print "Call status signal"
    reply = <object>userdata
    reply_cb = reply.get_cb()
    reply_data = reply.get_data()
    status_str = get_string(status)
    pyproperties = get_hash_table(properties)
    reply_cb(reply_data, id, status_str, pyproperties)



#
# PyFreeSmartPhoneCall class
#

cdef class PyFreeSmartphoneCall:
    cdef FreeSmartphone *fs

    #
    # Call signals
    #
    cdef object call_status

    #
    # SMS signals
    #
    cdef object incoming_message
    cdef object message_sent

    def __new__(self):
        self.fs = fso_init()
    def deinitialize(self):
        fso_free(self.fs)

#
# Call Functions
#
    def initiate(self, number,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_call_initiate(self.fs, <char*>number, <char*>"VOICE", &call_initiate_reply, <void*>reply_data)

    def get_call_status(self, id,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_call_get_call_status(self.fs, <int>id, &call_get_call_status_reply, <void*>reply_data)

    def activate(self, id,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_call_activate(self.fs,<int>id, &call_activate_reply, <void*>reply_data)

    def activate_conference(self, id,cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_call_activate_conference(self.fs,<int>id, &call_activate_reply, <void*>reply_data)

    def release(self, message, id, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_call_release(self.fs, <char*>message, <int>id,&call_release_reply, <void*>reply_data)

    def release_held(self, message, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_call_release_held(self.fs, <char*>message, &call_release_reply, <void*>reply_data)

    def release_all(self, message, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_call_release_all(self.fs, <char*>message, &call_release_reply, <void*>reply_data)

    def list_calls(self, cb,data=None):
        reply_data = ReplyData(cb,data)
        Py_INCREF(reply_data)
        return fso_gsm_call_list_calls(self.fs, &call_list_calls_reply, <void*>reply_data)

#
# Call Signals
#

    def status_signal(self, cb,data=None):
        self.call_status = ReplyData(cb,data)
        Py_INCREF(self.call_status)
        return fso_gsm_call_status_signal(self.fs, &call_status_signal, <void*>self.call_status)

    def status_signal_remove(self):
        fso_gsm_call_status_signal_remove(self.fs, &call_status_signal, <void*>self.call_status)
        Py_DECREF(self.call_status)

