# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen
from __future__ import with_statement
import threading, time

class IPCInterface:
    def __init__ (self):
        self.debug = True
        self.debug_signal = True
        self.sync_lock = threading.Lock()
        self.signals = []
        self.reply_event = threading.Event()
        self.call_id = None
        self.message_index = None
        self.timeout_fails = True
        self.reply_message = None

    def Initialize(self):
        self.Reset()
        self.call_id = None
        self.message_index = None
    def DeInitialize(self):
        pass

    def Reset(self):
        with self.sync_lock:
            self.signals = []
        self.error = None
        self.reply = None
        self.reply_event.clear()

    def getResultAndReset(self):
        self.reply_event.wait()
        if self.error:
            result = self.error
        else:
            result = self.reply
        self.Reset()
        return result

    def getReply(self):
        print "Waiting for reply"
        self.reply_event.wait()
        return self.reply
    def getError(self):
        print "Waiting for error"
        self.reply_event.wait()
        return self.error

    def ensure_value(self,name,value,expected_value=None):
        if type(expected_value) != tuple and expected_value != None:
            expected_value =(expected_value,)

        assert value == expected_value,"Didn't get %(expected_value)s as a %(name)s, got %(value)s instead!" % locals()

    def ensure_value_set(self,name,value,set):
        if set:
            assert value != None, "Value '%s' not set!" % name
        else:
            assert value == None, "Value '%s' is set!" % name

    def ensure_error(self,value=None):
        #Do additional check on error reply
        self.CheckError()
        if self.debug:
            print "Ensuring error is ",value
        self.ensure_value("error",self.getError(),value)

    def ensure_reply_initiate(self):
        self.ensure_reply_set(True)
        reply = self.getReply()
        assert type(reply) == tuple, "Reply %s is not tuple!" % str(reply)
        assert len(reply) == 1, "Reply %s has wrong number of elements!" % str(reply)
        self.call_id = reply[0]
        if self.debug:
            print "Initiate got call id %d" % self.call_id

    def ensure_reply(self,value=None):
        if self.debug:
            print "Ensuring reply is ",value
        self.ensure_value("reply",self.getReply(),value)

    def ensure_error_set(self,value=True):
        #Do additional check on error reply
        self.CheckError()
        if self.debug:
            if value == True:
                print "Ensuring error is set"
            else:
                print "Ensuring error is not set"
        
        self.ensure_value_set("error",self.getError(),value)
        
        
    def ensure_reply_set(self,value=True):
        if self.debug:
            if value == True:
                print "Ensuring reply is set"
            else:
                print "Ensuring reply is not set"
        self.ensure_value_set("reply",self.getReply(),value)

    def ReplyHandler(self,*arg):
        if len(arg) >= 1 and type(arg[0]) == tuple:
            arg = arg[0]

        self.reply = arg
        if self.debug:
            if self.reply_message:
                print self.reply_message,str(arg)
                self.reply_message = None
            else:
                print "Got reply: ",str(arg)
        self.reply_event.set()
    
    #Used to do ipc related checks (for example dbus timeout)
    def CheckError(self):
        pass

    def ErrorHandler(self,arg):
        self.error = arg
        if self.debug:
            print "Got error: ",arg
        if self.reply_message:
            self.reply_message = None
        self.reply_event.set()

    def AddSignal(self,signal):
        with self.sync_lock:
            if self.debug_signal:
                print "Received signal: ",signal
            self.signals.append(signal)

    def CheckSignal(self, tup):
        print "Checking signal",tup
        t0 = time.time()
        while time.time() - t0 < 5:
            with self.sync_lock:
                if len(self.signals) == 0:
                    pass
                else:
                    if (tup in self.signals):
                        self.signals.remove(tup)
                        return True
                    else:
                        if self.debug:
                            print "Previous signals: " + str (self.signals) + "\n"
                            print "Tup is: "+ str (tup) + "\n"
            time.sleep(0.2)
        raise Exception, 'time out - did not receive signal '+str(tup)

    def WaitSignalValues(self, *arg):
        print "Waiting for signal with values '%s'" % str(arg)
        t0 = time.time()
        while time.time() - t0 < 5:
            with self.sync_lock:
                if len(self.signals) == 0:
                    pass
                else:
                    def filter_func(tup):
                        i = 0
                        for a in arg:
                            if tup[i] != a:
                                return False
                            i+=1
                        
                        return True
                    filtered = filter(filter_func, self.signals)
                    if len(filtered) > 0:
                        tup = filtered[0]
                        self.signals.remove(tup)
                        return tup
                    else:
                        if self.debug:
                            print "Previous signals: " + str (self.signals) + "\n"
            time.sleep(0.2)
        raise Exception, 'time out - did not receive signal with these arguments'+str(arg)

    #Call Interface
    def Call_Emergency(self):
        raise Exception("Not implemented")
    def Call_Activate(self, id):
        raise Exception("Not implemented")
    def Call_ActivateConference(self, id):
        raise Exception("Not implemented")
    def Call_Release(self,message,id):
        raise Exception("Not implemented")
    def Call_ReleaseHeld(self,message):
        raise Exception("Not implemented")
    def Call_ReleaseAll(self,message):
        raise Exception("Not implemented")
    def Call_Initiate(self,number,type):
        raise Exception("Not implemented")
    def Call_ListCalls(self):
        raise Exception("Not implemented")
    def Call_GetStatus(self,id):
        raise Exception("Not implemented")
    def Call_SendDtmf(self):
        raise Exception("Not implemented")
    def Call_SetDtmfDuration(self,mode):
        raise Exception("Not implemented")
    def Call_GetDtmfDuration(self):
        raise Exception("Not implemented")
    def Call_SetSendIdentification(self,mode):
        raise Exception("Not implemented")
    def Call_GetSendIdentification(self):
        raise Exception("Not implemented")


    #Device interface
    def Device_GetInfo(self):
        raise Exception("Not implemented")
    def Device_SetAntennaPower(self,antenna_power):
        raise Exception("Not implemented")
    def Device_GetAntennaPower(self):
        raise Exception("Not implemented")
    def Device_GetFeatures(self):
        raise Exception("Not implemented")
    def Device_PrepareToSuspend(self):
        raise Exception("Not implemented")
    def Device_RecoverFromSuspend(self):
        raise Exception("Not implemented")



    #Network interface
    def Network_Register(self):
        raise Exception("Not implemented")
    def Network_GetStatus(self):
        raise Exception("Not implemented")
    def Network_ListProviders(self):
        raise Exception("Not implemented")
    def Network_RegisterWithProvider(self,index):
        raise Exception("Not implemented")
    def Network_GetSubscriberNumbers(self):
        raise Exception("Not implemented")
    def Network_GetCountryCode(self):
        raise Exception("Not implemented")
    def Network_GetHomeCountryCode(self):
        raise Exception("Not implemented")

    #SMS interface
    def SMS_SendMessage(self,message,number,want_report):
        raise Exception("Not implemented")
    
    #SIM interface
    def SIM_GetAuthStatus(self):
        raise Exception("Not implemented")
    def SIM_SendAuthCode(self,pin):
        raise Exception("Not implemented")
    def SIM_Unlock(self,puk,new_pin):
        raise Exception("Not implemented")
    def SIM_ChangeAuthCode(self,old_pin,new_pin):
        raise Exception("Not implemented")
    def SIM_GetSimInfo(self):
        raise Exception("Not implemented")
    def SIM_SendMessage(self,index,want_report):
        raise Exception("Not implemented")
    def SIM_ListMessages(self,status):
        raise Exception("Not implemented")
    def SIM_RetrieveMessage(self,index):
        raise Exception("Not implemented")
    def SIM_DeleteMessage(self,index):
        raise Exception("Not implemented")
    def SIM_StoreMessage(self,number,message):
        raise Exception("Not implemented")
    def SIM_GetPhonebookInfo(self):
        raise Exception("Not implemented")
    def SIM_DeleteEntry(self,index):
        raise Exception("Not implemented")
    def SIM_StoreEntry(self,index,name,number):
        raise Exception("Not implemented")
    def SIM_RetrieveEntry(self,index):
        raise Exception("Not implemented")


    #Phonebook interface

    #PDP interface
    def PDP_ListGprsClasses(self):
        raise Exception("Not implemented")
    def PDP_SelectGprsClass(self,klass):
        raise Exception("Not implemented")
    def PDP_Activate(self,index):
        raise Exception("Not implemented")
    def PDP_Deactivate(self,index):
        raise Exception("Not implemented")
    def PDP_SelectContext(self,context):
        raise Exception("Not implemented")
    def PDP_AddContext(self,context):
        raise Exception("Not implemented")
    def PDP_DeleteContext(self,index):
        raise Exception("Not implemented")
    def PDP_ListContexts(self):
        raise Exception("Not implemented")
