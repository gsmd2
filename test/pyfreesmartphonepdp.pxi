#
# PDP cdefs
#
cdef extern from "../libfreesmartphone/freesmartphone-pdp.h":
    ctypedef void (*FSGSMPDPReplyFunc) (GError *error,
                                   gpointer userdata )
    ctypedef void (*FSGSMPDPListGprsClassesReplyFunc) (gchar **classes,
                                                  GError *error,
                                                  gpointer userdata )
    ctypedef void (*FSGSMPDPAddContextReplyFunc) (gint index,
                                             GError *error,
                                             gpointer userdata )
    ctypedef void (*FSGSMPDPListContextsReplyFunc) (gchar **contexts,
                                               GError *error,
                                               gpointer userdata )
    gboolean fso_gsm_pdp_list_contexts(FreeSmartphone *fs,
                                   FSGSMPDPListContextsReplyFunc cb,
                                   gpointer userdata)
    gboolean fso_gsm_pdp_delete_context(FreeSmartphone *fs,
                                    gint index,
                                    FSGSMPDPReplyFunc cb,
                                    gpointer userdata)
    gboolean fso_gsm_pdp_add_context(FreeSmartphone *fs,
                                 gchar *context,
                                 FSGSMPDPAddContextReplyFunc cb,
                                 gpointer userdata)
    gboolean fso_gsm_pdp_select_context(FreeSmartphone *fs,
                                    gchar *context,
                                    FSGSMPDPReplyFunc cb,
                                    gpointer userdata)
    gboolean fso_gsm_pdp_deactivate(FreeSmartphone *fs,
                                gint index,
                                FSGSMPDPReplyFunc cb,
                                gpointer userdata)
    gboolean fso_gsm_pdp_activate(FreeSmartphone *fs,
                              gint index,
                              FSGSMPDPReplyFunc cb,
                              gpointer userdata)
    gboolean fso_gsm_pdp_select_gprs_class(FreeSmartphone *fs,
                                       gchar *klass,
                                       FSGSMPDPReplyFunc cb,
                                       gpointer userdata)
    gboolean fso_gsm_pdp_list_gprs_classes(FreeSmartphone *fs,
                                       FSGSMPDPListGprsClassesReplyFunc cb,
                                       gpointer userdata)
    ctypedef void (*FSGSMPDPContextSignalFunc) (gint id,
                                           gpointer userdata)
    gboolean fso_gsm_pdp_context_changed_signal_remove(FreeSmartphone *fs,
                                                   FSGSMPDPContextSignalFunc cb,
                                                   gpointer userdata)
    gboolean fso_gsm_pdp_context_changed_signal(FreeSmartphone *fs,
                                            FSGSMPDPContextSignalFunc cb,
                                            gpointer userdata)
    gboolean fso_gsm_pdp_context_deactivated_signal_remove(FreeSmartphone *fs,
                                                       FSGSMPDPContextSignalFunc cb,
                                                       gpointer userdata)
    gboolean fso_gsm_pdp_context_deactivated_signal(FreeSmartphone *fs,
                                                FSGSMPDPContextSignalFunc cb,
                                                gpointer userdata)
    gboolean fso_gsm_pdp_context_activated_signal_remove(FreeSmartphone *fs,
                                                     FSGSMPDPContextSignalFunc cb,
                                                     gpointer userdata)
    gboolean fso_gsm_pdp_context_activated_signal(FreeSmartphone *fs,
                                              FSGSMPDPContextSignalFunc cb,
                                              gpointer userdata)

