# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Vesa Pikki

import unittest,time
from gsmd_common import GsmdCommon
import unittest

class TimeoutTests(GsmdCommon):
    def test_TimeoutOnce(self):
        self.ipc.Call_Initiate("12345","")
        data = [("read","ATD12345;"),
                    #("write","TIME_OUT"),
                    ("ensure_error_set",True),
                   ("read",'AT'),
                   ("write","OK")]
        self.perform(data)
        
    def test_TimeoutTwice(self):
        self.ipc.Call_Initiate("12345","")
        data = [ ("read","ATD12345;"),
                    #("write","TIME_OUT"),
                    ("ensure_error_set",True),
                   ("read",'AT'),
                   ("reset"),
                   ("sleep",10)
                   #("write","TIME_OUT")
                   ]
        self.perform(data)
        print "Sending initiate"
        self.ipc.Call_Initiate("12345","")
        self.perform([("sleep",10)])
        #pos = self.init_data.index(("ensure_signal",("AuthStatus","READY")))
        #self.init_data[pos] = ("reset")
        self.check_init()
        

    
if __name__ == "__main__":
    unittest.main() # run all tests
