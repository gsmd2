##!/usr/bin/python
# coding:utf-8
#-*- python -*-

# Copyright(C) 2007,2008 Ixonos Plc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Boston, MA 02111.

# Written by Matti Katila, Yi Zheng, Vesa Pikki and Heikki Paajanen


import unittest,time,dbus
from gsmd_common import GsmdCommon

class SMSTests(GsmdCommon):
    def setUp(self):
        GsmdCommon.setUp(self)
        self.send_pdu = '0011000C915348500496870000AA0AE8329BFD4697D9EC37'
        self.send_local_pdu = '0011000A8140500496870000AA0AE8329BFD4697D9EC37'
        self.rev_pdu = '0791534850020200040C915348500496870000701102613493800AE8329BFD4697D9EC37'
        self.rev_local_pdu =  '0791534850020200040A8140500496870000701102613493800AE8329BFD4697D9EC37'
        self.ucs_send_pdu = '0011000C915348500496870008AA044F60597D'
        self.ucs_rev_pdu =   '0791534874895300040C91534874219678000880101271002180044F60597D'

    def smssend(self,len,smsdata):
        data = [    ("read",'AT+CMGS='+str(len)),
                        ("write",'\r\n> '),
                        ("read",smsdata),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure no reply
                        ("ensure_signal",("MessageSent",1,"")), #Ensure no reply
                        ("ensure_reply_set",True)] #Ensure that we got a reply
        self.perform(data)

    def test_unicode_send(self):
        self.ipc.SMS_SendMessage('你好','+358405406978',False)
        self.smssend(18,self.ucs_send_pdu)

    def test_sendLocalSms(self):
        self.ipc.SMS_SendMessage('hellohello','0405406978',False)
        self.smssend(22,self.send_local_pdu)


    def test_SendMessage(self):
        self.ipc.SMS_SendMessage('hellohello','00358405406978',False)
        self.smssend(23,self.send_pdu)

    def test_SendMessageFail(self):
        self.ipc.SMS_SendMessage("","",False)
        data = [("ensure_error_set",True)]
        self.perform(data)

        
    def readSMS(self,pdu,message,number):
        self.ipc.SIM_RetrieveMessage(28)
        data = [   ("read","AT+CMGR=28"),
                        ("write",'+CMGR: 0,"",28'),
                        ("write",pdu),
                        ("write",'OK'),
                        ("ensure_reply_set",True)]
        self.perform(data)
        #print "Reply is:",str(self.ipc.reply[1].encode("utf-8"))
        res = str(self.ipc.reply[1].encode("utf-8"))
        
        
        
            
        #assert self.ipc.reply[1] == message, "Incorrect message, wanted %s, got %s" % (message,str(self.ipc.reply[1]))
        assert res == message, "Incorrect message, wanted %s, got %s" % (message,str(self.ipc.reply[1]))
        assert self.ipc.reply[0] == number, "Incorrect number, wanted %s, got %s" % (number,str(self.ipc.reply[0]))
        
        
    def test_unicode_smsNew(self):
        self.readSMS(self.ucs_rev_pdu,'你好',"+358447126987")

    def test_smsNew(self):
        self.readSMS(self.rev_pdu,"hellohello","+358405406978")

    def test_smsLocalNew(self):
        self.readSMS(self.rev_local_pdu,"hellohello","0405406978")
    def test_sms_send_retry(self):
        self.ipc.SMS_SendMessage('hellohello','0405406978',False)
        data = [    ("read",'AT+CMGS=22'),
                        ("write","\r\n> "),
                        ("read",self.send_local_pdu),
                        ("write",'+CMS ERROR:42'),
                        ("read",'AT+CMGS=22'),
                        ("write",'\r\n> '),
                        ("read",self.send_local_pdu),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure no reply
                        ("ensure_signal",("MessageSent",1,""))]
        self.perform(data)

    def test_ListMessages(self):
        self.ipc.SIM_ListMessages('read')
        data = [    ("read",'AT+CMGL=1'),
                        ("write","+CMGL: 1,1,"",145"),
                        ("write",self.rev_pdu),
                        ("write","+CMGL: 2,1,"",145"),
                        ("write",self.rev_local_pdu),
                        ("write",'OK'),
                        ("ensure_error_set",False), #Ensure no reply
                        ("ensure_reply",([1,2],))]
        self.perform(data)

    

if __name__ == "__main__":
    unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
